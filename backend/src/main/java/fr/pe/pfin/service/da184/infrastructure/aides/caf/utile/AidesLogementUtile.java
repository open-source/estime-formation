package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.StatutOccupationLogementEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

public class AidesLogementUtile {

    private AidesLogementUtile() {
    }

    /**
     * Fonction permettant de déterminer si on a calculé le montant des aides au logement le mois précédent et s'il doit être versé ce mois-ci
     *
     * @return _________________________________________________________________________________________
     * |            |          |          |          |          |          |          |          |
     * | Mois décla |    M0    |    M1    |    M2    |    M3    |    M4    |    M5    |    M6    |
     * |            |          |          |          |          |          |          |          |
     * |  -  M0     |  C1/R0   |  C2/(V1) |   (V2)   |  C3/R2   |   (V3)   |    R3    |  C4/R3   |
     * |  -  M1     |    R0    |  C1/R0   |   (V1)   |    R1    |  C2/R1   |   (V2)   |    R2    |
     * |  -  M2     |    R0    |  C1/R0   |  C2/(V1) |   (V2)   |    R2    |  C3/R2   |   (V3)   |
     * |  -  M3     |  C1/R0   |  C2/(V1) |   (V2)   |  C3/R2   |   (V3)   |    R3    |  C4/R3   |
     * |____________|__________|__________|__________|__________|__________|__________|__________|
     */
    public static boolean isAideLogementAVerser(int numeroMoisSimule, Demandeur demandeur) {
        int prochaineDeclarationTrimestrielle = RessourcesFinancieresAvantSimulationUtile.getProchaineDeclarationTrimestrielle(demandeur);
        return (numeroMoisSimule == 2 || ((prochaineDeclarationTrimestrielle == 0 || prochaineDeclarationTrimestrielle == 3) && (numeroMoisSimule == 1 || numeroMoisSimule == 4))
                || ((prochaineDeclarationTrimestrielle == 1) && (numeroMoisSimule == 5))
                || ((prochaineDeclarationTrimestrielle == 2) && (numeroMoisSimule == 3 || numeroMoisSimule == 6)));
    }

    public static void reporterAideLogement(Simulation simulation, Map<String, Aide> aidesPourCeMois, int numeroMoisSimule, Demandeur demandeur) {
        Optional<Aide> aideLogementMoisPrecedent = getAideLogementSimuleeMoisPrecedent(simulation, numeroMoisSimule);
        if (aideLogementMoisPrecedent.isPresent()) {
            aidesPourCeMois.put(aideLogementMoisPrecedent.get().getCode(), aideLogementMoisPrecedent.get());
        } else if (isEligiblePourReportAideLogementDeclare(demandeur, numeroMoisSimule)) {
            Aide aideLogement = getAideLogementDeclare(demandeur);
            aidesPourCeMois.put(aideLogement.getCode(), aideLogement);
        }
    }

    private static Aide getAideLogementDeclare(Demandeur demandeur) {
        float montantDeclare = RessourcesFinancieresAvantSimulationUtile.getMontantAideLogementDeclare(demandeur);
        return creerAideLogement(montantDeclare, true);
    }

    private static Optional<Aide> getAideLogementSimuleeMoisPrecedent(Simulation simulation, int numeroMoisSimule) {
        int moisNMoins1 = numeroMoisSimule - 1;
        return AideUtile.getAidePourCeMoisSimule(simulation, AideEnum.AIDES_LOGEMENT.getCode(), moisNMoins1);
    }

    private static boolean isEligiblePourReportAideLogementDeclare(Demandeur demandeur, int numeroMoisSimule) {
        return RessourcesFinancieresAvantSimulationUtile.hasAllocationLogement(demandeur)
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getProchaineDeclarationTrimestrielle() != null
                && (numeroMoisSimule == 1 || numeroMoisSimule <= demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getProchaineDeclarationTrimestrielle());
    }

    public static Aide creerAideLogement(float montantAideLogement, boolean isAideReportee) {
        ArrayList<String> messagesAlerte = new ArrayList<>();
        messagesAlerte.add(MessageInformatifEnum.CHANGEMENT_MONTANT_PRESTATIONS_FAMILIALES.getMessage());
        return AideUtile.creerAide(AideEnum.AIDES_LOGEMENT, Optional.of(OrganismeEnum.CAF), Optional.of(messagesAlerte), isAideReportee, montantAideLogement);
    }

    public static boolean isEligibleAidesLogement(Demandeur demandeur) {
        boolean isEligible = false;
        if (demandeur.getInformationsPersonnelles() != null && demandeur.getInformationsPersonnelles().getLogement() != null
                && demandeur.getInformationsPersonnelles().getLogement().getStatutOccupationLogement() != null
                && (demandeur.getInformationsPersonnelles().getLogement().getStatutOccupationLogement().equals(StatutOccupationLogementEnum.LOCATAIRE_HLM.getLibelle())
                || demandeur.getInformationsPersonnelles().getLogement().getStatutOccupationLogement()
                .equals(StatutOccupationLogementEnum.LOCATAIRE_MEUBLE.getLibelle())
                || demandeur.getInformationsPersonnelles().getLogement().getStatutOccupationLogement()
                .equals(StatutOccupationLogementEnum.LOCATAIRE_NON_MEUBLE.getLibelle()))) {
            isEligible = true;
        }
        return isEligible;
    }
}
