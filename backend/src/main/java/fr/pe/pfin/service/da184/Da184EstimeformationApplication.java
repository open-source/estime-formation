package fr.pe.pfin.service.da184;

import fr.pe.pfin.service.da184.application.configuration.ClientExternePoleEmploiProperties;
import fr.pe.pfin.service.da184.application.configuration.PoleEmploiAuthentProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({ClientExternePoleEmploiProperties.class, PoleEmploiAuthentProperties.class})
public class Da184EstimeformationApplication {
    public static void main(String[] args) {
        SpringApplication.run(Da184EstimeformationApplication.class, args);
    }
}
