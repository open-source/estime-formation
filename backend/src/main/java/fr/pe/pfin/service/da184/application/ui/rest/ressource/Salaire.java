package fr.pe.pfin.service.da184.application.ui.rest.ressource;

import jakarta.validation.constraints.Positive;

public class Salaire {


    @Positive
    private float montantMensuelBrut;
    @Positive
    private float montantMensuelNet;

    public float getMontantMensuelBrut() {
        return montantMensuelBrut;
    }

    public void setMontantMensuelBrut(float montantMensuelBrut) {
        this.montantMensuelBrut = montantMensuelBrut;
    }

    public float getMontantMensuelNet() {
        return montantMensuelNet;
    }

    public void setMontantMensuelNet(float montantMensuelNet) {
        this.montantMensuelNet = montantMensuelNet;
    }

    @Override
    public String toString() {
        return "Salaire [montantMensuelBrut=" + montantMensuelBrut + ", montantMensuelNet=" + montantMensuelNet + "]";
    }
}
