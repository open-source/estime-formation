package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SimulationMensuelle;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class AideUtile {

    private AideUtile() {
    }

    public static Aide creerAide(AideEnum aideEnum, Optional<OrganismeEnum> organismeEnumOptional,
                                 Optional<List<String>> messageAlerteOptional, boolean isAideReportee, float montantAide) {
        Aide aide = new Aide();
        aide.setCode(aideEnum.getCode());
        if (messageAlerteOptional.isPresent()) {
            aide.setMessagesAlerte(messageAlerteOptional.get());
        }
        aide.setMontant(montantAide);
        aide.setNom(aideEnum.getNom());
        if (organismeEnumOptional.isPresent()) {
            aide.setOrganisme(organismeEnumOptional.get().getNomCourt());
        }
        aide.setReportee(isAideReportee);
        return aide;
    }

    public static float getMontantAidePourCeMoisSimule(Simulation simulation, String codeAide,
                                                       int numeroMoisMontantARecuperer) {
        Optional<Aide> aidePourCeMois = getAidePourCeMoisSimule(simulation, codeAide, numeroMoisMontantARecuperer);
        if (aidePourCeMois.isPresent()) {
            return aidePourCeMois.get().getMontant();
        }
        return 0;
    }

    public static Optional<Aide> getAidePourCeMoisSimule(Simulation simulation, String codeAide, int numeroMois) {
        if (simulation != null && simulation.getSimulationsMensuelles() != null) {
            int indexSimulationMensuelARecuperer = numeroMois - 1;
            if (indexSimulationMensuelARecuperer >= 0
                    && indexSimulationMensuelARecuperer < simulation.getSimulationsMensuelles().size()) {
                SimulationMensuelle simulationMensuelle = simulation.getSimulationsMensuelles()
                        .get(indexSimulationMensuelARecuperer);
                if (simulationMensuelle != null && simulationMensuelle.getAides() != null
                        && !simulationMensuelle.getAides().isEmpty()) {
                    Aide aide = simulationMensuelle.getAides().get(codeAide);
                    if (aide != null) {
                        return Optional.of(aide);
                    }
                }
            }
        }
        return Optional.empty();
    }

    public static LocalDate getMoisAvantSimulation(int numeroMoisMontantARecuperer, LocalDate dateDebutSimulation) {
        LocalDate dateDemandeSimulation = DateUtile.enleverMoisALocalDate(dateDebutSimulation, 1);
        if (numeroMoisMontantARecuperer < 0) {
            return DateUtile.enleverMoisALocalDate(dateDemandeSimulation, Math.abs(numeroMoisMontantARecuperer));
        }
        return dateDemandeSimulation;
    }
}
