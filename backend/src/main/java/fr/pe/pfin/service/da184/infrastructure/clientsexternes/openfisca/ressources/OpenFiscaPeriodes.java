package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;

@JsonPropertyOrder(alphabetic = true)
public class OpenFiscaPeriodes extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return OpenFiscaObjectMapperService.getJsonStringFromObject(this);
	}

}
