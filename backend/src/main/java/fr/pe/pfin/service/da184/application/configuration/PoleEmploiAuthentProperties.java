package fr.pe.pfin.service.da184.application.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "peam")
public record PoleEmploiAuthentProperties(String clientId, String clientSecret, String tokenUri,
                                          String introspectTokenURI, String userInfoURI) {
}
