package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.MoisTravailleAvantSimulation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Salaire;

import java.util.Arrays;
import java.util.Objects;

public class PeriodeTravailleeAvantSimulationUtile {

    public static final int INDEX_MAX_MOIS_TRAVAILLES_AVANT_SIMULATION = 13;


    private PeriodeTravailleeAvantSimulationUtile() {
    }

    public static int getNombreMoisTravaillesAuCoursDes3DerniersMoisAvantSimulation(Demandeur demandeur) {
        return getNombreMoisTravaillesAuCoursDesXDerniersMoisAvantSimulation(demandeur, 3);
    }

    public static int getNombreMoisTravaillesAuCoursDes6DerniersMoisAvantSimulation(Demandeur demandeur) {
        return getNombreMoisTravaillesAuCoursDesXDerniersMoisAvantSimulation(demandeur, 6);
    }

    /**
     * Fonction qui permet de récupérer le nombre de mois travaillés dans la péridoe
     * de X mois avant la simulation
     *
     * @return nombre mois travaillés au cours des X derniers mois
     */
    public static int getNombreMoisTravaillesAuCoursDesXDerniersMoisAvantSimulation(Demandeur demandeur, int nombreDeMoisAConsiderer) {
        int nombreMoisTravaillesDerniersMois = 0;
        for (int moisAvantSimulation = 0; moisAvantSimulation < nombreDeMoisAConsiderer; moisAvantSimulation++) {
            if (Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation()) &&
                    Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation()) &&
                    Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois()) &&
                    demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois().length > moisAvantSimulation) {
                MoisTravailleAvantSimulation moisTravailleAvantSimulation = demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois()[moisAvantSimulation];
                if (Objects.nonNull(moisTravailleAvantSimulation) && isMoisTravaille(moisTravailleAvantSimulation)) {
                    nombreMoisTravaillesDerniersMois++;
                }
            }
        }
        return nombreMoisTravaillesDerniersMois;
    }

    /**
     * Méthode permettant de récupérer le salaire du demandeur à un mois
     * donné avant ou après sa simulation selon le tableau suivant
     * <p>
     * <p>
     * Les colonnes représentent les mois simulés Les lignes représentent les mois
     * voulu dans les 12 derniers mois
     * <p>
     * 0 1 2 3 4 5 0 M-12 [11] M-11 [10] M-10 [9] M-9 [8] M-8 [7] M-7 [6] 1 M-11
     * [10] M-10 [9] M-9 [8] M-8 [7] M-7 [6] M-6 [5] 2 M-10 [9] M-9 [8] M-8 [7] M-7
     * [6] M-6 [5] M-5 [4] 3 M-9 [8] M-8 [7] M-7 [5] M-6 [5] M-5 [4] M-4 [3] 4 M-8
     * [7] M-7 [6] M-6 [5] M-5 [4] M-4 [3] M-3 [2] 5 M-7 [6] M-6 [5] M-5 [4] M-4 [3]
     * M-3 [2] M-2 [1] 6 M-6 [5] M-5 [4] M-4 [3] M-3 [2] M-2 [1] M-1 [0] 7 M-5 [4]
     * M-4 [3] M-3 [2] M-2 [1] M-1 [0] M0 / 8 M-4 [3] M-3 [2] M-2 [1] M-1 [0] M0 /
     * M1 futur 9 M-3 [2] M-2 [1] M-1 [0] M0 / M1 futur M2 futur 10 M-2 [1] M-1 [0]
     * M0 / M1 futur M2 futur M3 futur 11 M-1 [0] M0 / M1 futur M2 futur M3 futur M4
     * futur
     * <p>
     * Les salaires des mois M0 sont par défault valorisés à 0 car on ne connait pas
     * actuellement le montant du salaire du mois en cours
     *
     * @param numeroMoisPeriodeOpenfisca numéro du mois recherché parmis les 12
     *                                   derniers salaires
     */
    public static Salaire getSalaireAvantPeriodeSimulation(Demandeur demandeur, int numeroMoisPeriodeOpenfisca) {
        if (Objects.nonNull(demandeur.getFutureFormation()) && Objects.nonNull(demandeur.getFutureFormation().getSalaire())) {
            int indexMoisAvantPeriode = INDEX_MAX_MOIS_TRAVAILLES_AVANT_SIMULATION + (numeroMoisPeriodeOpenfisca);
            if (indexMoisAvantPeriode > INDEX_MAX_MOIS_TRAVAILLES_AVANT_SIMULATION) {
                return demandeur.getFutureFormation().getSalaire();
            }
            return getMoisTravaillesAvantSimulation(demandeur, Math.abs(numeroMoisPeriodeOpenfisca + 1)).getSalaire();
        }
        return null;
    }

    public static Salaire getSalaireAvecCumulAvantPeriodeSimulation(Demandeur demandeur, int numeroMoisPeriodeOpenfisca) {
        int indexMoisAvantPeriode = INDEX_MAX_MOIS_TRAVAILLES_AVANT_SIMULATION + (numeroMoisPeriodeOpenfisca + 1);
        if (indexMoisAvantPeriode > INDEX_MAX_MOIS_TRAVAILLES_AVANT_SIMULATION) {
            return getSalaireCumul(demandeur);
        }
        return getMoisTravaillesAvantSimulation(demandeur, Math.abs(numeroMoisPeriodeOpenfisca + 1)).getSalaire();
    }

    public static Salaire getSalaireCumul(Demandeur demandeur) {
        Salaire futurSalaire = demandeur.getFutureFormation().getSalaire();
        Salaire ancienSalaire = getMoisTravaillesAvantSimulation(demandeur, 0).getSalaire();
        float salaireNet = futurSalaire.getMontantMensuelNet() + ancienSalaire.getMontantMensuelNet();
        float salaireBrut = futurSalaire.getMontantMensuelBrut() + ancienSalaire.getMontantMensuelBrut();
        Salaire salaireCumule = new Salaire();
        salaireCumule.setMontantMensuelNet(salaireNet);
        salaireCumule.setMontantMensuelBrut(salaireBrut);

        return salaireCumule;
    }

    public static Salaire getSalaireAvantPeriodeSimulationPersonne(Personne personne, int numeroMoisPeriodeOpenfisca) {
        int indexMoisAvantPeriode = INDEX_MAX_MOIS_TRAVAILLES_AVANT_SIMULATION + (numeroMoisPeriodeOpenfisca + 1);
        if (indexMoisAvantPeriode > INDEX_MAX_MOIS_TRAVAILLES_AVANT_SIMULATION &&
                Objects.nonNull(personne.getRessourcesFinancieres()) &&
                Objects.nonNull(personne.getRessourcesFinancieres().getSalaire())) {
            return personne.getRessourcesFinancieres().getSalaire();
        } else {
            return getMoisTravaillesAvantSimulationPersonne(personne, Math.abs(numeroMoisPeriodeOpenfisca + 1)).getSalaire();
        }
    }

    public static MoisTravailleAvantSimulation getMoisTravaillesAvantSimulation(Demandeur demandeur, int index) {
        MoisTravailleAvantSimulation moisTravaillesAvantSimulation = getMoisTravailleVide();
        if (hasTravailleMoisAvantSimulation(demandeur, index) && hasSalairesAvantPeriodeSimulation(demandeur, index)) {
            moisTravaillesAvantSimulation = demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois()[index];
        }
        return moisTravaillesAvantSimulation;
    }

    public static MoisTravailleAvantSimulation getMoisTravaillesAvantSimulationPersonne(Personne personne, int index) {
        MoisTravailleAvantSimulation moisTravaillesAvantSimulation = getMoisTravailleVide();
        if (hasSalairesAvantPeriodeSimulationPersonne(personne, index)) {
            moisTravaillesAvantSimulation = personne.getRessourcesFinancieres().getPeriodeTravailleeAvantSimulation().getMois()[index];
        }
        return moisTravaillesAvantSimulation;
    }

    private static MoisTravailleAvantSimulation getMoisTravailleVide() {
        MoisTravailleAvantSimulation moisTravailleAvantSimulationVide = new MoisTravailleAvantSimulation();
        Salaire salaireVide = new Salaire();
        moisTravailleAvantSimulationVide.setSalaire(salaireVide);
        return moisTravailleAvantSimulationVide;
    }

    private static boolean hasTravailleMoisAvantSimulation(Demandeur demandeur, int index) {
        return Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation()) &&
                Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation()) &&
                Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois()) &&
                demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois().length > index &&
                Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois()[index]);
    }

    public static boolean hasSalairesAvantPeriodeSimulation(Demandeur demandeur) {
        if (Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation()) &&
                Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation()) &&
                Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois())) {
            return Arrays.stream(
                            demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois())
                    .anyMatch(Objects::nonNull);
        }
        return false;
    }

    public static boolean hasSalairesAvantPeriodeSimulation(Demandeur demandeur, int index) {
        return Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation()) &&
                Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation()) &&
                Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois()) &&
                demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois().length > index &&
                Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois()[index]);
    }

    public static boolean hasSalairesAvantPeriodeSimulationPersonne(Personne personne, int index) {
        return Objects.nonNull(personne.getRessourcesFinancieres()) &&
                Objects.nonNull(personne.getRessourcesFinancieres().getPeriodeTravailleeAvantSimulation()) &&
                Objects.nonNull(personne.getRessourcesFinancieres().getPeriodeTravailleeAvantSimulation().getMois()) &&
                personne.getRessourcesFinancieres().getPeriodeTravailleeAvantSimulation().getMois().length > index &&
                Objects.nonNull(personne.getRessourcesFinancieres().getPeriodeTravailleeAvantSimulation().getMois()[index]);
    }

    public static boolean isMoisTravaille(MoisTravailleAvantSimulation moisTravailleAvantSimulation) {
        return moisTravailleAvantSimulation.getSalaire().getMontantMensuelNet() > 0;
    }
}
