package fr.pe.pfin.service.da184.domaine.use_cases;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.ConditionsRemunerations;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SituationRemuneration;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class ObtenirConditionsRemuneration {

    private final ConditionsRemunerationPort conditionsRemuneration;

    public ObtenirConditionsRemuneration(ConditionsRemunerationPort conditionsRemunerationPort) {
        this.conditionsRemuneration = conditionsRemunerationPort;
    }

    public List<ConditionsRemunerations> executer(SituationRemuneration situationRemuneration) throws URISyntaxException, IOException {
        return this.conditionsRemuneration.getConditionsList(situationRemuneration);
    }
}
