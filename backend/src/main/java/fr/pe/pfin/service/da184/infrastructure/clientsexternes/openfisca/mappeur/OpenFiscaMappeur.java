package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.SituationFamilialeUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaFamille;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaMenage;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class OpenFiscaMappeur {

    private OpenFiscaMappeur() {
    }

    public static OpenFiscaRoot mapDemandeurToOpenFiscaPayload(Demandeur demandeur, LocalDate dateDebutSimulation) {
        OpenFiscaRoot payloadOpenFisca = new OpenFiscaRoot();
        Optional<List<Personne>> personneAChargeOptional = SituationFamilialeUtile.getPersonnesAChargeAgeInferieurAgeLimite(demandeur);
        payloadOpenFisca.setIndividus(creerIndividusOpenFisca(demandeur, personneAChargeOptional, dateDebutSimulation));
        payloadOpenFisca.setFamilles(creerFamillesOpenFisca(demandeur, personneAChargeOptional, dateDebutSimulation));
        payloadOpenFisca.setMenages(creerMenagesOpenFisca(demandeur, dateDebutSimulation));
        return payloadOpenFisca;
    }

    private static Map<String, OpenFiscaIndividu> creerIndividusOpenFisca(Demandeur demandeur, Optional<List<Personne>> personneAChargeAgeInferieureAgeLimiteOptional, LocalDate dateDebutSimulation) {
        Map<String, OpenFiscaIndividu> individus = new HashMap<>();
        individus.put(ParametresOpenFisca.DEMANDEUR, OpenFiscaMappeurIndividu.creerDemandeurOpenFisca(demandeur, dateDebutSimulation));
        if (personneAChargeAgeInferieureAgeLimiteOptional.isPresent()) {
            OpenFiscaMappeurIndividu.ajouterPersonneAChargeIndividus(individus, personneAChargeAgeInferieureAgeLimiteOptional.get(), dateDebutSimulation);
        }
        if (SituationFamilialeUtile.isEnCouple(demandeur)) {
            individus.put(ParametresOpenFisca.CONJOINT, OpenFiscaMappeurIndividu.creerConjointOpenFisca(demandeur.getSituationFamiliale().getConjoint(), dateDebutSimulation));
        }
        return individus;
    }

    private static Map<String, OpenFiscaFamille> creerFamillesOpenFisca(Demandeur demandeur, Optional<List<Personne>> personneAChargeAgeInferieureAgeLimiteOptional, LocalDate dateDebutSimulation) {
        Map<String, OpenFiscaFamille> familles = new HashMap<>();
        familles.put(ParametresOpenFisca.FAMILLE1, OpenFiscaMappeurFamille.creerFamilleOpenFisca(demandeur, personneAChargeAgeInferieureAgeLimiteOptional, dateDebutSimulation));
        return familles;
    }

    private static Map<String, OpenFiscaMenage> creerMenagesOpenFisca(Demandeur demandeur, LocalDate dateDebutSimulation) {
        Map<String, OpenFiscaMenage> menages = new HashMap<>();
        menages.put(ParametresOpenFisca.MENAGE1, OpenFiscaMappeurMenage.creerMenageOpenFisca(demandeur, dateDebutSimulation));
        return menages;
    }

}
