package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.PeriodeTravailleeAvantSimulationUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;

public class PrimeActiviteASSUtile {

    private PrimeActiviteASSUtile() {
    }

    /**
     * Fonction permettant de déterminer si on a calculé le(s) montant(s) de la
     * prime d'activité et/ou du RSA le mois précédent et s'il(s) doi(ven)t être
     * versé(s) ce mois-ci
     *
     * @return _______________________________________________________________________________________________________
     * | | | | | | | | | | Mois cumul salaire / ass | M0 | M1 | M2 | M3 | M4
     * | M5 | M6 | | | | | | | | | | | - 0 | | | | (C1) | V1 | R1 | R1 | | -
     * 1 | | | (C1) | V1 | R1 | R1/(C2) | V2 | | - 2 | | (C1) | V1 | R1 |
     * R1/(C2) | V2 | R2 | | - 3 | (C1) | V1 | R1 | R1/C2 | V2 | R2 | R2 |
     * |__________________________|__________|__________|__________|__________|__________|__________|__________|
     */
    public static boolean isPrimeActiviteACalculer(int numeroMoisSimule, Demandeur demandeur) {
        if (RessourcesFinancieresAvantSimulationUtile.hasTravailleAuCoursDerniersMoisAvantSimulation(demandeur)) {
            int nombreMoisTravaillesDerniersMois = PeriodeTravailleeAvantSimulationUtile
                    .getNombreMoisTravaillesAuCoursDes3DerniersMoisAvantSimulation(demandeur);
            return nombreMoisTravaillesDerniersMois == 1 && (numeroMoisSimule == 2 || numeroMoisSimule == 5)
                    || (nombreMoisTravaillesDerniersMois == 2 && (numeroMoisSimule == 1 || numeroMoisSimule == 4))
                    || (nombreMoisTravaillesDerniersMois == 3 && (numeroMoisSimule == 0 || numeroMoisSimule == 3));
        }
        return numeroMoisSimule == 3;
    }

    /**
     * Fonction permettant de déterminer si on a calculé le montant de la prime
     * d'activité le mois précédent et s'il doit être versé ce mois-ci
     *
     * @return _______________________________________________________________________________________________________
     * | | | | | | | | | | Mois cumul salaire / ass | M0 | M1 | M2 | M3 | M4
     * | M5 | M6 | | | | | | | | | | | - 0 | | | | C1 | (V1) | R1 | R1 | | -
     * 1 | | | C1 | (V1) | R1 | R1/C2 | (V2) | | - 2 | | C1 | (V1) | R1 |
     * R1/C2 | (V2) | R2 | | - 3 | C1 | (V1) | R1 | R1/C2 | (V2) | R2 | R2 |
     * |__________________________|__________|__________|__________|__________|__________|__________|__________|
     */
    public static boolean isPrimeActiviteAVerser(int numeroMoisSimule, Demandeur demandeur) {
        if (RessourcesFinancieresAvantSimulationUtile.hasTravailleAuCoursDerniersMoisAvantSimulation(demandeur)) {
            int nombreMoisTravaillesDerniersMois = PeriodeTravailleeAvantSimulationUtile
                    .getNombreMoisTravaillesAuCoursDes3DerniersMoisAvantSimulation(demandeur);
            return nombreMoisTravaillesDerniersMois == 1 && (numeroMoisSimule == 3 || numeroMoisSimule == 6)
                    || (nombreMoisTravaillesDerniersMois == 2 && (numeroMoisSimule == 2 || numeroMoisSimule == 5))
                    || (nombreMoisTravaillesDerniersMois == 3 && (numeroMoisSimule == 1 || numeroMoisSimule == 4));
        }
        return numeroMoisSimule == 6;
    }
}
