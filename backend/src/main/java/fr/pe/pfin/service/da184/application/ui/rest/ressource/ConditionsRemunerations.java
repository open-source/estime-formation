package fr.pe.pfin.service.da184.application.ui.rest.ressource;

public record ConditionsRemunerations(String condition, Integer remuneration) {
}
