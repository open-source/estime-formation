package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.InformationsPersonnelles;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.NationaliteEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

public class InformationsPersonnellesUtile {

    private InformationsPersonnellesUtile() {
    }

    public static boolean isFrancais(InformationsPersonnelles informationsPersonnelles) {
        return NationaliteEnum.FRANCAISE.getValeur().equalsIgnoreCase(informationsPersonnelles.getNationalite());
    }

    public static boolean isEuropeenOuSuisse(InformationsPersonnelles informationsPersonnelles) {
        return NationaliteEnum.RESSORTISSANT_UNION_EUROPEENNE.getValeur()
                .equalsIgnoreCase(informationsPersonnelles.getNationalite())
                || NationaliteEnum.RESSORTISSANT_ESPACE_ECONOMIQUE_EUROPEEN.getValeur()
                        .equalsIgnoreCase(informationsPersonnelles.getNationalite())
                || NationaliteEnum.SUISSE.getValeur().equalsIgnoreCase(informationsPersonnelles.getNationalite());
    }

    public static boolean isNotFrancaisOuEuropeenOuSuisse(InformationsPersonnelles informationsPersonnelles) {
        return NationaliteEnum.AUTRE.getValeur().equalsIgnoreCase(informationsPersonnelles.getNationalite());
    }

    public static boolean hasTitreSejourEnFranceValide(InformationsPersonnelles informationsPersonnelles) {
        return informationsPersonnelles.hasTitreSejourEnFranceValide();
    }

    public static boolean isBeneficiaireACRE(Demandeur demandeur) {
        return (demandeur.getInformationsPersonnelles().isBeneficiaireACRE()
                && hasMicroEntreprise(demandeur));
    }

    public static int getNombreMoisDepuisCreationEntreprise(Demandeur demandeur, LocalDate dateDebutSimulation) {
        if (hasMicroEntreprise(demandeur)) {
            return DateUtile.getNbrMoisEntreDeuxLocalDates(DateUtile.getDateDernierJourDuMois(demandeur
                    .getInformationsPersonnelles().getMicroEntreprise().getDateRepriseCreationEntreprise()),
                    dateDebutSimulation);
        }
        return 0;
    }

    public static boolean hasCodePostal(Demandeur demandeur) {
        return demandeur.getInformationsPersonnelles() != null
                && demandeur.getInformationsPersonnelles().getLogement() != null
                && demandeur.getInformationsPersonnelles().getLogement().getCoordonnees() != null
                && demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().getCodePostal() != null;
    }

    public static boolean hasMicroEntreprise(Demandeur demandeur) {
        return (demandeur != null && demandeur.getInformationsPersonnelles() != null
                && demandeur.getInformationsPersonnelles().isMicroEntrepreneur()
                && demandeur.getInformationsPersonnelles().getMicroEntreprise() != null);
    }

    public static float getRevenusActuelsSur1MoisMicroEntreprise(Demandeur demandeur) {
        if (hasMicroEntreprise(demandeur)) {
            return BigDecimal
                    .valueOf(demandeur.getInformationsPersonnelles().getMicroEntreprise().getChiffreAffairesN())
                    .divide(BigDecimal.valueOf(12), 0, RoundingMode.HALF_UP).floatValue();
        }
        return 0;
    }

    public static boolean isSalarie(Demandeur demandeur) {
        return (demandeur.getInformationsPersonnelles() != null
                && demandeur.getInformationsPersonnelles().isSalarie());
    }

    public static boolean hasCumulAncienEtNouveauSalaire(Demandeur demandeur) {
        return (isSalarie(demandeur)
                && demandeur.getInformationsPersonnelles().hasCumulAncienEtNouveauSalaire());
    }
}
