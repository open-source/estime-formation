package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.RessourcesFinancieresAvantSimulation;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.PersonneUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;

import java.time.LocalDate;

public class OpenFiscaMappeurRessourcesPersonne {

    private OpenFiscaMappeurRessourcesPersonne() {
    }

    public static void addRessourcesFinancieresPersonne(OpenFiscaIndividu personneOpenFisca, Personne personne, LocalDate dateDebutSimulation) {
        RessourcesFinancieresAvantSimulation ressourcesFinancieres = personne.getRessourcesFinancieres();
        if (ressourcesFinancieres != null) {
            if (RessourcesFinancieresAvantSimulationUtile.hasTravailleAuCoursDerniersMoisAvantSimulation(ressourcesFinancieres)) {
                OpenFiscaMappeurPeriode.creerPeriodesSalairePersonne(personneOpenFisca, personne, dateDebutSimulation);
            } else if (RessourcesFinancieresAvantSimulationUtile.hasSalaire(ressourcesFinancieres)) {
                personneOpenFisca.setSalaireNet(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(
                        ressourcesFinancieres.getSalaire().getMontantMensuelNet(), dateDebutSimulation));
            }
            if (RessourcesFinancieresAvantSimulationUtile.hasRevenusTravailleurIndependant(ressourcesFinancieres)) {
                personneOpenFisca.setChiffreAffairesIndependant(OpenFiscaMappeurPeriode.creerPeriodesAnnees(
                        RessourcesFinancieresAvantSimulationUtile.getRevenusTravailleurIndependant(ressourcesFinancieres),
                        dateDebutSimulation));
            }
            if (RessourcesFinancieresAvantSimulationUtile.hasRevenusMicroEntreprise(ressourcesFinancieres)) {
                personneOpenFisca.setBeneficesMicroEntreprise(OpenFiscaMappeurPeriode.creerPeriodesAnnees(
                        RessourcesFinancieresAvantSimulationUtile.getRevenusMicroEntreprise(ressourcesFinancieres),
                        dateDebutSimulation));
            }
            if (RessourcesFinancieresAvantSimulationUtile.hasPensionRetraite(ressourcesFinancieres)) {
                personneOpenFisca.setPensionRetraite(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(
                        RessourcesFinancieresAvantSimulationUtile.getPensionRetraite(ressourcesFinancieres), dateDebutSimulation));
            }
            if (RessourcesFinancieresAvantSimulationUtile.hasRevenusImmobilier(ressourcesFinancieres)) {
                personneOpenFisca.setRevenusLocatifs(OpenFiscaMappeurPeriode.creerPeriodesRevenusImmobilier(
                        RessourcesFinancieresAvantSimulationUtile.getRevenusImmobilierSur1Mois(ressourcesFinancieres),
                        dateDebutSimulation));
            }
            addRessourcesFinancieresCAF(personneOpenFisca, ressourcesFinancieres, dateDebutSimulation);
            addRessourcesFinancieresPoleEmploi(personneOpenFisca, personne, dateDebutSimulation);
            addRessourcesFinancieresCPAM(personneOpenFisca, ressourcesFinancieres, dateDebutSimulation);
        }
    }

    private static void addRessourcesFinancieresCAF(OpenFiscaIndividu personneOpenFisca, RessourcesFinancieresAvantSimulation ressourcesFinancieres, LocalDate dateDebutSimulation) {
        if (ressourcesFinancieres.getAidesCAF() != null
                && ressourcesFinancieres.getAidesCAF().getAllocationAAH() != null
                && ressourcesFinancieres.getAidesCAF().getAllocationAAH() > 0) {
            personneOpenFisca.setAllocationAdulteHandicape(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(
                    ressourcesFinancieres.getAidesCAF().getAllocationAAH(), dateDebutSimulation));
        }
    }

    private static void addRessourcesFinancieresPoleEmploi(OpenFiscaIndividu personneOpenFisca, Personne personne, LocalDate dateDebutSimulation) {
        RessourcesFinancieresAvantSimulation ressourcesFinancieres = personne.getRessourcesFinancieres();
        if (PersonneUtile.hasAllocationARE(personne)) {
            personneOpenFisca.setARE(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(
                    ressourcesFinancieres.getAidesPoleEmploi().getAllocationARE().getAllocationMensuelleNet(),
                    dateDebutSimulation));
        }
        if (PersonneUtile.hasAllocationASS(personne)) {
            personneOpenFisca.setAllocationSolidariteSpecifique(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(
                    ressourcesFinancieres.getAidesPoleEmploi().getAllocationASS().getAllocationMensuelleNet(),
                    dateDebutSimulation));
        }
    }

    private static void addRessourcesFinancieresCPAM(OpenFiscaIndividu personneOpenFisca, RessourcesFinancieresAvantSimulation ressourcesFinancieres, LocalDate dateDebutSimulation) {
        if (ressourcesFinancieres.getAidesCPAM() != null
                && ressourcesFinancieres.getAidesCPAM().getPensionInvalidite() != null
                && ressourcesFinancieres.getAidesCPAM().getPensionInvalidite() > 0) {
            personneOpenFisca.setPensionInvalidite(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(
                    ressourcesFinancieres.getAidesCPAM().getPensionInvalidite(), dateDebutSimulation));
        }
    }
}
