package fr.pe.pfin.service.da184.application.ui.rest.ressource;

public class Notation {
    private String idRedis;

    private Integer notation;

    public String getIdRedis() {
        return idRedis;
    }

    public void setIdRedis(String idRedis) {
        this.idRedis = idRedis;
    }

    public Integer getNotation() {
        return notation;
    }

    public void setNotation(Integer notation) {
        this.notation = notation;
    }

    @Override
    public String toString() {
        return "Notation{" +
                "idRedis='" + idRedis + '\'' +
                ", notation=" + notation +
                '}';
    }
}
