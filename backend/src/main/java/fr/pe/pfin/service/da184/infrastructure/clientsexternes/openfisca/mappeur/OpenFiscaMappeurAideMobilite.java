package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.FutureFormationUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Objects;

import static fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.ParametresOpenFisca.DEMANDEUR;

public class OpenFiscaMappeurAideMobilite {

    private static final String CONTEXTE_FORMATION = "formation";
    private static final String TYPE_CONTRAT = "formation";
    private static final String CATEGORIE_DEMANDEUR_EMPLOI = "categorie_4";
    private static final String LIEU_EMPLOI_OU_FORMATION = "metropole_hors_corse";

    private OpenFiscaMappeurAideMobilite() {
    }

    public static float getMontantAideMobilite(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getIndividus() != null && openFiscaRoot.getIndividus().get(DEMANDEUR) != null) {
            OpenFiscaPeriodes openFiscaAideMobilite = openFiscaRoot.getIndividus().get(DEMANDEUR).getAideMobilite();
            String periodeFormateeAideMobilite = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaAideMobilite != null && openFiscaAideMobilite.get(periodeFormateeAideMobilite) != null) {
                Double montantAideMobilite = (Double) openFiscaAideMobilite.get(periodeFormateeAideMobilite);
                return BigDecimal.valueOf(montantAideMobilite).setScale(0, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }

    public static void addAideMobiliteOpenFiscaIndividu(OpenFiscaIndividu openFiscaIndividu, Demandeur demandeur, LocalDate dateDebutSimulation) {

        openFiscaIndividu.setContexteActivite(
                OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(CONTEXTE_FORMATION, dateDebutSimulation));

        openFiscaIndividu.setDebutContratTravail(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(
                DateUtile.convertDateToStringOpenFisca(dateDebutSimulation), dateDebutSimulation));

        openFiscaIndividu.setAideMobiliteDateDemande(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(
                DateUtile.convertDateToStringOpenFisca(dateDebutSimulation), dateDebutSimulation));

        openFiscaIndividu.setTypeContratTravail(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(
                TYPE_CONTRAT, dateDebutSimulation));

        openFiscaIndividu.setFormationValideePE(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca("true", dateDebutSimulation));
        openFiscaIndividu.setFormationFinanceeCofinancee(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca("true", dateDebutSimulation));

        openFiscaIndividu.setDureeFormation(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(
                FutureFormationUtile.calculDureeFormation(demandeur, dateDebutSimulation), dateDebutSimulation));

        openFiscaIndividu.setCategorieDemandeur(
                OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(CATEGORIE_DEMANDEUR_EMPLOI, dateDebutSimulation));

        openFiscaIndividu.setStagiaire(
                OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(true, dateDebutSimulation));

        openFiscaIndividu.setLieuEmploiOuFormation(
                OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(LIEU_EMPLOI_OU_FORMATION, dateDebutSimulation));

        if (Objects.nonNull(demandeur.getFutureFormation())) {
            openFiscaIndividu.setDistanceActiviteDomicile(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(
                    demandeur.getFutureFormation().getDistanceActiviteDomicile(), dateDebutSimulation));
            openFiscaIndividu.setNombreAllersRetours(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(
                    demandeur.getFutureFormation().getNombreTrajetsDomicileTravail(), dateDebutSimulation));
        }

        if(!demandeur.getIsFromRegion()){
            openFiscaIndividu
                    .setAideMobilite(OpenFiscaMappeurPeriode.creerPeriodeCalculeeUniqueOpenFisca(dateDebutSimulation));
        }
    }
}
