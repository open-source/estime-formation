package fr.pe.pfin.service.da184.application.ui.rest.ressource;

import java.util.Objects;

public class Aide extends RessourceFinanciere {

    // true si l'aide n'a pas été créée mais reportée depuis un mois précédent
    // ex : prime d'acivité, l'aide créée au mois N est reportée au mois N+1 et N+2
    private boolean isReportee;
    private String organisme;

    public boolean isReportee() {
        return isReportee;
    }

    public void setReportee(boolean isReportee) {
        this.isReportee = isReportee;
    }

    public String getOrganisme() {
        return organisme;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    @Override
    public String toString() {
        return "Aide [ montant=" + getMontant() + ", isReportee=" + isReportee + ", organisme=" + organisme + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Aide aide)) return false;
        return isReportee == aide.isReportee && Objects.equals(organisme, aide.organisme);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isReportee, organisme);
    }
}
