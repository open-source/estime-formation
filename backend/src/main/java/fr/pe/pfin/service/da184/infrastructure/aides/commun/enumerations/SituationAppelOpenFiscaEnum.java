package fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations;

public enum SituationAppelOpenFiscaEnum {

	AGEPI, AM, PPA, RSA, AL, RSA_PPA, AL_PPA, AL_RSA, AL_PPA_RSA, ARE_AGEPI_AM, ARE_PPA, ARE_RSA, ARE_AL, ARE_RSA_PPA,
	ARE_AL_PPA, ARE_AL_RSA, ARE_AL_PPA_RSA, ARE
}
