package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.BeneficiaireAidesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;

public class TemporaliteCAFUtile {

    private TemporaliteCAFUtile() {
    }

    public static boolean isRSAAVerser(int numeroMoisSimule, Demandeur demandeur) {
        boolean isRSAAVerser = false;
        if (BeneficiaireAidesUtile.isBeneficiaireRSA(demandeur)) {
            isRSAAVerser = RSAUtile.isRSAAVerser(numeroMoisSimule, demandeur);
        }
        return isRSAAVerser;
    }

    public static boolean isAideLogementAVerser(int numeroMoisSimule, Demandeur demandeur) {
        return AidesLogementUtile.isEligibleAidesLogement(demandeur) && AidesLogementUtile.isAideLogementAVerser(numeroMoisSimule, demandeur);
    }

    public static boolean isPrimeActiviteAVerser(int numeroMoisSimule, Demandeur demandeur) {
        boolean isPrimeActiviteAVerser = false;
        if (BeneficiaireAidesUtile.isBeneficiaireCAF(demandeur) || BeneficiaireAidesUtile.isBeneficiaireAucuneAide(demandeur)) {
            int prochaineDeclarationTrimestrielle = RessourcesFinancieresAvantSimulationUtile.getProchaineDeclarationTrimestrielle(demandeur);
            isPrimeActiviteAVerser = PrimeActiviteUtile.isPrimeActiviteAVerserDeclarationTrimestrielle(numeroMoisSimule, prochaineDeclarationTrimestrielle);
        } else if (BeneficiaireAidesUtile.isBeneficiaireASS(demandeur)) {
            isPrimeActiviteAVerser = PrimeActiviteASSUtile.isPrimeActiviteAVerser(numeroMoisSimule, demandeur);
        } else if (BeneficiaireAidesUtile.isBeneficiaireARE(demandeur)) {
            isPrimeActiviteAVerser = PrimeActiviteAREUtile.isPrimeActiviteAVerser(numeroMoisSimule);
        }
        return isPrimeActiviteAVerser;
    }
}
