package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.aides.caf.utile.AllocationAdultesHandicapesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.PeriodeTravailleeAvantSimulationUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile.AllocationSolidariteSpecifiqueUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile.AreUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;

import java.time.LocalDate;
import java.util.Objects;

public class OpenFiscaMappeurPeriode {

    public static final int NUMERO_MOIS_PERIODE = 0;
    public static final int NOMBRE_MOIS_PERIODE_OPENFISCA_TNS_AUTO_ENTREPRENEUR_CHIFFRE_AFFAIRES = 3;
    public static final int NOMBRE_MOIS_PERIODE_OPENFISCA = 4;
    public static final int NOMBRE_MOIS_SIMULATION = 6;
    public static final int NOMBRE_MOIS_PERIODE_OPENFISCA_COMPLEMENT_ARE = 7;
    public static final int NOMBRE_MOIS_PERIODE_OPENFISCA_SALAIRES = 13;
    public static final int NOMBRE_MOIS_PERIODE_36 = 36;
    public static final String TYPE_ACTIVITE_CHOMEUR = "chomeur";
    public static final String TYPE_ACTIVITE_ACTIF = "actif";

    private OpenFiscaMappeurPeriode() {
    }

    public static OpenFiscaPeriodes creerPeriodesOpenFisca(Object valeur, LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        for (int numeroMoisPeriode = -NOMBRE_MOIS_PERIODE_OPENFISCA; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), valeur);
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesOpenFisca1Month(Object valeur, LocalDate dateSimulation) {
        OpenFiscaPeriodes openFiscaPeriodes = new OpenFiscaPeriodes();
        openFiscaPeriodes.put(getPeriodeFormateePlusMonth(dateSimulation, 1), valeur);
        return openFiscaPeriodes;
    }

    public static OpenFiscaPeriodes creerPeriodesActiviteOpenFisca(LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        for (int numeroMoisPeriode = -NOMBRE_MOIS_PERIODE_OPENFISCA; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            if (numeroMoisPeriode < 0) {
                periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), TYPE_ACTIVITE_CHOMEUR);
            } else {
                periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), TYPE_ACTIVITE_ACTIF);
            }
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodeUniqueAREOpenFisca(Demandeur demandeur, Object valeur, LocalDate dateDebutSimulation) {
        if (PeriodeTravailleeAvantSimulationUtile.hasSalairesAvantPeriodeSimulation(demandeur, 0)) {
            return creerPeriodeUniqueEffectivePremierMoisOpenFisca(valeur, dateDebutSimulation);
        }
        return creerPeriodeUniqueEffectiveDeuxiemeMoisOpenFisca(valeur, dateDebutSimulation);
    }

    public static OpenFiscaPeriodes creerPeriodeUniqueEffectivePremierMoisOpenFisca(Object valeur, LocalDate dateDebutSimulation) {
        return creerPeriodeUniqueEffectiveMoisXOpenFisca(valeur, dateDebutSimulation, 0);
    }

    public static OpenFiscaPeriodes creerPeriodeUniqueEffectiveDeuxiemeMoisOpenFisca(Object valeur, LocalDate dateDebutSimulation) {
        return creerPeriodeUniqueEffectiveMoisXOpenFisca(valeur, dateDebutSimulation, 1);
    }

    private static OpenFiscaPeriodes creerPeriodeUniqueEffectiveMoisXOpenFisca(Object valeur, LocalDate dateDebutSimulation, int numeroMoisEffectif) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        ObjectMapper mapper = new ObjectMapper();
        for (int numeroMoisPeriode = numeroMoisEffectif - 1; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            if (numeroMoisPeriode == numeroMoisEffectif - 1) {
                periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), valeur);
            } else {
                periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), mapper.nullNode());
            }
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesCalculeesOpenFisca(LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        ObjectMapper mapper = new ObjectMapper();
        for (int numeroMoisPeriode = -NOMBRE_MOIS_PERIODE_OPENFISCA; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), mapper.nullNode());
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesCalculeesNouvelleAideOpenFisca(LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        ObjectMapper mapper = new ObjectMapper();
        for (int numeroMoisPeriode = -NOMBRE_MOIS_PERIODE_OPENFISCA; numeroMoisPeriode < 0; numeroMoisPeriode++) {
            periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), 0.0);
        }
        for (int numeroMoisPeriode = 0; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), mapper.nullNode());
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesCalculeesAREOpenFisca(Demandeur demandeur, LocalDate dateDebutSimulation) {
        if (PeriodeTravailleeAvantSimulationUtile.hasSalairesAvantPeriodeSimulation(demandeur, 0)) {
            return creerPeriodesCalculeesEffectivePremierMoisOpenFisca(dateDebutSimulation);
        }
        return creerPeriodesCalculeesEffectiveDeuxiemeMoisOpenFisca(dateDebutSimulation);
    }

    public static OpenFiscaPeriodes creerPeriodesCalculeesEffectivePremierMoisOpenFisca(LocalDate dateDebutSimulation) {
        return creerPeriodesCalculeesEffectiveMoisXOpenFisca(dateDebutSimulation, 0);
    }

    public static OpenFiscaPeriodes creerPeriodesCalculeesEffectiveDeuxiemeMoisOpenFisca(LocalDate dateDebutSimulation) {
        return creerPeriodesCalculeesEffectiveMoisXOpenFisca(dateDebutSimulation, 1);
    }

    private static OpenFiscaPeriodes creerPeriodesCalculeesEffectiveMoisXOpenFisca(LocalDate dateDebutSimulation, int numeroMoisEffectif) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        ObjectMapper mapper = new ObjectMapper();
        for (int numeroMoisPeriode = numeroMoisEffectif - 1; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), mapper.nullNode());
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodeCalculeeUniqueOpenFisca(LocalDate dateDebutSimulation) {
        ObjectMapper mapper = new ObjectMapper();
        return creerPeriodeUniqueOpenFisca(mapper.nullNode(), dateDebutSimulation);
    }

    public static OpenFiscaPeriodes creerPeriodeUniqueOpenFisca(Object valeur, LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        periodesOpenFisca.put(getPeriodeFormatee(dateDebutSimulation), valeur);
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesAREAvantSimulation(Demandeur demandeur, LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        int moisPassageComplementARE = AreUtile.numeroMoisPassageAuComplementARE(demandeur);
        for (int numeroMoisPeriode = -NOMBRE_MOIS_PERIODE_OPENFISCA; numeroMoisPeriode < moisPassageComplementARE; numeroMoisPeriode++) {
            periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode),
                    AreUtile.calculerMontantMensuelARE(demandeur, numeroMoisPeriode, dateDebutSimulation));
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesValeurNulleEgaleZero(Object valeur, LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        for (int numeroMoisPeriode = -NOMBRE_MOIS_PERIODE_OPENFISCA; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), Objects.requireNonNullElse(valeur, 0.0));
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesRevenusImmobilier(Object valeur, LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        LocalDate datePlusNbrPeriodeMois = DateUtile.ajouterMoisALocalDate(dateDebutSimulation, NOMBRE_MOIS_SIMULATION);
        for (int nombreMoisAEnlever = NOMBRE_MOIS_PERIODE_36; nombreMoisAEnlever >= 1; nombreMoisAEnlever--) {
            LocalDate datePeriode = DateUtile.enleverMoisALocalDate(datePlusNbrPeriodeMois, nombreMoisAEnlever);
            periodesOpenFisca.put(getPeriodeFormatee(datePeriode), valeur);
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesAideLogement(Demandeur demandeur, LocalDate dateDebutSimulation) {
        if (Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation()) &&
                Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()) &&
                Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesLogement())) {
            AllocationsLogement allocationsLogement = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesLogement();
            int prochaineDeclarationTrimestrielle = RessourcesFinancieresAvantSimulationUtile.getProchaineDeclarationTrimestrielle(demandeur);
            return creerPeriodesAllocationsLogement(allocationsLogement, dateDebutSimulation, prochaineDeclarationTrimestrielle);
        }
        return creerPeriodesCalculeesNouvelleAideOpenFisca(dateDebutSimulation);
    }

    public static OpenFiscaPeriodes creerPeriodesAllocationsLogement(AllocationsLogement allocationsLogement, LocalDate dateDebutSimulation, int prochaineDeclarationTrimestrielle) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        ObjectMapper mapper = new ObjectMapper();
        if (allocationsLogement == null) {
            allocationsLogement = new AllocationsLogement();
        }
        for (int numeroMoisPeriode = -NOMBRE_MOIS_PERIODE_OPENFISCA; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            handlePeriodesAllocationsLogement(allocationsLogement, dateDebutSimulation, prochaineDeclarationTrimestrielle, numeroMoisPeriode, periodesOpenFisca, mapper);

        }
        return periodesOpenFisca;
    }

    private static void handlePeriodesAllocationsLogement(AllocationsLogement allocationsLogement, LocalDate dateDebutSimulation, int prochaineDeclarationTrimestrielle, int numeroMoisPeriode, OpenFiscaPeriodes periodesOpenFisca, ObjectMapper mapper) {

        if ((prochaineDeclarationTrimestrielle == 1 || prochaineDeclarationTrimestrielle == 2) && numeroMoisPeriode < 0) {
            if (numeroMoisPeriode == -1) {
                periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), allocationsLogement.getMoisNMoins1());
            }
        } else if ((prochaineDeclarationTrimestrielle == 0 || prochaineDeclarationTrimestrielle == 3) && numeroMoisPeriode < -1) {
            if (numeroMoisPeriode == -2) {
                periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), allocationsLogement.getMoisNMoins1());
            }
        } else {
            periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), mapper.nullNode());
        }
    }

    public static void creerPeriodesSalaireDemandeur(OpenFiscaIndividu demandeurOpenFisca, Demandeur demandeur, LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFiscaSalaireNet = new OpenFiscaPeriodes();

        for (int numeroMoisPeriodeOpenfisca = -NOMBRE_MOIS_PERIODE_OPENFISCA_SALAIRES; numeroMoisPeriodeOpenfisca < NOMBRE_MOIS_SIMULATION; numeroMoisPeriodeOpenfisca++) {
            Salaire salairePourSimulation = PeriodeTravailleeAvantSimulationUtile.getSalaireAvantPeriodeSimulation(demandeur, numeroMoisPeriodeOpenfisca);
            if (Objects.nonNull(salairePourSimulation)) {
                periodesOpenFiscaSalaireNet.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriodeOpenfisca), salairePourSimulation.getMontantMensuelNet());
            }
        }

        demandeurOpenFisca.setSalaireNet(periodesOpenFiscaSalaireNet);
    }

    public static void creerPeriodesSalaireCumulDemandeur(OpenFiscaIndividu demandeurOpenFisca, Demandeur demandeur, LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFiscaSalaireNet = new OpenFiscaPeriodes();

        for (int numeroMoisPeriodeOpenfisca = -NOMBRE_MOIS_PERIODE_OPENFISCA_SALAIRES; numeroMoisPeriodeOpenfisca < NOMBRE_MOIS_SIMULATION; numeroMoisPeriodeOpenfisca++) {
            Salaire salairePourSimulation = PeriodeTravailleeAvantSimulationUtile.getSalaireAvecCumulAvantPeriodeSimulation(demandeur, numeroMoisPeriodeOpenfisca);
            periodesOpenFiscaSalaireNet.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriodeOpenfisca), salairePourSimulation.getMontantMensuelNet());
        }

        demandeurOpenFisca.setSalaireNet(periodesOpenFiscaSalaireNet);
    }

    public static void creerPeriodesSalairePersonne(OpenFiscaIndividu personneOpenFisca, Personne personne, LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFiscaSalaireNet = new OpenFiscaPeriodes();

        for (int numeroMoisPeriodeOpenfisca = -NOMBRE_MOIS_PERIODE_OPENFISCA_SALAIRES; numeroMoisPeriodeOpenfisca < NOMBRE_MOIS_SIMULATION; numeroMoisPeriodeOpenfisca++) {
            Salaire salairePourSimulation = PeriodeTravailleeAvantSimulationUtile.getSalaireAvantPeriodeSimulationPersonne(personne, numeroMoisPeriodeOpenfisca);
            periodesOpenFiscaSalaireNet.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriodeOpenfisca), salairePourSimulation.getMontantMensuelNet());
        }

        personneOpenFisca.setSalaireNet(periodesOpenFiscaSalaireNet);
    }

    public static OpenFiscaPeriodes creerPeriodesOpenFiscaARE(Demandeur demandeur, Simulation simulation, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        int numeroMoisMontantARecuperer = numeroMoisSimule - (OpenFiscaMappeurPeriode.NOMBRE_MOIS_PERIODE_OPENFISCA - 1);
        for (int numeroMoisPeriode = NUMERO_MOIS_PERIODE; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            float montantAide = 0;
            if (!isNumeroMoisMontantARecupererDansPeriodeSimulation(numeroMoisMontantARecuperer)) {
                montantAide = AreUtile.getMontantAREAvantSimulation(numeroMoisMontantARecuperer, demandeur, dateDebutSimulation);
            } else if (isNumeroMoisComplementARE(numeroMoisMontantARecuperer)) {
                montantAide = AideUtile.getMontantAidePourCeMoisSimule(simulation, AideEnum.COMPLEMENT_AIDE_RETOUR_EMPLOI.getCode(), numeroMoisMontantARecuperer);
            } else {
                montantAide = AideUtile.getMontantAidePourCeMoisSimule(simulation, AideEnum.AIDE_RETOUR_EMPLOI.getCode(), numeroMoisMontantARecuperer);
            }
            periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), montantAide);
            numeroMoisMontantARecuperer++;
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesOpenFiscaASS(Demandeur demandeur, LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        for (int numeroMoisPeriode = -NOMBRE_MOIS_PERIODE_OPENFISCA; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            float montantAide = 0;
            if (isNumeroMoisMontantARecupererDansPeriodeSimulation(numeroMoisPeriode)) {
                montantAide = AllocationSolidariteSpecifiqueUtile.calculerMontantSiEligible(demandeur, numeroMoisPeriode, dateDebutSimulation);
            } else {
                montantAide = AllocationSolidariteSpecifiqueUtile.getMontantASSAvantSimulation(numeroMoisPeriode, demandeur, dateDebutSimulation);
            }
            periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode -1), montantAide);
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesOpenFiscaAAH(Demandeur demandeur, LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        for (int numeroMoisPeriode = -NOMBRE_MOIS_PERIODE_OPENFISCA; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            float montantAide = 0;
            if (isNumeroMoisMontantARecupererDansPeriodeSimulation(numeroMoisPeriode)) {
                montantAide = AllocationAdultesHandicapesUtile.calculerMontant(demandeur, numeroMoisPeriode);
            } else {
                montantAide = AllocationAdultesHandicapesUtile.getMontantAAHAvantSimulation(demandeur);
            }
            periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), montantAide);
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesOpenFiscaRSA(Demandeur demandeur, LocalDate dateDebutSimulation) {
        float prochaineDeclarationRSA = RessourcesFinancieresAvantSimulationUtile.getProchaineDeclarationTrimestrielle(demandeur);
        float montantRSADemandeur = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAllocationRSA();

        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        ObjectMapper mapper = new ObjectMapper();
        for (int numeroMoisPeriode = -NOMBRE_MOIS_PERIODE_OPENFISCA; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            if (numeroMoisPeriode < prochaineDeclarationRSA - 1) {
                periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), montantRSADemandeur);
            } else {
                periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), mapper.nullNode());
            }
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesPrimeActiviteOpenFisca(Demandeur demandeur, LocalDate dateDebutSimulation) {
        int prochaineDeclarationTrimestrielle = RessourcesFinancieresAvantSimulationUtile.getProchaineDeclarationTrimestrielle(demandeur);

        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        ObjectMapper mapper = new ObjectMapper();
        for (int numeroMoisPeriode = -NOMBRE_MOIS_PERIODE_OPENFISCA; numeroMoisPeriode < NOMBRE_MOIS_SIMULATION; numeroMoisPeriode++) {
            if (numeroMoisPeriode < prochaineDeclarationTrimestrielle - 1) {
                periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode),
                        RessourcesFinancieresAvantSimulationUtile.getPrimeActivite(demandeur));
            } else {
                periodesOpenFisca.put(getPeriodeFormateePlusMonth(dateDebutSimulation, numeroMoisPeriode), mapper.nullNode());
            }
        }
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesAnnees(Object valeur, LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        int anneeDateSimulation = DateUtile.ajouterMoisALocalDate(dateDebutSimulation, NOMBRE_MOIS_SIMULATION).getYear();
        String anneeMoinsUnAnneeSimulation = String.valueOf(anneeDateSimulation - 1);
        String anneeMoinsDeuxAnneeSimulation = String.valueOf(anneeDateSimulation - 2);
        periodesOpenFisca.put(anneeMoinsUnAnneeSimulation, valeur);
        periodesOpenFisca.put(anneeMoinsDeuxAnneeSimulation, valeur);
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesChiffreAffairesMicroEntreprise(MicroEntreprise microEntreprise, LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        int anneeDateSimulation = DateUtile.enleverMoisALocalDate(dateDebutSimulation, 1).getYear();
        int anneeDateDebutSimulation = dateDebutSimulation.getYear();
        periodesOpenFisca.put(String.valueOf(anneeDateSimulation), microEntreprise.getChiffreAffairesN());
        periodesOpenFisca.put(String.valueOf(anneeDateDebutSimulation), microEntreprise.getChiffreAffairesN());
        periodesOpenFisca.put(String.valueOf(anneeDateSimulation - 1), microEntreprise.getChiffreAffairesNMoins1());
        periodesOpenFisca.put(String.valueOf(anneeDateSimulation - 2), microEntreprise.getChiffreAffairesNMoins2());
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes creerPeriodesEnfantACharge(LocalDate dateDebutSimulation) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        periodesOpenFisca.put(String.valueOf(dateDebutSimulation.getYear()), true);
        return periodesOpenFisca;
    }

    public static OpenFiscaPeriodes getPeriodeOpenfiscaCalculAideACalculer(LocalDate dateDebutSimulation, int numeroMoisSimule) {
        OpenFiscaPeriodes periodesOpenFisca = new OpenFiscaPeriodes();
        ObjectMapper mapper = new ObjectMapper();
        LocalDate dateDeclenchementCalculAide = dateDebutSimulation.plusMonths((long) numeroMoisSimule - 1);
        periodesOpenFisca.put(getPeriodeFormatee(dateDeclenchementCalculAide), mapper.nullNode());
        return periodesOpenFisca;
    }

    public static String getPeriodeFormatee(LocalDate datePeriode) {
        return datePeriode.getYear() + "-" + DateUtile.getMonthFromLocalDate(datePeriode);
    }

    public static String getPeriodeFormateeRessourceFinanciere(LocalDate dateDebutSimulation, int numeroMoisSimule, int numeroPeriodeOpenfisca) {
        LocalDate dateDeclenchementCalculAide = dateDebutSimulation.plusMonths(numeroMoisSimule);
        LocalDate dateDebutPeriodeOpenfisca = dateDeclenchementCalculAide.minusMonths(NOMBRE_MOIS_PERIODE_OPENFISCA - ((long) numeroPeriodeOpenfisca));
        return getPeriodeFormatee(dateDebutPeriodeOpenfisca);
    }

    public static String getPeriodeFormateePlusMonth(LocalDate dateDebutSimulation, int numeroPeriodeOpenfisca) {
        LocalDate dateDebutPeriodeOpenfisca = dateDebutSimulation.plusMonths(numeroPeriodeOpenfisca);
        return getPeriodeFormatee(dateDebutPeriodeOpenfisca);
    }

    public static String getPeriodeNumeroMoisSimule(LocalDate dateDebutSimulation, int numeroMoisSimule) {
        LocalDate dateDeclenchementCalculAide = dateDebutSimulation.plusMonths((long) numeroMoisSimule - 1);
        return getPeriodeFormatee(dateDeclenchementCalculAide);
    }

    /**
     * La simulation se fait sur N si le numeroMoisMontantARecuperer est < NUMERO_MOIS_PERIODE cela veut dire que l'on est sur un mois avant la simulation
     *
     * @param numeroMoisMontantARecuperer : numéro du mois pour lequel on souhaite récupérer le montant de l'aide
     * @return true si numeroMoisMontantARecuperer concerne un mois de la période de simulation false si numeroMoisMontantARecuperer concerne un mois
     * avant la période de simulation
     */
    private static boolean isNumeroMoisMontantARecupererDansPeriodeSimulation(int numeroMoisMontantARecuperer) {
        return numeroMoisMontantARecuperer >= NUMERO_MOIS_PERIODE;
    }

    /**
     * La simulation se fait sur N si le numeroMoisMontantARecuperer est < NUMERO_MOIS_PERIODE cela veut dire que l'on est sur un mois avant la simulation
     *
     * @param numeroMoisMontantARecuperer : numéro du mois pour lequel on souhaite récupérer le montant de l'aide
     * @return true si numeroMoisMontantARecuperer concerne un mois de la période de simulation false si numeroMoisMontantARecuperer concerne un mois
     * avant la période de simulation
     */
    private static boolean isNumeroMoisComplementARE(int numeroMoisMontantARecuperer) {
        return numeroMoisMontantARecuperer > NUMERO_MOIS_PERIODE + 1;
    }
}
