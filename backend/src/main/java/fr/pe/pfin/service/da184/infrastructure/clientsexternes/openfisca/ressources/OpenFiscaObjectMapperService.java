package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.InternalServerException;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.InternalServerMessages;

public class OpenFiscaObjectMapperService {

    private OpenFiscaObjectMapperService() {
    }

    private static ObjectMapper getOpenFiscaObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);

        return mapper;
    }

    public static String getJsonStringFromObject(Object object) {
        try {
            return String.format("%s", getOpenFiscaObjectMapper().writeValueAsString(object));
        } catch (JsonProcessingException e) {
            throw new InternalServerException(InternalServerMessages.SERIALIZATION_OPENFISCA_IMPOSSIBLE.getMessage(),
                    e.getCause());
        }
    }

    public static OpenFiscaRoot getOpenFiscaRootFromJsonString(String jsonString) {
        try {
            return getOpenFiscaObjectMapper().readValue(jsonString, OpenFiscaRoot.class);
        } catch (JsonProcessingException e) {
            throw new InternalServerException(InternalServerMessages.SERIALIZATION_OPENFISCA_IMPOSSIBLE.getMessage(),
                    e.getCause());
        }
    }

}
