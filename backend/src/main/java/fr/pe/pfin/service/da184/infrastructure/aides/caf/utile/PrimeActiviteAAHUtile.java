package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;

public class PrimeActiviteAAHUtile {

    private PrimeActiviteAAHUtile() {
    }

    /**
     * Fonction permettant de déterminer si le montant de la prime d'activité doit
     * être calculé ce mois-ci
     *
     * @return _________________________________________________________________________________________
     * | | | | | | | | | | Mois décla | M0 | M1 | M2 | M3 | M4 | M5 | M6 | |
     * | | | | | | | | | - M0 | (C1)/R0 | V1 | R1 | (C2)/R1 | V2 | R2 |
     * (C3)/R2 | | - M1 | R0 | (C1)/R0 | V1 | R1 | (C2)/R1 | V2 | R2 | | -
     * M2 | R0 | R0 | (C1)/R0 | V1 | R1 | (C2)/R1 | V2 | | - M3 | C0 | R0 |
     * R0 | (C1)/R0 | V1 | R1 | (C2)/R1 |
     * |____________|__________|__________|__________|__________|__________|__________|__________|
     */
    public static boolean isPrimeActiviteACalculer(int numeroMoisSimule, Demandeur demandeur) {
        int prochaineDeclarationTrimestrielle = RessourcesFinancieresAvantSimulationUtile
                .getProchaineDeclarationTrimestrielle(demandeur);
        return PrimeActiviteUtile.isPrimeActiviteACalculerDeclarationTrimestrielle(numeroMoisSimule,
                prochaineDeclarationTrimestrielle);

    }

    /**
     * Fonction permettant de déterminer si on a calculé le(s) montant(s) de la
     * prime d'activité et/ou du RSA le mois précédent et s'il(s) doi(ven)t être
     * versé(s) ce mois-ci
     *
     * @return _________________________________________________________________________________________
     * | | | | | | | | | | Mois décla | M0 | M1 | M2 | M3 | M4 | M5 | M6 | |
     * | | | | | | | | | - M0 | C1/R0 | (V1) | R1 | C2/R1 | (V2) | R2 |
     * C3/R2 | | - M1 | R0 | C1/R0 | (V1) | R1 | C2/R1 | (V2) | R2 | | - M2
     * | R0 | R0 | C1/R0 | (V1) | R1 | C2/R1 | (V2) | | - M3 | C0 | (R0) |
     * R0 | C1/R0 | (V1) | R1 | C2/R1 |
     * |____________|__________|__________|__________|__________|__________|__________|__________|
     */
    public static boolean isPrimeActiviteAVerser(int numeroMoisSimule, Demandeur demandeur) {
        int prochaineDeclarationTrimestrielle = RessourcesFinancieresAvantSimulationUtile
                .getProchaineDeclarationTrimestrielle(demandeur);
        return PrimeActiviteUtile.isPrimeActiviteAVerserDeclarationTrimestrielle(numeroMoisSimule,
                prochaineDeclarationTrimestrielle);
    }
}
