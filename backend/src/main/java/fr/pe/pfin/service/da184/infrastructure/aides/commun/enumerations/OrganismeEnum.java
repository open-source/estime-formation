package fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations;

public enum OrganismeEnum {

	CAF("CAF", "Caisse d'allocation familiales"),
	FT("FT", "France travail"),
	CPAM("CPAM", "Caisse Primaire d'Assurance Maladie");

	private final String nomCourt;
	private final String nom;

	OrganismeEnum(String nomCourt, String nom) {
		this.nomCourt = nomCourt;
		this.nom = nom;
	}

	public String getNomCourt() {
		return nomCourt;
	}

	public String getNom() {
		return nom;
	}
}
