package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.caf.SimulateurAidesCAF;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AgeUtile;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


public class SituationFamilialeUtile {

    static final int MOIS_LIMITE_3_ANS = 37;

    private SituationFamilialeUtile() {
    }

    public static boolean isEnCouple(Demandeur demandeur) {
        return Objects.nonNull(demandeur.getSituationFamiliale()) && demandeur.getSituationFamiliale().getIsEnCouple() != null
                && demandeur.getSituationFamiliale().getIsEnCouple().booleanValue();
    }

    public static boolean hasPersonnesACharge(Demandeur demandeur) {
        return demandeur.getSituationFamiliale() != null
                && demandeur.getSituationFamiliale().getPersonnesACharge() != null
                && !demandeur.getSituationFamiliale().getPersonnesACharge().isEmpty();
    }

    public static boolean hasPersonnesAChargeEligiblesPourPriseEnComptePrimeActivite(Demandeur demandeur) {
        return getNombrePersonnesAChargeAgeInferieureAgeLimite(demandeur,
                SimulateurAidesCAF.AGE_MAX_PERSONNE_A_CHARGE_PPA_RSA) > 0;
    }

    public static int getNombrePersonnesAChargeAgeInferieureAgeLimite(Demandeur demandeur, int ageLimite) {
        if (hasPersonnesACharge(demandeur)) {
            return (int) demandeur.getSituationFamiliale().getPersonnesACharge().stream()
                    .filter(personneACharge -> AgeUtile.calculerAge(personneACharge) < ageLimite).count();
        }
        return 0;
    }

    public static Optional<List<Personne>> getPersonnesAChargeAgeInferieurAgeLimite(Demandeur demandeur) {
        if (hasPersonnesACharge(demandeur)) {
            return Optional.of(demandeur.getSituationFamiliale().getPersonnesACharge().stream().toList());
        }
        return Optional.empty();
    }

    public static Optional<List<Personne>> getPersonnesAChargeAgeInferieur37MoisLimitePourMoisSimule(
            Demandeur demandeur, int numeroMoisSimule) {
        if (hasPersonnesACharge(demandeur)) {
            return Optional.of(demandeur
                    .getSituationFamiliale().getPersonnesACharge().stream().filter(personneACharge -> AgeUtile
                            .calculerAgeMoisMoisSimule(personneACharge, numeroMoisSimule) < MOIS_LIMITE_3_ANS)
                    .toList());
        }
        return Optional.empty();
    }

    public static boolean isSeulPlusDe18Mois(Demandeur demandeur) {
        return demandeur.getSituationFamiliale().getIsSeulPlusDe18Mois() != null
                && demandeur.getSituationFamiliale().getIsSeulPlusDe18Mois().booleanValue();
    }

    public static boolean hasEnfantMoinsDe3AnsPourMoisSimule(Demandeur demandeur, int numeroMoisSimule) {
        if (hasPersonnesACharge(demandeur)) {
            Optional<List<Personne>> personnesACharge = getPersonnesAChargeAgeInferieur37MoisLimitePourMoisSimule(
                    demandeur, numeroMoisSimule);
            return !personnesACharge.orElse(Collections.emptyList()).isEmpty();
        }
        return false;
    }
}
