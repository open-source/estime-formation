package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;


public class PrimeActiviteUtile {

    private PrimeActiviteUtile() {
    }

    public static void reporterPrimeActivite(Simulation simulation, Map<String, Aide> aidesPourCeMois, Demandeur demandeur, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        Optional<Aide> primeActiviteMoisPrecedent = getPrimeActiviteMoisPrecedent(simulation, numeroMoisSimule);
        if (primeActiviteMoisPrecedent.isPresent()) {
            aidesPourCeMois.put(AideEnum.PRIME_ACTIVITE.getCode(), primeActiviteMoisPrecedent.get());
        } else if (isEligiblePourReportPrimeActiviteDeclare(numeroMoisSimule, demandeur)) {
            aidesPourCeMois.put(AideEnum.PRIME_ACTIVITE.getCode(), getPrimeActiviteDeclare(demandeur, dateDebutSimulation, numeroMoisSimule));
        }
    }

    public static Aide creerAidePrimeActivite(float montantPrimeActivite, boolean isAideReportee, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        ArrayList<String> messagesAlerte = new ArrayList<>();
        messagesAlerte.add(getMessageAlerteDemandePPA(dateDebutSimulation, numeroMoisSimule));
        return AideUtile.creerAide(AideEnum.PRIME_ACTIVITE, Optional.of(OrganismeEnum.CAF), Optional.of(messagesAlerte), isAideReportee, montantPrimeActivite);
    }

    public static Optional<Aide> getPrimeActiviteMoisPrecedent(Simulation simulation, int numeroMoisSimule) {
        int moisNMoins1 = numeroMoisSimule - 1;
        return AideUtile.getAidePourCeMoisSimule(simulation, AideEnum.PRIME_ACTIVITE.getCode(), moisNMoins1);
    }

    private static String getMessageAlerteDemandePPA(LocalDate dateDebutSimulation, int numeroMoisSimule) {
        LocalDate dateMoisMMoins1 = DateUtile.ajouterMoisALocalDate(dateDebutSimulation, numeroMoisSimule - 2);
        String libelleMoisMMoins1 = DateUtile.getMonthNameFromLocalDate(dateMoisMMoins1);

        if (libelleMoisMMoins1.equals("avril") || libelleMoisMMoins1.equals("août") || libelleMoisMMoins1.equals("octobre")) {
            return String.format(MessageInformatifEnum.DEMANDE_PPA.getMessage(), "'" + libelleMoisMMoins1);
        } else {
            return String.format(MessageInformatifEnum.DEMANDE_PPA.getMessage(), "e " + libelleMoisMMoins1);
        }
    }

    /**
     * Fonction permettant de déterminer si le montant de la prime d'activité doit être calculé ce mois-ci
     *
     * @return _________________________________________________________________________________________
     * |            |          |          |          |          |          |          |          |
     * | Mois décla |    M0    |    M1    |    M2    |    M3    |    M4    |    M5    |    M6    |
     * |            |          |          |          |          |          |          |          |
     * |  -  M0     | (C1)/R0  |    V1    |    R1    | (C2)/R1  |    V2    |    R2    | (C3)/R2  |
     * |  -  M1     |    R0    | (C1)/R0  |    V1    |    R1    | (C2)/R1  |    V2    |    R2    |
     * |  -  M2     |    R0    |    R0    |  (C1)/R0 |    V1    |    R1    | (C2)/R1  |    V2    |
     * |  -  M3     |    C0    |    R0    |    R0    | (C1)/R0  |    V1    |    R1    | (C2)/R1  |
     * |____________|__________|__________|__________|__________|__________|__________|__________|
     */
    public static boolean isPrimeActiviteACalculerDeclarationTrimestrielle(int numeroMoisSimule, int prochaineDeclarationTrimestrielle) {
        return (((prochaineDeclarationTrimestrielle == 0) && (numeroMoisSimule == 3))
                || ((prochaineDeclarationTrimestrielle == 1) && (numeroMoisSimule == 1 || numeroMoisSimule == 4))
                || ((prochaineDeclarationTrimestrielle == 2) && (numeroMoisSimule == 2 || numeroMoisSimule == 5))
                || ((prochaineDeclarationTrimestrielle == 3) && (numeroMoisSimule == 3)));
    }

    /**
     * Fonction permettant de déterminer si on a calculé le(s) montant(s) de la prime d'activité et/ou du RSA le mois précédent et s'il(s) doi(ven)t être versé(s) ce mois-ci
     *
     * @return _________________________________________________________________________________________
     * |            |          |          |          |          |          |          |          |
     * | Mois décla |    M0    |    M1    |    M2    |    M3    |    M4    |    M5    |    M6    |
     * |            |          |          |          |          |          |          |          |
     * |  -  M0     |  C1/R0   |   (V1)   |    R1    |  C2/R1   |   (V2)   |    R2    |  C3/R2   |
     * |  -  M1     |    R0    |  C1/R0   |   (V1)   |    R1    |  C2/R1   |   (V2)   |    R2    |
     * |  -  M2     |    R0    |    R0    |   C1/R0  |   (V1)   |    R1    |  C2/R1   |   (V2)   |
     * |  -  M3     |    C0    |   (R0)   |    R0    |  C1/R0   |   (V1)   |    R1    |  C2/R1   |
     * |____________|__________|__________|__________|__________|__________|__________|__________|
     */
    public static boolean isPrimeActiviteAVerserDeclarationTrimestrielle(int numeroMoisSimule, int prochaineDeclarationTrimestrielle) {
        return (((prochaineDeclarationTrimestrielle == 0) && (numeroMoisSimule == 1 || numeroMoisSimule == 4))
                || ((prochaineDeclarationTrimestrielle == 1) && (numeroMoisSimule == 2 || numeroMoisSimule == 5))
                || ((prochaineDeclarationTrimestrielle == 2) && (numeroMoisSimule == 3 || numeroMoisSimule == 6))
                || ((prochaineDeclarationTrimestrielle == 3) && (numeroMoisSimule == 4)));
    }

    private static Aide getPrimeActiviteDeclare(Demandeur demandeur, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        float montantDeclare = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getPrimeActivite();
        return creerAidePrimeActivite(montantDeclare, true, dateDebutSimulation, numeroMoisSimule);
    }

    public static boolean isEligiblePourReportPrimeActiviteDeclare(int numeroMoisSimule, Demandeur demandeur) {
        boolean isEligiblePourReportPrimeActiviteDeclare = false;
        if (RessourcesFinancieresAvantSimulationUtile.hasPrimeActivite(demandeur)) {
            int prochaineDeclarationTrimestrielle = RessourcesFinancieresAvantSimulationUtile.getProchaineDeclarationTrimestrielle(demandeur);
            isEligiblePourReportPrimeActiviteDeclare = prochaineDeclarationTrimestrielle != 0 && ((prochaineDeclarationTrimestrielle == 3) && numeroMoisSimule <= 1)
                    || (prochaineDeclarationTrimestrielle == 1 && numeroMoisSimule <= 2) || (prochaineDeclarationTrimestrielle == 2 && numeroMoisSimule <= 3);

        }
        return isEligiblePourReportPrimeActiviteDeclare;
    }
}
