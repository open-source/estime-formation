package fr.pe.pfin.service.da184.application.ui.rest.ressource;

import jakarta.validation.constraints.NotNull;

public class FutureFormation {

    private float dureeFormationEnMois;
    private float dureeFormationHebdomadaire;
    private boolean formationValideeFT;
    private boolean formationFinanceeCofinancee;
    private float distanceActiviteDomicile;
    private int nombreTrajetsDomicileTravail;
    @NotNull
    private Salaire salaire;

    public float getDureeFormationEnMois() {
        return dureeFormationEnMois;
    }

    public void setDureeFormationEnMois(float dureeFormationEnMois) {
        this.dureeFormationEnMois = dureeFormationEnMois;
    }

    public float getDureeFormationHebdomadaire() {
        return dureeFormationHebdomadaire;
    }

    public void setDureeFormationHebdomadaire(float dureeFormationHebdomadaire) {
        this.dureeFormationHebdomadaire = dureeFormationHebdomadaire;
    }

    public boolean isFormationValideeFT() {
        return formationValideeFT;
    }

    public void setFormationValideeFT(boolean formationValideeFT) {
        this.formationValideeFT = formationValideeFT;
    }

    public boolean isFormationFinanceeCofinancee() {
        return formationFinanceeCofinancee;
    }

    public void setFormationFinanceeCofinancee(boolean formationFinanceeCofinancee) {
        this.formationFinanceeCofinancee = formationFinanceeCofinancee;
    }

    public float getDistanceActiviteDomicile() {
        return distanceActiviteDomicile;
    }

    public void setDistanceActiviteDomicile(float distanceActiviteDomicile) {
        this.distanceActiviteDomicile = distanceActiviteDomicile;
    }

    public Salaire getSalaire() {
        return salaire;
    }

    public void setSalaire(Salaire salaire) {
        this.salaire = salaire;
    }

    public int getNombreTrajetsDomicileTravail() {
        return nombreTrajetsDomicileTravail;
    }

    public void setNombreTrajetsDomicileTravail(int nombreTrajetsDomicileTravail) {
        this.nombreTrajetsDomicileTravail = nombreTrajetsDomicileTravail;
    }

    @Override
    public String toString() {
        return "FutureFormation [dureeFormation=" + dureeFormationEnMois
                + ", formationValideeFT=" + formationValideeFT + ", formationFinanceeCofinancee="
                + formationFinanceeCofinancee + ", distanceActiviteDomicile=" + distanceActiviteDomicile + ", salaire="
                + salaire +", nombreTrajetsDomicileTravail=" + nombreTrajetsDomicileTravail + "]";
    }
}
