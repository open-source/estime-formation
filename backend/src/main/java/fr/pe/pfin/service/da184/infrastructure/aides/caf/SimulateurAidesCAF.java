package fr.pe.pfin.service.da184.infrastructure.aides.caf;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.caf.utile.AidesFamilialesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.caf.utile.AllocationAdultesHandicapesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.BeneficiaireAidesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.InformationsPersonnellesUtile;

import java.util.Map;


public class SimulateurAidesCAF {

    // Pour la prime d'activité et le RSA , le majeur âgé de plus de 25 ans ne peut
    // pas être considéré comme à charge
    public static final int AGE_MAX_PERSONNE_A_CHARGE_PPA_RSA = 25;

    private SimulateurAidesCAF() {
    }

    public static void simuler(Map<String, Aide> aidesPourCeMois, int numeroMoisSimule, Demandeur demandeur) {
        if (isEligibleAidesCAF(demandeur)) {
            if (BeneficiaireAidesUtile.isBeneficiaireAAH(demandeur)) {
                AllocationAdultesHandicapesUtile.simulerAide(aidesPourCeMois, numeroMoisSimule, demandeur);
            }
            if (AidesFamilialesUtile.isEligibleAidesFamiliales(demandeur, numeroMoisSimule)) {
                AidesFamilialesUtile.simulerAidesFamiliales(aidesPourCeMois, demandeur, numeroMoisSimule);
            }
        }
    }

    private static boolean isEligibleAidesCAF(Demandeur demandeur) {
        return InformationsPersonnellesUtile.isFrancais(demandeur.getInformationsPersonnelles())
                || InformationsPersonnellesUtile.isEuropeenOuSuisse(demandeur.getInformationsPersonnelles())
                || (InformationsPersonnellesUtile
                .isNotFrancaisOuEuropeenOuSuisse(demandeur.getInformationsPersonnelles())
                && InformationsPersonnellesUtile
                .hasTitreSejourEnFranceValide(demandeur.getInformationsPersonnelles()));
    }
}
