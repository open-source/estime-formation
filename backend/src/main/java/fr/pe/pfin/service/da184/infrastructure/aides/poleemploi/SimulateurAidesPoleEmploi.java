package fr.pe.pfin.service.da184.infrastructure.aides.poleemploi;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.BeneficiaireAidesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile.AllocationSolidariteSpecifiqueUtile;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

public class SimulateurAidesPoleEmploi {

    private SimulateurAidesPoleEmploi() {
    }

    public static void simuler(Map<String, Aide> aidesPourCeMois, int numeroMoisSimule, Demandeur demandeur,
                               LocalDate dateDebutSimulation) {

        if (BeneficiaireAidesUtile.isBeneficiaireASS(demandeur) && AllocationSolidariteSpecifiqueUtile
                .isEligible(numeroMoisSimule, demandeur, dateDebutSimulation)) {
            Optional<Aide> aideOptional = AllocationSolidariteSpecifiqueUtile.simulerAide(demandeur,
                    numeroMoisSimule, dateDebutSimulation);
            aideOptional.ifPresent(aide -> aidesPourCeMois.put(AideEnum.ALLOCATION_SOLIDARITE_SPECIFIQUE.getCode(), aide));
        }
    }
}
