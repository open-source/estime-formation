package fr.pe.pfin.service.da184.infrastructure;

import fr.pe.pfin.service.da184.domaine.use_cases.DonneesBRSAPort;
import fr.pe.pfin.service.da184.infrastructure.database.redis.models.BRSAData;
import fr.pe.pfin.service.da184.infrastructure.database.redis.repositories.BRSADataRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DonneesBRSAAdapter implements DonneesBRSAPort {
    private final BRSADataRepository brsaDataRepository;

    public DonneesBRSAAdapter(BRSADataRepository brsaDataRepository) {
        this.brsaDataRepository = brsaDataRepository;
    }

    @Override
    public InputStream extraireDonneesBRSA(LocalDate dateDebut, LocalDate dateFin) {
        List<BRSAData> brsaDataList = this.brsaDataRepository.findAll();
        return new ByteArrayInputStream(brsaDataList.stream().filter(brsaData ->
                brsaData.getDateSimulation().isAfter(dateDebut.atStartOfDay()) && brsaData.getDateSimulation().isBefore(dateFin.atStartOfDay().plusDays(1).minusSeconds(1))
        ).map(BRSAData::toCSV).collect(Collectors.joining("\n")).getBytes(StandardCharsets.UTF_8));
    }
    @Override
    public HttpStatus addNotation(String id, Integer notation){
        Optional<BRSAData> simu = this.brsaDataRepository.findById(id);
        if (simu.isPresent()){
            simu.get().setNotation(notation);
            brsaDataRepository.save(simu.get());

            return HttpStatus.OK;
        }
        return HttpStatus.NOT_FOUND;
    }

}
