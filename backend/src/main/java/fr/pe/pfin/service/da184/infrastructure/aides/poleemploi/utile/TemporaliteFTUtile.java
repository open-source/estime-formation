package fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.SimulateurAidesUtile;

public class TemporaliteFTUtile {

    private TemporaliteFTUtile() {
    }

    public static boolean isAREAReporter(Demandeur demandeur, int numeroMoisSimule) {
        return AreUtile.isAreAReporter(demandeur, numeroMoisSimule);
    }

    public static boolean isComplementAREAVerser(Demandeur demandeur, int numeroMoisSimule) {
        return AreUtile.isComplementAREAVerser(demandeur, numeroMoisSimule);
    }

    public static boolean isAgepiAVerser(int numeroMoisSimule) {
        return SimulateurAidesUtile.isSecondMois(numeroMoisSimule);
    }

    public static boolean isAideMobiliteAVerser(int numeroMoisSimule) {
        return SimulateurAidesUtile.isCinqDerniersMois(numeroMoisSimule);
    }
}
