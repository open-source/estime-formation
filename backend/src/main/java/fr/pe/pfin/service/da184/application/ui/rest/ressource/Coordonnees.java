package fr.pe.pfin.service.da184.application.ui.rest.ressource;

public class Coordonnees {

	private String codeInsee;
	private String codePostal;

	public String getCodeInsee() {
		return codeInsee;
	}

	public void setCodeInsee(String codeInsee) {
		this.codeInsee = codeInsee;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	@Override
	public String toString() {
		return "Coordonnees [codeInsee=" + codeInsee + ", codePostal=" + codePostal + "]";
	}
}
