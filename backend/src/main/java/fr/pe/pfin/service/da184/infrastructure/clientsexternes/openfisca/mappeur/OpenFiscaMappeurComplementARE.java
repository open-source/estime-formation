package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.AllocationARE;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile.AreUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

import static fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.ParametresOpenFisca.DEMANDEUR;

public class OpenFiscaMappeurComplementARE {

    private OpenFiscaMappeurComplementARE() {
    }

    public static float getMontantComplementAREBrut(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getIndividus() != null && openFiscaRoot.getIndividus().get(DEMANDEUR) != null) {
            OpenFiscaPeriodes openFiscaComplementARE = openFiscaRoot.getIndividus().get(DEMANDEUR).getComplementAREBrut();
            String periodeFormateeComplementARE = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaComplementARE != null && openFiscaComplementARE.get(periodeFormateeComplementARE) != null) {
                Double montantComplementARE = (Double) openFiscaComplementARE.get(periodeFormateeComplementARE);
                return BigDecimal.valueOf(montantComplementARE).setScale(0, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }

    public static float getMontantDeductionsComplementARE(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getIndividus() != null && openFiscaRoot.getIndividus().get(DEMANDEUR) != null) {
            OpenFiscaPeriodes openFiscaDeductionsComplementARE = openFiscaRoot.getIndividus().get(DEMANDEUR).getDeductionsComplementARE();
            String periodeFormateeComplementARE = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaDeductionsComplementARE != null && openFiscaDeductionsComplementARE.get(periodeFormateeComplementARE) != null) {
                Double deductionsMensuelles = (Double) openFiscaDeductionsComplementARE.get(periodeFormateeComplementARE);
                return BigDecimal.valueOf(deductionsMensuelles).setScale(0, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }

    public static float getMontantCRCComplementARE(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getIndividus() != null && openFiscaRoot.getIndividus().get(DEMANDEUR) != null) {
            OpenFiscaPeriodes openFiscaMontantCRCComplementARE = openFiscaRoot.getIndividus().get(DEMANDEUR).getMontantCRCComplementARE();
            String periodeFormateeComplementARE = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaMontantCRCComplementARE != null && openFiscaMontantCRCComplementARE.get(periodeFormateeComplementARE) != null) {
                Double deductionsMensuelles = (Double) openFiscaMontantCRCComplementARE.get(periodeFormateeComplementARE);
                return BigDecimal.valueOf(deductionsMensuelles).abs().setScale(2, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }

    public static float getMontantCRDSComplementARE(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getIndividus() != null && openFiscaRoot.getIndividus().get(DEMANDEUR) != null) {
            OpenFiscaPeriodes openFiscaMontantCRDSComplementARE = openFiscaRoot.getIndividus().get(DEMANDEUR).getMontantCRDSComplementARE();
            String periodeFormateeComplementARE = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaMontantCRDSComplementARE != null && openFiscaMontantCRDSComplementARE.get(periodeFormateeComplementARE) != null) {
                Double deductionsMensuelles = (Double) openFiscaMontantCRDSComplementARE.get(periodeFormateeComplementARE);
                return BigDecimal.valueOf(deductionsMensuelles).abs().setScale(2, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }

    public static float getMontantCSGComplementARE(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getIndividus() != null && openFiscaRoot.getIndividus().get(DEMANDEUR) != null) {
            OpenFiscaPeriodes openFiscaMontantCSGComplementARE = openFiscaRoot.getIndividus().get(DEMANDEUR).getMontantCSGComplementARE();
            String periodeFormateeComplementARE = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaMontantCSGComplementARE != null && openFiscaMontantCSGComplementARE.get(periodeFormateeComplementARE) != null) {
                Double deductionsMensuelles = (Double) openFiscaMontantCSGComplementARE.get(periodeFormateeComplementARE);
                return BigDecimal.valueOf(deductionsMensuelles).abs().setScale(2, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }

    public static float getMontantComplementARENet(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getIndividus() != null && openFiscaRoot.getIndividus().get(DEMANDEUR) != null) {
            OpenFiscaPeriodes openFiscaComplementARE = openFiscaRoot.getIndividus().get(DEMANDEUR).getComplementARENet();
            String periodeFormateeComplementARE = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaComplementARE != null && openFiscaComplementARE.get(periodeFormateeComplementARE) != null) {
                Double montantComplementARE = (Double) openFiscaComplementARE.get(periodeFormateeComplementARE);
                return BigDecimal.valueOf(montantComplementARE).setScale(0, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }

    public static float getNombreJoursIndemnisesComplementARE(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getIndividus() != null && openFiscaRoot.getIndividus().get(DEMANDEUR) != null) {
            OpenFiscaPeriodes openFiscaNombreJoursIndemnisesComplementARE = openFiscaRoot.getIndividus().get(DEMANDEUR).getNombreJoursIndemnisesComplementARE();
            String periodeFormateeComplementARE = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaNombreJoursIndemnisesComplementARE != null && openFiscaNombreJoursIndemnisesComplementARE.get(periodeFormateeComplementARE) != null) {
                Double nombresJoursIndemnises = (Double) openFiscaNombreJoursIndemnisesComplementARE.get(periodeFormateeComplementARE);
                return BigDecimal.valueOf(nombresJoursIndemnises).setScale(0, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }

    public static float getNombreJoursRestantsARE(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getIndividus() != null && openFiscaRoot.getIndividus().get(DEMANDEUR) != null) {
            OpenFiscaPeriodes openFiscaNombreJoursRestantsARE = openFiscaRoot.getIndividus().get(DEMANDEUR).getNombreJoursRestantsARE();
            String periodeFormateeComplementARE = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaNombreJoursRestantsARE != null && openFiscaNombreJoursRestantsARE.get(periodeFormateeComplementARE) != null) {
                Double nombreJoursRestantsARE = (Double) openFiscaNombreJoursRestantsARE.get(periodeFormateeComplementARE);
                return BigDecimal.valueOf(nombreJoursRestantsARE).setScale(0, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }

    public static void addComplementAREOpenFiscaIndividu(OpenFiscaIndividu openFiscaIndividu, Demandeur demandeur, LocalDate dateDebutSimulation) {

        AllocationARE allocationARE = demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE();

        openFiscaIndividu.setDegressiviteAre(OpenFiscaMappeurPeriode
                .creerPeriodesOpenFisca(allocationARE.getHasDegressiviteAre(), dateDebutSimulation));

        openFiscaIndividu.setAllocationJournaliere(OpenFiscaMappeurPeriode
                .creerPeriodesOpenFisca(allocationARE.getAllocationJournaliereBrute(), dateDebutSimulation));

        openFiscaIndividu.setAllocationJournaliereTauxPlein(OpenFiscaMappeurPeriode
                .creerPeriodesOpenFisca(allocationARE.getAllocationJournaliereBruteTauxPlein(), dateDebutSimulation));

        openFiscaIndividu.setSalaireJournalierReference(OpenFiscaMappeurPeriode
                .creerPeriodesOpenFisca(allocationARE.getSalaireJournalierReferenceBrut(), dateDebutSimulation));

        openFiscaIndividu.setNombreJoursRestantsARE(OpenFiscaMappeurPeriode.creerPeriodeUniqueAREOpenFisca(
                demandeur,
                AreUtile.getNombreJoursRestantsApresPremierMois(demandeur, dateDebutSimulation),
                dateDebutSimulation));

        openFiscaIndividu.setNombreJoursIndemnisesComplementARE(
                OpenFiscaMappeurPeriode.creerPeriodesCalculeesAREOpenFisca(demandeur, dateDebutSimulation));

        openFiscaIndividu.setComplementAREBrut(
                OpenFiscaMappeurPeriode.creerPeriodesCalculeesAREOpenFisca(demandeur, dateDebutSimulation));

        openFiscaIndividu.setDeductionsComplementARE(
                OpenFiscaMappeurPeriode.creerPeriodesCalculeesAREOpenFisca(demandeur, dateDebutSimulation));

        openFiscaIndividu.setMontantCRCComplementARE(
                OpenFiscaMappeurPeriode.creerPeriodesCalculeesAREOpenFisca(demandeur, dateDebutSimulation));

        openFiscaIndividu.setMontantCRDSComplementARE(
                OpenFiscaMappeurPeriode.creerPeriodesCalculeesAREOpenFisca(demandeur, dateDebutSimulation));

        openFiscaIndividu.setMontantCSGComplementARE(
                OpenFiscaMappeurPeriode.creerPeriodesCalculeesAREOpenFisca(demandeur, dateDebutSimulation));

        openFiscaIndividu.setComplementARENet(
                OpenFiscaMappeurPeriode.creerPeriodesCalculeesAREOpenFisca(demandeur, dateDebutSimulation));
    }

    public static void addAREAvantSimulationOpenFiscaIndividu(OpenFiscaIndividu openFiscaIndividu, Demandeur demandeur, LocalDate dateDebutSimulation) {
        openFiscaIndividu.setARE(OpenFiscaMappeurPeriode.creerPeriodesAREAvantSimulation(demandeur, dateDebutSimulation));
    }
}
