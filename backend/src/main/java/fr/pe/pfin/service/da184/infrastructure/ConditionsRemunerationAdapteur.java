package fr.pe.pfin.service.da184.infrastructure;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.ConditionsRemunerations;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Critere;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.CritereConditionRemuneration;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SituationRemuneration;
import fr.pe.pfin.service.da184.domaine.use_cases.ConditionsRemunerationPort;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConditionsRemunerationAdapteur implements ConditionsRemunerationPort {
   private JsonParser jsonParser;
   private final ObjectMapper jsonObjectMapper = new ObjectMapper();
   private final Map<String, List<CritereConditionRemuneration>> inMemoryRegionsCrits = new HashMap<>();
   private static final String FOLDER_JSON_NAME = "/conditionsRemuneration/";
   private static final String DEFAULT_REGION = "0";

   private static final Logger log = LoggerFactory.getLogger(ConditionsRemunerationAdapteur.class);

   public ConditionsRemunerationAdapteur() {
      initAllReferences();
   }

   @Override
   public List<ConditionsRemunerations> getConditionsList(SituationRemuneration situationRem) {
      List<ConditionsRemunerations> condsRemsList = new ArrayList<>();
      List<CritereConditionRemuneration> regionList = inMemoryRegionsCrits.getOrDefault(situationRem.getRegion(), inMemoryRegionsCrits.get(DEFAULT_REGION));
      if (regionList != null) {
         condsRemsList = retrieveConditionRemuneration(regionList, situationRem.getAge(), situationRem.getNbPersACharge());
      }
      return condsRemsList;
   }

   private List<ConditionsRemunerations> retrieveConditionRemuneration(List<CritereConditionRemuneration> list, Float valeurAgeRecuDuSimul, Float valeurNbEnfantRecuDuSimul) {
      //construction de la map avec les valeurs afin de pouvoir faire les controles de condition.
      List<ConditionsRemunerations> toReturn = new ArrayList<>();
      Map<String, Float> valsFromSimul = new HashMap<>();
      valsFromSimul.put("age", valeurAgeRecuDuSimul);
      valsFromSimul.put("nbPersACharge", valeurNbEnfantRecuDuSimul);

      CritereConditionRemuneration condFind = null;
      for (CritereConditionRemuneration cc : list) {
         if (cc.check(valsFromSimul)) {
            condFind = cc;
            break;
         }
      }
      //condFind contient le CritCond dont tout les critère DE sont validées on a donc la liste des condition à afficher et les rémunérations associées
      if (condFind != null) {
         toReturn.addAll(condFind.getConditionsRemunerationsList());
      }
      return toReturn;
   }

   private List<Path> getConditionsFile() {
      List<Path> toReturn = new ArrayList<>();
      URL resURL = ConditionsRemunerationAdapteur.class.getResource(FOLDER_JSON_NAME);
      try {
         if (resURL != null) {
            URI resURI = resURL.toURI();

            Path jsonPath;
            if (resURI != null && resURI.getScheme() != null && resURI.getScheme().equals("jar")) {
               try (FileSystem fileSystem = FileSystems.newFileSystem(resURI, Collections.emptyMap())) {
                  jsonPath = fileSystem.getPath(FOLDER_JSON_NAME);
               }
            } else {
               assert resURI != null;
               jsonPath = Paths.get(resURI);
            }

            try (Stream<Path> strWalk = Files.walk(jsonPath, 1)) {
               for (Iterator<Path> it = strWalk.iterator(); it.hasNext();) {
                  Path filepath = it.next();
                  if (filepath.toString().endsWith(".json")) {
                     toReturn.add(filepath);
                  }
               }
            }
         }
      } catch (IOException | URISyntaxException ex) {
         //Pas de fichier présent / pb chargement.
         log.warn("Problème sur le chargement des fichiers json de condition de rémunération de références");
      }
      return toReturn;
   }

   private void initAllReferences() {
      List<Path> allResFiles = getConditionsFile();
      if (!allResFiles.isEmpty()) {
         allResFiles.forEach(path -> {
            try {
               InputStream jsonInputStream = ConditionsRemunerationAdapteur.class.getResourceAsStream(FOLDER_JSON_NAME + path.getFileName());
               jsonParser = new JsonFactory().createParser(jsonInputStream);
               readAJson();
            } catch (IOException ex) {
               log.warn(String.format("Problème à la lecture du fichier json : %s%s", FOLDER_JSON_NAME, path.getFileName()));
            }
         });
      }
   }

   private void readAJson() throws IOException {
      // Récupérer le nœud racine du JSON
      JsonNode jsonRoot = jsonObjectMapper.readTree(jsonParser);
      JsonNode allCritConds = jsonRoot.get("criteres_conditions");
      List<CritereConditionRemuneration> critCondRemList = new ArrayList<>();
      for (JsonNode critCondNod : allCritConds) {
         List<Critere> crits = new ArrayList<>();
         for (JsonNode critNod : critCondNod.get("criteres")) {
            String critName = critNod.get("name").asText();
            Float[] critRange = new Float[2];
            int i = 0;
            for (JsonNode rangeValNod : critNod.get("range")) {
               critRange[i] = (float) rangeValNod.asDouble();
               i++;
            }
            crits.add(new Critere(critName, critRange));
         }
         List<ConditionsRemunerations> condsRems = new ArrayList<>();
         for (JsonNode condRemNod : critCondNod.get("conditions_remunerations")) {
            String cond = condRemNod.get("condition").asText();
            Integer rem = condRemNod.get("remuneration").asInt();
            condsRems.add(new ConditionsRemunerations(cond, rem));
         }
         critCondRemList.add(new CritereConditionRemuneration(crits, condsRems));
      }
      inMemoryRegionsCrits.put(jsonRoot.get("codeRegion").asText(), critCondRemList);
   }
}