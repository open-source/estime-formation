package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;


public class PersonneUtile {

    private PersonneUtile() {
    }

    public static boolean hasAllocationARE(Personne personne) {
        return BeneficiaireAidesUtile.isBeneficiaireARE(personne) && personne.getRessourcesFinancieres() != null
                && personne.getRessourcesFinancieres().getAidesPoleEmploi() != null
                && personne.getRessourcesFinancieres().getAidesPoleEmploi().getAllocationARE() != null
                && personne.getRessourcesFinancieres().getAidesPoleEmploi().getAllocationARE()
                .getAllocationMensuelleNet() != null
                && personne.getRessourcesFinancieres().getAidesPoleEmploi().getAllocationARE()
                .getAllocationMensuelleNet() > 0;
    }

    public static boolean hasAllocationASS(Personne personne) {
        return BeneficiaireAidesUtile.isBeneficiaireASS(personne) && personne.getRessourcesFinancieres() != null
                && personne.getRessourcesFinancieres().getAidesPoleEmploi() != null
                && personne.getRessourcesFinancieres().getAidesPoleEmploi().getAllocationASS() != null
                && personne.getRessourcesFinancieres().getAidesPoleEmploi().getAllocationASS()
                .getAllocationMensuelleNet() != null
                && personne.getRessourcesFinancieres().getAidesPoleEmploi().getAllocationASS()
                .getAllocationMensuelleNet() > 0;
    }
}
