package fr.pe.pfin.service.da184.infrastructure.database.redis.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.RessourcesFinancieresAvantSimulation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.database.redis.models.BRSAData;
import fr.pe.pfin.service.da184.infrastructure.database.redis.repositories.BRSADataRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class RedisUtile {
    private final BRSADataRepository brsaDataRepository;

    public RedisUtile(BRSADataRepository brsaDataRepository) {
        this.brsaDataRepository = brsaDataRepository;
    }

    public List<String> getAllocations(RessourcesFinancieresAvantSimulation ressourcesFinancieresAvantSimulation) {
        List<String> acronyme = new ArrayList<>();
        if (ressourcesFinancieresAvantSimulation.getAidesCAF() != null) {
            if (ressourcesFinancieresAvantSimulation.getAidesCAF().getAllocationRSA() != null) {
                acronyme.add(ressourcesFinancieresAvantSimulation.getAidesCAF().getAllocationRSA() != 0 ? AideEnum.RSA.getCode() : "");
            }
            if (ressourcesFinancieresAvantSimulation.getAidesCAF().getAllocationAAH() != null) {
                acronyme.add(ressourcesFinancieresAvantSimulation.getAidesCAF().getAllocationAAH() != 0 ? AideEnum.ALLOCATION_ADULTES_HANDICAPES.getCode() : "");
            }
        }
        if (ressourcesFinancieresAvantSimulation.getAidesCPAM() != null && ressourcesFinancieresAvantSimulation.getAidesCPAM().getPensionInvalidite() != null) {
            acronyme.add(ressourcesFinancieresAvantSimulation.getAidesCPAM().getPensionInvalidite() != 0 ? AideEnum.PENSION_INVALIDITE.getCode() : "");
        }
        if (ressourcesFinancieresAvantSimulation.getAidesPoleEmploi() != null) {
            if (ressourcesFinancieresAvantSimulation.getAidesPoleEmploi().getAllocationARE() != null && ressourcesFinancieresAvantSimulation.getAidesPoleEmploi().getAllocationARE().getAllocationJournaliereBrute() != null) {
                acronyme.add(ressourcesFinancieresAvantSimulation.getAidesPoleEmploi().getAllocationARE().getAllocationJournaliereBrute() != 0 ? AideEnum.AIDE_RETOUR_EMPLOI.getCode() : "");
            }
            if (ressourcesFinancieresAvantSimulation.getAidesPoleEmploi().getAllocationASS() != null && ressourcesFinancieresAvantSimulation.getAidesPoleEmploi().getAllocationASS().getAllocationJournaliereNet() != null) {
                acronyme.add(ressourcesFinancieresAvantSimulation.getAidesPoleEmploi().getAllocationASS().getAllocationJournaliereNet() != 0 ? AideEnum.ALLOCATION_SOLIDARITE_SPECIFIQUE.getCode() : "");
            }
        }
        return acronyme;
    }

    public String saveBRSAAndRedis(Demandeur demandeur, Simulation simulation) {

        if (demandeur.getIdDemandeurEmploi() != null && demandeur.getIdDemandeurEmploi().equals("SONDE000")) {
            if (simulation != null) {
                simulation.setIdRedis("SONDE000");
                return simulation.getIdRedis();
            }
        } else {
            BRSAData brsaData = new BRSAData(null,
                    demandeur.getIdDemandeurEmploi(),
                    demandeur.getCodeRegionFT(),
                    demandeur.isConseillerFT(),
                    LocalDateTime.now(),
                    -1,
                    demandeur.getParcoursFormation(),
                    getAllocations(demandeur.getRessourcesFinancieresAvantSimulation()),
                    demandeur.getInformationsPersonnelles().getDepartement());
            BRSAData saved = brsaDataRepository.save(brsaData);
            if (simulation != null) {
                simulation.setIdRedis(saved.getId());
            }
            return saved.getId();
        }
        return null;
    }
}
