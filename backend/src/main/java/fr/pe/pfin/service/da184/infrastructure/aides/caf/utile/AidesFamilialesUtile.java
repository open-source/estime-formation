package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.SituationFamilialeUtile;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

public class AidesFamilialesUtile {

    private AidesFamilialesUtile() {
    }

    public static void simulerAidesFamiliales(Map<String, Aide> aidesPourCeMois, Demandeur demandeur,
                                              int numeroMoisSimule) {
        if (isEligibleAllocationsFamiliales(demandeur))
            simulerAllocationsFamiliales(aidesPourCeMois, demandeur);
        if (isEligibleAllocationSoutienFamilial(demandeur))
            simulerAllocationSoutienFamilial(aidesPourCeMois, demandeur);
        if (isEligibleComplementFamilial(demandeur))
            simulerComplementFamilial(aidesPourCeMois, demandeur);
        if (isEligiblePrestationAccueilJeuneEnfant(demandeur, numeroMoisSimule))
            simulerPrestationAccueilJeuneEnfant(aidesPourCeMois, demandeur);
        if (isEligiblePensionsAlimentaires(demandeur))
            simulerPensionsAlimentaires(aidesPourCeMois, demandeur);
    }

    private static void simulerAllocationSoutienFamilial(Map<String, Aide> aidesPourCeMois, Demandeur demandeur) {
        float montantAllocationSoutienFamilial = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .getAidesFamiliales().getAllocationSoutienFamilial();
        aidesPourCeMois.put(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode(),
                creerAideAllocationSoutientFamilial(montantAllocationSoutienFamilial));
    }

    private static void simulerAllocationsFamiliales(Map<String, Aide> aidesPourCeMois, Demandeur demandeur) {
        float montantAllocationsFamiliales = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .getAidesFamiliales().getAllocationsFamiliales();
        aidesPourCeMois.put(AideEnum.ALLOCATIONS_FAMILIALES.getCode(),
                creerAideAllocationsFamiliales(montantAllocationsFamiliales));

    }

    private static void simulerComplementFamilial(Map<String, Aide> aidesPourCeMois, Demandeur demandeur) {
        float montantComplementFamilial = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .getAidesFamiliales().getComplementFamilial();
        aidesPourCeMois.put(AideEnum.COMPLEMENT_FAMILIAL.getCode(),
                creerAideComplementFamilial(montantComplementFamilial));

    }

    private static void simulerPrestationAccueilJeuneEnfant(Map<String, Aide> aidesPourCeMois,
                                                            Demandeur demandeur) {
        float montantPrestationAccueilJeuneEnfant = demandeur.getRessourcesFinancieresAvantSimulation()
                .getAidesCAF().getAidesFamiliales().getPrestationAccueilJeuneEnfant();
        aidesPourCeMois.put(AideEnum.PRESTATION_ACCUEIL_JEUNE_ENFANT.getCode(),
                creerAidePrestationAccueilJeuneEnfant(montantPrestationAccueilJeuneEnfant));

    }

    private static void simulerPensionsAlimentaires(Map<String, Aide> aidesPourCeMois, Demandeur demandeur) {
        float montantPensionsAlimentaires = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .getAidesFamiliales().getPensionsAlimentairesFoyer();
        aidesPourCeMois.put(AideEnum.PENSIONS_ALIMENTAIRES.getCode(),
                creerAidePensionsAlimentaires(montantPensionsAlimentaires));

    }

    private static Aide creerAideAllocationSoutientFamilial(float montant) {
        return AideUtile.creerAide(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL, Optional.of(OrganismeEnum.CAF),
                Optional.of(Collections.singletonList(MessageInformatifEnum.CHANGEMENT_MONTANT_PRESTATIONS_FAMILIALES.getMessage())), true, montant);
    }

    private static Aide creerAideAllocationsFamiliales(float montant) {
        return AideUtile.creerAide(AideEnum.ALLOCATIONS_FAMILIALES, Optional.of(OrganismeEnum.CAF),
                Optional.of(Collections.singletonList(MessageInformatifEnum.CHANGEMENT_MONTANT_PRESTATIONS_FAMILIALES.getMessage())), true, montant);
    }

    private static Aide creerAideComplementFamilial(float montant) {
        return AideUtile.creerAide(AideEnum.COMPLEMENT_FAMILIAL, Optional.of(OrganismeEnum.CAF),
                Optional.of(Collections.singletonList(MessageInformatifEnum.CHANGEMENT_MONTANT_PRESTATIONS_FAMILIALES.getMessage())), true, montant);
    }

    private static Aide creerAidePrestationAccueilJeuneEnfant(float montant) {
        return AideUtile.creerAide(AideEnum.PRESTATION_ACCUEIL_JEUNE_ENFANT, Optional.of(OrganismeEnum.CAF),
                Optional.of(Collections.singletonList(MessageInformatifEnum.CHANGEMENT_MONTANT_PRESTATIONS_FAMILIALES.getMessage())), true, montant);
    }

    private static Aide creerAidePensionsAlimentaires(float montant) {
        return AideUtile.creerAide(AideEnum.PENSIONS_ALIMENTAIRES, Optional.of(OrganismeEnum.CAF),
                Optional.of(Collections.singletonList(MessageInformatifEnum.CHANGEMENT_MONTANT_PRESTATIONS_FAMILIALES.getMessage())), true, montant);
    }

    public static boolean isEligibleAidesFamiliales(Demandeur demandeur, int numeroMoisSimule) {
        return isEligibleAllocationsFamiliales(demandeur) || isEligibleAllocationSoutienFamilial(demandeur)
                || isEligibleComplementFamilial(demandeur)
                || isEligiblePrestationAccueilJeuneEnfant(demandeur, numeroMoisSimule)
                || isEligiblePensionsAlimentaires(demandeur);
    }

    static boolean isEligibleAllocationsFamiliales(Demandeur demandeur) {
        return RessourcesFinancieresAvantSimulationUtile.hasAllocationsFamiliales(demandeur);
    }

    static boolean isEligibleAllocationSoutienFamilial(Demandeur demandeur) {
        return RessourcesFinancieresAvantSimulationUtile.hasAllocationSoutienFamilial(demandeur);
    }

    static boolean isEligibleComplementFamilial(Demandeur demandeur) {
        return RessourcesFinancieresAvantSimulationUtile.hasComplementFamilial(demandeur);
    }

    static boolean isEligiblePrestationAccueilJeuneEnfant(Demandeur demandeur, int numeroMoisSimule) {
        return RessourcesFinancieresAvantSimulationUtile.hasPrestationAccueilJeuneEnfant(demandeur)
                && SituationFamilialeUtile.hasEnfantMoinsDe3AnsPourMoisSimule(demandeur, numeroMoisSimule);
    }

    public static boolean isEligiblePensionsAlimentaires(Demandeur demandeur) {
        return RessourcesFinancieresAvantSimulationUtile.hasPensionsAlimentaires(demandeur);
    }
}
