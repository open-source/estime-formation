package fr.pe.pfin.service.da184.application.ui.rest.ressource;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

public record Critere(String name, Float[] values) {

    public boolean check(Map<String, Float> valFromAngular) {
        Float valToCheck = valFromAngular.get(name);
        return valToCheck >= values[0] && valToCheck <= values[1];
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Critere critere = (Critere) o;
        return name == critere.name && Arrays.equals(values, critere.values);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name);
        result = 31 * result + Arrays.hashCode(values);
        return result;
    }

    @Override
    public String toString() {
        return "Critere{" +
                "values=" + Arrays.toString(values) +
                ", name=" + name +
                '}';
    }
}
