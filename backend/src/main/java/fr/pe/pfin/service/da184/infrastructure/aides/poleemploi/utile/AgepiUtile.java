package fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;

import java.util.ArrayList;
import java.util.Optional;

public class AgepiUtile {

    private AgepiUtile() {
    }

    public static Aide creerAgepi(float montantAide) {
        ArrayList<String> messagesAlerte = new ArrayList<>();
        messagesAlerte.add(MessageInformatifEnum.AGEPI_IDF.getMessage());
        messagesAlerte.add(MessageInformatifEnum.AGEPI_AM_DELAI_DEMANDE.getMessage());
        return AideUtile.creerAide(AideEnum.AGEPI, Optional.of(OrganismeEnum.FT), Optional.of(messagesAlerte), false,
                montantAide);
    }
}
