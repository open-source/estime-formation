package fr.pe.pfin.service.da184.application.ui.rest.ressource;

import java.util.List;
import java.util.Map;

public class CritereConditionRemuneration {
    private final List<Critere> critereLists;

    private final List<ConditionsRemunerations> conditionsRemunerationsList;

    public CritereConditionRemuneration(List<Critere> critereLists, List<ConditionsRemunerations> conditionsRemunerationsList) {
        this.critereLists = critereLists;
        this.conditionsRemunerationsList = conditionsRemunerationsList;
    }

    public boolean check(Map<String, Float> valuesFromSimulateur) {
        boolean toReturn = true;
        for (Critere crit : critereLists) {
            toReturn = crit.check(valuesFromSimulateur);
            if (!toReturn) {
                break;
            }
        }
        return toReturn;
    }

    public List<ConditionsRemunerations> getConditionsRemunerationsList() {
        return conditionsRemunerationsList;
    }
}
