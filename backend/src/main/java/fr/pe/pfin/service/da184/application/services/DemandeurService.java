package fr.pe.pfin.service.da184.application.services;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import fr.pe.pfin.service.da184.domaine.use_cases.ObtenirSimulationAides;
import org.springframework.stereotype.Service;


@Service
public class DemandeurService {

    private final ObtenirSimulationAides obtenirSimulationAides;

    public DemandeurService(ObtenirSimulationAides obtenirSimulationAides) {
        this.obtenirSimulationAides = obtenirSimulationAides;
    }

    public Simulation simulerMesAides(Demandeur demandeur) {
        return this.obtenirSimulationAides.executer(demandeur);
    }
}
