package fr.pe.pfin.service.da184.application.ui.rest.ressource;

public class AidesPoleEmploi {

	private AllocationARE allocationARE;
	private AllocationASS allocationASS;

	public AllocationARE getAllocationARE() {
		return allocationARE;
	}

	public void setAllocationARE(AllocationARE allocationARE) {
		this.allocationARE = allocationARE;
	}

	public AllocationASS getAllocationASS() {
		return allocationASS;
	}

	public void setAllocationASS(AllocationASS allocationASS) {
		this.allocationASS = allocationASS;
	}
}
