package fr.pe.pfin.service.da184.application.ui.rest.ressource;

import java.util.List;

public class Simulation {

	private String idRedis;
	private float montantRessourcesFinancieresMoisAvantSimulation;
	private List<SimulationMensuelle> simulationsMensuelles;

	public List<SimulationMensuelle> getSimulationsMensuelles() {
		return simulationsMensuelles;
	}

	public void setSimulationsMensuelles(List<SimulationMensuelle> simulationsMensuelles) {
		this.simulationsMensuelles = simulationsMensuelles;
	}

	public float getMontantRessourcesFinancieresMoisAvantSimulation() {
		return montantRessourcesFinancieresMoisAvantSimulation;
	}

	public void setMontantRessourcesFinancieresMoisAvantSimulation(
			float montantRessourcesFinancieresMoisAvantSimulation) {
		this.montantRessourcesFinancieresMoisAvantSimulation = montantRessourcesFinancieresMoisAvantSimulation;
	}

	public String getIdRedis() {
		return idRedis;
	}

	public void setIdRedis(String idRedis) {
		this.idRedis = idRedis;
	}

	@Override
	public String toString() {
		return "Simulation{" +
				"idRedis='" + idRedis + '\'' +
				", montantRessourcesFinancieresMoisAvantSimulation=" + montantRessourcesFinancieresMoisAvantSimulation +
				", simulationsMensuelles=" + simulationsMensuelles +
				'}';
	}
}
