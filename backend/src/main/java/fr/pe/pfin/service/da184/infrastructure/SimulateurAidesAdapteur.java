package fr.pe.pfin.service.da184.infrastructure;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.domaine.use_cases.SimulateurAidesPort;
import fr.pe.pfin.service.da184.infrastructure.aides.caf.SimulateurAidesCAF;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.RessourceFinanciereUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.SimulateurAidesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.InformationsPersonnellesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.SimulateurAidesPoleEmploi;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.OpenFiscaRetourSimulationHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.TemporaliteOpenFiscaHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import fr.pe.pfin.service.da184.infrastructure.database.redis.utile.RedisUtile;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.IntStream;

@Service
public class SimulateurAidesAdapteur implements SimulateurAidesPort {
    private final OpenFiscaClient openFiscaClient;
    private final RedisUtile redisUtile;

    public SimulateurAidesAdapteur(OpenFiscaClient openFiscaClient, RedisUtile redisUtile) {
        this.openFiscaClient = openFiscaClient;
        this.redisUtile = redisUtile;
    }

    @Override
    public Simulation simuler(Demandeur demandeur) {
        Simulation simulation = new Simulation();
        simulation.setSimulationsMensuelles(new ArrayList<>());

        LocalDate dateDemandeSimulation = DateUtile.getDateJour();
        LocalDate dateDebutSimulation = SimulateurAidesUtile.getDateDebutSimulation(dateDemandeSimulation);
        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);

        redisUtile.saveBRSAAndRedis(demandeur, simulation);

        int nombreMoisASimuler = SimulateurAidesUtile.getNombreMoisASimuler(demandeur);
        IntStream.rangeClosed(1, nombreMoisASimuler)
                .forEach(numeroMoisSimule -> simulerMesAidesPourCeMois(openFiscaRoot, simulation, dateDebutSimulation, numeroMoisSimule, demandeur));

        simulation.setMontantRessourcesFinancieresMoisAvantSimulation(
                getMontantRessourcesFinancieresMoisAvantSimulation(openFiscaRoot, demandeur,
                        dateDebutSimulation));
        return simulation;
    }

    private float getMontantRessourcesFinancieresMoisAvantSimulation(OpenFiscaRoot openFiscaRoot,
                                                                     Demandeur demandeur, LocalDate dateDebutSimulation) {
        BigDecimal montant = BigDecimal.valueOf(RessourcesFinancieresAvantSimulationUtile
                .calculerMontantRessourcesFinancieresMoisAvantSimulation(demandeur));
        if (InformationsPersonnellesUtile.hasMicroEntreprise(demandeur)) {
            montant = montant.add(BigDecimal.valueOf(
                    OpenFiscaRetourSimulationHelper.calculerMontantBeneficesMicroEntreprise(openFiscaRoot, dateDebutSimulation)));
        }
        return montant.floatValue();
    }

    private void simulerMesAidesPourCeMois(OpenFiscaRoot openFiscaRoot, Simulation simulation,
                                           LocalDate dateDebutSimulation, int numeroMoisSimule, Demandeur demandeur) {
        LocalDate dateMoisASimuler = DateUtile.getDateMoisASimuler(dateDebutSimulation, numeroMoisSimule);

        SimulationMensuelle simulationMensuelle = new SimulationMensuelle();
        simulationMensuelle.setDatePremierJourMoisSimule(dateMoisASimuler);
        simulation.getSimulationsMensuelles().add(simulationMensuelle);

        HashMap<String, Aide> aidesPourCeMois = new HashMap<>();
        simulationMensuelle.setAides(aidesPourCeMois);

        ajouterAidesSansCalcul(aidesPourCeMois, demandeur);
        TemporaliteOpenFiscaHelper.simulerTemporaliteAppelOpenfisca(openFiscaRoot, simulation, aidesPourCeMois,
                dateDebutSimulation, numeroMoisSimule, demandeur);
        SimulateurAidesCAF.simuler(aidesPourCeMois, numeroMoisSimule, demandeur);
        if (numeroMoisSimule <= 1) {
            SimulateurAidesPoleEmploi.simuler(aidesPourCeMois, numeroMoisSimule, demandeur, dateDebutSimulation);
        }
        HashMap<String, RessourceFinanciere> ressourcesFinancieresPourCeMois = new HashMap<>();
        simulationMensuelle.setRessourcesFinancieres(ressourcesFinancieresPourCeMois);

        ajouterRessourcesFinancieres(openFiscaRoot, ressourcesFinancieresPourCeMois, demandeur, numeroMoisSimule,
                dateDebutSimulation);
    }

    private void ajouterAidesSansCalcul(Map<String, Aide> aidesPourCeMois, Demandeur demandeur) {
        if (RessourcesFinancieresAvantSimulationUtile.hasPensionInvalidite(demandeur)) {
            float montantPensionInvalidite = RessourcesFinancieresAvantSimulationUtile
                    .getPensionInvalidite(demandeur);
            aidesPourCeMois.put(AideEnum.PENSION_INVALIDITE.getCode(), creerAideSansCalcul(AideEnum.PENSION_INVALIDITE,
                    Optional.of(OrganismeEnum.CPAM), montantPensionInvalidite));
        }
    }

    private void ajouterRessourcesFinancieres(OpenFiscaRoot openFiscaRoot,
                                              Map<String, RessourceFinanciere> ressourcesFinancieresPourCeMois, Demandeur demandeur,
                                              int numeroMoisSimule, LocalDate dateDebutSimulation) {
        if (InformationsPersonnellesUtile.hasMicroEntreprise(demandeur)) {
            ArrayList<String> messagesAlerte = new ArrayList<>();
            messagesAlerte.add(MessageInformatifEnum.MOYENNE_REVENUS_ENTREPRENEURS.getMessage());
            float montantBeneficesMicroEntrepriseSur1Mois = OpenFiscaRetourSimulationHelper
                    .calculerMontantBeneficesMicroEntreprise(openFiscaRoot, dateDebutSimulation);
            ressourcesFinancieresPourCeMois.put(AideEnum.MICRO_ENTREPRENEUR.getCode(), creerRessourceFinanciere(
                    AideEnum.MICRO_ENTREPRENEUR, Optional.of(messagesAlerte), montantBeneficesMicroEntrepriseSur1Mois));
        }
        if (RessourcesFinancieresAvantSimulationUtile.hasRevenusImmobilier(demandeur)) {
            float montantRevenusImmobilier = RessourcesFinancieresAvantSimulationUtile
                    .getRevenusImmobilierSur1Mois(demandeur);
            ressourcesFinancieresPourCeMois.put(AideEnum.IMMOBILIER.getCode(),
                    creerRessourceFinanciere(AideEnum.IMMOBILIER, Optional.empty(), montantRevenusImmobilier));
        }
        if (RessourcesFinancieresAvantSimulationUtile.hasFuturSalaire(demandeur) && numeroMoisSimule > 1) {
            ArrayList<String> messagesAlerte = new ArrayList<>();
            messagesAlerte.add(MessageInformatifEnum.MESSAGE_REMU.getMessage());
            if (numeroMoisSimule == 2 && RessourcesFinancieresAvantSimulationUtile.hasAllocationSolidariteSpecifique(demandeur.getRessourcesFinancieresAvantSimulation())) {
                messagesAlerte.add(MessageInformatifEnum.ASS_FORMATION.getMessage());
            }
            float montantSalaire = RessourcesFinancieresAvantSimulationUtile.getFuturSalaire(demandeur);
            ressourcesFinancieresPourCeMois.put(AideEnum.REMU.getCode(),
                    creerRessourceFinanciere(AideEnum.REMU, Optional.of(messagesAlerte), montantSalaire));
        }
    }

    private Aide creerAideSansCalcul(AideEnum aideEnum, Optional<OrganismeEnum> organismeEnumOptional, float montant) {
        return AideUtile.creerAide(aideEnum, organismeEnumOptional, Optional.empty(), false, montant);
    }

    private RessourceFinanciere creerRessourceFinanciere(AideEnum aideEnum,
                                                         Optional<List<String>> messageAlerteOptional, float montant) {
        return RessourceFinanciereUtile.creerRessourceFinanciere(aideEnum, messageAlerteOptional, montant);
    }

}
