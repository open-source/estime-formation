package fr.pe.pfin.service.da184.domaine.use_cases;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.ConditionsRemunerations;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SituationRemuneration;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public interface ConditionsRemunerationPort {
    List<ConditionsRemunerations> getConditionsList(SituationRemuneration situationRemuneration) throws URISyntaxException, IOException;
}
