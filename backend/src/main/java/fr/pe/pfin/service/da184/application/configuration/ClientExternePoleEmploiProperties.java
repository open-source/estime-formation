package fr.pe.pfin.service.da184.application.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "poleemploi")
public record ClientExternePoleEmploiProperties(String openFisca) {
}
