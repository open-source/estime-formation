package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;

import java.time.Period;

public class AgeUtile {

    private AgeUtile() {
    }

    public static int calculerAge(Personne personne) {
        return Period.between(personne.getInformationsPersonnelles().getDateNaissance(), DateUtile.getDateJour())
                .getYears();
    }

    public static int calculerAgeMoisMoisSimule(Personne personne, int numeroMoisSimule) {
        return DateUtile.getNbrMoisEntreDeuxLocalDates(personne.getInformationsPersonnelles().getDateNaissance(),
                DateUtile.getDateJour().plusMonths(numeroMoisSimule));
    }
}
