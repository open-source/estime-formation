package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.RessourceFinanciere;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;

import java.util.List;
import java.util.Optional;

public class RessourceFinanciereUtile {

    private RessourceFinanciereUtile() {
    }

    public static RessourceFinanciere creerRessourceFinanciere(AideEnum aideEnum, Optional<List<String>> messageAlerteOptional,
                                                               float montantRessourceFinanciere) {
        RessourceFinanciere ressourceFinanciere = new RessourceFinanciere();
        ressourceFinanciere.setCode(aideEnum.getCode());
        ressourceFinanciere.setMontant(montantRessourceFinanciere);
        ressourceFinanciere.setNom(aideEnum.getNom());
        messageAlerteOptional.ifPresent(ressourceFinanciere::setMessagesAlerte);
        return ressourceFinanciere;
    }
}
