package fr.pe.pfin.service.da184.domaine.use_cases;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;

public interface SimulateurAidesPort {
    Simulation simuler(Demandeur demandeur);

}
