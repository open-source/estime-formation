package fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.SimulateurAidesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.BeneficiaireAidesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.PeriodeTravailleeAvantSimulationUtile;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

public class AreUtile {
    private static final float ARE_MINI = 29.56f;
    private static final float SEUIL_CSG_CRDS = 53f;
    private static final float TAUX_DEDUCTION_CRC = 0.03f;
    private static final float TAUX_DEDUCTION_CSG = 0.9825f * 0.062f;
    private static final float TAUX_DEDUCTION_CRDS = 0.9825f * 0.005f;


    private AreUtile() {
    }

    public static void reporterARE(Map<String, Aide> aidesPourCeMois, Demandeur demandeur, int numeroMoisSimule,
                                   LocalDate dateDebutSimulation) {
        float montantMensuelARE = calculerMontantMensuelARE(demandeur, numeroMoisSimule, dateDebutSimulation);
        float nombreJoursRestantsApresPremierMois = getNombreJoursRestantsApresPremierMois(demandeur,
                dateDebutSimulation);
        Aide are = creerARE(montantMensuelARE, nombreJoursRestantsApresPremierMois);
        aidesPourCeMois.put(AideEnum.AIDE_RETOUR_EMPLOI.getCode(), are);
    }

    public static float calculerMontantMensuelARE(Demandeur demandeur, int numeroMoisSimule,
                                                  LocalDate dateDebutSimulation) {
        float allocationJournaliereBrute = demandeur.getRessourcesFinancieresAvantSimulation()
                .getAidesPoleEmploi().getAllocationARE().getAllocationJournaliereBrute();
        float sjr = demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE()
                .getSalaireJournalierReferenceBrut();
        int nombreJoursRestants = getNombreJoursRestantsARE(demandeur, numeroMoisSimule, dateDebutSimulation);
        float allocationJournaliereNette = calculerAllocationJournaliereNetteARE(allocationJournaliereBrute, sjr);

        return (float) Math.floor(allocationJournaliereNette * nombreJoursRestants);
    }

    private static float calculerAllocationJournaliereNetteARE(float allocationJournaliereBrute, float sjr) {
        float deductions = 0.0f;
        if (allocationJournaliereBrute > ARE_MINI) {
            deductions = sjr * TAUX_DEDUCTION_CRC;
            if ((allocationJournaliereBrute - deductions) > SEUIL_CSG_CRDS) {
                deductions += allocationJournaliereBrute * TAUX_DEDUCTION_CSG;
                if ((allocationJournaliereBrute - deductions) > SEUIL_CSG_CRDS) {
                    deductions += allocationJournaliereBrute * TAUX_DEDUCTION_CRDS;
                }
            }
        }
        return (allocationJournaliereBrute - deductions);
    }

    private static int getNombreJoursRestantsARE(Demandeur demandeur, int numeroMoisSimule,
                                                 LocalDate dateDebutSimulation) {
        int nombreJoursDansLeMois = DateUtile
                .getNombreJoursDansLeMois(DateUtile.ajouterMoisALocalDate(dateDebutSimulation, numeroMoisSimule - 1));
        int nombreJoursRestantsRenseigne = getNombreJoursRestantsRenseigne(demandeur);

        return nombreJoursRestantsRenseigne < nombreJoursDansLeMois ? nombreJoursRestantsRenseigne
                : nombreJoursDansLeMois;
    }

    public static float getMontantAREAvantSimulation(int numeroMoisMontantARecuperer, Demandeur demandeur,
                                                     LocalDate dateDebutSimulation) {
        LocalDate moisAvantPeriodeSimulation = AideUtile.getMoisAvantSimulation(numeroMoisMontantARecuperer,
                dateDebutSimulation);
        return calculerMontantAreAvantSimulation(demandeur, moisAvantPeriodeSimulation);
    }

    public static Aide creerARE(float montantAide, float nombreJoursRestantsARE) {
        ArrayList<String> messagesAlerte = new ArrayList<>();
        messagesAlerte.add(MessageInformatifEnum.MONTANT_ARE_AVANT_PAS.getMessage());
        if (nombreJoursRestantsARE <= 0) {
            messagesAlerte.add(MessageInformatifEnum.FIN_DE_DROIT_ARE.getMessage());
        }
        return AideUtile.creerAide(AideEnum.AIDE_RETOUR_EMPLOI, Optional.of(OrganismeEnum.FT),
                Optional.of(messagesAlerte), false, montantAide);
    }

    public static Aide creerComplementARE(float montantAide, float nombreJoursRestantsARE) {
        ArrayList<String> messagesAlerte = new ArrayList<>();
        messagesAlerte.add(MessageInformatifEnum.ACTUALISATION_ARE.getMessage());
        if (nombreJoursRestantsARE <= 0) {
            messagesAlerte.add(MessageInformatifEnum.FIN_DE_DROIT_ARE.getMessage());
        }
        return AideUtile.creerAide(AideEnum.COMPLEMENT_AIDE_RETOUR_EMPLOI, Optional.of(OrganismeEnum.FT),
                Optional.of(messagesAlerte), false, montantAide);
    }

    private static int getNombreJoursRestantsRenseigne(Demandeur demandeur) {
        int nombreJoursRestantsRenseigne = 0;
        if (demandeur != null && demandeur.getRessourcesFinancieresAvantSimulation() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi()
                .getAllocationARE() != null)
            nombreJoursRestantsRenseigne = demandeur.getRessourcesFinancieresAvantSimulation()
                    .getAidesPoleEmploi().getAllocationARE().getNombreJoursRestants();
        return nombreJoursRestantsRenseigne;
    }

    public static float getNombreJoursRestantsApresPremierMois(Demandeur demandeur,
                                                               LocalDate dateDebutSimulation) {
        int nombreJoursDansLeMois = 0;
        if (!PeriodeTravailleeAvantSimulationUtile.hasSalairesAvantPeriodeSimulation(demandeur, 0)) {
            nombreJoursDansLeMois = DateUtile.getNombreJoursDansLeMois(dateDebutSimulation);
        }
        float nombreJoursRestants = getNombreJoursRestantsRenseigne(demandeur);

        return Math.max(0f, nombreJoursRestants - nombreJoursDansLeMois);
    }

    public static float calculerMontantAreAvantSimulation(Demandeur demandeur, LocalDate mois) {
        int nombreJoursDansLeMois = DateUtile.getNombreJoursDansLeMois(mois);
        float allocationJournaliereBrute = demandeur.getRessourcesFinancieresAvantSimulation()
                .getAidesPoleEmploi().getAllocationARE().getAllocationJournaliereBrute();
        float sjr = demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE()
                .getSalaireJournalierReferenceBrut();

        float allocationJournaliereNette = calculerAllocationJournaliereNetteARE(allocationJournaliereBrute, sjr);
        return BigDecimal.valueOf(nombreJoursDansLeMois).multiply(BigDecimal.valueOf(allocationJournaliereNette))
                .setScale(0, RoundingMode.DOWN).floatValue();
    }

    public static int numeroMoisPassageAuComplementARE(Demandeur demandeur) {
        return PeriodeTravailleeAvantSimulationUtile.hasSalairesAvantPeriodeSimulation(demandeur, 0) ? 0 : 1;
    }

    public static boolean isAreAReporter(Demandeur demandeur, int numeroMoisSimule) {
        if (!PeriodeTravailleeAvantSimulationUtile.hasSalairesAvantPeriodeSimulation(demandeur, 0)) {
            return SimulateurAidesUtile.isPremierMois(numeroMoisSimule)
                    && BeneficiaireAidesUtile.isBeneficiaireARE(demandeur);
        }
        return false;
    }

    public static boolean isComplementAREAVerser(Demandeur demandeur, int numeroMoisSimule) {
        return !isAreAReporter(demandeur, numeroMoisSimule)
                && BeneficiaireAidesUtile.isBeneficiaireARE(demandeur);
    }
}
