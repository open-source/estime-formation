package fr.pe.pfin.service.da184.application.ui.rest.valideur;

import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.BadRequestMessages;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.AidesCAF;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.AidesPoleEmploi;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.RessourcesFinancieresAvantSimulation;

public class RessourcesFinancieresApiValideur {

    private static final String MESSAGE_RESSOURCES_FINANCIERE_OBLIGATOIRE = "ressourcesFinancieres dans Demandeur";

    private RessourcesFinancieresApiValideur() {
    }

    public static void controlerDemandeurAllocationsPoleEmploiASS(
            RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        if (ressourcesFinancieres == null) {
            throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                    MESSAGE_RESSOURCES_FINANCIERE_OBLIGATOIRE));
        }
        AidesPoleEmploi aidesPoleEmploi = ressourcesFinancieres.getAidesPoleEmploi();
        if (aidesPoleEmploi == null) {
            throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                    "aidesPoleEmploi dans RessourcesFinancieres de Demandeur"));
        }
        if (aidesPoleEmploi.getAllocationASS() != null
                && aidesPoleEmploi.getAllocationASS().getAllocationJournaliereNet() <= 0) {
            throw new BadRequestException(
                    String.format(BadRequestMessages.MONTANT_INCORRECT_INFERIEUR_EGAL_ZERO.getMessage(),
                            "allocationJournaliereNetASS"));
        }
        controlerHasTravailleAvantSimulation(ressourcesFinancieres);
    }

    public static void controlerDemandeurAllocationsCafAAH(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        if (ressourcesFinancieres == null) {
            throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                    MESSAGE_RESSOURCES_FINANCIERE_OBLIGATOIRE));
        }
        AidesCAF aidesCAF = ressourcesFinancieres.getAidesCAF();
        if (aidesCAF == null) {
            throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                    "allocationsCAF dans RessourcesFinancieres de Demandeur"));
        }
        if (aidesCAF.getAllocationAAH() != null && aidesCAF.getAllocationAAH() <= 0) {
            throw new BadRequestException(
                    String.format(BadRequestMessages.MONTANT_INCORRECT_INFERIEUR_EGAL_ZERO.getMessage(),
                            "allocationMensuelleNetAAH"));
        }
        controlerHasTravailleAvantSimulation(ressourcesFinancieres);
    }

    public static void controlerDemandeurAllocationsCafRSA(Demandeur demandeur) {
        RessourcesFinancieresAvantSimulation ressourcesFinancieres = demandeur
                .getRessourcesFinancieresAvantSimulation();
        if (ressourcesFinancieres == null) {
            throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                    MESSAGE_RESSOURCES_FINANCIERE_OBLIGATOIRE));
        }
        AidesCAF aidesCAF = ressourcesFinancieres.getAidesCAF();
        if (aidesCAF == null) {
            throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                    "allocationsCAF dans RessourcesFinancieres de Demandeur"));
        }
        if (aidesCAF.getAllocationRSA() == null) {
            throw new BadRequestException(
                    String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "allocationMensuelleNetRSA"));
        }
        if (aidesCAF.getAllocationRSA() <= 0) {
            throw new BadRequestException(
                    String.format(BadRequestMessages.MONTANT_INCORRECT_INFERIEUR_EGAL_ZERO.getMessage(),
                            "allocationMensuelleNetRSA"));
        }
        if (aidesCAF.getProchaineDeclarationTrimestrielle() == null) {
            throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                    "prochaineDeclarationTrimestrielle dans allocationsCAF de RessourcesFinancieres de Demandeur"));
        }
        if (aidesCAF.getProchaineDeclarationTrimestrielle() < 0
                || aidesCAF.getProchaineDeclarationTrimestrielle() >= 4) {
            throw new BadRequestException(
                    String.format(BadRequestMessages.VALEUR_INCORRECT_PROCHAINE_DECLARATION_TRIMESTRIELLE.getMessage(),
                            aidesCAF.getProchaineDeclarationTrimestrielle()));
        }
        controlerHasTravailleAvantSimulation(ressourcesFinancieres);
    }

    private static void controlerHasTravailleAvantSimulation(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        if (ressourcesFinancieres.getHasTravailleAuCoursDerniersMois() == null) {
            throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                    "hasTravailleAuCoursDerniersMois dans RessourcesFinancieres"));
        }

        if (ressourcesFinancieres.getHasTravailleAuCoursDerniersMois() != null
                && ressourcesFinancieres.getHasTravailleAuCoursDerniersMois().booleanValue()
                && ressourcesFinancieres.getPeriodeTravailleeAvantSimulation() == null) {
            throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                    "periodeTravailleeAvantSimulation dans RessourcesFinancieres de Demandeur"));
        }
    }
}
