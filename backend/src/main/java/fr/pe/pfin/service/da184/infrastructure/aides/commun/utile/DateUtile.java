package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile;

import java.text.DecimalFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class DateUtile {

    public static final ZoneId ZONE_ID_FRANCE = ZoneId.of("Europe/Paris");
    public static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";

    private DateUtile() {
    }

    public static int getNbrMoisEntreDeuxLocalDates(LocalDate dateDebut, LocalDate dateFin) {
        return (int) ChronoUnit.MONTHS.between(dateDebut, dateFin);
    }

    public static LocalDate getDateMoisASimuler(LocalDate dateDebutSimulation, int numeroMoisSimule) {
        int nombreMoisToAdd = numeroMoisSimule - 1;
        return ajouterMoisALocalDate(dateDebutSimulation, nombreMoisToAdd);
    }

    public static LocalDate getDatePremierJourDuMois(LocalDate dateCourante) {
        YearMonth yearMonth = YearMonth.of(dateCourante.getYear(), dateCourante.getMonthValue());
        return yearMonth.atDay(1);
    }

    public static LocalDate getDateDernierJourDuMois(LocalDate dateCourante) {
        YearMonth yearMonth = YearMonth.of(dateCourante.getYear(), dateCourante.getMonthValue());
        return yearMonth.atEndOfMonth();
    }

    public static LocalDate getDateJour() {
        return LocalDate.now(ZONE_ID_FRANCE);
    }
    public static LocalDate getDate(String date) {
        return LocalDate.parse(date);
    }

    public static LocalDate getDateDebutMois() {
        return LocalDate.now(ZONE_ID_FRANCE).with(TemporalAdjusters.firstDayOfMonth());
    }

    public static String getMonthNameFromLocalDate(LocalDate localDate) {
        Month month = localDate.getMonth();
        return month.getDisplayName(TextStyle.FULL_STANDALONE, Locale.FRANCE);
    }

    public static String getMonthFromLocalDate(LocalDate localDate) {
        DecimalFormat decimalFormat = new DecimalFormat("00");
        return decimalFormat.format(Double.valueOf(localDate.getMonthValue()));
    }

    public static LocalDate convertDateToLocalDate(Date dateToConvert) {
        return dateToConvert.toInstant().atZone(ZONE_ID_FRANCE).toLocalDate();
    }

    public static String convertDateToStringOpenFisca(LocalDate dateToConvert) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT_YYYY_MM_DD);
        return formatter.format(dateToConvert);
    }

    public static String convertDateToString(LocalDate dateToConvert, String dateFormat) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
        return formatter.format(dateToConvert);
    }

    // Fonction qui permet d'ajouter 1 an et 1 mois aux enfants de moins d'1 an
    // pour contourner une erreur dans le calcul des ages des enfants de moins d'un
    // an de l'API AGEPI OpenFisca
    public static LocalDate getDateNaissanceModifieeEnfantMoinsDUnAn(LocalDate dateNaissance) {
        if (Objects.nonNull(dateNaissance) && getAge(dateNaissance) < 1) {
            return enleverMoisALocalDate(enleverAnneesALocalDate(dateNaissance, 1), 1);
        }
        return dateNaissance;
    }

    public static LocalDate enleverAnneesALocalDate(LocalDate localDate, int nombreAnneesAAjouter) {
        if (Objects.nonNull(localDate)) {
            return localDate.minusYears(nombreAnneesAAjouter);
        }
        return null;
    }

    public static LocalDate ajouterMoisALocalDate(LocalDate localDate, int nombreMoisAAjouter) {
        if (Objects.isNull(localDate)) {
            return null;
        }
        return localDate.plusMonths(nombreMoisAAjouter);
    }

    public static LocalDate enleverMoisALocalDate(LocalDate localDate, int nombreMoisAEnlever) {
        if (Objects.isNull(localDate)) {
            return null;
        }
        return localDate.minusMonths(nombreMoisAEnlever);
    }

    public static LocalDate ajouterJourALocalDate(LocalDate localDate, int nombreJoursAAjouter) {
        if (Objects.isNull(localDate)) {
            return null;
        }
        return localDate.plusDays(nombreJoursAAjouter);
    }

    public static LocalDate enleverJoursALocalDate(LocalDate localDate, int nombreJoursAEnlever) {
        if (Objects.isNull(localDate)) {
            return null;
        }
        return localDate.minusDays(nombreJoursAEnlever);
    }

    public static boolean isDateAvant(LocalDate dateToCheck, LocalDate dateLimite) {
        return dateToCheck.isBefore(dateLimite);
    }

    public static int getNombreJoursDansLeMois(LocalDate date) {
        YearMonth yearMonthObject = YearMonth.of(date.getYear(), date.getMonthValue());
        return yearMonthObject.lengthOfMonth();
    }

    public static int getAge(LocalDate dateNaissance) {
        if (Objects.nonNull(dateNaissance)) {
            return Period.between(dateNaissance, LocalDate.now()).getYears();
        } else {
            return 0;
        }
    }

    public static LocalDate getDateNaissanceFromAge(int age) {
        return LocalDate.now().minusYears(age);
    }

    public static LocalDate getDateNaissanceFromAge(LocalDate dateReference, int age) {
        if (Objects.isNull(dateReference)) {
            return null;
        }
        return dateReference.minusYears(age);
    }

    public static boolean isDatePremierJourDuMois(LocalDate date) {
        return date.getDayOfMonth() == 1;
    }

    public static boolean isSameDate(LocalDate firstDate, LocalDate dateToCompare) {
        return firstDate.equals(dateToCompare);
    }
}
