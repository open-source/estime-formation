package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper;

import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.SituationAppelOpenFiscaEnum;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.*;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;

import java.time.LocalDate;

public class OpenFiscaRetourSimulationHelper {

    private OpenFiscaRetourSimulationHelper() {
    }

    public static OpenFiscaRetourSimulation calculerAidesSelonSituation(OpenFiscaRoot openFiscaRoot,
                                                                        LocalDate dateDebutSimulation, int numeroMoisSimule, SituationAppelOpenFiscaEnum situation) {
        OpenFiscaRetourSimulation openFiscaRetourSimulation = new OpenFiscaRetourSimulation();
        switch (situation) {
            case AGEPI:
                openFiscaRetourSimulation = calculerAgepi(openFiscaRoot, dateDebutSimulation);
                break;
            case AM:
                openFiscaRetourSimulation = calculerAideMobilite(openFiscaRoot, dateDebutSimulation);
                break;
            case PPA:
                openFiscaRetourSimulation = calculerPrimeActivite(openFiscaRoot, dateDebutSimulation, numeroMoisSimule);
                break;
            case RSA:
                openFiscaRetourSimulation = calculerRSA(openFiscaRoot, dateDebutSimulation, numeroMoisSimule);
                break;
            case AL:
                openFiscaRetourSimulation = calculerAideLogement(openFiscaRoot, dateDebutSimulation, numeroMoisSimule);
                break;
            case ARE:
                openFiscaRetourSimulation = calculerComplementARE(openFiscaRoot, dateDebutSimulation, numeroMoisSimule - 1);
                break;
            default:
                break;
        }
        return openFiscaRetourSimulation;

    }

    public static OpenFiscaRetourSimulation calculerAgepi(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation) {
        OpenFiscaRetourSimulation openFiscaRetourSimulation = new OpenFiscaRetourSimulation();
        openFiscaRetourSimulation
                .setMontantAgepi(OpenFiscaMappeurAgepi.getMontantAgepi(openFiscaRoot, dateDebutSimulation, 1));
        return openFiscaRetourSimulation;
    }

    public static OpenFiscaRetourSimulation calculerAideMobilite(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation) {
        OpenFiscaRetourSimulation openFiscaRetourSimulation = new OpenFiscaRetourSimulation();
        openFiscaRetourSimulation.setMontantAideMobilite(
                OpenFiscaMappeurAideMobilite.getMontantAideMobilite(openFiscaRoot, dateDebutSimulation, 1));
        return openFiscaRetourSimulation;
    }

    public static OpenFiscaRetourSimulation calculerPrimeActivite(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation,
                                                                  int numeroMoisSimule) {
        OpenFiscaRetourSimulation openFiscaRetourSimulation = new OpenFiscaRetourSimulation();
        openFiscaRetourSimulation.setMontantPrimeActivite(OpenFiscaMappeurPrimeActivite
                .getMontantPrimeActivite(openFiscaRoot, dateDebutSimulation, numeroMoisSimule - 1));
        return openFiscaRetourSimulation;
    }

    public static OpenFiscaRetourSimulation calculerRSA(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation,
                                                        int numeroMoisSimule) {
        OpenFiscaRetourSimulation openFiscaRetourSimulation = new OpenFiscaRetourSimulation();
        openFiscaRetourSimulation.setMontantRSA(
                OpenFiscaMappeurRSA.getMontantRSA(openFiscaRoot, dateDebutSimulation, numeroMoisSimule - 1));
        return openFiscaRetourSimulation;
    }

    public static OpenFiscaRetourSimulation calculerAideLogement(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation,
                                                                  int numeroMoisSimule) {
        OpenFiscaRetourSimulation openFiscaRetourSimulation = new OpenFiscaRetourSimulation();
        openFiscaRetourSimulation.setMontantAideLogement(OpenFiscaMappeurAidesLogement
                .getMontantAideLogement(openFiscaRoot, dateDebutSimulation, numeroMoisSimule - 1));
        openFiscaRetourSimulation.setTypeAideLogement(AideEnum.AIDES_LOGEMENT.getCode());
        return openFiscaRetourSimulation;
    }

    public static OpenFiscaRetourSimulation calculerComplementARE(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation,
                                                                  int numeroMoisSimule) {
        OpenFiscaRetourSimulation openFiscaRetourSimulation = new OpenFiscaRetourSimulation();
        openFiscaRetourSimulation.setMontantComplementARE(OpenFiscaMappeurComplementARE
                .getMontantComplementARENet(openFiscaRoot, dateDebutSimulation, numeroMoisSimule));
        openFiscaRetourSimulation.setNombreJoursRestantsARE(OpenFiscaMappeurComplementARE
                .getNombreJoursRestantsARE(openFiscaRoot, dateDebutSimulation, numeroMoisSimule));
        openFiscaRetourSimulation.setMontantDeductionsComplementARE(OpenFiscaMappeurComplementARE
                .getMontantDeductionsComplementARE(openFiscaRoot, dateDebutSimulation, numeroMoisSimule));
        openFiscaRetourSimulation.setMontantComplementAREBrut(OpenFiscaMappeurComplementARE
                .getMontantComplementAREBrut(openFiscaRoot, dateDebutSimulation, numeroMoisSimule));
        openFiscaRetourSimulation.setMontantCRCComplementARE(OpenFiscaMappeurComplementARE
                .getMontantCRCComplementARE(openFiscaRoot, dateDebutSimulation, numeroMoisSimule));
        openFiscaRetourSimulation.setMontantCRDSComplementARE(OpenFiscaMappeurComplementARE
                .getMontantCRDSComplementARE(openFiscaRoot, dateDebutSimulation, numeroMoisSimule));
        openFiscaRetourSimulation.setMontantCSGComplementARE(OpenFiscaMappeurComplementARE
                .getMontantCSGComplementARE(openFiscaRoot, dateDebutSimulation, numeroMoisSimule));
        openFiscaRetourSimulation.setNombreJoursIndemnisesComplementARE(OpenFiscaMappeurComplementARE
                .getNombreJoursIndemnisesComplementARE(openFiscaRoot, dateDebutSimulation, numeroMoisSimule));
        return openFiscaRetourSimulation;
    }

    public static float calculerMontantBeneficesMicroEntreprise(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation) {
        return OpenFiscaMappeurMicroEntreprise.getMontantBeneficesMicroEntreprise(openFiscaRoot, dateDebutSimulation);
    }
}
