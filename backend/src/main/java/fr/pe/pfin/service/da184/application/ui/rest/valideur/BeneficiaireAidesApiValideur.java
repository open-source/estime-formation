package fr.pe.pfin.service.da184.application.ui.rest.valideur;

import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.BadRequestMessages;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.BeneficiaireAides;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;


public class BeneficiaireAidesApiValideur {

    private BeneficiaireAidesApiValideur() {
    }

    public static void controlerDonnees(Demandeur demandeur) {
        BeneficiaireAides beneficiaireAides = demandeur.getBeneficiaireAides();
        if (beneficiaireAides == null) {
            throw new BadRequestException(
                    String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "beneficiaireAides"));
        }
        if (beneficiaireAides.isBeneficiaireAAH()) {
            RessourcesFinancieresApiValideur.controlerDemandeurAllocationsCafAAH(
                    demandeur.getRessourcesFinancieresAvantSimulation());
        }
        if (beneficiaireAides.isBeneficiaireRSA()) {
            RessourcesFinancieresApiValideur.controlerDemandeurAllocationsCafRSA(demandeur);
        }
        if (beneficiaireAides.isBeneficiaireASS()) {
            RessourcesFinancieresApiValideur.controlerDemandeurAllocationsPoleEmploiASS(
                    demandeur.getRessourcesFinancieresAvantSimulation());
        }
    }
}
