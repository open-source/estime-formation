package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Logement;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.CodeDepartementUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaMenage;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OpenFiscaMappeurMenage {

    private static final String RESIDENCE_METROPOLE = "metropole";
    private static final String RESIDENCE_DOM = "guadeloupe";

    private OpenFiscaMappeurMenage() {
    }

    public static OpenFiscaMenage creerMenageOpenFisca(Demandeur demandeur, LocalDate dateDebutSimulation) {
        OpenFiscaMenage menageOpenFisca = new OpenFiscaMenage();

        List<String> personneDeReference = new ArrayList<>();
        personneDeReference.add(ParametresOpenFisca.DEMANDEUR);
        menageOpenFisca.setPersonneDeReference(personneDeReference);
        if (Objects.nonNull(demandeur.getInformationsPersonnelles())) {
            Logement logement = demandeur.getInformationsPersonnelles().getLogement();
            if (logement != null) {
                menageOpenFisca.setDepcom(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(logement.getCoordonnees().getCodeInsee(), dateDebutSimulation));
                menageOpenFisca.setResidenceMayotte(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(CodeDepartementUtile.isDeMayotte(demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().getCodePostal()), dateDebutSimulation));
                String lieuResidence = CodeDepartementUtile.isDesDOM(demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().getCodePostal()) ? RESIDENCE_DOM
                        : RESIDENCE_METROPOLE;
                menageOpenFisca.setLieuResidence(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(lieuResidence, dateDebutSimulation));
                menageOpenFisca.setLoyer(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(logement.getMontantLoyer(), dateDebutSimulation));
                menageOpenFisca.setLogementChambre(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(logement.isChambre(), dateDebutSimulation));
                menageOpenFisca.setLogementCrous(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(logement.isCrous(), dateDebutSimulation));
                menageOpenFisca.setLogementConventionne(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(logement.isConventionne(), dateDebutSimulation));
                menageOpenFisca.setColoc(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(logement.isColloc(), dateDebutSimulation));
                menageOpenFisca.setStatutOccupationLogement(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(logement.getStatutOccupationLogement(), dateDebutSimulation));
            }
        }
        return menageOpenFisca;
    }
}
