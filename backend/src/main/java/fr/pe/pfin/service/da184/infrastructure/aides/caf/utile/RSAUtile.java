package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

public class RSAUtile {

    private RSAUtile() {
    }

    /**
     * Fonction permettant de déterminer si on a calculé le(s) montant(s) de la
     * prime d'activité et/ou du RSA le mois précédent et s'il(s) doi(ven)t être
     * versé(s) ce mois-ci
     *
     * @return _________________________________________________________________________________________
     * | | | | | | | | | | Mois décla | M0 | M1 | M2 | M3 | M4 | M5 | M6 | |
     * | | | | | | | | | - M0 | C0 | (R0) | R0 | C1/R0 | (V1) | R1 | C2/R1 |
     * | - M1 | R0 | C1/R0 | (V1) | R1 | C2/R1 | (V2) | R2 | | - M2 | R0 |
     * R0 | C1/R0 | (V1) | R1 | C2/R1 | (V2) | | - M3 | C0 | (R0) | R0 |
     * C1/R0 | (V1) | R1 | C2/R1 |
     * |____________|__________|__________|__________|__________|__________|__________|__________|
     */
    public static boolean isRSAAVerser(int numeroMoisSimule, Demandeur demandeur) {
        int prochaineDeclarationTrimestrielle = RessourcesFinancieresAvantSimulationUtile
                .getProchaineDeclarationTrimestrielle(demandeur);
        return (((prochaineDeclarationTrimestrielle == 0) && (numeroMoisSimule == 1 || numeroMoisSimule == 4))
                || ((prochaineDeclarationTrimestrielle == 1) && (numeroMoisSimule == 2 || numeroMoisSimule == 5))
                || ((prochaineDeclarationTrimestrielle == 2) && (numeroMoisSimule == 3 || numeroMoisSimule == 6))
                || ((prochaineDeclarationTrimestrielle == 3) && (numeroMoisSimule == 4)));
    }

    public static void reporterRsa(Simulation simulation, Map<String, Aide> aidesPourCeMois, int numeroMoisSimule,
                                   Demandeur demandeur) {
        Optional<Aide> rsaMoisPrecedent = getRSASimuleeMoisPrecedent(simulation, numeroMoisSimule);
        if (rsaMoisPrecedent.isPresent()) {
            Aide rsaReporte = creerAideRSA(rsaMoisPrecedent.get().getMontant(), true);
            ArrayList<String> messagesAlerte = new ArrayList<>();
            messagesAlerte.add(MessageInformatifEnum.RSA_FORMATION.getMessage());
            rsaReporte.setMessagesAlerte(messagesAlerte);
            aidesPourCeMois.put(AideEnum.RSA.getCode(), rsaReporte);
        } else if (isEligiblePourReportRSADeclare(numeroMoisSimule, demandeur)) {
            aidesPourCeMois.put(AideEnum.RSA.getCode(), getRSADeclare(demandeur));
        }
    }

    public static Aide creerAideRSA(float montantRSA, boolean isAideReportee) {
        ArrayList<String> messagesAlerte = new ArrayList<>();
        return AideUtile.creerAide(AideEnum.RSA, Optional.of(OrganismeEnum.CAF), Optional.of(messagesAlerte), isAideReportee,
                montantRSA);
    }

    private static Aide getRSADeclare(Demandeur demandeur) {
        float montantDeclare = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .getAllocationRSA();
        return creerAideRSA(montantDeclare, true);
    }

    private static Optional<Aide> getRSASimuleeMoisPrecedent(Simulation simulation, int numeroMoisSimule) {
        int moisNMoins1 = numeroMoisSimule - 1;
        return AideUtile.getAidePourCeMoisSimule(simulation, AideEnum.RSA.getCode(), moisNMoins1);
    }

    public static boolean isEligiblePourReportRSADeclare(int numeroMoisSimule, Demandeur demandeur) {
        boolean isEligiblePourReportRSADeclare = false;
        if (RessourcesFinancieresAvantSimulationUtile.hasAllocationsRSA(demandeur)) {
            int prochaineDeclarationTrimestrielle = RessourcesFinancieresAvantSimulationUtile
                    .getProchaineDeclarationTrimestrielle(demandeur);
            isEligiblePourReportRSADeclare = prochaineDeclarationTrimestrielle != 0
                    && ((prochaineDeclarationTrimestrielle == 3) && numeroMoisSimule <= 1)
                    || (prochaineDeclarationTrimestrielle == 1 && numeroMoisSimule <= 2)
                    || (prochaineDeclarationTrimestrielle == 2 && numeroMoisSimule <= 3);

        }
        return isEligiblePourReportRSADeclare;
    }
}
