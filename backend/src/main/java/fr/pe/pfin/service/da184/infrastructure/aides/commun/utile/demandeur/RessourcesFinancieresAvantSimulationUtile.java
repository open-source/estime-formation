package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile.AllocationSolidariteSpecifiqueUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile.AreUtile;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Objects;

public class RessourcesFinancieresAvantSimulationUtile {

    private RessourcesFinancieresAvantSimulationUtile() {
    }

    public static float calculerMontantRessourcesFinancieresMoisAvantSimulation(Demandeur demandeur) {
        BigDecimal montantTotal = BigDecimal.ZERO;
        //On se base sur une entrée en formation à mois N+1, donc le mois avant la simulation est le mois en cours
        LocalDate moisAvantSimulation = DateUtile.getDateJour();
        montantTotal = montantTotal
                .add(BigDecimal.valueOf(calculerMontantAidesMoisAvantSimulation(demandeur, moisAvantSimulation)));
       montantTotal = montantTotal.add(BigDecimal.valueOf(calculerMontantRevenusMoisAvantSimulation(demandeur)));

        return montantTotal.setScale(0, RoundingMode.DOWN).floatValue();
    }

    private static float calculerMontantAidesMoisAvantSimulation(Demandeur demandeur,
                                                                 LocalDate moisAvantSimulation) {
        BigDecimal montantTotal = BigDecimal.ZERO;
        if (BeneficiaireAidesUtile.isBeneficiaireASS(demandeur)) {
            montantTotal = montantTotal.add(BigDecimal.valueOf(
                    AllocationSolidariteSpecifiqueUtile.calculerMontant(demandeur, moisAvantSimulation)));
        }
        if (BeneficiaireAidesUtile.isBeneficiaireARE(demandeur)) {
            montantTotal = montantTotal.add(BigDecimal
                    .valueOf(AreUtile.calculerMontantAreAvantSimulation(demandeur, moisAvantSimulation)));
        }
        if (BeneficiaireAidesUtile.isBeneficiaireAAH(demandeur)) {
            montantTotal = montantTotal.add(BigDecimal.valueOf(
                    demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAllocationAAH()));
        }
        if (BeneficiaireAidesUtile.isBeneficiairePensionInvalidite(demandeur)) {
            montantTotal = montantTotal.add(BigDecimal.valueOf(
                    demandeur.getRessourcesFinancieresAvantSimulation().getAidesCPAM().getPensionInvalidite()));
        }
        if (BeneficiaireAidesUtile.isBeneficiaireRSA(demandeur)) {
            montantTotal = montantTotal.add(BigDecimal.valueOf(
                    demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAllocationRSA()));
        }
        if (hasAllocationsFamiliales(demandeur)) {
            montantTotal = montantTotal.add(BigDecimal.valueOf(demandeur.getRessourcesFinancieresAvantSimulation()
                    .getAidesCAF().getAidesFamiliales().getAllocationsFamiliales()));
        }
        if (hasComplementFamilial(demandeur)) {
            montantTotal = montantTotal.add(BigDecimal.valueOf(demandeur.getRessourcesFinancieresAvantSimulation()
                    .getAidesCAF().getAidesFamiliales().getComplementFamilial()));
        }
        if (hasAllocationSoutienFamilial(demandeur)) {
            montantTotal = montantTotal.add(BigDecimal.valueOf(demandeur.getRessourcesFinancieresAvantSimulation()
                    .getAidesCAF().getAidesFamiliales().getAllocationSoutienFamilial()));
        }
        if (hasPrestationAccueilJeuneEnfant(demandeur)) {
            montantTotal = montantTotal.add(BigDecimal.valueOf(demandeur.getRessourcesFinancieresAvantSimulation()
                    .getAidesCAF().getAidesFamiliales().getPrestationAccueilJeuneEnfant()));
        }
        if (hasPensionsAlimentaires(demandeur)) {
            montantTotal = montantTotal.add(BigDecimal.valueOf(demandeur.getRessourcesFinancieresAvantSimulation()
                    .getAidesCAF().getAidesFamiliales().getPensionsAlimentairesFoyer()));
        }
        if (hasAllocationLogement(demandeur)) {
            montantTotal = montantTotal.add(BigDecimal
                    .valueOf(getAllocationsLogementSur1Mois(demandeur.getRessourcesFinancieresAvantSimulation()
                            .getAidesCAF().getAidesLogement())));
        }
        if (hasPrimeActivite(demandeur)) {
            montantTotal = montantTotal.add(BigDecimal.valueOf(getPrimeActivite(demandeur)));
        }
        return montantTotal.setScale(0, RoundingMode.DOWN).floatValue();
    }

    private static float calculerMontantRevenusMoisAvantSimulation(Demandeur demandeur) {
        BigDecimal montantTotal = BigDecimal.ZERO;
        if (hasRevenusTravailleurIndependant(demandeur.getRessourcesFinancieresAvantSimulation())) {
            montantTotal = montantTotal
                    .add(BigDecimal.valueOf(getRevenusTravailleurIndependantSur1Mois(demandeur)));
        }
        if (hasRevenusImmobilier(demandeur.getRessourcesFinancieresAvantSimulation())) {
            montantTotal = montantTotal.add(BigDecimal
                    .valueOf(getRevenusImmobilierSur1Mois(demandeur.getRessourcesFinancieresAvantSimulation())));
        }
        if (InformationsPersonnellesUtile.isSalarie(demandeur)
                && PeriodeTravailleeAvantSimulationUtile.hasSalairesAvantPeriodeSimulation(demandeur, 0)) {
            montantTotal = montantTotal.add(BigDecimal.valueOf(PeriodeTravailleeAvantSimulationUtile
                    .getMoisTravaillesAvantSimulation(demandeur, 0).getSalaire().getMontantMensuelNet()));
        }
        return montantTotal.setScale(0, RoundingMode.DOWN).floatValue();
    }

    public static float getPrimeActivite(Demandeur demandeur) {
        if (hasPrimeActivite(demandeur)) {
            return demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getPrimeActivite();
        }
        return 0;
    }

    public static float getAllocationsLogementSur1Mois(AllocationsLogement allocationsLogement) {
        return BigDecimal.valueOf(allocationsLogement.getMoisNMoins1()).floatValue();
    }

    public static float getRevenusImmobilierSur1Mois(Demandeur demandeur) {
        float montantRevenusImmobilierSur1Mois = 0;
        if (hasRevenusImmobilier(demandeur)) {
            montantRevenusImmobilierSur1Mois = BigDecimal
                    .valueOf(demandeur.getRessourcesFinancieresAvantSimulation()
                            .getRevenusImmobilier3DerniersMois())
                    .divide(BigDecimal.valueOf(3), 0, RoundingMode.HALF_UP).floatValue();
        }
        return montantRevenusImmobilierSur1Mois;
    }

    public static float getFuturSalaire(Demandeur demandeur) {
        float montantFuturSalaire = 0;
        if (hasFuturSalaire(demandeur)) {
            montantFuturSalaire = BigDecimal
                    .valueOf(demandeur.getFutureFormation().getSalaire().getMontantMensuelNet()).floatValue();
            if (InformationsPersonnellesUtile.hasCumulAncienEtNouveauSalaire(demandeur)) {
                montantFuturSalaire = BigDecimal.valueOf(
                                PeriodeTravailleeAvantSimulationUtile.getSalaireCumul(demandeur).getMontantMensuelNet())
                        .floatValue();
            }
        }
        return montantFuturSalaire;
    }

    public static float getRevenusImmobilierSur1Mois(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        float montantRevenusImmobilierSur1Mois = 0;
        if (hasRevenusImmobilier(ressourcesFinancieres)) {
            montantRevenusImmobilierSur1Mois = BigDecimal
                    .valueOf(ressourcesFinancieres.getRevenusImmobilier3DerniersMois())
                    .divide(BigDecimal.valueOf(3), 0, RoundingMode.HALF_UP).floatValue();
        }
        return montantRevenusImmobilierSur1Mois;
    }

    public static float getRevenusTravailleurIndependantSur1Mois(Demandeur demandeur) {
        float montantRevenusIndependantSur1Mois = 0;
        if (hasRevenusTravailleurIndependant(demandeur)) {
            montantRevenusIndependantSur1Mois = BigDecimal
                    .valueOf(demandeur.getRessourcesFinancieresAvantSimulation()
                            .getChiffreAffairesIndependantDernierExercice())
                    .divide(BigDecimal.valueOf(12), 0, RoundingMode.HALF_UP).floatValue();
        }
        return montantRevenusIndependantSur1Mois;
    }

    public static float getRevenusTravailleurIndependant(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        float montantRevenusIndependant = 0;
        if (hasRevenusTravailleurIndependant(ressourcesFinancieres)) {
            montantRevenusIndependant = BigDecimal
                    .valueOf(ressourcesFinancieres.getChiffreAffairesIndependantDernierExercice()).floatValue();
        }
        return montantRevenusIndependant;
    }

    public static float getRevenusMicroEntreprise(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        float montantRevenusMicroEntreprise = 0;
        if (hasRevenusMicroEntreprise(ressourcesFinancieres)) {
            montantRevenusMicroEntreprise = BigDecimal
                    .valueOf(ressourcesFinancieres.getBeneficesMicroEntrepriseDernierExercice()).floatValue();
        }
        return montantRevenusMicroEntreprise;
    }

    public static float getPensionRetraite(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        float montantPensionRetraite = 0;
        if (hasPensionRetraite(ressourcesFinancieres)) {
            montantPensionRetraite = BigDecimal.valueOf(ressourcesFinancieres.getPensionRetraite()).floatValue();
        }
        return montantPensionRetraite;
    }

    public static AidesFamiliales getAidesFamiliales(Demandeur demandeur) {
        if (Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation()) && Objects.nonNull(demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF())) {
            return demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales();
        }
        return null;
    }

    public static float getAllocationsFamiliales(Demandeur demandeur) {
        float allocationsFamiliales = 0;
        if (getAidesFamiliales(demandeur) != null) {
            allocationsFamiliales = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                    .getAidesFamiliales().getAllocationsFamiliales();
        }
        return allocationsFamiliales;
    }

    public static float getComplementFamilial(Demandeur demandeur) {
        float complementFamilial = 0;
        if (getAidesFamiliales(demandeur) != null) {
            complementFamilial = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                    .getAidesFamiliales().getComplementFamilial();
        }
        return complementFamilial;
    }

    public static float getAllocationSoutienFamilial(Demandeur demandeur) {
        float allocationSoutienFamilial = 0;
        if (getAidesFamiliales(demandeur) != null) {
            allocationSoutienFamilial = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                    .getAidesFamiliales().getAllocationSoutienFamilial();
        }
        return allocationSoutienFamilial;
    }

    public static float getPrestationAccueilJeuneEnfant(Demandeur demandeur) {
        float prestationAccueilJeuneEnfant = 0;
        if (getAidesFamiliales(demandeur) != null) {
            prestationAccueilJeuneEnfant = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                    .getAidesFamiliales().getPrestationAccueilJeuneEnfant();
        }
        return prestationAccueilJeuneEnfant;
    }

    public static float getPensionInvalidite(Demandeur demandeur) {
        float pensionInvalidite = 0;
        if (hasPensionInvalidite(demandeur)) {
            pensionInvalidite = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCPAM()
                    .getPensionInvalidite();
        }
        return pensionInvalidite;
    }

    public static boolean hasAllocationsCAF(Demandeur demandeur) {
        return demandeur.getRessourcesFinancieresAvantSimulation() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF() != null;
    }

    public static boolean hasAllocationsRSA(Demandeur demandeur) {
        if (hasAllocationsCAF(demandeur)) {
            return demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAllocationRSA() != null
                    && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAllocationRSA() != 0;
        }
        return false;
    }

    public static boolean hasAidesFamiliales(Demandeur demandeur) {
        return demandeur.getRessourcesFinancieresAvantSimulation() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales() != null;
    }

    public static boolean hasAllocationAdultesHandicapes(Demandeur demandeur) {
        return demandeur.getRessourcesFinancieresAvantSimulation() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAllocationAAH() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAllocationAAH() > 0;
    }

    public static boolean hasPrimeActivite(Demandeur demandeur) {
        return demandeur.getRessourcesFinancieresAvantSimulation() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().hasPrimeActivite() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().hasPrimeActivite()
                .booleanValue()
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getPrimeActivite() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getPrimeActivite() > 0;
    }

    public static boolean hasAllocationLogement(Demandeur demandeur) {
        return demandeur.getRessourcesFinancieresAvantSimulation() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesLogement() != null
                && (demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesLogement().getMoisNMoins1() > 0);
    }

    public static boolean hasAllocationSolidariteSpecifique(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        return ressourcesFinancieres != null && ressourcesFinancieres.getAidesPoleEmploi() != null
                && ressourcesFinancieres.getAidesPoleEmploi().getAllocationASS() != null
                && ressourcesFinancieres.getAidesPoleEmploi().getAllocationASS().getAllocationJournaliereNet() != null
                && ressourcesFinancieres.getAidesPoleEmploi().getAllocationASS().getAllocationJournaliereNet() > 0;
    }

    public static boolean hasAllocationARE(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        return ressourcesFinancieres != null && ressourcesFinancieres.getAidesPoleEmploi() != null
                && ressourcesFinancieres.getAidesPoleEmploi().getAllocationARE() != null
                && ressourcesFinancieres.getAidesPoleEmploi().getAllocationARE().getAllocationJournaliereBrute() != null
                && ressourcesFinancieres.getAidesPoleEmploi().getAllocationARE().getAllocationJournaliereBrute() > 0
                && ressourcesFinancieres.getAidesPoleEmploi().getAllocationARE()
                .getSalaireJournalierReferenceBrut() != null
                && ressourcesFinancieres.getAidesPoleEmploi().getAllocationARE().getSalaireJournalierReferenceBrut() > 0
                && ressourcesFinancieres.getAidesPoleEmploi().getAllocationARE().getNombreJoursRestants() != null
                && ressourcesFinancieres.getAidesPoleEmploi().getAllocationARE().getNombreJoursRestants() > 0;
    }

    public static boolean hasPensionInvalidite(Demandeur demandeur) {
        return demandeur.getRessourcesFinancieresAvantSimulation() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCPAM() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCPAM()
                .getPensionInvalidite() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCPAM().getPensionInvalidite() > 0;
    }

    public static boolean hasRevenusImmobilier(Demandeur demandeur) {
        return hasRevenusImmobilier(demandeur.getRessourcesFinancieresAvantSimulation());
    }

    public static boolean hasRevenusImmobilier(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        return ressourcesFinancieres != null && ressourcesFinancieres.getRevenusImmobilier3DerniersMois() != null
                && ressourcesFinancieres.getRevenusImmobilier3DerniersMois() > 0;
    }

    public static boolean hasRevenusTravailleurIndependant(Demandeur demandeur) {
        return hasRevenusTravailleurIndependant(demandeur.getRessourcesFinancieresAvantSimulation());
    }

    public static boolean hasRevenusTravailleurIndependant(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        return ressourcesFinancieres != null
                && ressourcesFinancieres.getChiffreAffairesIndependantDernierExercice() != null
                && ressourcesFinancieres.getChiffreAffairesIndependantDernierExercice() > 0;
    }

    public static boolean hasRevenusMicroEntreprise(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        return ressourcesFinancieres != null
                && ressourcesFinancieres.getBeneficesMicroEntrepriseDernierExercice() != null
                && ressourcesFinancieres.getBeneficesMicroEntrepriseDernierExercice() > 0;
    }

    public static boolean hasPensionRetraite(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        return ressourcesFinancieres != null && ressourcesFinancieres.getPensionRetraite() != null
                && ressourcesFinancieres.getPensionRetraite() > 0;
    }

    public static boolean hasFuturSalaire(Demandeur demandeur) {
        return hasFuturSalaire(demandeur.getFutureFormation());
    }

    public static boolean hasFuturSalaire(FutureFormation futureFormation) {
        return futureFormation != null && futureFormation.getSalaire() != null
                && futureFormation.getSalaire().getMontantMensuelNet() > 0;
    }

    public static boolean hasSalaire(RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        return ressourcesFinancieres != null && ressourcesFinancieres.getSalaire() != null
                && ressourcesFinancieres.getSalaire().getMontantMensuelNet() > 0;
    }

    public static boolean hasTravailleAuCoursDerniersMoisAvantSimulation(Demandeur demandeur) {
        return hasTravailleAuCoursDerniersMoisAvantSimulation(
                demandeur.getRessourcesFinancieresAvantSimulation());
    }

    public static boolean hasTravailleAuCoursDerniersMoisAvantSimulation(
            RessourcesFinancieresAvantSimulation ressourcesFinancieres) {
        return ressourcesFinancieres != null && ressourcesFinancieres.getHasTravailleAuCoursDerniersMois() != null
                && ressourcesFinancieres.getHasTravailleAuCoursDerniersMois().booleanValue();
    }

    public static boolean hasAllocationsFamiliales(Demandeur demandeur) {
        return hasAidesFamiliales(demandeur) && demandeur.getRessourcesFinancieresAvantSimulation()
                .getAidesCAF().getAidesFamiliales().getAllocationsFamiliales() != 0;
    }

    public static boolean hasComplementFamilial(Demandeur demandeur) {
        return hasAidesFamiliales(demandeur) && demandeur.getRessourcesFinancieresAvantSimulation()
                .getAidesCAF().getAidesFamiliales().getComplementFamilial() != 0;
    }

    public static boolean hasAllocationSoutienFamilial(Demandeur demandeur) {
        return hasAidesFamiliales(demandeur) && demandeur.getRessourcesFinancieresAvantSimulation()
                .getAidesCAF().getAidesFamiliales().getAllocationSoutienFamilial() != 0;
    }

    public static boolean hasPrestationAccueilJeuneEnfant(Demandeur demandeur) {
        return hasAidesFamiliales(demandeur) && demandeur.getRessourcesFinancieresAvantSimulation()
                .getAidesCAF().getAidesFamiliales().getPrestationAccueilJeuneEnfant() != 0;
    }

    public static boolean hasPensionsAlimentaires(Demandeur demandeur) {
        return hasAidesFamiliales(demandeur) && demandeur.getRessourcesFinancieresAvantSimulation()
                .getAidesCAF().getAidesFamiliales().getPensionsAlimentairesFoyer() != 0;
    }

    public static float getMontantAideLogementDeclare(Demandeur demandeur) {
        float montant = 0;
        if (hasAllocationLogement(demandeur))
            montant = getAllocationsLogementSur1Mois(demandeur.getRessourcesFinancieresAvantSimulation()
                    .getAidesCAF().getAidesLogement());
        return montant;
    }

    public static int getProchaineDeclarationTrimestrielle(Demandeur demandeur) {
        int prochaineDeclarationTrimestrielle = 1;
        if (demandeur.getRessourcesFinancieresAvantSimulation() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .getProchaineDeclarationTrimestrielle() != null) {
            prochaineDeclarationTrimestrielle = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                    .getProchaineDeclarationTrimestrielle();
        }
        return prochaineDeclarationTrimestrielle;
    }
}
