package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Objects;

public class FutureFormationUtile {

    private FutureFormationUtile() {
    }

    public static float calculDureeFormation(Demandeur demandeur, LocalDate dateDebutSimulation) {
        if (Objects.nonNull(demandeur.getFutureFormation())) {
            float nbMois = demandeur.getFutureFormation().getDureeFormationEnMois();
            Calendar cal = Calendar.getInstance();
            float maxWeeknumber = 0;
            for (int i = 0; i < nbMois; i++) {
                cal.set(Calendar.YEAR, dateDebutSimulation.getYear());
                cal.set(Calendar.DAY_OF_MONTH, dateDebutSimulation.getDayOfMonth());
                cal.set(Calendar.MONTH, i);
                maxWeeknumber = cal.getActualMaximum(Calendar.WEEK_OF_MONTH);
                // Month value starts from 0 to 11 for Jan to Dec
            }
            return maxWeeknumber * demandeur.getFutureFormation().getDureeFormationHebdomadaire();
        }
        return 0;
    }
}
