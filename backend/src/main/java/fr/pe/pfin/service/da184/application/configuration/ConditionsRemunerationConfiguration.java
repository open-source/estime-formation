package fr.pe.pfin.service.da184.application.configuration;

import fr.pe.pfin.service.da184.infrastructure.ConditionsRemunerationAdapteur;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class ConditionsRemunerationConfiguration {

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public ConditionsRemunerationAdapteur conditionsRemunerationAdapteur() {
        return new ConditionsRemunerationAdapteur();
    }
}
