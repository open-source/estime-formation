package fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations;

public enum StatutMaritalEnum {
	MARIE("marie"), CELIBATAIRE("celibataire");

	private final String libelle;

	StatutMaritalEnum(String libelle) {
		this.libelle = libelle;
	}

	public String getLibelle() {
		return libelle;
	}
}
