package fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages;

public enum BadRequestMessages {

	CHAMP_OBLIGATOIRE("Le champ %s est obligatoire"),
	DEMANDEUR_OBLIGATOIRE("Des informations sur le demandeur sont obligatoires."),
	NATIONALITE_INCORRECTE("La nationalité du demandeur doit avoir pour valeur : %s"),
	MONTANT_INCORRECT_INFERIEUR_EGAL_ZERO("Le montant de %s doit être supérieur à 0."),
	SALAIRE_MENSUEL_NET_ZERO("Le salaire mensuel net ne peut pas être égale à 0"),
	VALEUR_INCORRECT_PROCHAINE_DECLARATION_TRIMESTRIELLE(
			"La valeur de prochaine déclaration trimestrielle est incorrect (inférieure à 0 ou supérieur à 3) : %s");

	private final String message;

	BadRequestMessages(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
