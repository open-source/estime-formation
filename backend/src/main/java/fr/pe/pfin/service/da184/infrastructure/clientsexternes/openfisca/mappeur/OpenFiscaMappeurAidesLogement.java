package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaFamille;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

public class OpenFiscaMappeurAidesLogement {

    private OpenFiscaMappeurAidesLogement() {
    }

    public static float getMontantAideLogement(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getFamilles() != null && openFiscaRoot.getFamilles().get(ParametresOpenFisca.FAMILLE1) != null) {
            OpenFiscaPeriodes openFiscaAideLogement = openFiscaRoot.getFamilles().get(ParametresOpenFisca.FAMILLE1).getAideLogement();
            String periodeFormateeAideLogement = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaAideLogement != null && openFiscaAideLogement.get(periodeFormateeAideLogement) != null) {
                Double montantAideLogement = (Double) openFiscaAideLogement.get(periodeFormateeAideLogement);
                return BigDecimal.valueOf(montantAideLogement).setScale(0, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }
    public static String getTypeAideLogement(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        String typeAideLogement = "";
        if (openFiscaRoot.getFamilles() != null && openFiscaRoot.getFamilles().get(ParametresOpenFisca.FAMILLE1) != null) {
            OpenFiscaFamille openFiscaFamille = openFiscaRoot.getFamilles().get(ParametresOpenFisca.FAMILLE1);
            String periodeFormateeAideLogement = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (isOpenFiscaAideLogement(openFiscaFamille, periodeFormateeAideLogement)) {
                typeAideLogement = AideEnum.AIDES_LOGEMENT.getCode();
            }
        }
        return typeAideLogement;
    }

    private static boolean isOpenFiscaAideLogement(OpenFiscaFamille openFiscaFamille, String periodeFormateeAideLogement) {
        OpenFiscaPeriodes openFiscaAideLogement = openFiscaFamille.getAideLogement();
        if (openFiscaAideLogement != null) {
            Float openFiscaAideLogementMontant = (Float) openFiscaAideLogement.get(periodeFormateeAideLogement);
            if (openFiscaAideLogementMontant != null) {
                double montantAideLogement = openFiscaAideLogementMontant.doubleValue();
                return BigDecimal.valueOf(montantAideLogement).setScale(0, RoundingMode.HALF_UP)
                        .floatValue() > 0;
            }
        }
        return false;
    }
}
