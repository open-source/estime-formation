package fr.pe.pfin.service.da184.infrastructure.database.redis.repositories;

import fr.pe.pfin.service.da184.infrastructure.database.redis.models.BRSAData;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BRSADataRepository extends ListCrudRepository<BRSAData, String> {
}
