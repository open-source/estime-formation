package fr.pe.pfin.service.da184.application.services;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.ConditionsRemunerations;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SituationRemuneration;
import fr.pe.pfin.service.da184.domaine.use_cases.ObtenirConditionsRemuneration;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class ConditionsRemunerationService {

    private final ObtenirConditionsRemuneration obtenirConditionsRemuneration;

    public ConditionsRemunerationService(ObtenirConditionsRemuneration obtenirConditionsRemuneration) {
        this.obtenirConditionsRemuneration = obtenirConditionsRemuneration;
    }

    public List<ConditionsRemunerations> getConditionsList(SituationRemuneration situationRemuneration) throws URISyntaxException, IOException {
        return this.obtenirConditionsRemuneration.executer(situationRemuneration);
    }
}
