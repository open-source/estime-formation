package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

import static fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.ParametresOpenFisca.FAMILLE1;

public class OpenFiscaMappeurRSA {

    private OpenFiscaMappeurRSA() {
    }

    public static float getMontantRSA(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getFamilles() != null && openFiscaRoot.getFamilles().get(FAMILLE1) != null) {
            OpenFiscaPeriodes openFiscaRevenuSolidariteActive = openFiscaRoot.getFamilles().get(FAMILLE1).getRevenuSolidariteActive();
            String periodeFormateeRSA = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaRevenuSolidariteActive != null && openFiscaRevenuSolidariteActive.get(periodeFormateeRSA) != null) {
                Double montantRSA = (Double) openFiscaRevenuSolidariteActive.get(periodeFormateeRSA);
                return BigDecimal.valueOf(montantRSA).setScale(0, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }
}
