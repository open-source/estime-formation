package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;

import java.time.LocalDate;

public class OpenFiscaMappeurASS {

    private OpenFiscaMappeurASS() {
    }

    public static void addASSOpenFiscaIndividu(OpenFiscaIndividu openFiscaIndividu, Demandeur demandeur, LocalDate dateDebutSimulation) {
        openFiscaIndividu.setAllocationSolidariteSpecifique(OpenFiscaMappeurPeriode.creerPeriodesOpenFiscaASS(demandeur, dateDebutSimulation));
    }
}
