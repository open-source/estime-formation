package fr.pe.pfin.service.da184.application.ui.rest.ressource;

public class AllocationsLogement {

	private float moisN;
	private float moisNMoins1;

	public float getMoisN() {
		return moisN;
	}

	public void setMoisN(float moisN) {
		this.moisN = moisN;
	}

	public float getMoisNMoins1() {
		return moisNMoins1;
	}

	public void setMoisNMoins1(float moisNMoins1) {
		this.moisNMoins1 = moisNMoins1;
	}


	@Override
	public String toString() {
		return "allocationsLogement [moisN=" + moisN + ", moisNMoins1=" + moisNMoins1 + "]";
	}
}
