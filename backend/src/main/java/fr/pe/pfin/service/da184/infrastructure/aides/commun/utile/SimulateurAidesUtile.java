package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;

import java.time.LocalDate;
import java.util.Objects;

public class SimulateurAidesUtile {

    public static final int NOMBRE_MOIS_MAX_A_SIMULER = 6;

    private SimulateurAidesUtile() {
    }

    public static LocalDate getDateDebutSimulation(LocalDate dateDemandeSimulation) {
        return DateUtile.getDatePremierJourDuMois(dateDemandeSimulation.plusMonths(1));
    }

    public static int getNombreMoisASimuler(Demandeur demandeurEmploi) {
        if (Objects.nonNull(demandeurEmploi.getFutureFormation())) {
            int dureeFormationEnMois = (int) demandeurEmploi.getFutureFormation().getDureeFormationEnMois();
            if (dureeFormationEnMois < NOMBRE_MOIS_MAX_A_SIMULER) {
                return dureeFormationEnMois;
            }
        }
        return NOMBRE_MOIS_MAX_A_SIMULER;
    }

    public static boolean isPremierMois(int numeroMoisSimule) {
        return numeroMoisSimule == 1;
    }

    public static boolean isSecondMois(int numeroMoisSimule) {
        return numeroMoisSimule == 2;
    }

    public static boolean isCinqDerniersMois(int numeroMoisSimule) {
        return numeroMoisSimule >= 2;
    }

}
