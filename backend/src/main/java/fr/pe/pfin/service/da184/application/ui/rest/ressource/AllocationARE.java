package fr.pe.pfin.service.da184.application.ui.rest.ressource;

import com.fasterxml.jackson.annotation.JsonProperty;


public class AllocationARE {

    private Float allocationJournaliereBrute;
    private Float allocationJournaliereBruteTauxReduit;
    private Float allocationJournaliereBruteTauxPlein;
    private Float allocationMensuelleNet;
    private Float salaireJournalierReferenceBrut;
    private Integer nombreJoursRestants;
    @JsonProperty("hasDegressiviteAre")
    private Boolean hasDegressiviteAre;
    private Boolean isTauxReduit;

    public Float getAllocationMensuelleNet() {
        return allocationMensuelleNet;
    }

    public void setAllocationMensuelleNet(Float allocationMensuelleNet) {
        this.allocationMensuelleNet = allocationMensuelleNet;
    }

    public Float getAllocationJournaliereBrute() {
        return allocationJournaliereBrute;
    }

    public void setAllocationJournaliereBrute(Float allocationJournaliereBrute) {
        this.allocationJournaliereBrute = allocationJournaliereBrute;
    }

    public Float getAllocationJournaliereBruteTauxReduit() {
        return allocationJournaliereBruteTauxReduit;
    }

    public void setAllocationJournaliereBruteTauxReduit(Float allocationJournaliereBruteTauxReduit) {
        this.allocationJournaliereBruteTauxReduit = allocationJournaliereBruteTauxReduit;
    }

    public Float getAllocationJournaliereBruteTauxPlein() {
        return allocationJournaliereBruteTauxPlein;
    }

    public void setAllocationJournaliereBruteTauxPlein(Float allocationJournaliereBruteTauxPlein) {
        this.allocationJournaliereBruteTauxPlein = allocationJournaliereBruteTauxPlein;
    }

    public Float getSalaireJournalierReferenceBrut() {
        return salaireJournalierReferenceBrut;
    }

    public void setSalaireJournalierReferenceBrut(Float salaireJournalierReferenceBrut) {
        this.salaireJournalierReferenceBrut = salaireJournalierReferenceBrut;
    }

    public Integer getNombreJoursRestants() {
        return nombreJoursRestants;
    }

    public void setNombreJoursRestants(Integer nombreJoursRestants) {
        this.nombreJoursRestants = nombreJoursRestants;
    }


    public Boolean getHasDegressiviteAre() {
        return hasDegressiviteAre;
    }

    public void setHasDegressiviteAre(Boolean hasDegressiviteAre) {
        this.hasDegressiviteAre = hasDegressiviteAre;
    }

    public Boolean getIsTauxReduit() {
        return isTauxReduit;
    }

    public void setIsTauxReduit(Boolean isTauxReduit) {
        this.isTauxReduit = isTauxReduit;
    }
}
