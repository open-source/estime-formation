package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.NationaliteEnum;

import java.util.Arrays;

public class NationalitesUtile {

    private NationalitesUtile() {
    }

    public static String getListeFormateeNationalitesPossibles() {
        return String.join(" / ", Arrays.asList(NationaliteEnum.values()).stream().map(Object::toString).toList());
    }
}
