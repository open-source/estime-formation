package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;

public class BeneficiaireAidesUtile {

    public static final float ALLOCATION_CHOMAGE_MAX_ELIGIBILITE_AIDE = 29.56f;
    public static final float ALLOCATION_CHOMAGE_MAX_ELIGIBILITE_AIDE_MAYOTTE = 14.77f;

    private BeneficiaireAidesUtile() {
    }

    public static boolean isBeneficiaireAides(Demandeur demandeur) {
        return demandeur.getBeneficiaireAides() != null;
    }

    public static boolean isBeneficiaireAideFTouCAF(Demandeur demandeur) {
        return isBeneficiaireAideMinimaSocial(demandeur) || isBeneficiaireAREMini(demandeur);
    }

    public static boolean isBeneficiaireCAF(Demandeur demandeur) {
        return isBeneficiaireAAH(demandeur) || isBeneficiaireRSA(demandeur);
    }

    public static boolean isBeneficiaireARE(Personne personne) {
        return personne.getBeneficiaireAides() != null && personne.getBeneficiaireAides().isBeneficiaireARE();
    }

    public static boolean isBeneficiaireARE(Demandeur demandeur) {
        return demandeur.getBeneficiaireAides() != null
                && demandeur.getBeneficiaireAides().isBeneficiaireARE();
    }

    public static boolean isBeneficiaireASS(Demandeur demandeur) {
        return demandeur.getBeneficiaireAides() != null
                && demandeur.getBeneficiaireAides().isBeneficiaireASS();
    }

    public static boolean isBeneficiaireASS(Personne personne) {
        return personne.getBeneficiaireAides() != null && personne.getBeneficiaireAides().isBeneficiaireASS();
    }

    public static boolean isBeneficiaireAAH(Demandeur demandeur) {
        return demandeur.getBeneficiaireAides() != null
                && demandeur.getBeneficiaireAides().isBeneficiaireAAH();
    }

    public static boolean isBeneficiairePensionInvalidite(Demandeur demandeur) {
        return demandeur.getBeneficiaireAides().isBeneficiairePensionInvalidite();
    }

    public static boolean isBeneficiaireAideMinimaSocial(Demandeur demandeur) {
        return isBeneficiaireAides(demandeur) && (demandeur.getBeneficiaireAides().isBeneficiaireAAH()
                || demandeur.getBeneficiaireAides().isBeneficiaireASS()
                || demandeur.getBeneficiaireAides().isBeneficiaireRSA());
    }

    private static boolean isBeneficiaireAREMini(Demandeur demandeur) {
        if (isBeneficiaireAides(demandeur) && demandeur.getRessourcesFinancieresAvantSimulation() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi()
                .getAllocationARE() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE()
                .getAllocationJournaliereBrute() != null) {
            float allocationJournaliereBrute = demandeur.getRessourcesFinancieresAvantSimulation()
                    .getAidesPoleEmploi().getAllocationARE().getAllocationJournaliereBrute();
            return (!CodeDepartementUtile.isDeMayotte(demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().getCodePostal())
                    && allocationJournaliereBrute <= ALLOCATION_CHOMAGE_MAX_ELIGIBILITE_AIDE)
                    || (CodeDepartementUtile.isDeMayotte(demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().getCodePostal())
                    && allocationJournaliereBrute <= ALLOCATION_CHOMAGE_MAX_ELIGIBILITE_AIDE_MAYOTTE);
        }
        return false;
    }

    public static boolean isBeneficiaireRSA(Demandeur demandeur) {
        return isBeneficiaireAideFTouCAF(demandeur) && isFoyerBeneficiaireRSA(demandeur);
    }

    private static boolean isFoyerBeneficiaireRSA(Demandeur demandeur) {
        return isDemandeurBeneficiaireRSA(demandeur) || isPersonneAChargeBeneficiaireRSA(demandeur)
                || isConjointBeneficiaireRSA(demandeur);
    }

    private static boolean isDemandeurBeneficiaireRSA(Demandeur demandeur) {
        return demandeur.getBeneficiaireAides() != null
                && demandeur.getBeneficiaireAides().isBeneficiaireRSA();
    }

    private static boolean isPersonneAChargeBeneficiaireRSA(Demandeur demandeur) {
        boolean isPersonneAChargeBeneficiaireRSA = false;
        if (SituationFamilialeUtile.hasPersonnesACharge(demandeur)) {
            for (Personne personneACharge : demandeur.getSituationFamiliale().getPersonnesACharge()) {
                if (personneACharge.getBeneficiaireAides() != null
                        && personneACharge.getBeneficiaireAides().isBeneficiaireRSA()) {
                    isPersonneAChargeBeneficiaireRSA = true;
                    break;
                }
            }
        }
        return isPersonneAChargeBeneficiaireRSA;
    }

    private static boolean isConjointBeneficiaireRSA(Demandeur demandeur) {
        boolean isConjointBeneficiaireRSA = false;
        if (SituationFamilialeUtile.isEnCouple(demandeur)) {
            Personne conjoint = demandeur.getSituationFamiliale().getConjoint();
            isConjointBeneficiaireRSA = conjoint.getBeneficiaireAides() != null
                    && conjoint.getBeneficiaireAides().isBeneficiaireRSA();
        }
        return isConjointBeneficiaireRSA;
    }

    public static boolean isBeneficiaireAucuneAide(Demandeur demandeur) {
        return !demandeur.getBeneficiaireAides().isBeneficiaireAAH()
                && !demandeur.getBeneficiaireAides().isBeneficiaireASS()
                && !demandeur.getBeneficiaireAides().isBeneficiaireARE()
                && !demandeur.getBeneficiaireAides().isBeneficiaireRSA()
                && !demandeur.getBeneficiaireAides().isBeneficiairePensionInvalidite();
    }
}
