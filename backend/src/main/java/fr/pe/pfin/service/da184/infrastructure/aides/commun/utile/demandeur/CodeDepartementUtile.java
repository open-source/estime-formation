package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.StringUtile;

public class CodeDepartementUtile {

    public static final String CODE_DEPARTEMENT_MAYOTTE = "976";
    public static final int CODE_DEPARTEMENT_VAL_OISE = 95;

    private CodeDepartementUtile() {
    }

    public static boolean isDesDOM(String codePostal) {
        return getCodeDepartement(codePostal).length() == 3;
    }

    public static boolean isDeMayotte(String codePostal) {
        return getCodeDepartement(codePostal).equals(CODE_DEPARTEMENT_MAYOTTE);
    }

    private static String getCodeDepartement(String codePostal) {
        String deuxPremiersCaracteresCodePostal = StringUtile.getPremiersCaracteres(codePostal, 2);
        if (StringUtile.isNumeric(deuxPremiersCaracteresCodePostal)
                && Integer.parseInt(deuxPremiersCaracteresCodePostal) > CODE_DEPARTEMENT_VAL_OISE) {
            return StringUtile.getPremiersCaracteres(codePostal, 3);
        }
        return deuxPremiersCaracteresCodePostal;
    }
}
