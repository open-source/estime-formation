package fr.pe.pfin.service.da184.application.ui.rest.valideur;

import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.BadRequestMessages;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.FutureFormation;

public class FutureFormationApiValideur {

    private FutureFormationApiValideur() {
    }

    public static void controlerDonnees(FutureFormation futureFormation) {
        if (futureFormation == null) {
            throw new BadRequestException(
                    String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "futureFormation"));
        } else {

            if (futureFormation.getSalaire() == null) {
                throw new BadRequestException(
                        String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "salaire de futureFormation"));
            }

            if (futureFormation.getSalaire().getMontantMensuelNet() == 0) {
                throw new BadRequestException(BadRequestMessages.SALAIRE_MENSUEL_NET_ZERO.getMessage());
            }
        }
    }
}
