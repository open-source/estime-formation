package fr.pe.pfin.service.da184.application.configuration;

import fr.pe.pfin.service.da184.infrastructure.database.redis.models.BRSAData;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@Configuration
@EnableRedisRepositories(basePackages = "fr.pe.pfin.service.da184.infrastructure.database.redis.repositories")
public class RedisConfiguration {

    @Bean
    public RedisTemplate<Long, BRSAData> redisTemplate(LettuceConnectionFactory connectionFactory) {
        RedisTemplate<Long, BRSAData> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        return template;
    }
}
