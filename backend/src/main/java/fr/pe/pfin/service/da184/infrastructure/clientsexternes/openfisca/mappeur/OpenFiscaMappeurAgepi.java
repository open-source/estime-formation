package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.FutureFormationUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Objects;

import static fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.ParametresOpenFisca.DEMANDEUR;

public class OpenFiscaMappeurAgepi {

    private static final String INTENSITE_ACTIVITE = "hebdomadaire";
    private static final String CATEGORIE_DEMANDEUR_EMPLOI_4 = "categorie_4";
    private static final String LIEU_EMPLOI_OU_FORMATION = "metropole_hors_corse";
    private static final String TYPE_CONTRAT = "formation";

    private OpenFiscaMappeurAgepi() {
    }

    public static float getMontantAgepi(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getIndividus() != null && openFiscaRoot.getIndividus().get(DEMANDEUR) != null) {
            OpenFiscaPeriodes openFiscaAgepi = openFiscaRoot.getIndividus().get(DEMANDEUR).getAgepi();
            String periodeFormateeAgepi = OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaAgepi != null && openFiscaAgepi.get(periodeFormateeAgepi) != null) {
                Double montantAgepi = (Double) openFiscaAgepi.get(periodeFormateeAgepi);
                return BigDecimal.valueOf(montantAgepi).setScale(0, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }

    public static void addAgepiOpenFiscaIndividu(OpenFiscaIndividu openFiscaIndividu, Demandeur demandeur, LocalDate dateDebutSimulation) {

        openFiscaIndividu.setIntensiteActivite(
                OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(INTENSITE_ACTIVITE, dateDebutSimulation));

        if (Objects.nonNull(demandeur.getFutureFormation())) {
            openFiscaIndividu.setTempsDeTravail(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(
                    demandeur.getFutureFormation().getDureeFormationHebdomadaire(), dateDebutSimulation));
        }

        openFiscaIndividu.setCategorieDemandeur(
                OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(CATEGORIE_DEMANDEUR_EMPLOI_4, dateDebutSimulation));

        openFiscaIndividu.setStagiaire(
                OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(true, dateDebutSimulation));

        openFiscaIndividu.setLieuEmploiOuFormation(
                OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(LIEU_EMPLOI_OU_FORMATION, dateDebutSimulation));

        openFiscaIndividu.setDebutContratTravail(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(
                DateUtile.convertDateToStringOpenFisca(dateDebutSimulation), dateDebutSimulation));

        openFiscaIndividu.setAgepiDateDemande(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(
                DateUtile.convertDateToStringOpenFisca(dateDebutSimulation), dateDebutSimulation));

        openFiscaIndividu.setTypeContratTravail(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(
                TYPE_CONTRAT, dateDebutSimulation));

        openFiscaIndividu.setDureeFormation(OpenFiscaMappeurPeriode.creerPeriodeUniqueOpenFisca(
                FutureFormationUtile.calculDureeFormation(demandeur, dateDebutSimulation), dateDebutSimulation));

        openFiscaIndividu.setAgepi(OpenFiscaMappeurPeriode.creerPeriodeCalculeeUniqueOpenFisca(dateDebutSimulation));
    }
}
