package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.SimulateurAidesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.PeriodeTravailleeAvantSimulationUtile;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.Optional;

public class AllocationAdultesHandicapesUtile {

    // Source : https://www.service-public.fr/particuliers/vosdroits/F21615
    public static final float MONTANT_SALAIRE_BRUT_PALIER = 503.68f;
    public static final float POURCENTAGE_SALAIRE_PALIER_1 = 0.2f;
    public static final float POURCENTAGE_SALAIRE_PALIER_2 = 0.6f;


    private AllocationAdultesHandicapesUtile() {
    }

    /**
     * Méthode permettant de calculer le montant de l'AAH sur la période de
     * simulation de N mois Voici la règle sur 6 mois :
     * <p>
     * si déjà travaillé 6 mois, AAH réduite cumulée avec salaire en M1 M2 M3 M4 M5
     * M6 si déjà travaillé 5 mois, AAH + salaire en M1 puis AAH réduite cumulée
     * avec salaire en M2 M3 M4 M5 M6 si déjà travaillé 4 mois, AAH + salaire en M1
     * et M2 puis AAH réduite cumulée avec salaire en M3 M4 M5 M6 si déjà travaillé
     * 3 mois, AAH + salaire en M1 M2 M3 puis AAH réduite cumulée avec salaire en M4
     * M5 M6 si déjà travaillé 2 mois, AAH + salaire en M1 M2 M3 M4 puis AAH réduite
     * cumulée avec salaire en M5 M6 si déjà travaillé 1 mois, AAH + salaire en M1
     * M2 M3 M4 M5 puis AAH réduite cumulée avec salaire en M6
     */
    public static void simulerAide(Map<String, Aide> aidesPourCeMois, int numeroMoisSimule, Demandeur demandeur) {
        float montant = calculerMontant(demandeur, numeroMoisSimule - 1);
        if (montant > 0) {
            aidesPourCeMois.put(AideEnum.ALLOCATION_ADULTES_HANDICAPES.getCode(), creerAide(montant));
        }
    }

    public static float calculerMontant(Demandeur demandeur, int numeroMoisSimule) {
        if (isMontantAReduire(demandeur, numeroMoisSimule)) {
            return calculerMontantReduit(demandeur);
        } else {
            // le demandeur cumule son AAH avant la simulation
            return demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAllocationAAH();
        }
    }

    public static float getMontantAAHAvantSimulation(Demandeur demandeur) {
        return demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAllocationAAH();
    }

    private static boolean isMontantAReduire(Demandeur demandeur, int numeroMoisSimule) {
        boolean isMontantReduit = false;
        int nombreMoisTravaillesDerniersMois = PeriodeTravailleeAvantSimulationUtile
                .getNombreMoisTravaillesAuCoursDes6DerniersMoisAvantSimulation(demandeur);

        if (numeroMoisSimule >= 0 && nombreMoisTravaillesDerniersMois > 0
                && (SimulateurAidesUtile.NOMBRE_MOIS_MAX_A_SIMULER
                - numeroMoisSimule) <= nombreMoisTravaillesDerniersMois) {
            isMontantReduit = true;
        }
        return isMontantReduit;
    }

    /**
     * Méthode permettant de calculer le montant réduit de l'AAH si le salaire brut
     * est inférieur à 466,38€, alors montant AAH moins 20% du salaire brut si le
     * salaire brut est supérieur à 466,38€, alors montant AAH moins 60% du salaire
     * brut
     *
     * @return montant AAH réduit
     */
    private static float calculerMontantReduit(Demandeur demandeur) {
        BigDecimal montantAllocationAAHAvantSimulation = BigDecimal
                .valueOf(demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAllocationAAH());

        BigDecimal partSalaireADeduire = calculerPartSalairePourDeduction(demandeur);
        float montantReduitAllocationAAH = montantAllocationAAHAvantSimulation.subtract(partSalaireADeduire)
                .setScale(0, RoundingMode.DOWN).floatValue();

        if (montantReduitAllocationAAH > 0) {
            return montantReduitAllocationAAH;
        } else {
            return 0;
        }
    }

    private static BigDecimal calculerPartSalairePourDeduction(Demandeur demandeur) {
        BigDecimal salaireBrut = BigDecimal
                .valueOf(demandeur.getFutureFormation().getSalaire().getMontantMensuelBrut());
        if (isSalaireInferieurOuEgalSalaireBrutPalier(salaireBrut)) {
            return salaireBrut.multiply(BigDecimal.valueOf(POURCENTAGE_SALAIRE_PALIER_1)).setScale(0,
                    RoundingMode.DOWN);
        }
        return salaireBrut.multiply(BigDecimal.valueOf(POURCENTAGE_SALAIRE_PALIER_2)).setScale(0, RoundingMode.DOWN);
    }

    private static boolean isSalaireInferieurOuEgalSalaireBrutPalier(BigDecimal salaireBrut) {
        return salaireBrut.compareTo(BigDecimal.valueOf(MONTANT_SALAIRE_BRUT_PALIER)) <= 0;
    }

    private static Aide creerAide(float montant) {
        return AideUtile.creerAide(AideEnum.ALLOCATION_ADULTES_HANDICAPES, Optional.of(OrganismeEnum.CAF),
                Optional.empty(), false, montant);
    }
}
