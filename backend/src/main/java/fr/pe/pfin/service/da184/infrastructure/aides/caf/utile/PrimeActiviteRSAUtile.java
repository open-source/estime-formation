package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;

import java.util.ArrayList;
import java.util.Optional;

public class PrimeActiviteRSAUtile {

    private PrimeActiviteRSAUtile() {
    }

    public static Aide creerAidePrimeActivite(float montantPrimeActivite, boolean isAideReportee) {
        ArrayList<String> messagesAlerte = new ArrayList<>();
        messagesAlerte.add(MessageInformatifEnum.PPA_AUTOMATIQUE_SI_BENEFICIAIRE_RSA.getMessage());
        return AideUtile.creerAide(AideEnum.PRIME_ACTIVITE, Optional.of(OrganismeEnum.CAF), Optional.of(messagesAlerte),
                isAideReportee, montantPrimeActivite);
    }
}
