package fr.pe.pfin.service.da184.domaine.use_cases;

import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.time.LocalDate;

@Service
public class ObtenirDonneesBRSA {
    private final DonneesBRSAPort donneesBRSAPort;

    public ObtenirDonneesBRSA(DonneesBRSAPort donneesBRSAPort) {
        this.donneesBRSAPort = donneesBRSAPort;
    }

    public InputStream executer(LocalDate dateDebut, LocalDate dateFin) {
        return this.donneesBRSAPort.extraireDonneesBRSA(dateDebut, dateFin);
    }

}
