package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;

import java.time.LocalDate;

public class OpenFiscaMappeurAAH {

    private OpenFiscaMappeurAAH() {
    }

    public static void addAAHOpenFiscaIndividu(OpenFiscaIndividu openFiscaIndividu, Demandeur demandeur, LocalDate dateDebutSimulation) {
        openFiscaIndividu.setAllocationAdulteHandicape(OpenFiscaMappeurPeriode.creerPeriodesOpenFiscaAAH(demandeur, dateDebutSimulation));
    }
}
