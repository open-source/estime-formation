package fr.pe.pfin.service.da184.application.ui.rest.valideur;

import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.BadRequestMessages;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;

public class DemandeurApiValideur {

    private DemandeurApiValideur() {
    }

    public static void validerDonneesEntreeServiceSimulerMesAides(Demandeur demandeur) {
        if (demandeur == null) {
            throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, BadRequestMessages.DEMANDEUR_OBLIGATOIRE.getMessage());
        } else {
            FutureFormationApiValideur.controlerDonnees(demandeur.getFutureFormation());
            BeneficiaireAidesApiValideur.controlerDonnees(demandeur);
            InformationsPersonnellesApiValideur.controlerDonnees(demandeur.getInformationsPersonnelles());
            SituationFamilialeApiValideur.controlerDonnees(demandeur.getSituationFamiliale(),
                    demandeur.getBeneficiaireAides());
        }
    }
}
