package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile;

import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;

public class StringUtile {

    public static final String EMPTY = "";
    private static final Pattern IS_NUMERIC = Pattern.compile("-?\\d+(\\.\\d+)?");

    private StringUtile() {
    }

    public static boolean isNumeric(String stringToCheck) {
        if (Objects.isNull(stringToCheck)) {
            return false;
        }
        return IS_NUMERIC.matcher(stringToCheck).matches();
    }

    public static String getPremiersCaracteres(String stringValue, int nombreCaracteres) {
        return stringValue.subSequence(0, nombreCaracteres).toString();
    }

    public static String generateRandomString() {
        UUID randomUUID = UUID.randomUUID();
        return randomUUID.toString().replace("_", "").substring(0, 15);
    }
}
