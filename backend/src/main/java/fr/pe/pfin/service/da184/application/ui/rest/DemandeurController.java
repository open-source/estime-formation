package fr.pe.pfin.service.da184.application.ui.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.pe.pfin.service.da184.application.services.ConditionsRemunerationService;
import fr.pe.pfin.service.da184.application.services.DemandeurService;
import fr.pe.pfin.service.da184.application.services.DonneesBRSAService;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.application.ui.rest.valideur.DemandeurApiValideur;
import fr.pe.pfin.service.da184.infrastructure.database.redis.utile.RedisUtile;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/demandeurs-emploi")
public class DemandeurController {

    private final DemandeurService demandeurService;
    private final DonneesBRSAService donneesBRSAService;
    private final ConditionsRemunerationService conditionsRemunerationService;
    private final RedisUtile redisUtile;

    public DemandeurController(DemandeurService demandeurService, DonneesBRSAService donneesBRSAService, ConditionsRemunerationService conditionsRemunerationService, RedisUtile redisUtile) {
        this.demandeurService = demandeurService;
        this.donneesBRSAService = donneesBRSAService;
        this.conditionsRemunerationService = conditionsRemunerationService;
        this.redisUtile = redisUtile;
    }

    @PostMapping(value = "/simulation-aides", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Simulation simulerMesAides(@RequestBody Demandeur demandeur) {
        DemandeurApiValideur.validerDonneesEntreeServiceSimulerMesAides(demandeur);
        return demandeurService.simulerMesAides(demandeur);
    }

    @PostMapping(value = "/send-notation", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpStatus sendNotation(@RequestBody Notation notationValues) {
        return this.donneesBRSAService.addNotation(notationValues.getIdRedis(), notationValues.getNotation());
    }

    @PostMapping(value = "/conditions-remuneration", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ConditionsRemunerations> getCondtionsRemuneration(@RequestBody SituationRemuneration situationRemuneration) throws URISyntaxException, IOException {
        return this.conditionsRemunerationService.getConditionsList(situationRemuneration);
    }

    @PostMapping(value = "/redis", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveInRedis(@RequestBody Demandeur demandeur) throws URISyntaxException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String response = redisUtile.saveBRSAAndRedis(demandeur, null);
        if (!response.trim().startsWith("{")) {
            response = objectMapper.writeValueAsString(response);
        }
        return response;
    }


}