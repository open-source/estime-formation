package fr.pe.pfin.service.da184.domaine.use_cases;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import org.springframework.stereotype.Service;

@Service
public class ObtenirSimulationAides {

    final SimulateurAidesPort simulateurAidesPort;

    public ObtenirSimulationAides(SimulateurAidesPort simulateurAidesPort) {
        this.simulateurAidesPort = simulateurAidesPort;
    }

    public Simulation executer(Demandeur demandeur) {
        return this.simulateurAidesPort.simuler(demandeur);
    }
}
