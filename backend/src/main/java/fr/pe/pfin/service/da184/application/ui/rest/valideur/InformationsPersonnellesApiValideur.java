package fr.pe.pfin.service.da184.application.ui.rest.valideur;

import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.BadRequestMessages;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.InformationsPersonnelles;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.NationaliteEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.NationalitesUtile;
import org.springframework.util.ObjectUtils;

import java.util.Arrays;

public class InformationsPersonnellesApiValideur {

    private InformationsPersonnellesApiValideur() {
    }

    public static void controlerDonnees(InformationsPersonnelles informationsPersonnelles) {
        if (informationsPersonnelles == null) {
            throw new BadRequestException(
                    String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "informationsPersonnelles"));
        } else {
            if (ObjectUtils.isEmpty(informationsPersonnelles.getLogement())) {
                throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                        "logement de informationsPersonnelles"));
            }
            if (ObjectUtils.isEmpty(informationsPersonnelles.getLogement().getCoordonnees())) {
                throw new BadRequestException(
                        String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "coordonnees de logement"));
            }
            if (ObjectUtils.isEmpty(informationsPersonnelles.getLogement().getCoordonnees().getCodePostal())) {
                throw new BadRequestException(
                        String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "codePostal de coordonnees"));
            }
            if (informationsPersonnelles.getDateNaissance() == null) {
                throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                        "dateNaissance de informationsPersonnelles"));
            }
            if (ObjectUtils.isEmpty(informationsPersonnelles.getNationalite())) {
                throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                        "nationalite de informationsPersonnelles"));
            } else {
                isValeurNationnaliteCorrecte(informationsPersonnelles.getNationalite());
            }
        }
    }

    private static void isValeurNationnaliteCorrecte(String nationalite) {
        boolean nationaliteCorrecte = Arrays.asList(NationaliteEnum.values()).stream()
                .anyMatch(nationaliteEnum -> nationaliteEnum.getValeur().equalsIgnoreCase(nationalite));
        if (!nationaliteCorrecte) {
            throw new BadRequestException(String.format(BadRequestMessages.NATIONALITE_INCORRECTE.getMessage(),
                    NationalitesUtile.getListeFormateeNationalitesPossibles()));
        }
    }
}
