package fr.pe.pfin.service.da184.domaine.use_cases;

import org.springframework.http.HttpStatus;

import java.io.InputStream;
import java.time.LocalDate;

public interface DonneesBRSAPort {
    InputStream extraireDonneesBRSA(LocalDate dateDebut, LocalDate dateFin);

    HttpStatus addNotation(String id, Integer notation);
}
