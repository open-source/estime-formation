package fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.MoisTravailleAvantSimulation;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.InformationsPersonnellesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.PeriodeTravailleeAvantSimulationUtile;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;

import static fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile.isSameDate;

public class AllocationSolidariteSpecifiqueUtile {

    private static final int NOMBRE_MOIS_MAX_ASS_MICRO_ENTREPRENEUR_ELIGIBLE = 3;
    private static final int NOMBRE_MOIS_MAX_ASS_ELIGIBLE = 1;
    private static final int NOMBRE_MOIS_MAX_ASS_BENEFICIAIRE_ACRE_ELIGIBLE = 6;

    private AllocationSolidariteSpecifiqueUtile() {
    }

    public static Optional<Aide> simulerAide(Demandeur demandeur, int numeroMoisSimule, LocalDate dateDebutSimulation) {
        LocalDate dateMoisASimuler = DateUtile.getDateMoisASimuler(dateDebutSimulation, numeroMoisSimule);
        float montantASS = calculerMontant(demandeur, dateMoisASimuler);
        if (montantASS > 0) {
            Aide aideAllocationSolidariteSpecifique = creerASS(montantASS);
                return Optional.of(aideAllocationSolidariteSpecifique);
        }
        return Optional.empty();
    }

    public static float calculerMontant(Demandeur demandeur, LocalDate moisSimule) {
        if (hasAllocationSolidariteSpecifiqueAvantSimulation(demandeur)) {
            LocalDate dateOuvertureDroitASS = demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().getDateDerniereOuvertureDroit();
            int nombreJoursDansLeMois = 0;
            if (dateOuvertureDroitASS != null && !DateUtile.isDateAvant(moisSimule, dateOuvertureDroitASS) && !isSameDate(moisSimule, dateOuvertureDroitASS)) {
                nombreJoursDansLeMois = DateUtile.getNombreJoursDansLeMois(Objects.requireNonNull(DateUtile.enleverMoisALocalDate(moisSimule, 1)));
                if(DateUtile.getMonthFromLocalDate(Objects.requireNonNull(DateUtile.enleverMoisALocalDate(moisSimule, 1))).equals(DateUtile.getMonthFromLocalDate(dateOuvertureDroitASS))){
                    if(dateOuvertureDroitASS.getDayOfMonth() != 1){
                        nombreJoursDansLeMois = DateUtile.getNombreJoursDansLeMois(Objects.requireNonNull(DateUtile.enleverMoisALocalDate(moisSimule, 1))) - (dateOuvertureDroitASS.getDayOfMonth() -1);
                    }
                }
                float montantJournalierNetSolidariteSpecifique = getMontantAllocationSolidariteSpecifiqueAvantSimulation(demandeur);
                return BigDecimal.valueOf(nombreJoursDansLeMois).multiply(BigDecimal.valueOf(montantJournalierNetSolidariteSpecifique)).setScale(0, RoundingMode.DOWN).floatValue();
            }
        }
        return 0;
    }

    public static float calculerMontantSiEligible(Demandeur demandeur, int numeroMoisPeriode, LocalDate dateDebutSimulation) {
        int nombreJoursDansLeMois = DateUtile.getNombreJoursDansLeMois(Objects.requireNonNull(DateUtile.ajouterMoisALocalDate(LocalDate.now(), numeroMoisPeriode)));
        if(numeroMoisPeriode == 0) {
            LocalDate dateOuvertureDroitASS = demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().getDateDerniereOuvertureDroit();
            nombreJoursDansLeMois = DateUtile.getNombreJoursDansLeMois(dateDebutSimulation) - dateOuvertureDroitASS.getDayOfMonth();
        }
        if (isEligible(numeroMoisPeriode, demandeur, dateDebutSimulation) && hasAllocationSolidariteSpecifiqueAvantSimulation(demandeur)) {
            float montantJournalierNetSolidariteSpecifique = getMontantAllocationSolidariteSpecifiqueAvantSimulation(demandeur);
            return BigDecimal.valueOf(nombreJoursDansLeMois).multiply(BigDecimal.valueOf(montantJournalierNetSolidariteSpecifique)).setScale(0, RoundingMode.DOWN).floatValue();
        }
        return 0;
    }

    private static boolean hasAllocationSolidariteSpecifiqueAvantSimulation(Demandeur demandeur) {
        return (demandeur != null && demandeur.getRessourcesFinancieresAvantSimulation() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS() != null
                && demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().getAllocationJournaliereNet() != null);
    }

    private static float getMontantAllocationSolidariteSpecifiqueAvantSimulation(Demandeur demandeur) {
        float montantAllocationSolidariteSpecifiqueAvantSimulation = 0;
        if (hasAllocationSolidariteSpecifiqueAvantSimulation(demandeur)) {
            montantAllocationSolidariteSpecifiqueAvantSimulation = demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                    .getAllocationJournaliereNet();
        }
        return montantAllocationSolidariteSpecifiqueAvantSimulation;
    }

    public static boolean isEligible(int numeroMoisSimule, Demandeur demandeur, LocalDate dateDebutSimulation) {
        return numeroMoisSimule - 1 < getNombreMoisEligibles(demandeur, dateDebutSimulation);
    }

    public static int getNombreMoisEligibles(Demandeur demandeur, LocalDate dateDebutSimulation) {
        if (InformationsPersonnellesUtile.hasMicroEntreprise(demandeur) && !InformationsPersonnellesUtile.isBeneficiaireACRE(demandeur)) {
            return getNombreMoisEligiblesMicroEntrepreneurNonACRE(InformationsPersonnellesUtile.getNombreMoisDepuisCreationEntreprise(demandeur, dateDebutSimulation));
        } else if (InformationsPersonnellesUtile.isBeneficiaireACRE(demandeur)
                && InformationsPersonnellesUtile.getNombreMoisDepuisCreationEntreprise(demandeur, dateDebutSimulation) < 12) {
            return getNombreMoisEligiblesMicroEntrepreneurBeneficiaireACRE(
                    InformationsPersonnellesUtile.getNombreMoisDepuisCreationEntreprise(demandeur, dateDebutSimulation));
        } else if (!InformationsPersonnellesUtile.hasMicroEntreprise(demandeur) && !InformationsPersonnellesUtile.isBeneficiaireACRE(demandeur)) {
            return getNombreMoisEligiblesCumulRestants(demandeur, dateDebutSimulation);
        }
        return 0;
    }

    public static int getNombreMoisEligiblesCumulRestants(Demandeur demandeur, LocalDate dateDebutSimulation) {
        int nombreMoisCumul = 0;
        int nombreMoisDePause = 0;
        int nombreMoisDepuisOuvertureASS = getNombreMoisDepuisOuvertureASS(demandeur, dateDebutSimulation);
        if (PeriodeTravailleeAvantSimulationUtile.hasSalairesAvantPeriodeSimulation(demandeur)) {
            MoisTravailleAvantSimulation[] mois = demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().getMois();
            for (int index = mois.length - 1; index >= 0; index--) {
                if (PeriodeTravailleeAvantSimulationUtile.isMoisTravaille(mois[index])) {
                    nombreMoisDePause = 0;
                    nombreMoisCumul++;
                } else {
                    nombreMoisDePause++;
                    if (nombreMoisDePause >= 3) {
                        nombreMoisCumul = 0;
                    }
                }
                if (index == nombreMoisDepuisOuvertureASS) {
                    nombreMoisDePause = 0;
                    nombreMoisCumul = 0;
                }
            }
        }
        return Math.max(0, NOMBRE_MOIS_MAX_ASS_ELIGIBLE - nombreMoisCumul);

    }

    private static int getNombreMoisDepuisOuvertureASS(Demandeur demandeur, LocalDate dateDebutSimulation) {
        LocalDate dateOuvertureDroitASS = demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().getDateDerniereOuvertureDroit();
        return DateUtile.getNbrMoisEntreDeuxLocalDates(dateOuvertureDroitASS, dateDebutSimulation);
    }

    public static Aide creerASS(float montantAide) {
        return AideUtile.creerAide(AideEnum.ALLOCATION_SOLIDARITE_SPECIFIQUE, Optional.of(OrganismeEnum.FT), Optional.empty(), false, montantAide);
    }

    public static float getMontantASSAvantSimulation(int numeroMoisMontantARecuperer, Demandeur demandeur, LocalDate dateDebutSimulation) {
        LocalDate moisAvantPeriodeSimulation = AideUtile.getMoisAvantSimulation(numeroMoisMontantARecuperer, dateDebutSimulation);
        return calculerMontant(demandeur, moisAvantPeriodeSimulation);
    }

    private static int getNombreMoisEligiblesMicroEntrepreneurBeneficiaireACRE(int nombreMoisDepuisCreationEntreprise) {
        if (nombreMoisDepuisCreationEntreprise > 6) {
            int nombreDeMoisApres6PremiersMois = nombreMoisDepuisCreationEntreprise - 6;
            return NOMBRE_MOIS_MAX_ASS_BENEFICIAIRE_ACRE_ELIGIBLE - nombreDeMoisApres6PremiersMois;
        }
        return NOMBRE_MOIS_MAX_ASS_BENEFICIAIRE_ACRE_ELIGIBLE;
    }

    private static int getNombreMoisEligiblesMicroEntrepreneurNonACRE(int nombreMoisDepuisCreationEntreprise) {
        return Math.max(0, NOMBRE_MOIS_MAX_ASS_MICRO_ENTREPRENEUR_ELIGIBLE - nombreMoisDepuisCreationEntreprise);
    }
}
