package fr.pe.pfin.service.da184.application.ui.rest.ressource;

public class SituationRemuneration {
    private String region;
    private Float age;
    private Float nbPersACharge;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Float getAge() {
        return age;
    }

    public void setAge(Float age) {
        this.age = age;
    }

    public Float getNbPersACharge() {
        return nbPersACharge;
    }

    public void setNbPersACharge(Float nbPersACharge) {
        this.nbPersACharge = nbPersACharge;
    }

    @Override
    public String toString() {
        return "SituationRemuneration{" +
                "region='" + region + '\'' +
                ", age=" + age +
                ", nbPersACharge=" + nbPersACharge +
                '}';
    }
}
