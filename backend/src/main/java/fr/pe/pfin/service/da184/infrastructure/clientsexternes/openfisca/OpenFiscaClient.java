package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca;

import fr.pe.pfin.service.da184.application.configuration.ClientExternePoleEmploiProperties;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.OpenFiscaMappeur;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.decorators.Decorators;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryRegistry;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.function.Supplier;

@Service
public class OpenFiscaClient {

    public static final String R4J_OPENFISCA_CALCULATE = "openfisca-calculate";

    private final RestTemplate restTemplate;

    private final CircuitBreakerRegistry circuitBreakerRegistry;
    private final RetryRegistry retryRegistry;

    private final ClientExternePoleEmploiProperties clientExternePoleEmploiProperties;

    public OpenFiscaClient(RestTemplate restTemplate, CircuitBreakerRegistry circuitBreakerRegistry, RetryRegistry retryRegistry, ClientExternePoleEmploiProperties clientExternePoleEmploiProperties) {
        this.restTemplate = restTemplate;
        this.circuitBreakerRegistry = circuitBreakerRegistry;
        this.retryRegistry = retryRegistry;
        this.clientExternePoleEmploiProperties = clientExternePoleEmploiProperties;
    }

    public OpenFiscaRoot callApiCalculate(Demandeur demandeur, LocalDate dateDebutSimulation) {
        CircuitBreaker circuitBreakerOpenFisca = circuitBreakerRegistry.circuitBreaker(R4J_OPENFISCA_CALCULATE);
        Retry retryOpenFisca = retryRegistry.retry(R4J_OPENFISCA_CALCULATE);

        OpenFiscaRoot openFiscaPayload;
        openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);
        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(openFiscaPayload.toString(), headers);

        Supplier<OpenFiscaRoot> openFiscaRootSupplier = () -> restTemplate.postForObject(this.clientExternePoleEmploiProperties.openFisca(), request, OpenFiscaRoot.class);
        return Decorators.ofSupplier(openFiscaRootSupplier)
                .withCircuitBreaker(circuitBreakerOpenFisca)
                .withRetry(retryOpenFisca)
                .get();
    }
}
