package fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages;

public enum InternalServerMessages {
    SERIALIZATION_OPENFISCA_IMPOSSIBLE("Erreur Technique : la serialization de l'objet OpenFiscaRoot a échoué.");

    private final String message;

    InternalServerMessages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
