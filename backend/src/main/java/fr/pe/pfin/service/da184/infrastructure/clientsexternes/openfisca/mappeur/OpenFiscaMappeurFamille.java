package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.BeneficiaireAidesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.SituationFamilialeUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaFamille;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OpenFiscaMappeurFamille {

    private OpenFiscaMappeurFamille() {
    }

    public static OpenFiscaFamille creerFamilleOpenFisca(Demandeur demandeur, Optional<List<Personne>> personneAChargeInferieur25ansOptional, LocalDate dateDebutSimulation) {

        OpenFiscaFamille openFiscaFamille = new OpenFiscaFamille();

        openFiscaFamille.setParents(creerParentsOpenFisca(demandeur));
        openFiscaFamille.setEnfants(creerPersonnesAChargeOpenFisca(personneAChargeInferieur25ansOptional));

        openFiscaFamille.setAllocationSoutienFamilial(OpenFiscaMappeurPeriode.creerPeriodesValeurNulleEgaleZero(
                RessourcesFinancieresAvantSimulationUtile.getAllocationSoutienFamilial(demandeur), dateDebutSimulation));

        openFiscaFamille.setAllocationsFamiliales(OpenFiscaMappeurPeriode.creerPeriodesValeurNulleEgaleZero(
                RessourcesFinancieresAvantSimulationUtile.getAllocationsFamiliales(demandeur), dateDebutSimulation));

        openFiscaFamille.setComplementFamilial(OpenFiscaMappeurPeriode.creerPeriodesValeurNulleEgaleZero(
                RessourcesFinancieresAvantSimulationUtile.getComplementFamilial(demandeur), dateDebutSimulation));

        openFiscaFamille.setPrestationAccueilJeuneEnfant(OpenFiscaMappeurPeriode.creerPeriodesValeurNulleEgaleZero(
                RessourcesFinancieresAvantSimulationUtile.getPrestationAccueilJeuneEnfant(demandeur), dateDebutSimulation));

        if (BeneficiaireAidesUtile.isBeneficiaireRSA(demandeur)) {

            openFiscaFamille.setRevenuSolidariteActive(
                    OpenFiscaMappeurPeriode.creerPeriodesOpenFiscaRSA(demandeur, dateDebutSimulation));

            openFiscaFamille.setRsaIsolementRecent(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(
                    !SituationFamilialeUtile.isSeulPlusDe18Mois(demandeur), dateDebutSimulation));
        }

        OpenFiscaMappeurPrimeActivite.addPrimeActiviteOpenFisca(openFiscaFamille, demandeur, dateDebutSimulation);

        openFiscaFamille.setAideLogement(OpenFiscaMappeurPeriode.creerPeriodesAideLogement(demandeur,dateDebutSimulation));

        return openFiscaFamille;
    }

    private static List<String> creerParentsOpenFisca(Demandeur demandeur) {
        List<String> parentsArray = new ArrayList<>();
        parentsArray.add(ParametresOpenFisca.DEMANDEUR);
        if (SituationFamilialeUtile.isEnCouple(demandeur)) {
            parentsArray.add(ParametresOpenFisca.CONJOINT);
        }
        return parentsArray;
    }

    private static List<String> creerPersonnesAChargeOpenFisca(Optional<List<Personne>> personneAChargeInferieur25ansOptional) {
        List<String> personnesAChargeArray = new ArrayList<>();
        if (personneAChargeInferieur25ansOptional.isPresent()) {
            for (int i = 1; i <= personneAChargeInferieur25ansOptional.get().size(); i++) {
                personnesAChargeArray.add(ParametresOpenFisca.ENFANT + i);
            }
        }
        return personnesAChargeArray;
    }
}
