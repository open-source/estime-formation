package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaFamille;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

import static fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.ParametresOpenFisca.FAMILLE1;

public class OpenFiscaMappeurPrimeActivite {

    private OpenFiscaMappeurPrimeActivite() {
    }

    public static float getMontantPrimeActivite(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation, int numeroMoisSimule) {
        if (openFiscaRoot.getFamilles() != null && openFiscaRoot.getFamilles().get(FAMILLE1) != null) {
            OpenFiscaPeriodes openFiscaPrimeActivite = openFiscaRoot.getFamilles().get(FAMILLE1).getPrimeActivite();
            String periodeFormateePrimeActivite = getPeriodeARecuperer(dateDebutSimulation, numeroMoisSimule);
            if (openFiscaPrimeActivite != null && openFiscaPrimeActivite.get(periodeFormateePrimeActivite) != null) {
                Double montantPrimeActivite = (Double) openFiscaPrimeActivite.get(periodeFormateePrimeActivite);
                return BigDecimal.valueOf(montantPrimeActivite).setScale(0, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }

    public static void addPrimeActiviteOpenFisca(OpenFiscaFamille openFiscaFamille, Demandeur demandeur, LocalDate dateDebutSimulation) {
        if (RessourcesFinancieresAvantSimulationUtile.hasPrimeActivite(demandeur)) {
            openFiscaFamille.setPrimeActivite(
                    OpenFiscaMappeurPeriode.creerPeriodesPrimeActiviteOpenFisca(demandeur, dateDebutSimulation));
        } else {
            openFiscaFamille.setPrimeActivite(OpenFiscaMappeurPeriode.creerPeriodesCalculeesOpenFisca(dateDebutSimulation));
        }
    }

    public static String getPeriodeARecuperer(LocalDate dateDebutSimulation, int numeroMoisSimule) {
        return OpenFiscaMappeurPeriode.getPeriodeNumeroMoisSimule(dateDebutSimulation, numeroMoisSimule);
    }
}
