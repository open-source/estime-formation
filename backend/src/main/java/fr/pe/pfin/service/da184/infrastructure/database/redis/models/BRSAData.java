package fr.pe.pfin.service.da184.infrastructure.database.redis.models;


import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@RedisHash("BRSADatas")
public class BRSAData implements Serializable {
    @Id
    private String id;
    @NotNull
    @Size(min = 8, max = 8)
    @Indexed
    private String idFT;
    @NotNull
    @Size(min = 8, max = 8)
    private String idPE;
    @NotNull
    @Size(min = 3, max = 3)
    private String codeRegionFT;
    @NotNull
    @Size(min = 3, max = 3)
    private String codeRegionPE;
    private Boolean hasConseillerFT;
    private Boolean hasConseillerPE;
    @NotNull
    private LocalDateTime dateSimulation;
    @NotNull
    private Integer notation;
    @NotNull
    private boolean isParcoursSimplifie;
    private Integer parcoursFormation;
    @NotNull
    private List<String> allocation;
    @NotNull
    private String departement;

    @SuppressWarnings("java:S107")
    public BRSAData(String id, String idFT, String codeRegionFT, Boolean hasConseillerFT, LocalDateTime dateSimulation, Integer notation, Integer parcoursFormation, List<String> allocation, String departement) {
        this.id = id;
        this.idFT = idFT;
        this.codeRegionFT = codeRegionFT;
        this.hasConseillerFT = hasConseillerFT != null && hasConseillerFT;
        this.dateSimulation = dateSimulation;
        this.notation = notation;
        this.parcoursFormation = parcoursFormation;
        this.allocation = allocation;
        this.departement = departement;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdFT() {
        return idFT;
    }

    public void setIdFT(String idFT) {
        this.idFT = idFT;
    }

    public String getCodeRegionFT() {
        return codeRegionFT;
    }

    public void setCodeRegionFT(String codeRegionFT) {
        this.codeRegionFT = codeRegionFT;
    }

    public boolean isHasConseillerFT() {
        return hasConseillerFT;
    }

    public void setHasConseillerFT(boolean hasConseillerFT) {
        this.hasConseillerFT = hasConseillerFT;
    }

    public LocalDateTime getDateSimulation() {
        return dateSimulation;
    }

    public void setDateSimulation(LocalDateTime dateSimulation) {
        this.dateSimulation = dateSimulation;
    }

    public int getParcoursFormation() { return parcoursFormation;}

    public void setParcoursFormation(int parcoursFormation) { this.parcoursFormation = parcoursFormation;}

    public Integer getNotation() {
        return notation;
    }

    public void setNotation(Integer notation) {
        this.notation = notation;
    }

    public boolean isParcoursSimplifie() {
        return isParcoursSimplifie;
    }

    public void setParcoursSimplifie(boolean parcoursSimplifie) {
        isParcoursSimplifie = parcoursSimplifie;
    }

    public List<String> getAllocation() {
        return allocation;
    }

    public void setAllocation(List<String> allocation) {
        this.allocation = allocation;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BRSAData brsaData)) return false;
        return hasConseillerFT == brsaData.hasConseillerFT
                && Objects.equals(id, brsaData.id)
                && Objects.equals(idFT, brsaData.idFT)
                && Objects.equals(codeRegionFT, brsaData.codeRegionFT)
                && Objects.equals(dateSimulation, brsaData.dateSimulation)
                && Objects.equals(notation, brsaData.notation)
                && Objects.equals(isParcoursSimplifie, brsaData.isParcoursSimplifie)
                && Objects.equals(parcoursFormation, brsaData.parcoursFormation)
                && Objects.equals(allocation, brsaData.allocation)
                && Objects.equals(departement, brsaData.departement);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idFT, codeRegionFT, hasConseillerFT, dateSimulation, notation, isParcoursSimplifie, parcoursFormation, allocation, departement);
    }

    @Override
    public String toString() {
        return "BRSAData{" +
                "id='" + id + '\'' +
                ", idFT='" + idFT + '\'' +
                ", codeRegionFT='" + codeRegionFT + '\'' +
                ", hasConseillerFT=" + hasConseillerFT +
                ", dateSimulation=" + dateSimulation +
                ", notation=" + notation +
                ", isParcoursSimplifie=" + isParcoursSimplifie +
                ", parcoursFormation=" + parcoursFormation +
                ", allocation=" + allocation +
                ", departement=" + departement + '}';
    }

    public String toCSV() {
        return id + ","
                + (idPE != null ? idPE : idFT) + ","
                + (codeRegionPE != null ? codeRegionPE : codeRegionFT) + ","
                + dateSimulation + ","
                + ((hasConseillerPE != null ? hasConseillerPE : hasConseillerFT) ? "Conseiller FT" : "Conseiller départemental ou rien") + ","
                + notation + ","
                + (isParcoursSimplifie ? "Parcours simplifié" : "Parcours complet") + ","
                + retrieveParcours(parcoursFormation) + ","
                + allocation + ","
                + departement + ",";
    }

    private String retrieveParcours(int parcoursFormation) {
        return switch (parcoursFormation) {
            case 1 -> "Parcours ultra simplifié";
            case 2 -> "Parcours rémunération";
            case 3 -> "Parcours simplifié";
            case 4 -> "Parcours complet";
            default -> "";
        };
    }
}
