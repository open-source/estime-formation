package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import fr.pe.pfin.service.da184.infrastructure.aides.caf.utile.*;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.SituationAppelOpenFiscaEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile.AgepiUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile.AideMobiliteUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile.AreUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile.TemporaliteFTUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;

import java.time.LocalDate;
import java.util.Map;

public class TemporaliteOpenFiscaHelper {

    private TemporaliteOpenFiscaHelper() {

    }

    /**
     * Fonction qui permet de déterminer les appels vers openfisca à effectuer,
     * cette méthode permet notamment de réduire les appels en rassemblant les appels pour les différentes aides qui doivent s'effectuer dans le même mois
     * <p>
     * Cette méthode s'appuie sur les temporalités des différentes aides en les aggrègeant
     *
     */
    public static void simulerTemporaliteAppelOpenfisca(OpenFiscaRoot openFiscaRoot, Simulation simulation, Map<String, Aide> aidesPourCeMois, LocalDate dateDebutSimulation, int numeroMoisSimule, Demandeur demandeur) {

        if (TemporaliteFTUtile.isAgepiAVerser(numeroMoisSimule) && !demandeur.isModeSimplifie()) {
            verserAide(openFiscaRoot, aidesPourCeMois, dateDebutSimulation, numeroMoisSimule, SituationAppelOpenFiscaEnum.AGEPI);
        }

        if (TemporaliteFTUtile.isAideMobiliteAVerser(numeroMoisSimule)) {
            verserAide(openFiscaRoot, aidesPourCeMois, dateDebutSimulation, numeroMoisSimule, SituationAppelOpenFiscaEnum.AM);
        }

        // Si l'on doit verser l'aide au logement et la prime d'activité pour les demandeurs ASS/AAH/ARE/RSA
        if (TemporaliteCAFUtile.isAideLogementAVerser(numeroMoisSimule, demandeur)) {
            verserAide(openFiscaRoot, aidesPourCeMois, dateDebutSimulation, numeroMoisSimule, SituationAppelOpenFiscaEnum.AL);
        } else {
            AidesLogementUtile.reporterAideLogement(simulation, aidesPourCeMois, numeroMoisSimule, demandeur);
        }
        if (TemporaliteCAFUtile.isRSAAVerser(numeroMoisSimule, demandeur)) {
            verserAide(openFiscaRoot, aidesPourCeMois, dateDebutSimulation, numeroMoisSimule, SituationAppelOpenFiscaEnum.RSA);
        } else {
            RSAUtile.reporterRsa(simulation, aidesPourCeMois, numeroMoisSimule, demandeur);
        }
        if (TemporaliteCAFUtile.isPrimeActiviteAVerser(numeroMoisSimule, demandeur)) {
            verserAide(openFiscaRoot, aidesPourCeMois, dateDebutSimulation, numeroMoisSimule, SituationAppelOpenFiscaEnum.PPA);
        } else {
            PrimeActiviteUtile.reporterPrimeActivite(simulation, aidesPourCeMois, demandeur, dateDebutSimulation, numeroMoisSimule);
        }
        if (TemporaliteFTUtile.isAREAReporter(demandeur, numeroMoisSimule)) {
            AreUtile.reporterARE(aidesPourCeMois, demandeur, numeroMoisSimule, dateDebutSimulation);
        }
        if (TemporaliteFTUtile.isComplementAREAVerser(demandeur, numeroMoisSimule)) {
            verserAide(openFiscaRoot, aidesPourCeMois, dateDebutSimulation, numeroMoisSimule, SituationAppelOpenFiscaEnum.ARE);
        }
    }

    private static void verserAide(OpenFiscaRoot openFiscaRoot, Map<String, Aide> aidesPourCeMois, LocalDate dateDebutSimulation, int numeroMoisSimule, SituationAppelOpenFiscaEnum situationAppelOpenFisca) {
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAidesSelonSituation(openFiscaRoot, dateDebutSimulation, numeroMoisSimule,
                situationAppelOpenFisca);

        if (openFiscaRetourSimulation.getMontantAideLogement() > 0) {
            Aide aideLogement = AidesLogementUtile.creerAideLogement(openFiscaRetourSimulation.getMontantAideLogement(), false);
            aidesPourCeMois.put(aideLogement.getCode(), aideLogement);
        }
        if (openFiscaRetourSimulation.getMontantPrimeActivite() > 0) {
            Aide primeActivite = PrimeActiviteUtile.creerAidePrimeActivite(openFiscaRetourSimulation.getMontantPrimeActivite(), false, dateDebutSimulation, numeroMoisSimule);
            aidesPourCeMois.put(primeActivite.getCode(), primeActivite);
        }
        if (openFiscaRetourSimulation.getMontantRSA() > 0) {
            Aide rsa = RSAUtile.creerAideRSA(openFiscaRetourSimulation.getMontantRSA(), false);
            aidesPourCeMois.put(rsa.getCode(), rsa);
        }
        if (openFiscaRetourSimulation.getMontantComplementARE() > 0) {
            Aide complementARE = AreUtile.creerComplementARE(openFiscaRetourSimulation.getMontantComplementARE(),
                    openFiscaRetourSimulation.getNombreJoursRestantsARE() - openFiscaRetourSimulation.getNombreJoursIndemnisesComplementARE());
            aidesPourCeMois.put(complementARE.getCode(), complementARE);
        }
        if (openFiscaRetourSimulation.getMontantAgepi() > 0) {
            Aide agepi = AgepiUtile.creerAgepi(openFiscaRetourSimulation.getMontantAgepi());
            aidesPourCeMois.put(agepi.getCode(), agepi);
        }
        if (openFiscaRetourSimulation.getMontantAideMobilite() > 0
                && AideMobiliteUtile.getMontantRestant(openFiscaRetourSimulation.getMontantAideMobilite(), numeroMoisSimule) > 0) {
            Aide aideMobilite;
            int montantRestant = AideMobiliteUtile.getMontantRestant(openFiscaRetourSimulation.getMontantAideMobilite(), numeroMoisSimule);
            if (AideMobiliteUtile.isMontantAideMobiliteLessThan5200(openFiscaRetourSimulation.getMontantAideMobilite(), numeroMoisSimule) && montantRestant < openFiscaRetourSimulation.getMontantAideMobilite()) {
                aideMobilite = AideMobiliteUtile.creerAideMobilite(montantRestant);
            } else {
                aideMobilite = AideMobiliteUtile.creerAideMobilite(openFiscaRetourSimulation.getMontantAideMobilite());
            }

            aidesPourCeMois.put(aideMobilite.getCode(), aideMobilite);
        }
        if (openFiscaRetourSimulation.getMontantPrimeActivite() > 0 && openFiscaRetourSimulation.getMontantRSA() > 0) {
            Aide primeActivite = PrimeActiviteRSAUtile.creerAidePrimeActivite(openFiscaRetourSimulation.getMontantPrimeActivite(), false);
            aidesPourCeMois.put(primeActivite.getCode(), primeActivite);
        }
    }
}
