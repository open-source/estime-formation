package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.MicroEntreprise;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypesBeneficesMicroEntrepriseEnum;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

public class OpenFiscaMappeurMicroEntreprise {

    private OpenFiscaMappeurMicroEntreprise() {
    }

    public static float getMontantBeneficesMicroEntreprise(OpenFiscaRoot openFiscaRoot, LocalDate dateDebutSimulation) {
        if (openFiscaRoot.getIndividus() != null && openFiscaRoot.getIndividus().get(ParametresOpenFisca.DEMANDEUR) != null) {
            OpenFiscaPeriodes openFiscaBeneficesMicroEntreprise = openFiscaRoot.getIndividus().get(ParametresOpenFisca.DEMANDEUR).getBeneficesMicroEntreprise();
            String periodeFormateeBeneficesMicroEntreprise = OpenFiscaMappeurPeriode.getPeriodeFormatee(dateDebutSimulation);
            if (openFiscaBeneficesMicroEntreprise != null && openFiscaBeneficesMicroEntreprise.get(periodeFormateeBeneficesMicroEntreprise) != null) {
                Double montantBeneficesMicroEntreprise = (Double) openFiscaBeneficesMicroEntreprise.get(periodeFormateeBeneficesMicroEntreprise);
                return BigDecimal.valueOf(montantBeneficesMicroEntreprise).setScale(0, RoundingMode.HALF_UP).floatValue();
            }
        }
        return BigDecimal.ZERO.floatValue();
    }

    public static void addChiffreAffairesMicroEntreprise(OpenFiscaIndividu demandeurOpenFisca, Demandeur demandeur, LocalDate dateDebutSimulation) {
        MicroEntreprise microEntreprise = demandeur.getInformationsPersonnelles().getMicroEntreprise();
        String typesBeneficesMicroEntreprise = microEntreprise.getTypeBenefices();

        if (typesBeneficesMicroEntreprise.equals(TypesBeneficesMicroEntrepriseEnum.AR.getCode())) {
            demandeurOpenFisca.setChiffreAffairesARMicroEntreprise(OpenFiscaMappeurPeriode
                    .creerPeriodesChiffreAffairesMicroEntreprise(microEntreprise, dateDebutSimulation));
        } else if (typesBeneficesMicroEntreprise.equals(TypesBeneficesMicroEntrepriseEnum.BIC.getCode())) {
            demandeurOpenFisca.setChiffreAffairesBICMicroEntreprise(OpenFiscaMappeurPeriode
                    .creerPeriodesChiffreAffairesMicroEntreprise(microEntreprise, dateDebutSimulation));
        } else if (typesBeneficesMicroEntreprise.equals(TypesBeneficesMicroEntrepriseEnum.BNC.getCode())) {
            demandeurOpenFisca.setChiffreAffairesBNCMicroEntreprise(OpenFiscaMappeurPeriode
                    .creerPeriodesChiffreAffairesMicroEntreprise(microEntreprise, dateDebutSimulation));
        }

        demandeurOpenFisca.setBeneficesMicroEntreprise(
                OpenFiscaMappeurPeriode.creerPeriodesCalculeesEffectivePremierMoisOpenFisca(dateDebutSimulation));
    }
}
