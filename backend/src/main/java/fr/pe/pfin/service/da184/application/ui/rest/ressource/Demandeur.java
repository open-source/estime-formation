package fr.pe.pfin.service.da184.application.ui.rest.ressource;

import jakarta.validation.constraints.NotNull;

public class Demandeur {

    @NotNull
    private BeneficiaireAides beneficiaireAides;
    @NotNull
    private FutureFormation futureFormation;
    @NotNull
    private InformationsPersonnelles informationsPersonnelles;
    private RessourcesFinancieresAvantSimulation ressourcesFinancieresAvantSimulation;
    @NotNull
    private SituationFamiliale situationFamiliale;
    private String idDemandeurEmploi;
    private boolean conseillerFT;
    private String codeRegionFT;
    private boolean isFromRegion;
    private boolean modeSimplifie;
    private Integer parcoursFormation;

    public RessourcesFinancieresAvantSimulation getRessourcesFinancieresAvantSimulation() {
        return ressourcesFinancieresAvantSimulation;
    }

    public void setRessourcesFinancieresAvantSimulation(
            RessourcesFinancieresAvantSimulation ressourcesFinancieresAvantSimulation) {
        this.ressourcesFinancieresAvantSimulation = ressourcesFinancieresAvantSimulation;
    }

    public SituationFamiliale getSituationFamiliale() {
        return situationFamiliale;
    }

    public void setSituationFamiliale(SituationFamiliale situationFamiliale) {
        this.situationFamiliale = situationFamiliale;
    }

    public FutureFormation getFutureFormation() {
        return futureFormation;
    }

    public void setFutureFormation(FutureFormation futureFormation) {
        this.futureFormation = futureFormation;
    }

    public InformationsPersonnelles getInformationsPersonnelles() {
        return informationsPersonnelles;
    }

    public void setInformationsPersonnelles(InformationsPersonnelles informationsPersonnelles) {
        this.informationsPersonnelles = informationsPersonnelles;
    }

    public String getIdDemandeurEmploi() {
        return this.idDemandeurEmploi;
    }

    public void setIdDemandeurEmploi(String idDemandeurEmploi) {
        this.idDemandeurEmploi = idDemandeurEmploi;
    }

    public boolean isConseillerFT() {
        return conseillerFT;
    }

    public void setConseillerFT(boolean conseillerFT) {
        this.conseillerFT = conseillerFT;
    }

    public String getCodeRegionFT() {
        return codeRegionFT;
    }

    public void setCodeRegionFT(String codeRegionFT) {
        this.codeRegionFT = codeRegionFT;
    }

    public BeneficiaireAides getBeneficiaireAides() {
        return beneficiaireAides;
    }

    public void setBeneficiaireAides(BeneficiaireAides beneficiaireAides) {
        this.beneficiaireAides = beneficiaireAides;
    }

    public boolean getIsFromRegion() {
        return isFromRegion;
    }

    public void setIsFromRegion(boolean isFromRegion) {
        this.isFromRegion = isFromRegion;
    }

    public boolean isModeSimplifie() {
        return modeSimplifie;
    }

    public void setModeSimplifie(boolean modeSimplifie) {
        this.modeSimplifie = modeSimplifie;
    }

    public Integer getParcoursFormation() {
        return parcoursFormation;
    }

    public void setParcoursFormation(Integer parcoursFormation) {
        this.parcoursFormation = parcoursFormation;
    }
}
