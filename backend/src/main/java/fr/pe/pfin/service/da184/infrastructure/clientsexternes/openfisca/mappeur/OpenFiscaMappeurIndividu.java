package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.StatutMaritalEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.InformationsPersonnellesUtile;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.RessourcesFinancieresAvantSimulationUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class OpenFiscaMappeurIndividu {

    private OpenFiscaMappeurIndividu() {
    }

    public static OpenFiscaIndividu creerConjointOpenFisca(Personne conjoint, LocalDate dateDebutSimulation) {
        OpenFiscaIndividu conjointOpenFisca = new OpenFiscaIndividu();
        OpenFiscaMappeurRessourcesPersonne.addRessourcesFinancieresPersonne(conjointOpenFisca, conjoint, dateDebutSimulation);
        return conjointOpenFisca;
    }

    public static OpenFiscaIndividu creerDemandeurOpenFisca(Demandeur demandeur, LocalDate dateDebutSimulation) {
        OpenFiscaIndividu demandeurOpenFisca = new OpenFiscaIndividu();

        demandeurOpenFisca.setDateNaissance(
                OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(getDateNaissance(demandeur), dateDebutSimulation));

        demandeurOpenFisca.setStatutMarital(
                OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(getStatutMarital(demandeur), dateDebutSimulation));

        demandeurOpenFisca.setActivite(OpenFiscaMappeurPeriode.creerPeriodesActiviteOpenFisca(dateDebutSimulation));

        addSalaireDemandeur(demandeurOpenFisca, demandeur, dateDebutSimulation);
        addRessourcesFinancieresDemandeur(demandeurOpenFisca, demandeur, dateDebutSimulation);

        demandeurOpenFisca.setRsaJeuneConditionHeuresTravailRemplie(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca("true", dateDebutSimulation));

        demandeurOpenFisca.setEnceinte(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca1Month(getIsEnceinte(demandeur), dateDebutSimulation));

        return demandeurOpenFisca;
    }

    public static void ajouterPersonneAChargeIndividus(Map<String, OpenFiscaIndividu> individusOpenFisca, List<Personne> personnesACharge, LocalDate dateJour) {
        int index = 1;
        for (Personne personneACharge : personnesACharge) {
            individusOpenFisca.put(ParametresOpenFisca.ENFANT + index++, creerEnfantOpenFisca(personneACharge, dateJour));
        }
    }

    private static void addRessourcesFinancieresDemandeur(OpenFiscaIndividu demandeurOpenFisca, Demandeur demandeur, LocalDate dateDebutSimulation) {
        if (RessourcesFinancieresAvantSimulationUtile.hasRevenusImmobilier(demandeur)) {
            demandeurOpenFisca.setRevenusLocatifs(OpenFiscaMappeurPeriode.creerPeriodesRevenusImmobilier(
                    RessourcesFinancieresAvantSimulationUtile.getRevenusImmobilierSur1Mois(demandeur), dateDebutSimulation));
        }
        if (RessourcesFinancieresAvantSimulationUtile.hasRevenusTravailleurIndependant(demandeur)) {
            demandeurOpenFisca.setChiffreAffairesIndependant(OpenFiscaMappeurPeriode.creerPeriodesAnnees(demandeur
                    .getRessourcesFinancieresAvantSimulation().getChiffreAffairesIndependantDernierExercice(), dateDebutSimulation));
        }
        if (InformationsPersonnellesUtile.hasMicroEntreprise(demandeur)) {
            OpenFiscaMappeurMicroEntreprise.addChiffreAffairesMicroEntreprise(demandeurOpenFisca, demandeur, dateDebutSimulation);
        }
        if (RessourcesFinancieresAvantSimulationUtile.hasPensionsAlimentaires(demandeur)) {
            demandeurOpenFisca.setPensionsAlimentaires(OpenFiscaMappeurPeriode
                    .creerPeriodesOpenFisca(demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                            .getAidesFamiliales().getPensionsAlimentairesFoyer(), dateDebutSimulation));
        }
        if (RessourcesFinancieresAvantSimulationUtile.hasPensionInvalidite(demandeur)) {
            demandeurOpenFisca.setPensionInvalidite(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(
                    demandeur.getRessourcesFinancieresAvantSimulation().getAidesCPAM().getPensionInvalidite(), dateDebutSimulation));
        }
        if (RessourcesFinancieresAvantSimulationUtile.hasAllocationARE(demandeur.getRessourcesFinancieresAvantSimulation())) {
            OpenFiscaMappeurComplementARE.addAREAvantSimulationOpenFiscaIndividu(demandeurOpenFisca, demandeur, dateDebutSimulation);
            OpenFiscaMappeurComplementARE.addComplementAREOpenFiscaIndividu(demandeurOpenFisca, demandeur, dateDebutSimulation);
        }
        if (RessourcesFinancieresAvantSimulationUtile
                .hasAllocationSolidariteSpecifique(demandeur.getRessourcesFinancieresAvantSimulation())) {
            OpenFiscaMappeurASS.addASSOpenFiscaIndividu(demandeurOpenFisca, demandeur, dateDebutSimulation);
        }
        if (RessourcesFinancieresAvantSimulationUtile.hasAllocationAdultesHandicapes(demandeur)) {
            OpenFiscaMappeurAAH.addAAHOpenFiscaIndividu(demandeurOpenFisca, demandeur, dateDebutSimulation);
        }

        OpenFiscaMappeurAgepi.addAgepiOpenFiscaIndividu(demandeurOpenFisca, demandeur, dateDebutSimulation);
        OpenFiscaMappeurAideMobilite.addAideMobiliteOpenFiscaIndividu(demandeurOpenFisca, demandeur, dateDebutSimulation);
    }

    private static void addSalaireDemandeur(OpenFiscaIndividu demandeurOpenFisca, Demandeur demandeur, LocalDate dateDebutSimulation) {
        if (InformationsPersonnellesUtile.hasCumulAncienEtNouveauSalaire(demandeur)) {
            OpenFiscaMappeurPeriode.creerPeriodesSalaireCumulDemandeur(demandeurOpenFisca, demandeur, dateDebutSimulation);
        } else {
            OpenFiscaMappeurPeriode.creerPeriodesSalaireDemandeur(demandeurOpenFisca, demandeur, dateDebutSimulation);
        }
    }

    private static OpenFiscaIndividu creerEnfantOpenFisca(Personne personneACharge, LocalDate dateDebutSimulation) {
        OpenFiscaIndividu enfant = new OpenFiscaIndividu();

        enfant.setDateNaissance(OpenFiscaMappeurPeriode.creerPeriodesOpenFisca(
                DateUtile.convertDateToString(DateUtile.getDateNaissanceModifieeEnfantMoinsDUnAn(personneACharge.getInformationsPersonnelles().getDateNaissance()),
                        DateUtile.DATE_FORMAT_YYYY_MM_DD), dateDebutSimulation));

        enfant.setEnfantACharge(OpenFiscaMappeurPeriode.creerPeriodesEnfantACharge(dateDebutSimulation));
        OpenFiscaMappeurRessourcesPersonne.addRessourcesFinancieresPersonne(enfant, personneACharge, dateDebutSimulation);

        return enfant;
    }

    private static String getStatutMarital(Demandeur demandeur) {
        if (Objects.nonNull(demandeur.getSituationFamiliale()) && demandeur.getSituationFamiliale().getIsEnCouple().booleanValue()) {
            return StatutMaritalEnum.MARIE.getLibelle();
        }
        return StatutMaritalEnum.CELIBATAIRE.getLibelle();
    }

    private static boolean getIsEnceinte(Demandeur demandeur) {
        if (Objects.nonNull(demandeur.getSituationFamiliale()) && demandeur.getSituationFamiliale().getIsEnceinte().booleanValue()) {
            return demandeur.getSituationFamiliale().getIsEnceinte();
        }
        return false;
    }

    private static String getDateNaissance(Demandeur demandeur) {
        if (Objects.nonNull(demandeur.getInformationsPersonnelles()) && Objects.nonNull(demandeur.getInformationsPersonnelles().getDateNaissance())) {
            return DateUtile.convertDateToString(demandeur.getInformationsPersonnelles().getDateNaissance(), DateUtile.DATE_FORMAT_YYYY_MM_DD);
        }
        return "";
    }
}
