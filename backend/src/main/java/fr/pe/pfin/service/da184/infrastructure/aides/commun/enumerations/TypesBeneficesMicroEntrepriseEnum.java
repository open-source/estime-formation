package fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations;

public enum TypesBeneficesMicroEntrepriseEnum {
	AR("achat_revente"), BIC("bic"), BNC("bnc");

	private final String code;

	TypesBeneficesMicroEntrepriseEnum(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
