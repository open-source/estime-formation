package fr.pe.pfin.service.da184.application.services;

import fr.pe.pfin.service.da184.domaine.use_cases.DonneesBRSAPort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.time.LocalDate;

@Service
public class DonneesBRSAService {
    private final DonneesBRSAPort donneesBRSAPort;

    public DonneesBRSAService(DonneesBRSAPort donneesBRSAPort) {
        this.donneesBRSAPort = donneesBRSAPort;
    }

    public InputStream obtenirDonneesBRSA(LocalDate dateDebut, LocalDate dateFin) {
        return this.donneesBRSAPort.extraireDonneesBRSA(dateDebut, dateFin);
    }

    public HttpStatus addNotation(String id, Integer notation) {
        return this.donneesBRSAPort.addNotation(id, notation);
    }
}
