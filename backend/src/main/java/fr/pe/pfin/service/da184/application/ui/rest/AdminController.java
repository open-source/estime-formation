package fr.pe.pfin.service.da184.application.ui.rest;

import fr.pe.pfin.service.da184.application.services.DonneesBRSAService;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

@RestController
@RequestMapping("/admin")
public class AdminController {
    static final String DATE_DEBUT_APRES_DATE_FIN = "La Date de début ne doit pas être après la date du jour";
    static final String DATE_FIN_AVANT_DATE_DEBUT = "La Date de fin ne peut pas être avant la date de début";
    static final String PAS_PLUS_DE_2_MOIS_ENTRE_2_DATES = "pas plus de 2 mois entre 2 dates";
    private final DonneesBRSAService donneesBRSAService;

    public AdminController(DonneesBRSAService donneesBRSAService) {
        this.donneesBRSAService = donneesBRSAService;
    }

    @GetMapping(value = "/brsa-datas")
    public ResponseEntity<InputStreamResource> obtenirDonneesBRSA(@RequestParam("debut") LocalDate dateDebut, @RequestParam("fin") LocalDate dateFin) {
        validerLesDatesEnEntree(dateDebut, dateFin);
        return ResponseEntity.ok()
                .header(CONTENT_TYPE, "text/csv")
                .body(new InputStreamResource(this.donneesBRSAService.obtenirDonneesBRSA(dateDebut, dateFin)));
    }

    private void validerLesDatesEnEntree(LocalDate dateDebut, LocalDate dateFin) {
        Optional<String> message = Optional.empty();
        if (dateDebut.isAfter(LocalDate.now())) {
            message = Optional.of(DATE_DEBUT_APRES_DATE_FIN);
        } else if (dateFin.isBefore(dateDebut)) {
            message = Optional.of(DATE_FIN_AVANT_DATE_DEBUT);
        } else if (Period.between(dateDebut, dateFin).getMonths() > 2) {
            message = Optional.of(PAS_PLUS_DE_2_MOIS_ENTRE_2_DATES);
        }
        if (message.isPresent()) {
            throw new BadRequestException(message.get());
        }

    }
}
