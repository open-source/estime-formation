package fr.pe.pfin.service.da184.application.ui.rest.valideur;

import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.BadRequestMessages;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.BeneficiaireAides;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.InformationsPersonnelles;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SituationFamiliale;

import java.util.List;

public class SituationFamilialeApiValideur {

    private SituationFamilialeApiValideur() {
    }

    public static void controlerDonnees(SituationFamiliale situationFamiliale, BeneficiaireAides beneficiaireAides) {
        if (situationFamiliale == null) {
            throw new BadRequestException(
                    String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "situationFamiliale"));
        } else {
            controlerEnCouple(situationFamiliale, beneficiaireAides);
            controlerPersonnesACharges(situationFamiliale.getPersonnesACharge());
        }
    }

    private static void controlerEnCouple(SituationFamiliale situationFamiliale, BeneficiaireAides beneficiaireAides) {
        if (situationFamiliale.getIsEnCouple() == null) {
            throw new BadRequestException(
                    String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "enCouple de situationFamiliale"));
        }
        if (situationFamiliale.getIsEnCouple().booleanValue() && situationFamiliale.getConjoint() == null) {
            throw new BadRequestException(
                    String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "conjoint de situationFamiliale"));
        }
        if (beneficiaireAides.isBeneficiaireRSA() && !situationFamiliale.getIsEnCouple().booleanValue()
                && situationFamiliale.getIsSeulPlusDe18Mois() == null) {
            throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                    "isSeulPlusDe18Mois dans situationFamiliale"));
        }
    }

    private static void controlerPersonnesACharges(List<Personne> personnesACharge) {
        if (personnesACharge != null) {
            for (Personne personne : personnesACharge) {
                controlerInformationsPersonnelles(personne.getInformationsPersonnelles());
            }
        }
    }

    private static void controlerInformationsPersonnelles(InformationsPersonnelles informationsPersonnelles) {
        if (informationsPersonnelles == null) {
            throw new BadRequestException(
                    String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "informationsPersonnelles"));
        } else {
            if (informationsPersonnelles.getDateNaissance() == null) {
                throw new BadRequestException(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                        "dateNaissance de informationsPersonnelles"));
            }
        }
    }
}
