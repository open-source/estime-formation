package fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;

import java.util.ArrayList;
import java.util.Optional;

public class AideMobiliteUtile {

    private AideMobiliteUtile() {
    }

    public static Aide creerAideMobilite(float montantAide) {
        ArrayList<String> messagesAlerte = new ArrayList<>();
        messagesAlerte.add(MessageInformatifEnum.AGEPI_AM_DELAI_DEMANDE.getMessage());
        return AideUtile.creerAide(AideEnum.AIDE_MOBILITE, Optional.of(OrganismeEnum.FT), Optional.of(messagesAlerte),
                false, montantAide);
    }

    public static boolean isMontantAideMobiliteLessThan5200(float montantAideMobilite, int numeroMoisSimule) {
        return Math.multiplyExact((int) montantAideMobilite, (numeroMoisSimule -2)) < 5200;
    }

    public static int getMontantRestant(float montantAideMobilite, int numeroMoisSimule) {
        return Math.subtractExact(5200, Math.multiplyExact((int) montantAideMobilite, (numeroMoisSimule -2)));
    }
}
