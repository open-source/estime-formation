package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


class PrimeActiviteAAHUtileTest {

    @Test
    @DisplayName("doit retourner faux pour un object demandeur vide pour une prime activité à calculer")
    void doitRetournerFauxPourUnDemandeurVideSurUnePrimeActiviteACalculer() {
        assertThat(PrimeActiviteAAHUtile.isPrimeActiviteACalculer(0, new Demandeur())).isFalse();
    }

    @Test
    @DisplayName("doit retourner une NPE pour un object demandeur NULL pour une prime activité à calculer")
    void doitRetournerNPEPourUnDemandeurNullSurUnePrimeActiviteACalculer() {
        assertThatThrownBy(() -> PrimeActiviteAAHUtile.isPrimeActiviteACalculer(0, null)).isInstanceOf(NullPointerException.class);
    }

    @Test
    @DisplayName("doit retourner faux pour un object demandeur vide pour une prime activité à verser")
    void doitRetournerFauxPourUnDemandeurVideSurUnePrimeActiviteAVerser() {
        assertThat(PrimeActiviteAAHUtile.isPrimeActiviteAVerser(0, new Demandeur())).isFalse();
    }

    @Test
    @DisplayName("doit retourner faux pour un object demandeur vide pour une prime activité à verser")
    void doitRetournerNPEPourUnDemandeurNullSurUnePrimeActiviteAVerser() {
        assertThatThrownBy(() -> PrimeActiviteAAHUtile.isPrimeActiviteAVerser(0, null)).isInstanceOf(NullPointerException.class);
    }


}
