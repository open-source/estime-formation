package fr.pe.pfin.service.da184.infrastructure;

import fr.pe.pfin.service.da184.infrastructure.database.redis.models.BRSAData;
import fr.pe.pfin.service.da184.infrastructure.database.redis.repositories.BRSADataRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class DonneesBRSAAdapterTest {
    @InjectMocks
    DonneesBRSAAdapter donneesBRSAAdapter;
    @Mock
    private BRSADataRepository brsaDataRepository;

    @BeforeEach
    void init() {
        lenient().when(brsaDataRepository.findById("147258369")).thenReturn(Optional.of(new BRSAData("147258369", "123345678", "043", null, LocalDateTime.now(), null, 3, new ArrayList<>(), "43")));
    }
    @Test
    @DisplayName("doit retourner une chaine au format csv pour un BRSAData")
    void doitRetournerUneChaineAuFormatCSVPourUnBRSAData() {
        assertThat(new BRSAData("id", "123345678", "043", null, LocalDateTime.now(), 2, 3, new ArrayList<>(), "43").toCSV()).contains(",");
    }

    @Test
    @DisplayName("doit retourner un inputstream pour un csv de BRSAData")
    void doitRetournerUnInputStreamAvecLeBRSAData() {
        assertThat(donneesBRSAAdapter.extraireDonneesBRSA(LocalDate.now().minusDays(1), LocalDate.now().plusDays(1))).isNotNull().isInstanceOf(InputStream.class);
    }

    @Test
    @DisplayName("doit ajouter une notation")
    void doitAjouterUneNotation() {
        HttpStatus status = donneesBRSAAdapter.addNotation("147258369", 3);
        assertThat(status).isEqualTo(HttpStatus.OK);
        assertThat(brsaDataRepository.findById("147258369").get().getNotation()).isEqualTo(3);
    }

}
