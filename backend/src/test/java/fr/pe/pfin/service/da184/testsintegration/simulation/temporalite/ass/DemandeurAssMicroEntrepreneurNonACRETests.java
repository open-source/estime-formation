package fr.pe.pfin.service.da184.testsintegration.simulation.temporalite.ass;

import com.nimbusds.jose.shaded.gson.JsonIOException;
import com.nimbusds.jose.shaded.gson.JsonSyntaxException;
import fr.pe.pfin.service.da184.RedisTestConfiguration;
import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.DemandeurController;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SimulationMensuelle;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.text.ParseException;

import static fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile.getMonthFromLocalDate;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = RedisTestConfiguration.class)
@ActiveProfiles("test")
class DemandeurAssMicroEntrepreneurNonACRETests extends Utile {

    private final DemandeurController demandeurController;

    @Autowired
    public DemandeurAssMicroEntrepreneurNonACRETests(DemandeurController demandeurController) {
        this.demandeurController = demandeurController;
    }


    @Test
    void simulerPopulationAssBeneficiaireACREDateCreation0MoisAvant()
            throws ParseException, JsonIOException, JsonSyntaxException {

        // Si DE Français de France métropolitaine né le 5/07/1986, célibataire, 1 enfant à charge de 9ans, asf = 117€
        // Montant net journalier ASS = 16,89€, 0 mois cumulé ASS + salaire sur 3 derniers mois

        Demandeur demandeur = createDemandeurEmploiMicroEntrepreneurNonACRE(0);

        // Lorsque je simule mes prestations le 01/05/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023 sont :
        // AGEPI : 416€, Aide mobilité : 450€, ASS : 506€
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOLIDARITE_SPECIFIQUE.getCode())).satisfies(ass -> {
                assertThat(ass.getMontant()).isEqualTo(523f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
        });
        // Alors les prestations du second mois 07/2023 sont :
        // ASS : 523€
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.AGEPI.getCode())).satisfies(agepi -> {
                assertThat(agepi.getMontant()).isEqualTo(416f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
        });
        // Alors les prestations du troisième mois 08/2023 sont :
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);

            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du quatrième mois 09/2023 sont :
        // aucune aide
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du cinquième mois 10/2023 sont :
        // Prime d'activité : 140€ (simulateur CAF : 129€)
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
        });
        // Alors les prestations du sixième mois 11/2023 sont :
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(125);
            });
        });
    }

    @Test
    void simulerPopulationAssBeneficiaireACREDateCreation1MoisAvant()
            throws ParseException, JsonIOException, JsonSyntaxException {

        // Si DE Français de France métropolitaine né le 5/07/1986, célibataire, 1 enfant à charge de 9ans, asf = 117€
        // Montant net journalier ASS = 16,89€, 0 mois cumulé ASS + salaire sur 3 derniers mois

        Demandeur demandeur = createDemandeurEmploiMicroEntrepreneurNonACRE(1);

        // Lorsque je simule mes prestations le 20/10/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023 sont :
        // AGEPI : 416€, Aide mobilité : 450€, ASS : 506€
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOLIDARITE_SPECIFIQUE.getCode())).satisfies(ass -> {
                assertThat(ass.getMontant()).isEqualTo(523f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
        });
        // Alors les prestations du second mois 07/2023 sont :
        // ASS : 523€
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.AGEPI.getCode())).satisfies(agepi -> {
                assertThat(agepi.getMontant()).isEqualTo(416f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
        });
        // Alors les prestations du troisième mois 08/2023 sont :
        // ASS : 523€
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du quatrième mois 09/2023 sont :
        // aucune aide
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du cinquième mois 10/2023 sont :
        // Prime d'activité : 140€ (simulateur CAF : 129€)
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du sixième mois 11/2023 sont :
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(249);
            });
        });
    }

    @Test
    void simulerPopulationAssBeneficiaireACREDateCreation2MoisAvant()
            throws ParseException, JsonIOException, JsonSyntaxException {

        // Si DE Français de France métropolitaine né le 5/07/1986, célibataire, 1 enfant à charge de 9ans, asf = 117€
        // Montant net journalier ASS = 16,89€, 0 mois cumulé ASS + salaire sur 3 derniers mois

        Demandeur demandeur = createDemandeurEmploiMicroEntrepreneurNonACRE(2);

        // Lorsque je simule mes prestations le 20/10/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023 sont :
        // AGEPI : 416€, Aide mobilité : 450€, ASS : 506€
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOLIDARITE_SPECIFIQUE.getCode())).satisfies(ass -> {
                assertThat(ass.getMontant()).isEqualTo(523f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
        });
        // Alors les prestations du second mois 07/2023 sont :
        // ASS : 523€
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.AGEPI.getCode())).satisfies(agepi -> {
                assertThat(agepi.getMontant()).isEqualTo(416f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
        });
        // Alors les prestations du troisième mois 08/2023 sont :
        // ASS : 523€
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du quatrième mois 09/2023 sont :
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du cinquième mois 10/2023 sont :
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du sixième mois 11/2023 sont :
        // Prime d'activité : 140€
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isBetween(126f, 400f);
            });
        });
    }

    @Test
    void simulerPopulationAssBeneficiaireACREDateCreation3MoisAvant()
            throws ParseException, JsonIOException, JsonSyntaxException {

        // Si DE Français de France métropolitaine né le 5/07/1986, célibataire, 1 enfant à charge de 9ans, asf = 117€
        // Montant net journalier ASS = 16,89€, 0 mois cumulé ASS + salaire sur 3 derniers mois

        Demandeur demandeur = createDemandeurEmploiMicroEntrepreneurNonACRE(3);

        // Lorsque je simule mes prestations le 01/05/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023 sont :
        // AGEPI : 416€, Aide mobilité : 450€, ASS : 506€
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(1);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
        });
        // Alors les prestations du second mois 07/2023 sont :
        // ASS : 523€
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.AGEPI.getCode())).satisfies(agepi -> {
                assertThat(agepi.getMontant()).isEqualTo(416f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
        });
        // Alors les prestations du troisième mois 08/2023 sont :
        // ASS : 523€
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du quatrième mois 09/2023 sont :
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du cinquième mois 10/2023 sont :
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
        });
        // Alors les prestations du sixième mois 11/2023 sont :
        // Prime d'activité : 140€
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isBetween(126f, 400f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
        });
    }

    @Test
    void simulerPopulationAssBeneficiaireACREDateCreation4MoisAvant()
            throws ParseException, JsonIOException, JsonSyntaxException {

        // Si DE Français de France métropolitaine né le 5/07/1986, célibataire, 1 enfant à charge de 9ans, asf = 117€
        // Montant net journalier ASS = 16,89€, 0 mois cumulé ASS + salaire sur 3 derniers mois

        Demandeur demandeur = createDemandeurEmploiMicroEntrepreneurNonACRE(4);

        // Lorsque je simule mes prestations le 01/05/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023 sont :
        // AGEPI : 416€, Aide mobilité : 450€, ASS : 506€
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(1);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
        });
        // Alors les prestations du second mois 07/2023 sont :
        // ASS : 523€
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.AGEPI.getCode())).satisfies(agepi -> {
                assertThat(agepi.getMontant()).isEqualTo(416f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
        });
        // Alors les prestations du troisième mois 08/2023 sont :
        // ASS : 523€
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du quatrième mois 09/2023 sont :
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du cinquième mois 10/2023 sont :
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du sixième mois 11/2023 sont :
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isBetween(126f, 400f);
            });
        });
    }

    @Test
    void simulerPopulationAssBeneficiaireACREDateCreation5MoisAvant()
            throws ParseException, JsonIOException, JsonSyntaxException {

        // Si DE Français de France métropolitaine né le 5/07/1986, célibataire, 1 enfant à charge de 9ans, asf = 117€
        // Montant net journalier ASS = 16,89€, 0 mois cumulé ASS + salaire sur 3 derniers mois

        Demandeur demandeur = createDemandeurEmploiMicroEntrepreneurNonACRE(5);

        // Lorsque je simule mes prestations le 01/05/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023 sont :
        // AGEPI : 416€, Aide mobilité : 450€, ASS : 506€
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(1);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
        });
        // Alors les prestations du second mois 07/2023 sont :
        // ASS : 523€
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.AGEPI.getCode())).satisfies(agepi -> {
                assertThat(agepi.getMontant()).isEqualTo(416f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du troisième mois 08/2023 sont :
        // ASS : 523€
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du quatrième mois 09/2023 sont :
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du cinquième mois 10/2023 sont :
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
        });
        // Alors les prestations du sixième mois 11/2023 sont :
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode())).satisfies(asf -> {
                assertThat(asf.getMontant()).isEqualTo(117);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isBetween(126f, 400f);
            });
        });
    }
}
