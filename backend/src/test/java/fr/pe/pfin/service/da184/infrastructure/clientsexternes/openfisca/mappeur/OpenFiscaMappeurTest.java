package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.NationaliteEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypePopulationEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypesBeneficesMicroEntrepriseEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurTest extends Utile {
    private final LocalDate dateDebutSimulation = LocalDate.of(2023, 1, 1);

    @Test
    void mapDemandeurAlToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile("clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-avec-al.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = demandeur.getRessourcesFinancieresAvantSimulation();
        AidesCAF aidesCAF = createAidesCAF();
        AllocationsLogement aidesLogement = new AllocationsLogement();
        aidesLogement.setMoisN(385);
        aidesLogement.setMoisNMoins1(380);
        aidesCAF.setAidesLogement(aidesLogement);
        ressourcesFinancieres.setAidesCAF(aidesCAF);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurRevenusLocatifsToOpenFiscaPayloadTest() throws Exception {


        String openFiscaPayloadExpected = getStringFromJsonFile("clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-avec-revenus-locatifs.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = demandeur.getRessourcesFinancieresAvantSimulation();
        ressourcesFinancieres.setRevenusImmobilier3DerniersMois(3000f);
        AidesCAF aidesCAF = createAidesCAF();
        ressourcesFinancieres.setAidesCAF(aidesCAF);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurRevenusTravailleurIndependantToOpenFiscaPayloadTest() throws Exception {


        String openFiscaPayloadExpected = getStringFromJsonFile("clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-avec-revenus-travailleur-independant.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = demandeur.getRessourcesFinancieresAvantSimulation();
        ressourcesFinancieres.setChiffreAffairesIndependantDernierExercice(1000f);
        AidesCAF aidesCAF = createAidesCAF();
        ressourcesFinancieres.setAidesCAF(aidesCAF);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurRevenusMicroEntrepriseARToOpenFiscaPayloadTest() throws Exception {


        String openFiscaPayloadExpected = getStringFromJsonFile("clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-avec-revenus-micro-entreprise-ar.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.NON_BENEFICIAIRE, false, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = demandeur.getRessourcesFinancieresAvantSimulation();
        AidesCAF aidesCAF = createAidesCAF();
        ressourcesFinancieres.setAidesCAF(aidesCAF);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        InformationsPersonnelles informationsPersonnelles = demandeur.getInformationsPersonnelles();
        informationsPersonnelles.setMicroEntrepreneur(true);
        MicroEntreprise microEntreprise = new MicroEntreprise();
        microEntreprise.setDateRepriseCreationEntreprise(DateUtile.enleverAnneesALocalDate(dateDebutSimulation, 2));
        microEntreprise.setTypeBenefices(TypesBeneficesMicroEntrepriseEnum.AR.getCode());
        microEntreprise.setChiffreAffairesN(4000f);
        microEntreprise.setChiffreAffairesNMoins1(3000f);
        microEntreprise.setChiffreAffairesNMoins2(2000f);
        informationsPersonnelles.setMicroEntreprise(microEntreprise);

        demandeur.setInformationsPersonnelles(informationsPersonnelles);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurRevenusMicroEntrepriseBICToOpenFiscaPayloadTest() throws Exception {


        String openFiscaPayloadExpected = getStringFromJsonFile("clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-avec-revenus-micro-entreprise-bic.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.NON_BENEFICIAIRE, false, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = demandeur.getRessourcesFinancieresAvantSimulation();
        AidesCAF aidesCAF = createAidesCAF();
        ressourcesFinancieres.setAidesCAF(aidesCAF);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        InformationsPersonnelles informationsPersonnelles = demandeur.getInformationsPersonnelles();
        informationsPersonnelles.setMicroEntrepreneur(true);
        MicroEntreprise microEntreprise = new MicroEntreprise();
        microEntreprise.setDateRepriseCreationEntreprise(DateUtile.enleverAnneesALocalDate(dateDebutSimulation, 2));
        microEntreprise.setTypeBenefices(TypesBeneficesMicroEntrepriseEnum.BIC.getCode());
        microEntreprise.setChiffreAffairesN(4000f);
        microEntreprise.setChiffreAffairesNMoins1(3000f);
        microEntreprise.setChiffreAffairesNMoins2(2000f);
        informationsPersonnelles.setMicroEntreprise(microEntreprise);

        demandeur.setInformationsPersonnelles(informationsPersonnelles);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurRevenusMicroEntrepriseBNCToOpenFiscaPayloadTest() throws Exception {


        String openFiscaPayloadExpected = getStringFromJsonFile("clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-avec-revenus-micro-entreprise-bnc.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.NON_BENEFICIAIRE, false, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = demandeur.getRessourcesFinancieresAvantSimulation();
        AidesCAF aidesCAF = createAidesCAF();
        ressourcesFinancieres.setAidesCAF(aidesCAF);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        InformationsPersonnelles informationsPersonnelles = demandeur.getInformationsPersonnelles();
        informationsPersonnelles.setMicroEntrepreneur(true);
        MicroEntreprise microEntreprise = new MicroEntreprise();
        microEntreprise.setDateRepriseCreationEntreprise(DateUtile.enleverAnneesALocalDate(dateDebutSimulation, 2));
        microEntreprise.setTypeBenefices(TypesBeneficesMicroEntrepriseEnum.BNC.getCode());
        microEntreprise.setChiffreAffairesN(4000f);
        microEntreprise.setChiffreAffairesNMoins1(3000f);
        microEntreprise.setChiffreAffairesNMoins2(2000f);
        informationsPersonnelles.setMicroEntreprise(microEntreprise);

        demandeur.setInformationsPersonnelles(informationsPersonnelles);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurPensionInvaliditeToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile("clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-avec-pension-invalidite.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = demandeur.getRessourcesFinancieresAvantSimulation();
        AidesCPAM aidesCPAM = new AidesCPAM();
        aidesCPAM.setPensionInvalidite(200f);
        ressourcesFinancieres.setAidesCPAM(aidesCPAM);
        AidesCAF aidesCAF = createAidesCAF();
        ressourcesFinancieres.setAidesCAF(aidesCAF);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurRSAToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile("clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-avec-rsa.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(400f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setProchaineDeclarationTrimestrielle(3);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurAREToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile("clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-avec-are.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ARE, false, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setAllocationJournaliereBrute(37f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setAllocationJournaliereBruteTauxPlein(37f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setSalaireJournalierReferenceBrut(48f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setNombreJoursRestants(65);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setHasDegressiviteAre(false);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurASSToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile("clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-avec-ass.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-cdi.json",
            "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-cdd.json",
            "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-interim.json",
            "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-iae.json"
    })
    void mapDemandeurContratToOpenFiscaPayloadTest(String emplacementFichier) throws Exception {
        String openFiscaPayloadExpected = getStringFromJsonFile(emplacementFichier);
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));
        demandeur.getFutureFormation().setDureeFormationHebdomadaire(35f);
        demandeur.getFutureFormation().setDureeFormationEnMois(3f);
        demandeur.getFutureFormation().setSalaire(creerSalaireNet(757));
        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);
        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurSalarieAvecPrimeActiviteToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile("clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/demandeur-avec-prime-activite.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.NON_BENEFICIAIRE, false, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = demandeur.getRessourcesFinancieresAvantSimulation();
        AidesCAF aidesCAF = ressourcesFinancieres.getAidesCAF();
        aidesCAF.setHasPrimeActivite(true);
        aidesCAF.setPrimeActivite(500f);
        aidesCAF.setProchaineDeclarationTrimestrielle(3);
        ressourcesFinancieres.setAidesCAF(aidesCAF);

        demandeur.getInformationsPersonnelles().setSalarie(true);
        demandeur.getInformationsPersonnelles().setCumulAncienEtNouveauSalaire(true);
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(true);
        PeriodeTravailleeAvantSimulation periodeTravailleeAvantSimulation = new PeriodeTravailleeAvantSimulation();
        Salaire[] salaires = creerSalaires(0, 0, 3);
        Salaire salaire = creerSalaire(1000, 1238);
        salaires = ajouterSalaire(salaires, salaire, 0);
        salaires = ajouterSalaire(salaires, salaire, 1);
        salaires = ajouterSalaire(salaires, salaire, 2);
        periodeTravailleeAvantSimulation.setMois(createMoisTravaillesAvantSimulation(salaires));
        ressourcesFinancieres.setPeriodeTravailleeAvantSimulation(periodeTravailleeAvantSimulation);

        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur, dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

}
