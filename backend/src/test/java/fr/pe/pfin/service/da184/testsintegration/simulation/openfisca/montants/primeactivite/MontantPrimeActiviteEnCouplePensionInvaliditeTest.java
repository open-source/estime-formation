package fr.pe.pfin.service.da184.testsintegration.simulation.openfisca.montants.primeactivite;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.RessourcesFinancieresAvantSimulation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Salaire;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.OpenFiscaRetourSimulationHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class MontantPrimeActiviteEnCouplePensionInvaliditeTest extends Utile {

    private static final int NUMERA_MOIS_SIMULE_PPA = 6;
    private final LocalDate dateDebutSimulation = DateUtile.getDateJour();
    private final OpenFiscaClient openFiscaClient;

    @Autowired
    public MontantPrimeActiviteEnCouplePensionInvaliditeTest(OpenFiscaClient openFiscaClient) {
        this.openFiscaClient = openFiscaClient;
    }

    @Test
    void calculerPrimeActiviteEnCouplePensionInvaliditeTest() {

        // Si DE France Métropolitaine, en couple, pas d'enfants,
        // conjoint 200€ pension invalidite,
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        ressourcesFinancieresConjoint.setAidesCPAM(createPensionInvaliditeConjoint());
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        // Lorsque je calcule le montant de la prime d'activité
        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isZero();
    }

    @Test
    void calculerPrimeActiviteEnCouplePensionInvalidite1EnfantTest() {

        // Si DE France Métropolitaine, en couple
        // 1 enfant de 4 ans
        // conjoint 200€ pension invalidite,
        // APL 150€
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(150f));

        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        ressourcesFinancieresConjoint.setAidesCPAM(createPensionInvaliditeConjoint());
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        // Lorsque je calcule le montant de la prime d'activité
        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isZero();
    }

    @Test
    void calculerPrimeActiviteEnCouplePensionInvaliditeConjointSalaireTest() {

        // Si DE France Métropolitaine, en couple, 
        // conjoint salaire 1000€ net et 200€ pension invalidite,
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        Salaire salaireConjoint = createSalaireConjoint(1000, 1291);
        ressourcesFinancieresConjoint.setSalaire(salaireConjoint);
        ressourcesFinancieresConjoint.setAidesCPAM(createPensionInvaliditeConjoint());
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        // Lorsque je calcule le montant de la prime d'activité
        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isZero();
    }

    @Test
    void calculerPrimeActiviteEnCouplePensionInvaliditeConjointSalaire1EnfantTest() {

        // Si DE France Métropolitaine, en couple, 1 enfant de 4 ans,
        // conjoint salaire 1000€ net et 200€ pension invalidite,
        // apl 150€
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(150f));

        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        Salaire salaireConjoint = createSalaireConjoint(1000, 1291);
        ressourcesFinancieresConjoint.setSalaire(salaireConjoint);
        ressourcesFinancieresConjoint.setAidesCPAM(createPensionInvaliditeConjoint());
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        // Lorsque je calcule le montant de la prime d'activité
        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isZero();
    }
}
