package fr.pe.pfin.service.da184.application.services;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.domaine.use_cases.ObtenirSimulationAides;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DemandeurServiceTest {

    @Mock
    ObtenirSimulationAides obtenirSimulationAides;
    DemandeurService demandeurService;

    Demandeur demandeur;
    Simulation simulation;


    @BeforeEach
    void init() {
        demandeurService = new DemandeurService(obtenirSimulationAides);
        demandeur = new Demandeur();
        demandeur.setIdDemandeurEmploi("1234567A");
        demandeur.setBeneficiaireAides(new BeneficiaireAides());
        demandeur.setFutureFormation(new FutureFormation());
        demandeur.setSituationFamiliale(new SituationFamiliale());
        demandeur.setInformationsPersonnelles(new InformationsPersonnelles());
        demandeur.setRessourcesFinancieresAvantSimulation(new RessourcesFinancieresAvantSimulation());
        simulation = new Simulation();
        simulation.setSimulationsMensuelles(List.of());
        simulation.setMontantRessourcesFinancieresMoisAvantSimulation(100f);
    }

    @DisplayName("doit retourner un objet simulation pour un objet demandeur entrant")
    @Test
    void doitRetournerUneSimulationPourUnDemandeur() {
        when(obtenirSimulationAides.executer(any())).thenReturn(simulation);
        Simulation simuResultat = demandeurService.simulerMesAides(demandeur);
        assertThat(simuResultat).isEqualTo(simulation);
    }


}
