package fr.pe.pfin.service.da184.testsintegration.simulation.openfisca.montants.primeactivite;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.AidesCPAM;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.OpenFiscaRetourSimulationHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class MontantPrimeActiviteCelibatairePensionInvaliditeTest extends Utile {

    private static final int NUMERA_MOIS_SIMULE_PPA = 6;
    private final LocalDate dateDebutSimulation = DateUtile.getDateJour();
    private final OpenFiscaClient openFiscaClient;

    @Autowired
    public MontantPrimeActiviteCelibatairePensionInvaliditeTest(OpenFiscaClient openFiscaClient) {
        this.openFiscaClient = openFiscaClient;
    }

    /***************************************** célibataire ***************************************************************/

    @Test
    @DisplayName("doit vérifier la non prime d'activite pension invalidite d'un demandeur célibataire ayant un ASF 117€, APL 150€ et pension invalidité 200€/mois")
    void calculerPrimeActivitePensionInvaliditeCelibataireTest() {

        // Si DE France Métropolitaine, célibataire, 0 enfant à charge,
        // ASF 117€, APL 150€
        // pension d'invalidité 200€ par mois,
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales().setAllocationSoutienFamilial(117f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAidesLogement(creerAllocationLogement(150f));

        AidesCPAM aidesCPAM = new AidesCPAM();
        aidesCPAM.setPensionInvalidite(200f);
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesCPAM(aidesCPAM);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot, dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isZero();
    }

    @Test
    @DisplayName("doit vérifier la non prime d'activite pension invalidite d'un demandeur célibataire ayant un enfant de 4 ans, un ASF 117€, APL 150€ et pension invalidité 200€/mois")
    void calculerPrimeActivitePensionInvaliditeCelibataire1EnfantTest() {

        // Si DE France Métropolitaine, célibataire, 1 enfant à charge de 4ans
        // ASF 117€, APL 150€
        // pension d'invalidité 200€ par mois,
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales().setAllocationSoutienFamilial(117f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAidesLogement(creerAllocationLogement(150f));

        AidesCPAM aidesCPAM = new AidesCPAM();
        aidesCPAM.setPensionInvalidite(200f);
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesCPAM(aidesCPAM);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot, dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isZero();
    }

    @Test
    @DisplayName("doit vérifier la non prime d'activite pension invalidite d'un demandeur célibataire ayant 2 enfants de 4 ans et 5 ans, un ASF 223€, AF 134€, APL 150€ et pension invalidité 200€/mois")
    void calculerPrimeActivitePensionInvaliditeCelibataire2EnfantsTest() {
        // Si DE France Métropolitaine, célibataire, 2 enfants à charge de 4 ans et 5 ans,
        // asf 233€, af 134€, apl 150€, pension invalidité 200€
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        personnesACharge.add(createEnfant(5));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales().setAllocationSoutienFamilial(233f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales().setAllocationsFamiliales(134f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAidesLogement(creerAllocationLogement(150f));

        AidesCPAM aidesCPAM = new AidesCPAM();
        aidesCPAM.setPensionInvalidite(200f);
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesCPAM(aidesCPAM);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot, dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isZero();
    }
}
