package fr.pe.pfin.service.da184.application.ui.rest.valideur;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.BadRequestMessages;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.BeneficiaireAides;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SituationFamiliale;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class SituationFamilialeApiValideurTest extends Utile {

    @Test
    void controlerDonneeesEntreeSituationFamilialeTest() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(false, personnesACharge);

        BeneficiaireAides beneficiaireAides = demandeur.getBeneficiaireAides();

        assertThatThrownBy(() -> SituationFamilialeApiValideur.controlerDonnees(null, beneficiaireAides))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "situationFamiliale");
    }

    @Test
    void controlerDonneeesEntreeSituationFamilialeCoupleNullTest() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(false, personnesACharge);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(1);
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);

        SituationFamiliale situationFamiliale = demandeur.getSituationFamiliale();
        situationFamiliale.setIsEnCouple(null);
        demandeur.setSituationFamiliale(situationFamiliale);
        BeneficiaireAides beneficiaireAides = demandeur.getBeneficiaireAides();

        assertThatThrownBy(() -> SituationFamilialeApiValideur.controlerDonnees(situationFamiliale, beneficiaireAides))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "enCouple de situationFamiliale");
    }

    @Test
    void controlerDonneeesEntreeSituationFamilialeConjointAbsentTest() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(true, personnesACharge);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(1);
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);

        SituationFamiliale situationFamiliale = demandeur.getSituationFamiliale();
        situationFamiliale.setConjoint(null);
        demandeur.setSituationFamiliale(situationFamiliale);
        BeneficiaireAides beneficiaireAides = demandeur.getBeneficiaireAides();

        assertThatThrownBy(() -> SituationFamilialeApiValideur.controlerDonnees(situationFamiliale, beneficiaireAides))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "conjoint de situationFamiliale");
    }
}
