package fr.pe.pfin.service.da184.testsintegration.simulation.temporalite.ass;

import com.nimbusds.jose.shaded.gson.JsonIOException;
import com.nimbusds.jose.shaded.gson.JsonSyntaxException;
import fr.pe.pfin.service.da184.RedisTestConfiguration;
import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.DemandeurController;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SimulationMensuelle;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile.getMonthFromLocalDate;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = RedisTestConfiguration.class)
@ActiveProfiles("test")
class DemandeurAssSansCumulSalaireAvantSimulationTests extends Utile {

    private final DemandeurController demandeurController;

    @Autowired
    public DemandeurAssSansCumulSalaireAvantSimulationTests(DemandeurController demandeurController) {
        this.demandeurController = demandeurController;
    }

    @Test
    void simulerPopulationAssSansCumulSalaire() throws JsonIOException, JsonSyntaxException {

        // Si DE Français de France métropolitaine né le 5/11/1986, célibataire, 1
        // enfant à charge de 9ans, asf = 117€
        // Montant net journalier ASS = 16,89€, 0 mois cumulé ASS + salaire sur 3
        // derniers mois
        // kilométrage domicile -> taf = 80kms + 12 trajets
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(9));
        Demandeur demandeur = createDemandeurASS(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);
        demandeur.getFutureFormation().setDistanceActiviteDomicile(80f);
        demandeur.getFutureFormation().setNombreTrajetsDomicileTravail(12);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 0, 0));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationSoutienFamilial(117f);

        // Lorsque je simule mes prestations le 01/05/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023 sont :
        // AGEPI : 416€, Aide mobilité : 450€, ASS : 523€
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOLIDARITE_SPECIFIQUE.getCode()))
                    .satisfies(ass -> {
                        assertThat(ass.getMontant()).isEqualTo(523f);
                    });
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode()))
                    .satisfies(asf -> {
                        assertThat(asf.getMontant()).isEqualTo(117);
                    });
        });
        // Alors les prestations du second mois 07/2023 sont :
        // ASS : 506€
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.AGEPI.getCode())).satisfies(agepi -> {
                assertThat(agepi.getMontant()).isEqualTo(416f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode()))
                    .satisfies(asf -> {
                        assertThat(asf.getMontant()).isEqualTo(117);
                    });
        });
        // Alors les prestations du troisième mois 08/2023 sont :
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);

            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode()))
                    .satisfies(asf -> {
                        assertThat(asf.getMontant()).isEqualTo(117);
                    });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
        });
        // Alors les prestations du quatrième mois 09/2023 sont :
        // aucune aide
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode()))
                    .satisfies(asf -> {
                        assertThat(asf.getMontant()).isEqualTo(117);
                    });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
        });
        // Alors les prestations du cinquième mois 10/2023 sont :
        // Prime d'activité : 140€ (simulateur CAF : 129€)
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode()))
                    .satisfies(asf -> {
                        assertThat(asf.getMontant()).isEqualTo(117);
                    });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
        });
        // Alors les prestations du sixième mois 11/2023 sont :
        // Prime d'activité : 140€
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode()))
                    .satisfies(asf -> {
                        assertThat(asf.getMontant()).isEqualTo(117);
                    });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(395);
                    });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode()))
                    .satisfies(pa -> {
                        assertThat(pa.getMontant()).isEqualTo(374);
                    });
        });
    }
}
