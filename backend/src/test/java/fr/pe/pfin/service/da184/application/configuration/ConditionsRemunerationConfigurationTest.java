package fr.pe.pfin.service.da184.application.configuration;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/**
 *
 * @author EXGO2620
 */
@ActiveProfiles("test")
@SpringBootTest
public class ConditionsRemunerationConfigurationTest {

    @Autowired
    ConditionsRemunerationConfiguration instance;

    public ConditionsRemunerationConfigurationTest() {
    }

    /**
     * Test of conditionsRemunerationAdapteur method, of class
     * ConditionsRemunerationConfiguration.
     */
    @Test
    @DisplayName("Controle de l'initialisation du singleton -> non null")
    void testConditionsRemunerationAdapteur() {
        System.out.println("conditionsRemunerationAdapteur");
        assertNotNull(instance.conditionsRemunerationAdapteur());
    }
}
