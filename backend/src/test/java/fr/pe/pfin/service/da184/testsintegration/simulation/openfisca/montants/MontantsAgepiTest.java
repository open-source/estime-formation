package fr.pe.pfin.service.da184.testsintegration.simulation.openfisca.montants;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.OpenFiscaRetourSimulationHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class MontantsAgepiTest extends Utile {

    private final OpenFiscaClient openFiscaClient;

    @Autowired
    public MontantsAgepiTest(OpenFiscaClient openFiscaClient) {
        this.openFiscaClient = openFiscaClient;
    }

    @Test
    void calculer1EnfantsTest() throws Exception {


        // Si DE France Métropolitaine, célibataire, 1 enfant à charge moins de 10 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot, dateDebutSimulation);
        // Alors le demandeur est éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isEqualTo(416);
    }

    @Test
    void calculer2EnfantsTest() throws Exception {


        // Si DE France Métropolitaine, célibataire, 2 enfants à charge moins de 10 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        personnesACharge.add(createEnfant(5));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot, dateDebutSimulation);

        // Alors le demandeur est éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isEqualTo(478);
    }

    @Test
    void calculer3EnfantsTest() throws Exception {


        // Si DE France Métropolitaine, célibataire, 3 enfant à charge moins de 10 ans, 1 enfant à charge plus de 10 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(12));
        personnesACharge.add(createEnfant(9));
        personnesACharge.add(createEnfant(7));
        personnesACharge.add(createEnfant(2));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot, dateDebutSimulation);
        // Alors le demandeur est éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isEqualTo(478);
    }

    @Test
    void calculer4EnfantsTest() throws Exception {


        // Si DE France Métropolitaine, célibataire, 3 enfant à charge moins de 10 ans, 1 enfant à charge plus de 10 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        personnesACharge.add(createEnfant(9));
        personnesACharge.add(createEnfant(7));
        personnesACharge.add(createEnfant(2));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot, dateDebutSimulation);
        // Alors le demandeur est éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isEqualTo(541);
    }

    @Test
    void calculer1EnfantMayotteTest() throws Exception {


        // Si DE France Métropolitaine, célibataire, 1 enfant à charge moins de 10 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "97611"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("97600");
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot, dateDebutSimulation);
        // Alors le demandeur est éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isEqualTo(208);
    }

    @Test
    void calculer2EnfantsMayotteTest() throws Exception {


        // Si DE France Métropolitaine, célibataire, 2 enfants à charge moins de 10 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        personnesACharge.add(createEnfant(5));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "97611"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("97600");
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot, dateDebutSimulation);
        // Alors le demandeur est éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isEqualTo(239);
    }

    @Test
    void calculer3EnfantsMayotteTest() throws Exception {


        // Si DE France Métropolitaine, célibataire, 3 enfant à charge moins de 10 ans, 1 enfant à charge plus de 10 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(12));
        personnesACharge.add(createEnfant(9));
        personnesACharge.add(createEnfant(7));
        personnesACharge.add(createEnfant(5));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "97611"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("97600");
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot, dateDebutSimulation);
        // Alors le demandeur est éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isEqualTo(239);
    }
}
