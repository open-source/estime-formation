package fr.pe.pfin.service.da184.testsintegration.simulation.openfisca.montants.rsa;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.StatutOccupationLogementEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.OpenFiscaRetourSimulationHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class MontantRSACelibataireTest extends Utile {

    private static final int NUMERA_MOIS_SIMULE_RSA = 2;
    private final LocalDate dateDebutSimulation = DateUtile.getDateJour();
    private final OpenFiscaClient openFiscaClient;

    @Autowired
    public MontantRSACelibataireTest(OpenFiscaClient openFiscaClient) {
        this.openFiscaClient = openFiscaClient;
    }

    @Test
    void calculerRSACelibataireMoins26Test() {

        // Si DE Français, célibataire, seul depuis plus de 18 mois, non propriétaire
        // sans forfait logement
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1998, 7, 5));
        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(true);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(0f);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelNet(552f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerRSA(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_RSA);

        assertThat(openFiscaRetourSimulation.getMontantRSA()).isEqualTo(621.0f);
    }

    @Test
    void calculerRSACelibatairePlus26Test() {

        // Si DE Français, célibataire, seul depuis plus de 18 mois, non propriétaire
        // sans forfait logement
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(true);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.getInformationsPersonnelles().getLogement().setStatutOccupationLogement(StatutOccupationLogementEnum.NON_RENSEIGNE.getLibelle());
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(0f);
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerRSA(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_RSA);

        assertThat(openFiscaRetourSimulation.getMontantRSA()).isEqualTo(636f);
    }

    @Test
    void calculerRSACelibataire1EnfantTest() {

        // Si DE Français, célibataire, seul depuis plus de 18 mois, non propriétaire
        // 1 enfant à charge de 6ans,
        // sans forfait logement
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(true);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.getInformationsPersonnelles().getLogement().setStatutOccupationLogement(StatutOccupationLogementEnum.NON_RENSEIGNE.getLibelle());
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(0f);
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerRSA(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_RSA);

        assertThat(openFiscaRetourSimulation.getMontantRSA()).isEqualTo(954f);
    }

    @Test
    void calculerRSACelibataire2EnfantsTest() {

        // Si DE Français, célibataire, seul depuis plus de 18 mois, non propriétaire
        // 2 enfants à charge de 6 ans et 8 ans
        // sans forfait logement
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(true);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.getInformationsPersonnelles().getLogement().setStatutOccupationLogement(StatutOccupationLogementEnum.NON_RENSEIGNE.getLibelle());
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(0f);
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerRSA(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_RSA);

        assertThat(openFiscaRetourSimulation.getMontantRSA()).isEqualTo(1144f);
    }

    @Test
    void calculerRSACelibataire3EnfantsAPLTest() {

        // Si DE Français, célibataire, seul depuis plus de 18 mois, non propriétaire
        // 3 enfants à charge de 6 ans, 8 ans et 12 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        personnesACharge.add(createEnfant(12));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(true);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);
        demandeur.getInformationsPersonnelles().getLogement().setStatutOccupationLogement(StatutOccupationLogementEnum.LOCATAIRE_NON_MEUBLE.getLibelle());
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(550f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(450f));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerRSA(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_RSA);

        assertThat(openFiscaRetourSimulation.getMontantRSA()).isEqualTo(1210f);
    }

    @Test
    void calculerRSACelibataire4EnfantsALSTest() {

        // Si DE Français, célibataire, seul depuis plus de 18 mois, non propriétaire
        // 3 enfants à charge de 4, 6, 10 et 12 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(10));
        personnesACharge.add(createEnfant(12));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(true);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);
        demandeur.getInformationsPersonnelles().getLogement().setStatutOccupationLogement(StatutOccupationLogementEnum.LOCATAIRE_MEUBLE.getLibelle());
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(550f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(250f));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerRSA(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_RSA);

        assertThat(openFiscaRetourSimulation.getMontantRSA()).isEqualTo(1464f);
    }
}
