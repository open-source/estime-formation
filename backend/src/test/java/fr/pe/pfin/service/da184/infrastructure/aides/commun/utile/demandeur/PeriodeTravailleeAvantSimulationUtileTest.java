package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypePopulationEnum;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

class PeriodeTravailleeAvantSimulationUtileTest extends Utile {

    Demandeur demandeur;

    @Test
    @DisplayName("doit retourner 8 pour le nombre de mois travailles au cours des X derniers mois avant simulation")
    void doitRetournerHuitPourXDerniersMoisAvantSimulation() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setPeriodeTravailleeAvantSimulation(creerPeriodeTravailleeAvantSimulation(250, 273, 12));
        assertThat(PeriodeTravailleeAvantSimulationUtile.getNombreMoisTravaillesAuCoursDesXDerniersMoisAvantSimulation(demandeur, 8)).isEqualTo(8);
    }

    @Test
    @DisplayName("doit retourner un salaire avant période simulation")
    void doitRetournerSalaireAvantPeriodeSimulation() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setPeriodeTravailleeAvantSimulation(creerPeriodeTravailleeAvantSimulation(250, 273, 12));
        assertThat(PeriodeTravailleeAvantSimulationUtile.getSalaireAvantPeriodeSimulation(demandeur, 8)).isNotNull();
    }

    @Test
    @DisplayName("doit retourner un MoisTravailles avant simulation")
    void doitRetournerMoisTravaillesAvantSimulation() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, true, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setPeriodeTravailleeAvantSimulation(creerPeriodeTravailleeAvantSimulation(250, 273, 12));
        assertThat(PeriodeTravailleeAvantSimulationUtile.getMoisTravaillesAvantSimulation(demandeur, 2)).isNotNull();
    }


    @Test
    @DisplayName("doit retourner un salaire avant période simulation pour une personne")
    void doitRetournerSalaireAvantPeriodeSimulationPourUnePersonne() {
        assertThat(PeriodeTravailleeAvantSimulationUtile.getSalaireAvantPeriodeSimulationPersonne(createPersonne(LocalDate.of(2000, 1, 1)), 2)).isNotNull();
    }

    @Test
    @DisplayName("doit retourner vrai sur salaire avant periode simulation")
    void doitRetournerVraiSurSalaireAvantPeriodeSimulation() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, true, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setPeriodeTravailleeAvantSimulation(creerPeriodeTravailleeAvantSimulation(250, 273, 12));
        assertThat(PeriodeTravailleeAvantSimulationUtile.hasSalairesAvantPeriodeSimulation(demandeur)).isTrue();
    }

    @Test
    @DisplayName("doit retourner vrai sur salaire avant periode simulation et Index")
    void doitRetournerVraiSurSalaireAvantPeriodeSimulationEtIndex() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, true, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setPeriodeTravailleeAvantSimulation(creerPeriodeTravailleeAvantSimulation(250, 273, 12));
        assertThat(PeriodeTravailleeAvantSimulationUtile.hasSalairesAvantPeriodeSimulation(demandeur, 2)).isTrue();
    }

    @Test
    @DisplayName("doit retourner faux sur salaire avant periode simulation et Index pour une personne")
    void doitRetournerFauxSurSalaireAvantPeriodeSimulationEtIndexPourUnePersonne() {
        assertThat(PeriodeTravailleeAvantSimulationUtile.hasSalairesAvantPeriodeSimulationPersonne(createPersonne(LocalDate.of(2000, 1, 1)), 2)).isFalse();
    }


}
