package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.caf.SimulateurAidesCAF;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypePopulationEnum;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur.SituationFamilialeUtile.MOIS_LIMITE_3_ANS;
import static org.assertj.core.api.Assertions.assertThat;

class SituationFamilialeUtileTest extends Utile {

    Demandeur demandeur;

    @Test
    @DisplayName("doit retourner vrai pour un demandeur en couple")
    void doitRetournerVraiPourUnDemandeurEnCouple() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, true, Collections.emptyList());
        assertThat(SituationFamilialeUtile.isEnCouple(demandeur)).isTrue();
    }

    @Test
    @DisplayName("doit retourner faux pour un demandeur pas en couple")
    void doitRetournerFauxPourUnDemandeurPasEnCouple() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        assertThat(SituationFamilialeUtile.isEnCouple(demandeur)).isFalse();
    }

    @Test
    @DisplayName("doit retourner vrai pour un demandeur ayant des personnes à charge")
    void doitRetournerVraiPourUnDemandeurAyantDesPersonnesACharge() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, List.of(createEnfant(1)));
        assertThat(SituationFamilialeUtile.hasPersonnesACharge(demandeur)).isTrue();
    }


    @Test
    @DisplayName("doit retourner faux pour un demandeur n'ayant pas des personnes à charge")
    void doitRetournerFauxPourUnDemandeurNAyantPasDesPersonnesACharge() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        assertThat(SituationFamilialeUtile.hasPersonnesACharge(demandeur)).isFalse();
    }


    @Test
    @DisplayName("doit retourner vrai pour un demandeur ayant des personnes à charge eligible pour prise en compte Prime d'Activite")
    void doitRetournerVraiPourUnDemandeurAyantDesPersonnesAChargeEligiblePrimeActivite() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, List.of(createEnfant(1)));
        assertThat(SituationFamilialeUtile.hasPersonnesAChargeEligiblesPourPriseEnComptePrimeActivite(demandeur)).isTrue();
    }

    @Test
    @DisplayName("doit retourner faux pour un demandeur n'ayant pas des personnes à charge eligible pour prise en compte Prime d'Activite")
    void doitRetournerFauxPourUnDemandeurNAyantPasDesPersonnesAChargeEligiblePrimeActivite() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, List.of(createEnfant(SimulateurAidesCAF.AGE_MAX_PERSONNE_A_CHARGE_PPA_RSA + 1)));
        assertThat(SituationFamilialeUtile.hasPersonnesAChargeEligiblesPourPriseEnComptePrimeActivite(demandeur)).isFalse();
    }

    @Test
    @DisplayName("doit retourner faux pour un demandeur n'ayant pas des personnes à charge et donc eligible pour prise en compte Prime d'Activite")
    void doitRetournerFauxPourUnDemandeurNAyantPasDesPersonnesAChargeEtDoncPasEligiblePrimeActivite() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        assertThat(SituationFamilialeUtile.hasPersonnesAChargeEligiblesPourPriseEnComptePrimeActivite(demandeur)).isFalse();
    }


    @Test
    @DisplayName("doit retourner une personne à charge pour un demandeur ayant une personne à charge")
    void doitRetournerUnePourUnDemandeurAyantUnePersonneACharge() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, List.of(createEnfant(1)));
        assertThat(SituationFamilialeUtile.getPersonnesAChargeAgeInferieurAgeLimite(demandeur)).isPresent().get().asList().hasSize(1);
    }

    @Test
    @DisplayName("doit retourner empty pour un demandeur n'ayant pas de personne à charge")
    void doitRetournerEmptyPourUnDemandeurNAyantPasDePersonneACharge() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        assertThat(SituationFamilialeUtile.getPersonnesAChargeAgeInferieurAgeLimite(demandeur)).isEmpty();
    }

    @Test
    @DisplayName("doit retourner une personne à charge pour un demandeur ayant une personne à charge dans le mois simule")
    void doitRetournerUnePourUnDemandeurAyantUnePersonneAChargeMoisSimule() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, List.of(createEnfant(1)));
        assertThat(SituationFamilialeUtile.getPersonnesAChargeAgeInferieur37MoisLimitePourMoisSimule(demandeur, 1)).isPresent().get().asList().hasSize(1);
    }

    @Test
    @DisplayName("doit retourner empty à charge pour un demandeur n'ayant pas de personne à charge dans le mois simule")
    void doitRetournerEmptyPourUnDemandeurNAyantPasDePersonneAChargeMoisSimule() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        assertThat(SituationFamilialeUtile.getPersonnesAChargeAgeInferieur37MoisLimitePourMoisSimule(demandeur, 1)).isEmpty();
    }

    @Test
    @DisplayName("doit retourner empty pour un demandeur n'ayant pas de personne à charge de mois de 3 ans dans le mois simule")
    void doitRetournerUnePourUnDemandeurNAyantPasDePersonneAChargeMoisSimule() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, List.of(createEnfant(MOIS_LIMITE_3_ANS / 12 + 1)));
        assertThat(SituationFamilialeUtile.getPersonnesAChargeAgeInferieur37MoisLimitePourMoisSimule(demandeur, 1)).isPresent().get().asList().isEmpty();
    }

    @Test
    @DisplayName("doit retourner vrai pour un demandeur seul de plus de 18 mois")
    void doitRetournerVraiPourDemandeurSeulPlus18Mois() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(true);
        assertThat(SituationFamilialeUtile.isSeulPlusDe18Mois(demandeur)).isTrue();
    }

    @Test
    @DisplayName("doit retourner faux pour un demandeur pas seul")
    void doitRetournerFauxPourDemandeurPasSeul() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(false);
        assertThat(SituationFamilialeUtile.isSeulPlusDe18Mois(demandeur)).isFalse();
    }


    @Test
    @DisplayName("doit retourner vrai pour un demandeur ayant une personne à charge de - de 3 ans dans le mois simule")
    void doitRetournerVraiPourUnDemandeurAyantUnePersonneAChargeMoisSimuleMoins3Ans() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, List.of(createEnfant(2)));
        assertThat(SituationFamilialeUtile.hasEnfantMoinsDe3AnsPourMoisSimule(demandeur, 1)).isTrue();
    }

    @Test
    @DisplayName("doit retourner faux pour un demandeur ayant une personne à charge de + de 3 ans dans le mois simule")
    void doitRetournerFauxPourUnDemandeurAyantUnePersonneAChargeMoisSimulePlus3Ans() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, List.of(createEnfant(4)));
        assertThat(SituationFamilialeUtile.hasEnfantMoinsDe3AnsPourMoisSimule(demandeur, 1)).isFalse();
    }

    @Test
    @DisplayName("doit retourner faux pour un demandeur n'ayant pas de personne à charge de - de 3 ans dans le mois simule")
    void doitRetournerFauxPourUnDemandeurNAyantPasUnePersonneAChargeMoisSimuleMoins3Ans() {
        demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        assertThat(SituationFamilialeUtile.hasEnfantMoinsDe3AnsPourMoisSimule(demandeur, 1)).isFalse();
    }
}
