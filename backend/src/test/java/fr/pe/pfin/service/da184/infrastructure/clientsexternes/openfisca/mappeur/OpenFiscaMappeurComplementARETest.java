package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurComplementARETest {

    final LocalDate dateDeSimulation = LocalDate.now();

    @Test
    void doitRetournerZeroPourMontantComplementAREBrutAvecOpenFiscaRootVide() {
        assertThat(OpenFiscaMappeurComplementARE.getMontantComplementAREBrut(new OpenFiscaRoot(), dateDeSimulation, 0)).isEqualTo(0f);
    }

    @Test
    void doitRetournerZeroPourMontantComplementARENetAvecOpenFiscaRootVide() {
        assertThat(OpenFiscaMappeurComplementARE.getMontantComplementARENet(new OpenFiscaRoot(), dateDeSimulation, 0)).isEqualTo(0f);
    }

    @Test
    void doitRetournerZeroPourMontantDeductionsComplementAREAvecOpenFiscaRootVide() {
        assertThat(OpenFiscaMappeurComplementARE.getMontantDeductionsComplementARE(new OpenFiscaRoot(), dateDeSimulation, 0)).isEqualTo(0f);
    }

    @Test
    void doitRetournerZeroPourMontantCRCComplementAREAvecOpenFiscaRootVide() {
        assertThat(OpenFiscaMappeurComplementARE.getMontantCRCComplementARE(new OpenFiscaRoot(), dateDeSimulation, 0)).isEqualTo(0f);
    }

    @Test
    void doitRetournerZeroPourMontantCRDSComplementAREAvecOpenFiscaRootVide() {
        assertThat(OpenFiscaMappeurComplementARE.getMontantCRDSComplementARE(new OpenFiscaRoot(), dateDeSimulation, 0)).isEqualTo(0f);
    }

    @Test
    void doitRetournerZeroPourNombreJoursIndemnisesComplementAREAvecOpenFiscaRootVide() {
        assertThat(OpenFiscaMappeurComplementARE.getNombreJoursIndemnisesComplementARE(new OpenFiscaRoot(), dateDeSimulation, 0)).isEqualTo(0f);
    }

    @Test
    void doitRetournerZeroPourNombreJoursRestantsAREAvecOpenFiscaRootVide() {
        assertThat(OpenFiscaMappeurComplementARE.getNombreJoursRestantsARE(new OpenFiscaRoot(), dateDeSimulation, 0)).isEqualTo(0f);
    }
}
