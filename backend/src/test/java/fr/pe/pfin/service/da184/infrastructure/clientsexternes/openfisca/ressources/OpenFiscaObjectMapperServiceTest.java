package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author EXGO2620
 */
class OpenFiscaObjectMapperServiceTest {
    /**
     * Test of getJsonStringFromObject method, of class OpenFiscaObjectMapperService.
     */
    @Test
    void testGetJsonStringFromObject() {
        System.out.println("getJsonStringFromObject");
        //A complexifier et rendre utile en remplissant l'objet OpenFiscaFamille
        Object object = new OpenFiscaFamille();
        String expResult = "{}";

        String result = OpenFiscaObjectMapperService.getJsonStringFromObject(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpenFiscaRootFromJsonString method, of class OpenFiscaObjectMapperService.
     */
    @Test
    void testGetOpenFiscaRootFromJsonString() {
        System.out.println("getOpenFiscaRootFromJsonString");
        //On peut trouver mieux à faire que de passer par un controle sur le .toString()
        //Idem que le test precedent, il faudrait enrichir le contenu de l'objet pour que le test soit plus crédible et utile.
        String jsonString = "{}";
        OpenFiscaRoot expResult = new OpenFiscaRoot();

        OpenFiscaRoot result = OpenFiscaObjectMapperService.getOpenFiscaRootFromJsonString(jsonString);
        assertEquals(expResult.toString(), result.toString());
    }
}
