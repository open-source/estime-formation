package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class PrimeActiviteRSAUtileTest {

    @Test
    @DisplayName("doit retourner un objet Aide pour un montant et un report")
    void doitRetournerUnObjetAidePourMontantEtReport() {
        Aide aide = new Aide();
        aide.setReportee(true);
        aide.setOrganisme(OrganismeEnum.CAF.getNomCourt());
        aide.setMontant(100f);
        aide.setMessagesAlerte(List.of(MessageInformatifEnum.PPA_AUTOMATIQUE_SI_BENEFICIAIRE_RSA.getMessage()));
        assertThat(PrimeActiviteRSAUtile.creerAidePrimeActivite(100f, true))
                .isNotNull().isEqualTo(aide);
    }

    @Test
    @DisplayName("doit retourner un objet Aide pour un montant à zéro et pas de report")
    void doitRetournerUnObjetAidePourPasDeMontantEtPasDeReport() {
        Aide aide = new Aide();
        aide.setReportee(false);
        aide.setOrganisme(OrganismeEnum.CAF.getNomCourt());
        aide.setMontant(0f);
        aide.setMessagesAlerte(List.of(MessageInformatifEnum.PPA_AUTOMATIQUE_SI_BENEFICIAIRE_RSA.getMessage()));
        assertThat(PrimeActiviteRSAUtile.creerAidePrimeActivite(0f, false))
                .isNotNull().isEqualTo(aide);
    }
}
