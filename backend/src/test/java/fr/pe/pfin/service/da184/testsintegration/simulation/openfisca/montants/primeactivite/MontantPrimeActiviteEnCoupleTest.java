package fr.pe.pfin.service.da184.testsintegration.simulation.openfisca.montants.primeactivite;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.RessourcesFinancieresAvantSimulation;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.OpenFiscaRetourSimulationHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class MontantPrimeActiviteEnCoupleTest extends Utile {

    private static final int NUMERA_MOIS_SIMULE_PPA = 6;
    private final LocalDate dateDebutSimulation = DateUtile.getDateJour();
    private final OpenFiscaClient openFiscaClient;

    @Autowired
    public MontantPrimeActiviteEnCoupleTest(OpenFiscaClient openFiscaClient) {
        this.openFiscaClient = openFiscaClient;
    }

    /*****************************************
     * en couple
     ***************************************************************/

    @Test
    void calculerPrimeActiviteEnCoupleTest() {

        // Si DE France Métropolitaine, en couple, conjoint salaire 1200€, 0 enfant,
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        ressourcesFinancieresConjoint.setSalaire(createSalaireConjoint(1200, 1544));
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(170f);
    }

    @Test
    void calculerPrimeActiviteEnCouple1EnfantTest() {

        // Si DE France Métropolitaine, en couple, conjoint salaire 1200€, 1 enfant de 6ans,
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        ressourcesFinancieresConjoint.setSalaire(createSalaireConjoint(1200, 1544));
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(321f);
    }

    @Test
    void calculerPrimeActiviteEnCouple2EnfantsTest() {

        // Si DE France Métropolitaine, en couple, conjoint salaire 1200€,
        // 2 enfants à charge de 6ans et 8ans, af 132€
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationsFamiliales(132f);
        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        ressourcesFinancieresConjoint.setSalaire(createSalaireConjoint(1200, 1544));
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(376f);
    }

    @Test
    void calculerPrimeActiviteEnCouple2EnfantsAPLTest() {

        // Si DE France Métropolitaine, en couple, conjoint salaire 1200€,
        // 2 enfants à charge de 6ans et 8ans,
        // af 132€, apl 20€
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationsFamiliales(132f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(450f));
        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        ressourcesFinancieresConjoint.setSalaire(createSalaireConjoint(1200, 1544));
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(376f);
    }

    @Test
    void calculerPrimeActiviteEnCouple3EnfantsTest() {

        // Si DE France Métropolitaine, en couple, conjoint salaire 1200€,
        // 3 enfants à charge de 4ans, 6ans et 8ans,
        // af 303€, cf 259€,
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationsFamiliales(303f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setComplementFamilial(259f);
        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        ressourcesFinancieresConjoint.setSalaire(createSalaireConjoint(1200, 1544));
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(260f);
    }

    @Test
    void calculerPrimeActiviteEnCoupleTest3EnfantsAPL() {

        // Si DE France Métropolitaine, en couple, conjoint salaire 1200€,
        // 3 enfants à charge de 4ans, 6ans et 8ans,
        // af 472€, asf 466€, cf 259€, apl 520€,
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationsFamiliales(472f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationSoutienFamilial(466f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setComplementFamilial(259f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(520f));
        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        ressourcesFinancieresConjoint.setSalaire(createSalaireConjoint(1200, 1544));
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isZero();
    }

    @Test
    void calculerPrimeActiviteEnCouple4EnfantsTest() {

        // Si DE France Métropolitaine, en couple, conjoint salaire 1200€,
        // 4 enfants à charge de 4ans, 6ans, 8ans et 12 ans,
        // af 472€, cf 259€,
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        personnesACharge.add(createEnfant(12));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationsFamiliales(472f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setComplementFamilial(259f);
        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        ressourcesFinancieresConjoint.setSalaire(createSalaireConjoint(1200, 1544));
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(340f);
    }

    @Test
    void calculerPrimeActiviteEnCouple4EnfantsAPLTest() {

        // Si DE France Métropolitaine, en couple, conjoint salaire 1200€,
        // 4 enfants à charge de 4ans, 6ans, 8ans et 12 ans,
        // af 472€, cf 259€, apl 230€,
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        personnesACharge.add(createEnfant(12));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationsFamiliales(472f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setComplementFamilial(259f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(230f));
        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        ressourcesFinancieresConjoint.setSalaire(createSalaireConjoint(1200, 1544));
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(340f);
    }
}
