package fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.AllocationASS;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypePopulationEnum;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

class AllocationSolidariteSpecifiqueUtileTest extends Utile {

    @Test
    @DisplayName("doit simuler une aide ASS")
    void doitRetournerUnAideAREAvantApplicationPrelevementSource() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ASS, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(10.0f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setDateDerniereOuvertureDroit(LocalDate.of(2023,11,01));
        Optional<Aide> aideAttendu = AllocationSolidariteSpecifiqueUtile.simulerAide(demandeur, 12, LocalDate.of(2023,12,01));
        assertThat(aideAttendu).isPresent();
    }

    @Test
    @DisplayName("doit calculer le montant de l'ass avec une OD sur le mois en cours")
    void doitCalculerMontantASSAvecODSurMoisEnCours() {
        // Si OD au 15/12/2023, avec 10€/j d'ASS => 300€ d'ASS en décembre, 160€ d'ASS en janvier
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ASS, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(10f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setDateDerniereOuvertureDroit(LocalDate.of(2023,12,15));
        assertThat(AllocationSolidariteSpecifiqueUtile.calculerMontant(demandeur, LocalDate.of(2023,12,20))).isEqualTo(300f);
    }

    @Test
    @DisplayName("doit calculer le montant de l'ass avec une OD sur le mois à N-1")
    void doitCalculerMontantASSAvecODSurMoisPrecedent() {
        // Si OD au 15/11/2023, avec 10e/j d'ASS  => 160€ d'ASS en décembre, 160€ d'ASS en janvier
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ASS, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(10f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setDateDerniereOuvertureDroit(LocalDate.of(2023,11,15));
        assertThat(AllocationSolidariteSpecifiqueUtile.calculerMontant(demandeur, LocalDate.of(2023,12,20))).isEqualTo(160f);
    }

    @Test
    @DisplayName("doit calculer le montant de l'ass avec une OD sur le mois à N+1")
    void doitCalculerMontantASSAvecODSurMoisSuivant() {
        // Si OD au 01/12/2023 avec 10€/j d'ASS => 310€ d'ASS en décembre, 310€ d'ASS en janvier
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ASS, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(10f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setDateDerniereOuvertureDroit(LocalDate.of(2023,12,01));
        assertThat(AllocationSolidariteSpecifiqueUtile.calculerMontant(demandeur, LocalDate.of(2024, 1, 1))).isEqualTo(310f);
    }

    @Test
    @DisplayName("doit retourner un demandeur sans ASS")
    void doitRetrounerDemandeurSansASS() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        assertThat(AllocationSolidariteSpecifiqueUtile.calculerMontant(demandeur, LocalDate.of(2024, 1, 1))).isEqualTo(0f);
    }

    @Test
    @DisplayName("doit retourner un demandeur avec ASS mais sans montant journalier")
    void doitRetrounerDemandeurAvecASSSansMontantJournalier() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ASS, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().setAllocationASS(new AllocationASS());
        assertThat(AllocationSolidariteSpecifiqueUtile.calculerMontant(demandeur, LocalDate.of(2024, 1, 1))).isEqualTo(0f);
    }
}
