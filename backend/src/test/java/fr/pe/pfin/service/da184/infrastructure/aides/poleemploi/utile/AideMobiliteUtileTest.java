package fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class AideMobiliteUtileTest {

    @Test
    void messageAlerteAideMobiliteTest() {

        Aide aideMobilite = AideMobiliteUtile.creerAideMobilite(500f);

        List<String> messagesAlertesAttendus = new ArrayList<>();
        messagesAlertesAttendus.add(MessageInformatifEnum.AGEPI_AM_DELAI_DEMANDE.getMessage());

        // le message d'alerte sur le délai de demande Aide mobilité est présent
        assertThat(aideMobilite.getMessagesAlerte()).isEqualTo(messagesAlertesAttendus);
    }
}
