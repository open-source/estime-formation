package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.periodes;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.OpenFiscaMappeurPeriode;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurPeriodesSalairesConjointTest extends Utile {

    private final LocalDate dateDebutSimulation = LocalDate.of(2023, 1, 1);

    /*****************
     * TESTS SUR creerPeriodesSalaireDemandeur
     ********************************************************/

    /**
     * CONTEXTE DES TESTS
     * <p>
     * date demande simulation : 01-06-2023
     * <p>
     * avant simulation M-2 05/2023 M-1 06/2023
     * <p>
     * période de la simulation sur 6 mois M1 07/2023 M2 08/2023 M3 09/2023 M4
     * 10/2023 M5 11/2023 M6 12/2023
     ********************************************************************************/

    /**
     * Demandeur RSA + salaire dans les 3 mois avant la simulation
     * Création de la période salaire pour une simulation au mois M5
     */
    @Test
    void mapPeriodeSalaireTest0() throws Exception {

        String openFiscaPayloadSalaireNetExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesSalairesTests/conjoint/salaire-net-8-mois-salaire-avant-simulation.json");

        Demandeur demandeur = creerDemandeurSalairesTests();

        OpenFiscaIndividu conjointOpenFisca = new OpenFiscaIndividu();
        OpenFiscaMappeurPeriode.creerPeriodesSalairePersonne(conjointOpenFisca,
                demandeur.getSituationFamiliale().getConjoint(), dateDebutSimulation);

        assertThat(conjointOpenFisca.getSalaireNet().toString())
                .hasToString(openFiscaPayloadSalaireNetExpected);
    }

    private Demandeur creerDemandeurSalairesTests() throws ParseException {

        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);

        SituationFamiliale situationFamiliale = new SituationFamiliale();
        situationFamiliale.setIsEnCouple(true);
        Personne conjoint = new Personne();
        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        Salaire salaireConjoint = new Salaire();
        salaireConjoint.setMontantMensuelNet(1200);
        salaireConjoint.setMontantMensuelBrut(1544);
        ressourcesFinancieresConjoint.setSalaire(salaireConjoint);

        ressourcesFinancieresConjoint.setHasTravailleAuCoursDerniersMois(true);
        PeriodeTravailleeAvantSimulation periodeTravailleeAvantSimulation = new PeriodeTravailleeAvantSimulation();
        Salaire[] salaires = creerSalaires(0, 0, 12);
        Salaire salaireMoisMoins1 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins3 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins4 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins5 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins7 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins9 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins11 = creerSalaire(850, 1101);
        salaires = ajouterSalaire(salaires, salaireMoisMoins1, 1);
        salaires = ajouterSalaire(salaires, salaireMoisMoins3, 3);
        salaires = ajouterSalaire(salaires, salaireMoisMoins4, 4);
        salaires = ajouterSalaire(salaires, salaireMoisMoins5, 5);
        salaires = ajouterSalaire(salaires, salaireMoisMoins7, 7);
        salaires = ajouterSalaire(salaires, salaireMoisMoins9, 9);
        salaires = ajouterSalaire(salaires, salaireMoisMoins11, 11);
        periodeTravailleeAvantSimulation.setMois(createMoisTravaillesAvantSimulation(salaires));
        ressourcesFinancieresConjoint.setPeriodeTravailleeAvantSimulation(periodeTravailleeAvantSimulation);
        conjoint.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);
        situationFamiliale.setConjoint(conjoint);
        demandeur.setSituationFamiliale(situationFamiliale);

        return demandeur;
    }
}
