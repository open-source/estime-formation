package fr.pe.pfin.service.da184.testsintegration.simulation.openfisca.montants.rsa;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.StatutOccupationLogementEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.OpenFiscaRetourSimulationHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class MontantRSACoupleTest extends Utile {

    private static final int NUMERA_MOIS_SIMULE_RSA = 2;
    private final LocalDate dateDebutSimulation = DateUtile.getDateJour();
    private final OpenFiscaClient openFiscaClient;

    @Autowired
    public MontantRSACoupleTest(OpenFiscaClient openFiscaClient) {
        this.openFiscaClient = openFiscaClient;
    }

    @Test
    void calculerRSACoupleALF() {

        // Si DE Français, couple, non propriétaire
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getInformationsPersonnelles().getLogement().setStatutOccupationLogement(StatutOccupationLogementEnum.LOCATAIRE_HLM.getLibelle());
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(550f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(250f));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerRSA(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_RSA);

        assertThat(openFiscaRetourSimulation.getMontantRSA()).isEqualTo(801f);
    }

    @Test
    void calculerRSACouple1EnfantTest() {

        // Si DE Français, couple, propriétaire
        // 1 enfant à charge de 6ans,
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getInformationsPersonnelles().getLogement().setStatutOccupationLogement(StatutOccupationLogementEnum.PROPRIETAIRE.getLibelle());
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(0f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerRSA(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_RSA);

        assertThat(openFiscaRetourSimulation.getMontantRSA()).isEqualTo(955f);
    }

    @Test
    void calculerRSACouple2EnfantsTest() {

        // Si DE Français, couple, propriétaire
        // 2 enfants à charge de 6 ans et 8 ans
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getInformationsPersonnelles().getLogement().setStatutOccupationLogement(StatutOccupationLogementEnum.PROPRIETAIRE_AVEC_EMPRUNT.getLibelle());
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(0f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerRSA(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_RSA);

        assertThat(openFiscaRetourSimulation.getMontantRSA()).isEqualTo(1146f);
    }

    @Test
    void calculerRSACouple3EnfantsTest() {

        // Si DE Français, couple, logé gratuitement
        // 3 enfants à charge de 6 ans, 8 ans et 12 ans
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        personnesACharge.add(createEnfant(12));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getInformationsPersonnelles().getLogement().setStatutOccupationLogement(StatutOccupationLogementEnum.LOGE_GRATUITEMENT.getLibelle());
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(0f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerRSA(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_RSA);

        assertThat(openFiscaRetourSimulation.getMontantRSA()).isEqualTo(1400f);
    }

    @Test
    void calculerRSACouple4EnfantsTest() {

        // Si DE Français, couple, non propriétaire
        // 4 enfants à charge de 4, 6, 10 et 12 ans
        // sans forfait logement
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(10));
        personnesACharge.add(createEnfant(12));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getInformationsPersonnelles().getLogement().setStatutOccupationLogement(StatutOccupationLogementEnum.NON_RENSEIGNE.getLibelle());
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(0f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerRSA(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_RSA);

        assertThat(openFiscaRetourSimulation.getMontantRSA()).isEqualTo(1844f);
    }
}
