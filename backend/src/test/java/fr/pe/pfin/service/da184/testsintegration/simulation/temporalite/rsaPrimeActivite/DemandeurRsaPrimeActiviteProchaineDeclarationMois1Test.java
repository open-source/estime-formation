package fr.pe.pfin.service.da184.testsintegration.simulation.temporalite.rsaPrimeActivite;

import fr.pe.pfin.service.da184.RedisTestConfiguration;
import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.DemandeurController;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = RedisTestConfiguration.class)
@ActiveProfiles("test")
class DemandeurRsaPrimeActiviteProchaineDeclarationMois1Test extends Utile {
    private static final int PROCHAINE_DECLARATION_TRIMESTRIELLE = 1;
    private final DemandeurController demandeurController;

    @Autowired
    public DemandeurRsaPrimeActiviteProchaineDeclarationMois1Test(DemandeurController demandeurController) {
        this.demandeurController = demandeurController;
    }

    @SuppressWarnings("java:S5961")
    @Test
    void simulerMesRessourcesFinancieresCelibataireMois1() {

        // Si DE Français, date naissance 5/07/1986, code postal 44200, célibataire,
        // seul depuis plus de 18 mois, non propriétaire
        // Future formation 35h, salaire net 757€, durée 3 mois
        // kilométrage domicile -> taf = 10kms + 20 trajets
        // RSA 500€, déclaration trimetrielle en M1, non travaillé au cours des 3
        // derniers mois
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(true);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(PROCHAINE_DECLARATION_TRIMESTRIELLE);
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);

        // Lorsque je simule mes prestations le 01/05/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(1);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500);
            });
        });
        // Alors les prestations du second mois 07/2023
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(535f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(282f);
            });
        });
        // Alors les prestations du troisième mois 08/2023
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(535f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(282f);
            });
        });
        // Alors les prestations du quatrième mois 09/2023
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(535f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(282f);
            });
        });
        // Alors les prestations du cinquième mois 10/2023
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(165f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(30f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(282f);
            });
        });
        // Alors les prestations du sixième mois 11/2023
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(165f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(30f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(282f);
            });
        });
    }

    @SuppressWarnings("java:S5961")
    @Test
    void simulerMesRessourcesFinancieresCouple1EnfantMoins10Mois1() {

        // Si DE Français, date naissance 5/07/1986, code postal 44200, en couple, 1 enfant moins de 10
        // non propriétaire
        // Future formation 35h, salaire net 757€, durée 3 mois
        // kilométrage domicile -> taf = 10kms + 20 trajets
        // RSA 170€, déclaration trimetrielle en M1,
        // non travaillé au cours des 3 derniers mois
        // conjoint salaire 700€
        // enfant 6 ans (05/03/2017)
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(170f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(PROCHAINE_DECLARATION_TRIMESTRIELLE);
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);

        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        Salaire salaireConjoint = new Salaire();
        salaireConjoint.setMontantMensuelNet(700);
        salaireConjoint.setMontantMensuelBrut(912);
        ressourcesFinancieresConjoint.setSalaire(salaireConjoint);
        demandeur.getSituationFamiliale().getConjoint()
                .setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        demandeur.getSituationFamiliale().getPersonnesACharge().get(0).getInformationsPersonnelles()
                .setDateNaissance(LocalDate.of(2017, 3, 5));

        // Lorsque je simule mes prestations le 01/05/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(1);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(170f);
            });
        });
        // Alors les prestations du second mois 07/2023
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(435f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(213f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395f);
            });
        });
        // Alors les prestations du troisième mois 08/2023
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(435f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(213f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395f);
            });
        });
        // Alors les prestations du quatrième mois 09/2023
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(435f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(213f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395f);
            });
        });
        // Alors les prestations du cinquième mois 10/2023
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(378f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395f);
            });
        });
        // Alors les prestations du sixième mois 11/2023
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(378f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(395f);
            });
        });
    }

    @SuppressWarnings("java:S5961")
    @Test
    void simulerMesRessourcesFinancieresCelibataireALMois1() {

        // Si DE Français, date naissance 5/07/1986, code postal 44200, célibataire,
        // seul depuis plus de 18 mois, non propriétaire
        // Future formation 35h, salaire net 757€, durée 3 mois
        // kilométrage domicile -> taf = 10kms + 20 trajets
        // RSA 500€, déclaration trimetrielle en M1,
        // non travaillé au cours des 3 derniers mois
        // AL 310€
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(true);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(PROCHAINE_DECLARATION_TRIMESTRIELLE);
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(310f));

        // Lorsque je simule mes prestations le 01/05/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isEqualTo(310f);
                    });
        });
        // Alors les prestations du second mois 07/2023
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(535f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isBetween(237f, 240f);
                    });
        });
        // Alors les prestations du troisième mois 08/2023
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(535f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isBetween(237f, 240f);
                    });
        });
        // Alors les prestations du quatrième mois 09/2023
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(535f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isBetween(237f, 240f);
                    });
        });
        // Alors les prestations du cinquième mois 10/2023
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(165f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(30f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isBetween(237f, 240f);
                    });
        });
        // Alors les prestations du sixième mois 11/2023
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(165f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(30f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode()))
                    .satisfies(al -> {
                        assertThat(al.getMontant()).isBetween(237f, 240f);
                    });
        });
    }
}
