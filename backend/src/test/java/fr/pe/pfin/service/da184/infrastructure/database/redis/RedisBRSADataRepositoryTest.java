package fr.pe.pfin.service.da184.infrastructure.database.redis;

import fr.pe.pfin.service.da184.RedisTestConfiguration;
import fr.pe.pfin.service.da184.infrastructure.database.redis.models.BRSAData;
import fr.pe.pfin.service.da184.infrastructure.database.redis.repositories.BRSADataRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = RedisTestConfiguration.class)
@ActiveProfiles("test")
class RedisBRSADataRepositoryTest {
    @Autowired
    private BRSADataRepository brsaDataRepository;

    @AfterEach()
    void deleteAll() {
        brsaDataRepository.deleteAll();
    }


    @Test
    @DisplayName("doit retourner un objet BRSAData suite à la sauvegarde dans le repository")
    void shouldSaveData_toRedis() {
        BRSAData brsaData = new BRSAData(null, "12345678", "049", true, LocalDateTime.now(), null, 3, new ArrayList<>(), "43");

        BRSAData saved = brsaDataRepository.save(brsaData);

        assertNotNull(saved);
        assertThat(saved.getId()).isNotEmpty();
        assertThat(saved.getIdFT()).isEqualTo(brsaData.getIdFT());
        assertThat(saved.getCodeRegionFT()).isEqualTo(brsaData.getCodeRegionFT());
        assertThat(saved.getDateSimulation()).isEqualTo(brsaData.getDateSimulation());
        assertThat(saved.isHasConseillerFT()).isTrue();
        assertThat(saved.getParcoursFormation()).isEqualTo(3);
    }

    @Test
    @DisplayName("doit retourner un objet BRSAData suite à la récupération dans le repository via l'ID")
    void save_and_findById() {
        BRSAData brsaData = new BRSAData(null, "12345678", "049", true, LocalDateTime.now(), null, 3, new ArrayList<>(), "43");
        //WHEN
        BRSAData saved = brsaDataRepository.save(brsaData);
        Optional<BRSAData> retrieved = brsaDataRepository.findById(saved.getId());
        //THEN
        assertThat(retrieved).isNotEmpty();

        BRSAData retrievedUpdate = retrieved.get();
        retrievedUpdate.setCodeRegionFT("013");
        retrievedUpdate.setIdFT("987654321");
        retrievedUpdate.setHasConseillerFT(false);
        retrievedUpdate.setParcoursFormation(4);

        BRSAData saved2 = brsaDataRepository.save(retrievedUpdate);

        assertNotNull(saved2);
        assertThat(saved2.getId()).isNotEmpty();
        assertThat(saved2.getIdFT()).isEqualTo(retrievedUpdate.getIdFT());
        assertThat(saved2.getCodeRegionFT()).isEqualTo(retrievedUpdate.getCodeRegionFT());
        assertThat(saved2.getDateSimulation()).isEqualTo(retrievedUpdate.getDateSimulation());
        assertThat(saved2.isHasConseillerFT()).isFalse();
        assertThat(saved2.getParcoursFormation()).isEqualTo(4);
        assertThat(saved2.getAllocation()).isNullOrEmpty();
        assertThat(saved2.getDepartement()).isEqualTo("43");
    }

    @Test
    @DisplayName("doit retourner des objets BRSAData suite à la récupération dans le repository de tout")
    void save_and_findAll() {
        BRSAData brsaData = new BRSAData(null, "12345678", "049", true, LocalDateTime.now(), 2, 3, null, "43");
        BRSAData brsaData2 = new BRSAData(null, "99999999", "013", true, LocalDateTime.now(), 4, 3, null, "43");
        //WHEN
        BRSAData saved = brsaDataRepository.save(brsaData);
        BRSAData saved2 = brsaDataRepository.save(brsaData2);
        List<BRSAData> brsaDatas = brsaDataRepository.findAll();
        //THEN
        assertThat(brsaDatas).hasSize(2).contains(saved2, saved);
        assertThat(saved.equals(brsaData)).isTrue();
    }
}
