package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.periodes;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.PeriodeTravailleeAvantSimulation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.OpenFiscaMappeurPeriode;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurPeriodesASSTest extends Utile {

    private final LocalDate dateDebutSimulation = LocalDate.of(2023, 1, 1);

    private static Stream<Arguments> listeFichierEtNombreDeMois() {
        return Stream.of(
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesASSTests/periode-ass-cumul-0-mois-ass-salaire-simulation.json", 0),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesASSTests/periode-ass-cumul-1-mois-ass-salaire-simulation.json", 1),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesASSTests/periode-ass-cumul-2-mois-ass-salaire-simulation.json", 2),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesASSTests/periode-ass-cumul-3-mois-ass-salaire-simulation.json", 3)
        );
    }

    /********************
     * TESTS SUR creerPeriodesAidee
     ************************************************/

    @ParameterizedTest
    @MethodSource("listeFichierEtNombreDeMois")
    void creerPeriodeAideeASS(String emplacementFichier, int nbMois) throws Exception {
        String openFiscaPayloadExpected = getStringFromJsonFile(emplacementFichier);
        Demandeur demandeur = creerDemandeurASSTests(nbMois);
        OpenFiscaPeriodes periodeAideeASS = OpenFiscaMappeurPeriode.creerPeriodesOpenFiscaASS(demandeur, dateDebutSimulation);
        assertThat(periodeAideeASS.toString()).hasToString(openFiscaPayloadExpected);
    }

    /***************** METHODES UTILES POUR TESTS *******************************/

    private Demandeur creerDemandeurASSTests(int nombreMoisTravaillesAvantSimulation) throws ParseException {

        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(dateDebutSimulation, 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);
        if (nombreMoisTravaillesAvantSimulation > 0) {
            PeriodeTravailleeAvantSimulation periodeTravailleeAvantSimulation = creerPeriodeTravailleeAvantSimulation(850, 1101, nombreMoisTravaillesAvantSimulation);
            demandeur.getRessourcesFinancieresAvantSimulation()
                    .setPeriodeTravailleeAvantSimulation(periodeTravailleeAvantSimulation);
        }

        return demandeur;
    }

    /*****************
     * TESTS SUR creerPeriodesSalaireDemandeur
     ********************************************************/

    /**
     * CONTEXTE DES TESTS
     *
     * date demande simulation : 01-01-2023
     *
     * avant simulation M-2 01-12-2023 M-1 01-11-2023
     *
     * période de la simulation sur 6 mois M1 02/2023 M2 03/2023 M3 04/2023 M4
     * 05/2023 M5 06/2023 M6 07/2023
     *
     *
     ********************************************************************************/

}
