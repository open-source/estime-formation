package fr.pe.pfin.service.da184.application.ui.rest.valideur;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.BadRequestMessages;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;


class BeneficiaireAidesApiValideurTest extends Utile {
    @Test
    void controlerDonneeesEntreeBeneficiaireAidesTest() {
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(false, personnesACharge);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));
        demandeur.setBeneficiaireAides(null);

        assertThatThrownBy(() -> BeneficiaireAidesApiValideur.controlerDonnees(demandeur))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "beneficiaireAides");
    }
}
