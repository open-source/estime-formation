package fr.pe.pfin.service.da184.domaine.use_cases;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ObtenirDonneesBRSATest {
    @Mock
    DonneesBRSAPort donneesBRSAPort;
    @InjectMocks
    ObtenirDonneesBRSA obtenirDonneesBRSA;

    @Test
    @DisplayName("doit retourner un inputstream csv contenant les BRSADatas")
    void doitRetournerUnInputStream() {
        when(donneesBRSAPort.extraireDonneesBRSA(any(), any())).thenReturn(new ByteArrayInputStream("TEST".getBytes(StandardCharsets.UTF_8)));
        InputStream input = obtenirDonneesBRSA.executer(LocalDate.now().minusDays(1), LocalDate.now().plusDays(1));
        assertThat(input).isNotNull();
    }
}
