package fr.pe.pfin.service.da184.domaine.use_cases;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ObtenirSimulationAidesTest {
    @Mock
    SimulateurAidesPort simulateurAidesPort;
    ObtenirSimulationAides obtenirSimulationAides;

    Demandeur demandeur = new Demandeur();
    Simulation simulation = new Simulation();

    @BeforeEach
    void init() {
        obtenirSimulationAides = new ObtenirSimulationAides(simulateurAidesPort);
        demandeur.setIdDemandeurEmploi("1234567A");
        demandeur.setBeneficiaireAides(new BeneficiaireAides());
        demandeur.setFutureFormation(new FutureFormation());
        demandeur.setSituationFamiliale(new SituationFamiliale());
        demandeur.setInformationsPersonnelles(new InformationsPersonnelles());
        demandeur.setRessourcesFinancieresAvantSimulation(new RessourcesFinancieresAvantSimulation());
        simulation.setSimulationsMensuelles(List.of());
        simulation.setMontantRessourcesFinancieresMoisAvantSimulation(100f);
    }

    @DisplayName("doit retourner un objet simulation pour un objet demandeur")
    @Test
    void doitRetournerUneSimulationPourUnDemandeur() {
        when(simulateurAidesPort.simuler(demandeur)).thenReturn(simulation);
        Simulation resultat = obtenirSimulationAides.executer(demandeur);
        assertThat(resultat).isEqualTo(simulation);
    }


}
