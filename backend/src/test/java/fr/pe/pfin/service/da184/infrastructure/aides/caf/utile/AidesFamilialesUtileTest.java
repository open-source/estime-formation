package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypePopulationEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

class AidesFamilialesUtileTest extends Utile {

    @Test
    @DisplayName("doit retour une Aide eligible à l'allocation familiale")
    void doitRetournerUneAideEligibleAllocationFamiliales() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesCAF(createAidesCAF());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales().setAllocationsFamiliales(200f);
        Map<String, Aide> aidesRetour = new HashMap<>();
        Aide aideAttendu = AideUtile.creerAide(AideEnum.ALLOCATIONS_FAMILIALES, Optional.of(OrganismeEnum.CAF), Optional.of(List.of(MessageInformatifEnum.CHANGEMENT_MONTANT_PRESTATIONS_FAMILIALES.getMessage())), true, 200f);
        AidesFamilialesUtile.simulerAidesFamiliales(aidesRetour, demandeur, 1);
        assertThat(aidesRetour).hasSize(1).containsKey(AideEnum.ALLOCATIONS_FAMILIALES.getCode()).containsValue(aideAttendu);
    }


    @Test
    @DisplayName("doit retour une Aide eligible à l'allocation soutien familiale")
    void doitRetournerUneAideEligibleAllocationSoutienFamiliales() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesCAF(createAidesCAF());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales().setAllocationSoutienFamilial(200f);
        Map<String, Aide> aidesRetour = new HashMap<>();
        Aide aideAttendu = AideUtile.creerAide(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL, Optional.of(OrganismeEnum.CAF), Optional.of(List.of(MessageInformatifEnum.CHANGEMENT_MONTANT_PRESTATIONS_FAMILIALES.getMessage())), true, 200f);
        AidesFamilialesUtile.simulerAidesFamiliales(aidesRetour, demandeur, 1);
        assertThat(aidesRetour).hasSize(1).containsKey(AideEnum.ALLOCATION_SOUTIEN_FAMILIAL.getCode()).containsValue(aideAttendu);
    }

    @Test
    @DisplayName("doit retour une Aide eligible au complément familiale")
    void doitRetournerUneAideEligibleComplementFamiliales() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesCAF(createAidesCAF());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales().setComplementFamilial(200f);
        Map<String, Aide> aidesRetour = new HashMap<>();
        Aide aideAttendu = AideUtile.creerAide(AideEnum.COMPLEMENT_FAMILIAL, Optional.of(OrganismeEnum.CAF), Optional.of(List.of(MessageInformatifEnum.CHANGEMENT_MONTANT_PRESTATIONS_FAMILIALES.getMessage())), true, 200f);
        AidesFamilialesUtile.simulerAidesFamiliales(aidesRetour, demandeur, 1);
        assertThat(aidesRetour).hasSize(1).containsKey(AideEnum.COMPLEMENT_FAMILIAL.getCode()).containsValue(aideAttendu);
    }


    @Test
    @DisplayName("doit retour une Aide eligible au Prestation Accueil Jeune Enfant")
    void doitRetournerUneAideEligiblePrestationAccueilJeuneEnfant() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, List.of(createEnfant(1)));
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesCAF(createAidesCAF());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales().setPrestationAccueilJeuneEnfant(200f);
        Map<String, Aide> aidesRetour = new HashMap<>();
        Aide aideAttendu = AideUtile.creerAide(AideEnum.PRESTATION_ACCUEIL_JEUNE_ENFANT, Optional.of(OrganismeEnum.CAF), Optional.of(List.of(MessageInformatifEnum.CHANGEMENT_MONTANT_PRESTATIONS_FAMILIALES.getMessage())), true, 200f);
        AidesFamilialesUtile.simulerAidesFamiliales(aidesRetour, demandeur, 1);
        assertThat(aidesRetour).hasSize(1).containsKey(AideEnum.PRESTATION_ACCUEIL_JEUNE_ENFANT.getCode()).containsValue(aideAttendu);
    }

    @Test
    @DisplayName("doit retour une Aide eligible au Pension Alimentaire")
    void doitRetournerUneAideEligiblePensionAlimentaire() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, List.of(createEnfant(1)));
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesCAF(createAidesCAF());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales().setPensionsAlimentairesFoyer(200f);
        Map<String, Aide> aidesRetour = new HashMap<>();
        Aide aideAttendu = AideUtile.creerAide(AideEnum.PENSIONS_ALIMENTAIRES, Optional.of(OrganismeEnum.CAF), Optional.of(List.of(MessageInformatifEnum.CHANGEMENT_MONTANT_PRESTATIONS_FAMILIALES.getMessage())), true, 200f);
        AidesFamilialesUtile.simulerAidesFamiliales(aidesRetour, demandeur, 1);
        assertThat(aidesRetour).hasSize(1).containsKey(AideEnum.PENSIONS_ALIMENTAIRES.getCode()).containsValue(aideAttendu);
    }


}
