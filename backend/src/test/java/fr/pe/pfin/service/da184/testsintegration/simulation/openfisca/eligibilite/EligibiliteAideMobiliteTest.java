package fr.pe.pfin.service.da184.testsintegration.simulation.openfisca.eligibilite;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.OpenFiscaRetourSimulationHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class EligibiliteAideMobiliteTest extends Utile {
    private final OpenFiscaClient openFiscaClient;

    @Autowired
    public EligibiliteAideMobiliteTest(OpenFiscaClient openFiscaClient) {
        this.openFiscaClient = openFiscaClient;
    }

    @Test
    @DisplayName("doit vérifier l'éligibilité à l'aide mobilité d'un demandeur habitant en france métropolitaine célibataire ayant un enfant de moins de 10 ans et ayant un trajet travail/domicile de 60km sur 5 jours/semaine")
    void calculerEligibleCelibataire1EnfantMoins10_60km_5jours() {


        // Si DE France Métropolitaine, RSA, célibataire, 60km trajet travail-domicile 5 jours
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 60, 10));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);
        // Lorsque je calcule le montant de l'Aide Mobilité
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideMobilite(openFiscaRoot,
                dateDebutSimulation);

        // Alors le demandeur est éligible à l'Aide Mobilité
        assertThat(openFiscaRetourSimulation.getMontantAideMobilite()).isNotZero();
    }

    @Test
    @DisplayName("doit vérifier l'éligibilité à l'aide mobilité d'un demandeur habitant DOM célibataire ayant un enfant de moins de 10 ans et ayant un trajet travail/domicile de 30km sur 5 jours/semaine")
    void calculerEligibleCelibataire1EnfantMoins10_DOM_30km_5jours() {


        // Si DE DOM, RSA, célibataire, 30km trajet travail-domicile 5 jours
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "97611"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 30, 10));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("97600");
        // Lorsque je calcule le montant de l'Aide Mobilité
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideMobilite(openFiscaRoot,
                dateDebutSimulation);

        // Alors le demandeur est éligible à l'Aide Mobilité
        assertThat(openFiscaRetourSimulation.getMontantAideMobilite()).isNotZero();
    }

    @Test
    @DisplayName("doit vérifier la non éligibilité à l'aide mobilité d'un demandeur habitant France métropolitaine célibataire ayant un enfant de moins de 10 ans et ayant un trajet travail/domicile de 14km sur 5 jours/semaine")
    void calculerNonEligibleCelibataire1EnfantMoins10_14km_5jours() {


        // Si DE France Métropolitaine, RSA, célibataire, 14km trajet travail-domicile 5 jours
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getFutureFormation().setDistanceActiviteDomicile(14f);
        demandeur.getFutureFormation().setNombreTrajetsDomicileTravail(10);
        // Lorsque je calcule le montant de l'Aide Mobilité
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideMobilite(openFiscaRoot,
                dateDebutSimulation);

        // Alors le demandeur n'est pas éligible à l'Aide Mobilité
        assertThat(openFiscaRetourSimulation.getMontantAideMobilite()).isZero();
    }

    @Test
    @DisplayName("doit vérifier la non éligibilité à l'aide mobilité d'un demandeur habitant DOM célibataire ayant un enfant de moins de 10 ans et ayant un trajet travail/domicile de 10km sur 5 jours/semaine")
    void calculerNonEligibleCelibataire1EnfantMoins10_DOM_10km_5jours() {


        // Si DE DOM, RSA, célibataire, 10km trajet travail-domicile 5 jours
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "97611"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("97600");
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 10, 10));

        // Lorsque je calcule le montant de l'Aide Mobilité
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideMobilite(openFiscaRoot,
                dateDebutSimulation);

        // Alors le demandeur n'est pas éligible à l'Aide Mobilité
        assertThat(openFiscaRetourSimulation.getMontantAideMobilite()).isZero();
    }
}
