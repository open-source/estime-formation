package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypePopulationEnum;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class PrimeActiviteASSUtileTest extends Utile {

    @Test
    @DisplayName("doit retourner faux pour un object demandeur vide pour une prime activité à calculer")
    void doitRetournerFauxPourUnDemandeurVideSurUnePrimeActiviteACalculer() {
        assertThat(PrimeActiviteASSUtile.isPrimeActiviteACalculer(0, new Demandeur())).isFalse();
    }


    @Test
    @DisplayName("doit retourner vrai pour un object demandeur ok pour une prime activité à calculer")
    void doitRetournerVraiPourUnDemandeurOkSurUnePrimeActiviteACalculer() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setPeriodeTravailleeAvantSimulation(creerPeriodeTravailleeAvantSimulation(1200, 1544, 13));
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(true);
        assertThat(PrimeActiviteASSUtile.isPrimeActiviteACalculer(3, demandeur)).isTrue();
    }

    @Test
    @DisplayName("doit retourner une NPE pour un object demandeur NULL pour une prime activité à calculer")
    void doitRetournerNPEPourUnDemandeurNullSurUnePrimeActiviteACalculer() {
        assertThatThrownBy(() -> PrimeActiviteASSUtile.isPrimeActiviteACalculer(0, null)).isInstanceOf(NullPointerException.class);
    }

    @Test
    @DisplayName("doit retourner faux pour un object demandeur vide pour une prime activité à verser")
    void doitRetournerFauxPourUnDemandeurVideSurUnePrimeActiviteAVerser() {
        assertThat(PrimeActiviteASSUtile.isPrimeActiviteAVerser(0, new Demandeur())).isFalse();
    }

    @Test
    @DisplayName("doit retourner vrai pour un object demandeur okpour une prime activité à verser")
    void doitRetournerVraiPourUnDemandeurOkSurUnePrimeActiviteAVerser() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.RSA, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setPeriodeTravailleeAvantSimulation(creerPeriodeTravailleeAvantSimulation(1200, 1544, 13));
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(true);
        assertThat(PrimeActiviteASSUtile.isPrimeActiviteAVerser(1, demandeur)).isTrue();

    }

    @Test
    @DisplayName("doit retourner une NPE pour un object demandeur NULL pour une prime activité à verser")
    void doitRetournerNPEPourUnDemandeurNullSurUnePrimeActiviteAVerser() {
        assertThatThrownBy(() -> PrimeActiviteASSUtile.isPrimeActiviteAVerser(0, null)).isInstanceOf(NullPointerException.class);
    }


}
