package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.periodes;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.OpenFiscaMappeurPeriode;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurPeriodesSalairesPersonneAChargeTest extends Utile {

    private final LocalDate dateDebutSimulation = LocalDate.of(2023, 1, 1);

    /*****************
     * TESTS SUR creerPeriodesSalaireDemandeur
     ********************************************************/

    /**
     * CONTEXTE DES TESTS
     * <p>
     * date demande simulation : 01-06-2023
     * <p>
     * avant simulation M-2 05/2023 M-1 06/2023
     * <p>
     * période de la simulation sur 6 mois M1 07/2023 M2 08/2023 M3 09/2023 M4
     * 10/2023 M5 11/2023 M6 12/2023
     ********************************************************************************/

    /**
     * Demandeur ASS sans cumul ASS + salaire dans les 3 mois avant la simulation
     * Création de la période salaire pour une simulation au mois M5
     */
    @Test
    void mapPeriodeSalaireTest() throws Exception {

        String openFiscaPayloadSalaireNetExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesSalairesTests/personne-a-charge/salaire-net-8-mois-salaire-avant-simulation.json");

        Demandeur demandeur = creerDemandeurSalairesTests();

        OpenFiscaIndividu personneOpenFisca = new OpenFiscaIndividu();
        OpenFiscaMappeurPeriode.creerPeriodesSalairePersonne(personneOpenFisca,
                demandeur.getSituationFamiliale().getPersonnesACharge().get(0), dateDebutSimulation);

        assertThat(personneOpenFisca.getSalaireNet().toString())
                .hasToString(openFiscaPayloadSalaireNetExpected);
    }

    private Demandeur creerDemandeurSalairesTests() throws Exception {

        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelBrut(1291);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelNet(1000);
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);

        SituationFamiliale situationFamiliale = new SituationFamiliale();
        Personne personneACharge = createPersonne(LocalDate.of(2000, 7, 5));
        BeneficiaireAides beneficiaireAides = new BeneficiaireAides();
        personneACharge.setBeneficiaireAides(beneficiaireAides);
        RessourcesFinancieresAvantSimulation ressourcesFinancieresPersonneACharge = new RessourcesFinancieresAvantSimulation();
        Salaire salairepersonneACharge = new Salaire();
        salairepersonneACharge.setMontantMensuelNet(1200);
        salairepersonneACharge.setMontantMensuelBrut(1544);
        ressourcesFinancieresPersonneACharge.setSalaire(salairepersonneACharge);
        ressourcesFinancieresPersonneACharge.setHasTravailleAuCoursDerniersMois(true);
        ressourcesFinancieresPersonneACharge.setHasTravailleAuCoursDerniersMois(true);
        PeriodeTravailleeAvantSimulation periodeTravailleeAvantSimulation = new PeriodeTravailleeAvantSimulation();
        Salaire[] salaires = creerSalaires(0, 0, 12);
        Salaire salaireMoisMoins1 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins3 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins4 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins5 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins7 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins9 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins11 = creerSalaire(850, 1101);
        salaires = ajouterSalaire(salaires, salaireMoisMoins1, 1);
        salaires = ajouterSalaire(salaires, salaireMoisMoins3, 3);
        salaires = ajouterSalaire(salaires, salaireMoisMoins4, 4);
        salaires = ajouterSalaire(salaires, salaireMoisMoins5, 5);
        salaires = ajouterSalaire(salaires, salaireMoisMoins7, 7);
        salaires = ajouterSalaire(salaires, salaireMoisMoins9, 9);
        salaires = ajouterSalaire(salaires, salaireMoisMoins11, 11);
        periodeTravailleeAvantSimulation.setMois(createMoisTravaillesAvantSimulation(salaires));
        ressourcesFinancieresPersonneACharge.setPeriodeTravailleeAvantSimulation(periodeTravailleeAvantSimulation);
        personneACharge.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresPersonneACharge);
        personnesACharge.add(personneACharge);
        situationFamiliale.setPersonnesACharge(personnesACharge);
        situationFamiliale.setIsEnCouple(false);
        demandeur.setSituationFamiliale(situationFamiliale);

        return demandeur;
    }
}
