package fr.pe.pfin.service.da184.application.ui.rest.exceptions;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;


class RestExceptionHandlerTest {

    final RestExceptionHandler restExceptionHandler = new RestExceptionHandler();

    @Test
    @DisplayName("doit retourner une ReponseEntity de type BadRequestException")
    void doitRetournerUneResponseEntityDeTypeBadRequestException() {
        String message = "BadRequest error";
        BadRequestException badRequestException = new BadRequestException(message);
        ResponseEntity<ExceptionResponse> responseEntity = restExceptionHandler.handleCritereRechercheIncorrectException(badRequestException);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().getMessage()).isSameAs(message);
        assertThat(responseEntity.getBody().getException()).isEqualTo(badRequestException.toString());
    }

    @Test
    @DisplayName("doit retourner une ReponseEntity de type ResourceNotFoundException")
    void doitRetournerUneResponseEntityDeTypeResourceNotFoundException() {
        String message = "BadRequest error";
        ResourceNotFoundException exception = new ResourceNotFoundException(message);
        ResponseEntity<ExceptionResponse> responseEntity = restExceptionHandler.handleResourceNonExistanteException(exception);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().getMessage()).isSameAs(message);
        assertThat(responseEntity.getBody().getException()).isEqualTo(exception.toString());
    }

    @Test
    @DisplayName("doit retourner une ReponseEntity de type UnauthorizedException")
    void doitRetournerUneResponseEntityDeTypeUnauthorizedException() {
        String message = "BadRequest error";
        UnauthorizedException exception = new UnauthorizedException(message);
        ResponseEntity<ExceptionResponse> responseEntity = restExceptionHandler.handleInterdictionException(exception);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().getMessage()).isSameAs(message);
        assertThat(responseEntity.getBody().getException()).isEqualTo(exception.toString());
    }

    @Test
    @DisplayName("doit retourner une ReponseEntity de type InternalServerException")
    void doitRetournerUneResponseEntityDeTypeInternalServerException() {
        String message = "BadRequest error";
        InternalServerException exception = new InternalServerException(message);
        ResponseEntity<ExceptionResponse> responseEntity = restExceptionHandler.handleErreurServeurException(exception);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().getMessage()).isSameAs(message);
        assertThat(responseEntity.getBody().getException()).isEqualTo(exception.toString());
    }

}
