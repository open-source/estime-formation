package fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypePopulationEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.AideUtile;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

class AreUtileTest extends Utile {

    @Test
    @DisplayName("doit retourner une Aide ARE avant l'application du prélèvement à la source")
    void doitRetournerUnAideAREAvantApplicationPrelevementSource() {
        float montantAide = 200f;
        Aide aideAttendu = AideUtile.creerAide(AideEnum.AIDE_RETOUR_EMPLOI, Optional.of(OrganismeEnum.FT), Optional.of(List.of(MessageInformatifEnum.MONTANT_ARE_AVANT_PAS.getMessage())), false, montantAide);
        assertThat(AreUtile.creerARE(montantAide, 12f)).isEqualTo(aideAttendu);
    }

    @Test
    @DisplayName("doit retourner une Aide ARE en Fin de droit")
    void doitRetournerUnAideAREEnFinDeDroit() {
        float montantAide = 200f;
        Aide aideAttendu = AideUtile.creerAide(AideEnum.AIDE_RETOUR_EMPLOI, Optional.of(OrganismeEnum.FT), Optional.of(List.of(MessageInformatifEnum.FIN_DE_DROIT_ARE.getMessage())), false, montantAide);
        assertThat(AreUtile.creerARE(montantAide, 0)).isEqualTo(aideAttendu);
    }


    @Test
    @DisplayName("doit retourner une Aide Complément ARE avant l'application du prélèvement à la source")
    void doitRetournerUnAideComplementAREAvantApplicationPrelevementSource() {
        float montantAide = 200f;
        Aide aideAttendu = AideUtile.creerAide(AideEnum.COMPLEMENT_AIDE_RETOUR_EMPLOI, Optional.of(OrganismeEnum.FT), Optional.of(List.of(MessageInformatifEnum.ACTUALISATION_ARE.getMessage())), false, montantAide);
        assertThat(AreUtile.creerComplementARE(montantAide, 12f)).isEqualTo(aideAttendu);
    }

    @Test
    @DisplayName("doit retourner une Aide une Aide Complément ARE en Fin de droit")
    void doitRetournerUnAideComplementAREEnFinDeDroit() {
        float montantAide = 200f;
        Aide aideAttendu = AideUtile.creerAide(AideEnum.COMPLEMENT_AIDE_RETOUR_EMPLOI, Optional.of(OrganismeEnum.FT), Optional.of(List.of(MessageInformatifEnum.FIN_DE_DROIT_ARE.getMessage())), false, montantAide);
        assertThat(AreUtile.creerComplementARE(montantAide, 0)).isEqualTo(aideAttendu);
    }

    @Test
    @DisplayName("doit retourner vrai pour un demandeur ayant un complementARE à verser")
    void doitRetournerVraiPourUnDemandeurAyantUnComplementAREAVerser() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ARE, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setPeriodeTravailleeAvantSimulation(creerPeriodeTravailleeAvantSimulation(1500, 1600, 1));
        assertThat(AreUtile.isComplementAREAVerser(demandeur, 1)).isTrue();
    }

    @Test
    @DisplayName("doit retourner faux pour un demandeur n'ayant pas de complementARE à verser")
    void doitRetournerFauxPourUnDemandeurNAyantPasComplementAREAVerser() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ARE, false, Collections.emptyList());
        assertThat(AreUtile.isComplementAREAVerser(demandeur, 1)).isFalse();
    }


    @Test
    @DisplayName("doit retourner le numéro de mois pour le passage au complément ARE")
    void doitRetournerLeNumeroDuMois0PourPassageAuComplementARE() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ARE, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setPeriodeTravailleeAvantSimulation(creerPeriodeTravailleeAvantSimulation(1500, 1600, 1));
        assertThat(AreUtile.numeroMoisPassageAuComplementARE(demandeur)).isZero();
    }

    @Test
    @DisplayName("doit retourner le numéro de mois pour le passage au complément ARE")
    void doitRetournerLeNumeroDuMois1PourPassageAuComplementARE() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ARE, false, Collections.emptyList());
        assertThat(AreUtile.numeroMoisPassageAuComplementARE(demandeur)).isEqualTo(1);
    }

    @Test
    @DisplayName("doit calculer le montant de l'are avant la simulation")
    void doitCalculerMontantAREAvantSimulation() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ARE, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setAllocationJournaliereBrute(17f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setSalaireJournalierReferenceBrut(12f);
        assertThat(AreUtile.calculerMontantAreAvantSimulation(demandeur, LocalDate.now())).isEqualTo(527f);
    }

    @Test
    @DisplayName("doit retourner le nombre de jours restant après premiers mois ")
    void doitRetournerNbrJoursRestantApresPremierMois() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ARE, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setAllocationJournaliereBrute(17f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setSalaireJournalierReferenceBrut(12f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setNombreJoursRestants(48);
        assertThat(AreUtile.getNombreJoursRestantsApresPremierMois(demandeur, LocalDate.now())).isEqualTo(17f);
    }

    @Test
    @DisplayName("doit retourner le montant de l'are avant la simulation")
    void doitRetournerMontantAREAvantSimulation() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ARE, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setAllocationJournaliereBrute(17f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setSalaireJournalierReferenceBrut(12f);
        assertThat(AreUtile.getMontantAREAvantSimulation(1, demandeur, LocalDate.now())).isEqualTo(510f);
    }

    @Test
    @DisplayName("doit retourner le nombre de jours restant ARE")
    void doitCalculerMontantMensuelARE() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ARE, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setAllocationJournaliereBrute(17f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setSalaireJournalierReferenceBrut(12f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setNombreJoursRestants(48);
        assertThat(AreUtile.calculerMontantMensuelARE(demandeur, 1, LocalDate.now())).isEqualTo(527f);
    }


    @Test
    @DisplayName("doit retourner une aide de type ARE")
    void doitRetournerUneAideDeTypeARE() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ARE, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setAllocationJournaliereBrute(17f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setSalaireJournalierReferenceBrut(12f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setNombreJoursRestants(48);
        Map<String, Aide> aidesRetour = new HashMap<>();
        Aide aideAttendu = AideUtile.creerAide(AideEnum.AIDE_RETOUR_EMPLOI, Optional.of(OrganismeEnum.FT), Optional.of(List.of(MessageInformatifEnum.MONTANT_ARE_AVANT_PAS.getMessage())), false, 510f);
        AreUtile.reporterARE(aidesRetour, demandeur, 1, LocalDate.now());
        assertThat(aidesRetour).hasSize(1).containsKey(AideEnum.AIDE_RETOUR_EMPLOI.getCode()).containsValue(aideAttendu);
    }

    @Test
    @DisplayName("doit retourner une aide de type ARE en Fin de droit")
    void doitRetournerUneAideDeTypeAREEnFinDeDRoit() {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ARE, false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setAllocationJournaliereBrute(17f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setSalaireJournalierReferenceBrut(12f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setNombreJoursRestants(0);
        Map<String, Aide> aidesRetour = new HashMap<>();
        Aide aideAttendu = AideUtile.creerAide(AideEnum.AIDE_RETOUR_EMPLOI, Optional.of(OrganismeEnum.FT), Optional.of(List.of(MessageInformatifEnum.FIN_DE_DROIT_ARE.getMessage())), false, 0);
        AreUtile.reporterARE(aidesRetour, demandeur, 1, LocalDate.now());
        assertThat(aidesRetour).hasSize(1).containsKey(AideEnum.AIDE_RETOUR_EMPLOI.getCode()).containsValue(aideAttendu);
    }

}
