package fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author EXGO2620
 */
class InternalServerMessagesTest {

    /**
     * Test of values method, of class InternalServerMessages.
     */
    @Test
    void testValues() {
        System.out.println("values");
        InternalServerMessages[] result = InternalServerMessages.values();
        
        //A modifier si on ajoute des elements dans l'enum
        assertEquals(1, result.length);
    }

    /**
     * Test of valueOf method, of class InternalServerMessages.
     */
    @Test
    void testValueOf() {
        System.out.println("valueOf");
        String name = "SERIALIZATION_OPENFISCA_IMPOSSIBLE";
        InternalServerMessages expResult = InternalServerMessages.SERIALIZATION_OPENFISCA_IMPOSSIBLE;
        InternalServerMessages result = InternalServerMessages.valueOf(name);
        assertEquals(expResult, result);
    }

    /**
     * Test of getMessage method, of class InternalServerMessages.
     */
    @Test
    void testGetMessage() {
        System.out.println("getMessage");
        InternalServerMessages instance = InternalServerMessages.SERIALIZATION_OPENFISCA_IMPOSSIBLE;
        //A modifier si on est amener à changer le message associé dans l'enum
        String expResult = "Erreur Technique : la serialization de l'objet OpenFiscaRoot a échoué.";
        String result = instance.getMessage();
        assertEquals(expResult, result);
    }
}
