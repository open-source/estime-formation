package fr.pe.pfin.service.da184.application.ui.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.services.ConditionsRemunerationService;
import fr.pe.pfin.service.da184.application.services.DemandeurService;
import fr.pe.pfin.service.da184.application.services.DonneesBRSAService;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.database.redis.utile.RedisUtile;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = DemandeurController.class)
@ActiveProfiles("test")
class DemandeurControllerTest extends Utile {

    Demandeur demandeur;
    SituationRemuneration situationRemuneration;
    @MockBean
    DemandeurService demandeurService;
    @MockBean
    ConditionsRemunerationService conditionsRemunerationService;
    @MockBean
    DonneesBRSAService donneesBRSAService;
    MockMvc mockMvc;
    ObjectMapper objectMapper;
    @MockBean
    RedisUtile redisUtile;

    @Autowired
    public DemandeurControllerTest(MockMvc mockMvc, ObjectMapper objectMapper) {
        this.mockMvc = mockMvc;
        this.objectMapper = objectMapper;
        demandeur = createDemandeurRSA(false, new ArrayList<>());
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1998, 7, 5));
        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(true);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(0f);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelNet(552f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setProchaineDeclarationTrimestrielle(2);
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesPoleEmploi(new AidesPoleEmploi());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().setAllocationARE(new AllocationARE());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setAllocationJournaliereBruteTauxReduit(27.0f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setIsTauxReduit(true);
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(true);
        demandeur.getRessourcesFinancieresAvantSimulation().setPeriodeTravailleeAvantSimulation(new PeriodeTravailleeAvantSimulation());
        MoisTravailleAvantSimulation moisTravailleavantSimu = new MoisTravailleAvantSimulation();
        moisTravailleavantSimu.setDate(LocalDate.now().minusMonths(1));
        moisTravailleavantSimu.setSalaire(creerSalaire(1200, 1600));
        moisTravailleavantSimu.setSansSalaire(false);
        demandeur.getRessourcesFinancieresAvantSimulation().getPeriodeTravailleeAvantSimulation().setMois(new MoisTravailleAvantSimulation[]{moisTravailleavantSimu});
        situationRemuneration = new SituationRemuneration();
        situationRemuneration.setRegion("013");
        situationRemuneration.setAge(23f);
        situationRemuneration.setNbPersACharge(1f);
    }


    @Test
    @DisplayName("doit vérifier l'appel correct à la ressource avec un demandeur valide")
    void doitVerifierLAppelCorrectALaRessourceAvecUnDemandeurValide() throws Exception {
        when(demandeurService.simulerMesAides(demandeur)).thenReturn(new Simulation());
        this.mockMvc.perform(MockMvcRequestBuilders.post("/demandeurs-emploi/simulation-aides")
                        .content(this.objectMapper.writeValueAsString(this.demandeur))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());
        verify(demandeurService).simulerMesAides(any());
    }

    @Test
    @DisplayName("doit retourner une badRequestException pour un demandeur vide")
    void doitRetournerUneBadRequestExceptionPourUnDemandeurVide() throws Exception {
        when(demandeurService.simulerMesAides(any())).thenReturn(new Simulation());
        this.mockMvc.perform(MockMvcRequestBuilders.post("/demandeurs-emploi/simulation-aides")
                        .content("{}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(BadRequestException.class))
                .andExpect(jsonPath("status").value(400));
        verify(demandeurService, times(0)).simulerMesAides(any());
    }

    @Test
    @DisplayName("doit retourner une HttpMessageNotReadableException pour une chaine vide")
    void doitRetournerUneHttpMessageNotReadableExceptionPourUneChaineVide() throws Exception {
        when(demandeurService.simulerMesAides(any())).thenReturn(new Simulation());
        this.mockMvc.perform(MockMvcRequestBuilders.post("/demandeurs-emploi/simulation-aides")
                        .content("")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(HttpMessageNotReadableException.class))
                .andExpect(jsonPath("status").value(400));
        verify(demandeurService, times(0)).simulerMesAides(any());
    }

    @Test
    @DisplayName("doit appeler le endpoint send-notation")
    void doitAppelerSendNotation() throws Exception {
        when(donneesBRSAService.addNotation(any(), anyInt())).thenReturn(HttpStatus.OK);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/demandeurs-emploi/send-notation")
                        .content(this.objectMapper.writeValueAsString(new Notation()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());
    }
    @Test
    @DisplayName("doit vérifier l'appel a la ressource d'admin du CondtionsRemunration")
    void doitVerifierLAppelCorrectALaRessourceCondtionsRemunration() throws Exception {
        when(conditionsRemunerationService.getConditionsList(any())).thenReturn(new ArrayList<>());
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post("/demandeurs-emploi/conditions-remuneration")
                        .content(this.objectMapper.writeValueAsString(this.situationRemuneration))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk()).andReturn();

        verify(conditionsRemunerationService).getConditionsList(any());
        assertThat(mvcResult.getResponse().getContentType()).isEqualTo(MediaType.APPLICATION_JSON_VALUE);
    }

    @Test
    @DisplayName("doit vérifier l'appel et retour redis")
    void doitVerifierLAppelCorrectARedis() throws Exception {
        String stringResul = "1111";
        when(redisUtile.saveBRSAAndRedis(any(),any())).thenReturn(stringResul);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post("/demandeurs-emploi/redis")
                        .content(this.objectMapper.writeValueAsString(this.demandeur))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk()).andReturn();
        assertThat(mvcResult.getResponse().getContentAsString().substring(1, 5)).isEqualTo("1111");
    }
}
