package fr.pe.pfin.service.da184.infrastructure;

import fr.pe.pfin.service.da184.application.configuration.ConditionsRemunerationConfiguration;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.ConditionsRemunerations;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Critere;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SituationRemuneration;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.in;

import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
@SpringBootTest
class ConditionsRemunerationAdapteurTest {
    @Autowired
    ConditionsRemunerationConfiguration instance;
       
    ConditionsRemunerationAdapteur conditionsRemunerationAdapteur;
    SituationRemuneration situationRemuneration;

    public ConditionsRemunerationAdapteurTest() {}

    @Test
    @DisplayName("doit retourner une liste de conditions pour un objet situationRemuneration entrant")
    void doitRetournerUneListeConditionsPourUneSituationRemuneration() throws IOException, URISyntaxException {
        conditionsRemunerationAdapteur=instance.conditionsRemunerationAdapteur();
        situationRemuneration = new SituationRemuneration();
        situationRemuneration.setRegion("013");
        situationRemuneration.setAge(20f);
        situationRemuneration.setNbPersACharge(1f);

        List<ConditionsRemunerations> conditionsRemunerationsList = conditionsRemunerationAdapteur.getConditionsList(situationRemuneration);
        assertThat(conditionsRemunerationsList).isNotEmpty();
    }

    @Test
    @DisplayName("doit retourner une liste de conditions par défaut car code region inconnu")
    void returnNull() throws IOException, URISyntaxException {
        conditionsRemunerationAdapteur = instance.conditionsRemunerationAdapteur();
        situationRemuneration = new SituationRemuneration();
        situationRemuneration.setRegion("010");
        situationRemuneration.setAge(21f);
        situationRemuneration.setNbPersACharge(1f);

        List<ConditionsRemunerations> conditionsRemunerations = conditionsRemunerationAdapteur.getConditionsList(situationRemuneration);
        assertThat(conditionsRemunerations.size()).isEqualTo(4);
        assertThat(conditionsRemunerations.get(0).condition()).isEqualTo("Personne séparée, veuve, abandonnée ou célibataire");
        assertThat(conditionsRemunerations.get(0).remuneration()).isEqualTo(757);
    }

    @Test
    @DisplayName("doit retourner une liste de condition avec un seul élément pour une personne mineur, seule et enceinte de région avec le code 013")
    void doitRetournerUneListeDeConditionsAvecUnSeulElement() throws IOException, URISyntaxException {
        conditionsRemunerationAdapteur=instance.conditionsRemunerationAdapteur();
        situationRemuneration = new SituationRemuneration();
        situationRemuneration.setRegion("013");
        situationRemuneration.setAge(17f);
        situationRemuneration.setNbPersACharge(0.5f);

        List<ConditionsRemunerations> conditionsRemunerationsList = conditionsRemunerationAdapteur.getConditionsList(situationRemuneration);
        assertThat(conditionsRemunerationsList.size()).isEqualTo(1);
        assertThat(conditionsRemunerationsList.get(0).condition()).isEqualTo("");
        assertThat(conditionsRemunerationsList.get(0).remuneration()).isEqualTo(1002);
    }

    @Test
    @DisplayName("doit retourner une liste de condition avec plusieurs elements pour une personne de 20 ans, avec 1 enfant avec le code 046")
    void doitRetournerUneListeDeConditionsRegion046() throws IOException, URISyntaxException {
        conditionsRemunerationAdapteur=instance.conditionsRemunerationAdapteur();
        situationRemuneration = new SituationRemuneration();
        situationRemuneration.setRegion("046");
        situationRemuneration.setAge(020f);
        situationRemuneration.setNbPersACharge(1f);

        List<ConditionsRemunerations> conditionsRemunerationsList = conditionsRemunerationAdapteur.getConditionsList(situationRemuneration);
        assertThat(conditionsRemunerationsList.size()).isEqualTo(4);
        assertThat(conditionsRemunerationsList.get(0).condition()).isEqualTo("Personne séparée, veuve, abandonnée ou célibataire");
        assertThat(conditionsRemunerationsList.get(0).remuneration()).isEqualTo(757);
    }

    @Test
    @DisplayName("doit retourner une liste de condition avec un element pour une personne de 28 ans, seul et enceinte avec le code 13")
    void doitRetournerUneListeDeConditionsRegion0() throws IOException, URISyntaxException {
        conditionsRemunerationAdapteur=instance.conditionsRemunerationAdapteur();
        situationRemuneration = new SituationRemuneration();
        situationRemuneration.setRegion("013");
        situationRemuneration.setAge(28f);
        situationRemuneration.setNbPersACharge(0.5f);

        List<ConditionsRemunerations> conditionsRemunerationsList = conditionsRemunerationAdapteur.getConditionsList(situationRemuneration);
        assertThat(conditionsRemunerationsList.size()).isEqualTo(1);
        assertThat(conditionsRemunerationsList.get(0).condition()).isEqualTo("");
        assertThat(conditionsRemunerationsList.get(0).remuneration()).isEqualTo(1002);
    }


    @Test
    @DisplayName("doit retourner une liste de condition avec un seul élément pour une personne mineur, seule et enceinte de région avec le code 046")
    void doitRetournerUneListeDeConditionsAvecUnSeulElementOccitanie() throws IOException, URISyntaxException {
        conditionsRemunerationAdapteur=instance.conditionsRemunerationAdapteur();
        situationRemuneration = new SituationRemuneration();
        situationRemuneration.setRegion("046");
        situationRemuneration.setAge(17f);
        situationRemuneration.setNbPersACharge(0.5f);

        List<ConditionsRemunerations> conditionsRemunerationsList = conditionsRemunerationAdapteur.getConditionsList(situationRemuneration);
        assertThat(conditionsRemunerationsList.size()).isEqualTo(1);
        assertThat(conditionsRemunerationsList.get(0).condition()).isEqualTo("");
        assertThat(conditionsRemunerationsList.get(0).remuneration()).isEqualTo(757);
    }

    @Test
    @DisplayName("doit retourner la liste des critéres 'Region par defaut' pour un situationRemuneration entrant")
    void doitRetournerUneListePourUneSituationRemunerationDefautRegion() throws IOException, URISyntaxException {
        conditionsRemunerationAdapteur=instance.conditionsRemunerationAdapteur();
        situationRemuneration = new SituationRemuneration();
        situationRemuneration.setRegion("001");
        situationRemuneration.setAge(20f);
        situationRemuneration.setNbPersACharge(0f);

        List<ConditionsRemunerations> conditionsRemunerationsList = conditionsRemunerationAdapteur.getConditionsList(situationRemuneration);
        assertThat(conditionsRemunerationsList).isNotEmpty();
    }

    @Test
    @DisplayName("doit retourner la liste des critéres différent pour 2 situationRemuneration avec 2 regions différente (1 abondée l'autre en defaut)")
    void doitRetournerUneListeDifférentPourDeuxRegionsSituationRemuneration() throws IOException, URISyntaxException {
        conditionsRemunerationAdapteur=instance.conditionsRemunerationAdapteur();
        //Attention on doit toujours faire un delta entre une region abondé et une region avec config spécifique
        //mais il faut comme on compare le nombre de resultat s'assurer que le nombre de retour est bien différent (sinon il faudra faire un test un peu plus precis).
        situationRemuneration = new SituationRemuneration();
        situationRemuneration.setRegion("001");
        situationRemuneration.setAge(17f);
        situationRemuneration.setNbPersACharge(0f);
        
        SituationRemuneration situationRemuneration13 = new SituationRemuneration();
        situationRemuneration13.setRegion("013");
        situationRemuneration13.setAge(17f);
        situationRemuneration13.setNbPersACharge(0f);
        
        List<ConditionsRemunerations> conditionsRemunerationsList0 = conditionsRemunerationAdapteur.getConditionsList(situationRemuneration);
        List<ConditionsRemunerations> conditionsRemunerationsList13 = conditionsRemunerationAdapteur.getConditionsList(situationRemuneration13);
        //Cas defaut retour une condition unique alors que le cas 13 propose 3 conditions.
        Assertions.assertNotEquals(conditionsRemunerationsList0.size(), conditionsRemunerationsList13.size());
    }

    @Test
    @DisplayName("controle du singleton")
    void adapteurUnique() throws IOException, URISyntaxException {
        ConditionsRemunerationAdapteur conditionsRemunerationAdapteur1=instance.conditionsRemunerationAdapteur();
        ConditionsRemunerationAdapteur conditionsRemunerationAdapteur2=instance.conditionsRemunerationAdapteur();

        Assertions.assertEquals(conditionsRemunerationAdapteur1, conditionsRemunerationAdapteur2);
    }
    
    @Test
    @DisplayName("doit afficher les critères trouves")
    void doitAfficherCriteres() {
        Float[] intergerList = new Float[2];
        Critere critere = new Critere("age", intergerList);
        String critereString = critere.toString();
        boolean isCritere = critere.equals(new Critere("age", intergerList));
        assertThat(critereString).isEqualTo("Critere{values=[null, null], name=age}");
        assertThat(critere.hashCode()).isEqualTo(2993763);
        assertThat(isCritere).isTrue();
    }
}
