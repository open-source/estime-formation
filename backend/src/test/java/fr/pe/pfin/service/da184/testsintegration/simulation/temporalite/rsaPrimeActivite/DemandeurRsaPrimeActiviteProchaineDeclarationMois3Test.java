package fr.pe.pfin.service.da184.testsintegration.simulation.temporalite.rsaPrimeActivite;

import fr.pe.pfin.service.da184.RedisTestConfiguration;
import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.DemandeurController;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SimulationMensuelle;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = RedisTestConfiguration.class)
@ActiveProfiles("test")
class DemandeurRsaPrimeActiviteProchaineDeclarationMois3Test extends Utile {
    private static final int PROCHAINE_DECLARATION_TRIMESTRIELLE = 3;
    private final DemandeurController demandeurController;

    @Autowired
    public DemandeurRsaPrimeActiviteProchaineDeclarationMois3Test(DemandeurController demandeurController) {
        this.demandeurController = demandeurController;
    }

    @Test
    @SuppressWarnings("java:S5961")
    void simulerMesRessourcesFinancieresCelibataireMois3() {

        // Si DE Français, date naissance 5/07/1986, code postal 44200, célibataire, seul depuis plus de 18 mois, non propriétaire
        // Future formation 35h, salaire net 757€, durée 6 mois
        // kilométrage domicile -> taf = 10kms + 20 trajets
        // RSA 500€, déclaration trimetrielle en M3, non travaillé au cours des 3 derniers mois
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(true);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44019"));
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 10, 20));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setProchaineDeclarationTrimestrielle(PROCHAINE_DECLARATION_TRIMESTRIELLE);
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);

        // Lorsque je simule mes prestations le 01/05/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(265f);
            });
        });
        // Alors les prestations du second mois 07/2023
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(265f);
            });
        });
        // Alors les prestations du troisième mois 08/2023
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(265f);
            });
        });
        // Alors les prestations du quatrième mois 09/2023
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(83f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(282f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(265f);
            });
        });
        // Alors les prestations du cinquième mois 10/2023
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(83f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(282f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(265f);
            });
        });
        // Alors les prestations du sixième mois 11/2023
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(83f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(282f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(265f);
            });
        });
    }

    @Test
    @SuppressWarnings("java:S5961")
    void simulerMesRessourcesFinancieresCelibataireConventionneMois3() {

        // Si DE Français, date naissance 5/07/1986, code postal 44200, célibataire, seul depuis plus de 18 mois, non propriétaire
        // Future formation 35h, salaire net 757€, durée 6 mois
        // kilométrage domicile -> taf = 10kms + 20 trajets
        // RSA 500€, déclaration trimetrielle en M3, non travaillé au cours des 3 derniers mois
        // AL 310€
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getSituationFamiliale().setIsSeulPlusDe18Mois(true);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44019"));
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 10, 20));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setProchaineDeclarationTrimestrielle(PROCHAINE_DECLARATION_TRIMESTRIELLE);
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAidesLogement(creerAllocationLogement(310f));

        // Lorsque je simule mes prestations le 01/06/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isBetween(223f, 226f);
            });
        });
        // Alors les prestations du second mois 07/2023
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isBetween(223f, 226f);
            });
        });
        // Alors les prestations du troisième mois 08/2023
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(2);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isBetween(223f, 226f);
            });
        });
        // Alors les prestations du quatrième mois 09/2023
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(pa -> {
                assertThat(pa.getMontant()).isEqualTo(83f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isBetween(223f, 226f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(282f);
            });
        });
        // Alors les prestations du cinquième mois 10/2023
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(83f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isBetween(223f, 226f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(282f);
            });
        });
        // Alors les prestations du sixième mois 11/2023
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getRessourcesFinancieres().get(AideEnum.REMU.getCode())).isNotNull();
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(83f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isBetween(223f, 226f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(282f);
            });
        });
    }
}
