package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.periodes;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.PeriodeTravailleeAvantSimulation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Salaire;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.NationaliteEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypePopulationEnum;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.OpenFiscaMappeurPeriode;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurPeriodesSalairesTests extends Utile {

    private final LocalDate dateDebutSimulation = LocalDate.of(2023, 1, 1);


    /*****************
     * TESTS SUR creerPeriodesSalaireDemandeur
     ********************************************************/

    /**
     * CONTEXTE DES TESTS
     * <p>
     * date demande simulation : 01-06-2023
     * <p>
     * avant simulation
     * M-2 05/2023
     * M-1 06/2023
     * <p>
     * période de la simulation sur 6 mois
     * M1 07/2023
     * M2 08/2023
     * M3 09/2023
     * M4 10/2023
     * M5 11/2023
     * M6 12/2023
     ********************************************************************************/

    /**
     * Demandeur ASS sans cumul ASS + salaire dans les 3 mois avant la simulation
     * Création de la période salaire pour une simulation au mois M5
     */
    @Test
    void mapPeriodeSalaireTest() throws Exception {

        String openFiscaPayloadSalaireNetExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesSalairesTests/demandeur/salaire-net-8-mois-salaire-avant-simulation.json");

        Demandeur demandeur = creerDemandeurSalairesTests();

        OpenFiscaIndividu demandeurOpenFisca = new OpenFiscaIndividu();
        OpenFiscaMappeurPeriode.creerPeriodesSalaireDemandeur(demandeurOpenFisca, demandeur, dateDebutSimulation);

        assertThat(demandeurOpenFisca.getSalaireNet().toString()).hasToString(openFiscaPayloadSalaireNetExpected);
    }

    /**
     * Demandeur ASS sans cumul ASS + salaire dans les 3 mois avant la simulation
     * Création de la période salaire pour une simulation au mois M5
     */
    @Test
    void mapPeriodeCumulAncienEtNouveauSalaireTest() throws Exception {

        String openFiscaPayloadSalaireNetExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesSalairesTests/demandeur/salaire-net-cumul-ancien-et-nouveau-salaire.json");

        Demandeur demandeur = creerDemandeurCumulAncienEtNouveauSalairesTests();

        OpenFiscaIndividu demandeurOpenFisca = new OpenFiscaIndividu();
        OpenFiscaMappeurPeriode.creerPeriodesSalaireCumulDemandeur(demandeurOpenFisca, demandeur, dateDebutSimulation);

        assertThat(demandeurOpenFisca.getSalaireNet().toString()).hasToString(openFiscaPayloadSalaireNetExpected);
    }

    private Demandeur creerDemandeurSalairesTests() throws ParseException {

        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(isEnCouple, personnesACharge);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelBrut(1291);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelNet(1000);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(16.89f);

        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(true);
        PeriodeTravailleeAvantSimulation periodeTravailleeAvantSimulation = new PeriodeTravailleeAvantSimulation();
        Salaire[] salaires = creerSalaires(0, 0, 12);
        Salaire salaireMoisMoins1 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins3 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins4 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins5 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins7 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins9 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins11 = creerSalaire(850, 1101);
        salaires = ajouterSalaire(salaires, salaireMoisMoins1, 1);
        salaires = ajouterSalaire(salaires, salaireMoisMoins3, 3);
        salaires = ajouterSalaire(salaires, salaireMoisMoins4, 4);
        salaires = ajouterSalaire(salaires, salaireMoisMoins5, 5);
        salaires = ajouterSalaire(salaires, salaireMoisMoins7, 7);
        salaires = ajouterSalaire(salaires, salaireMoisMoins9, 9);
        salaires = ajouterSalaire(salaires, salaireMoisMoins11, 11);
        periodeTravailleeAvantSimulation.setMois(createMoisTravaillesAvantSimulation(salaires));
        demandeur.getRessourcesFinancieresAvantSimulation().setPeriodeTravailleeAvantSimulation(periodeTravailleeAvantSimulation);

        return demandeur;
    }

    private Demandeur creerDemandeurCumulAncienEtNouveauSalairesTests() throws ParseException {

        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.NON_BENEFICIAIRE, isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.getFutureFormation().getSalaire().setMontantMensuelBrut(1291);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelNet(1000);
        demandeur.getInformationsPersonnelles().setSalarie(true);
        demandeur.getInformationsPersonnelles().setCumulAncienEtNouveauSalaire(true);

        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(true);
        PeriodeTravailleeAvantSimulation periodeTravailleeAvantSimulation = new PeriodeTravailleeAvantSimulation();
        Salaire[] salaires = creerSalaires(0, 0, 12);
        Salaire salaireMois0 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins1 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins3 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins4 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins5 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins7 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins9 = creerSalaire(850, 1101);
        Salaire salaireMoisMoins11 = creerSalaire(850, 1101);
        salaires = ajouterSalaire(salaires, salaireMois0, 0);
        salaires = ajouterSalaire(salaires, salaireMoisMoins1, 1);
        salaires = ajouterSalaire(salaires, salaireMoisMoins3, 3);
        salaires = ajouterSalaire(salaires, salaireMoisMoins4, 4);
        salaires = ajouterSalaire(salaires, salaireMoisMoins5, 5);
        salaires = ajouterSalaire(salaires, salaireMoisMoins7, 7);
        salaires = ajouterSalaire(salaires, salaireMoisMoins9, 9);
        salaires = ajouterSalaire(salaires, salaireMoisMoins11, 11);
        periodeTravailleeAvantSimulation.setMois(createMoisTravaillesAvantSimulation(salaires));
        demandeur.getRessourcesFinancieresAvantSimulation().setPeriodeTravailleeAvantSimulation(periodeTravailleeAvantSimulation);

        return demandeur;
    }
}
