package fr.pe.pfin.service.da184.infrastructure.aides.poleemploi.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Aide;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.MessageInformatifEnum;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class AgepiUtileTest {

    @Test
    void messageAlerteAgepiTest() {

        Aide agepi = AgepiUtile.creerAgepi(500f);

        List<String> messagesAlertesAttendus = new ArrayList<>();
        messagesAlertesAttendus.add(MessageInformatifEnum.AGEPI_IDF.getMessage());
        messagesAlertesAttendus.add(MessageInformatifEnum.AGEPI_AM_DELAI_DEMANDE.getMessage());

        // le message d'alerte sur le délai de demande Agepi est présent
        assertThat(agepi.getMessagesAlerte()).isEqualTo(messagesAlertesAttendus);
    }
}
