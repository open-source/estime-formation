package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class NationalitesUtileTest {

    @Test
    @DisplayName("doit retourner une chaine de caractères possédant la liste des nationalitées possibles")
    void doitRetrounerUneListeFormateeEnChaineDesNationalitesPossibles() {
        assertThat(NationalitesUtile.getListeFormateeNationalitesPossibles()).isEqualTo("FRANCAISE / RESSORTISSANT_UNION_EUROPEENNE / RESSORTISSANT_ESPACE_ECONOMIQUE_EUROPEEN / SUISSE / AUTRE");
    }

}
