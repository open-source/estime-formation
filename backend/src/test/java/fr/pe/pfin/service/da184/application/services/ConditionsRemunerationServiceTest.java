package fr.pe.pfin.service.da184.application.services;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.ConditionsRemunerations;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SituationRemuneration;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/**
 *
 * @author EXGO2620
 */
@ActiveProfiles("test")
@SpringBootTest
class ConditionsRemunerationServiceTest {
    @Autowired
    ConditionsRemunerationService instance;

    public ConditionsRemunerationServiceTest() {
    }
    
    /**
     * Test of getConditionsList method, of class ConditionsRemunerationService.
     */
    @Test
    void testGetConditionsList() throws Exception {
        System.out.println("getConditionsList");
        SituationRemuneration situationRemuneration = new SituationRemuneration();
        situationRemuneration.setRegion("013");
        situationRemuneration.setAge(20f);
        situationRemuneration.setNbPersACharge(1f);

        List<ConditionsRemunerations> result = instance.getConditionsList(situationRemuneration);
        assertEquals(5, result.size());
    }
}
