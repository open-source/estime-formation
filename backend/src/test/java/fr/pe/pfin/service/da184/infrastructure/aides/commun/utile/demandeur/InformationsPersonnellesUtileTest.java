package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.demandeur;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Coordonnees;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.InformationsPersonnelles;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Logement;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.MicroEntreprise;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author EXGO2620
 */
class InformationsPersonnellesUtileTest {

    /**
     * Test of isFrancais method, of class InformationsPersonnellesUtile.
     */
    @Test
    void testIsFrancais() {
        System.out.println("isFrancais");
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setNationalite("Française");
        boolean expResult = true;
        boolean result = InformationsPersonnellesUtile.isFrancais(informationsPersonnelles);
        assertEquals(expResult, result);

        expResult = false;
        informationsPersonnelles.setNationalite("Ressortissant de l'Union Européenne");

        result = InformationsPersonnellesUtile.isFrancais(informationsPersonnelles);
        assertEquals(expResult, result);

        informationsPersonnelles.setNationalite("Ressortissant de l'Espace Économique Européen");
        result = InformationsPersonnellesUtile.isFrancais(informationsPersonnelles);
        assertEquals(expResult, result);

        informationsPersonnelles.setNationalite("Suisse");
        result = InformationsPersonnellesUtile.isFrancais(informationsPersonnelles);
        assertEquals(expResult, result);

        informationsPersonnelles.setNationalite("Autre");
        result = InformationsPersonnellesUtile.isFrancais(informationsPersonnelles);
        assertFalse(result);

        informationsPersonnelles.setNationalite("TOTO");
        result = InformationsPersonnellesUtile.isFrancais(informationsPersonnelles);
        assertFalse(result);
    }

    /**
     * Test of isEuropeenOuSuisse method, of class
     * InformationsPersonnellesUtile.
     */
    @Test
    void testIsEuropeenOuSuisse() {
        System.out.println("isEuropeenOuSuisse");
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setNationalite("Ressortissant de l'Union Européenne");
        boolean expResult = true;

        boolean result = InformationsPersonnellesUtile.isEuropeenOuSuisse(informationsPersonnelles);
        assertEquals(expResult, result);

        informationsPersonnelles.setNationalite("Ressortissant de l'Espace Économique Européen");
        result = InformationsPersonnellesUtile.isEuropeenOuSuisse(informationsPersonnelles);
        assertEquals(expResult, result);

        informationsPersonnelles.setNationalite("Suisse");
        result = InformationsPersonnellesUtile.isEuropeenOuSuisse(informationsPersonnelles);
        assertEquals(expResult, result);

        informationsPersonnelles.setNationalite("Française");
        result = InformationsPersonnellesUtile.isEuropeenOuSuisse(informationsPersonnelles);
        assertFalse(result);

        informationsPersonnelles.setNationalite("Autre");
        result = InformationsPersonnellesUtile.isEuropeenOuSuisse(informationsPersonnelles);
        assertFalse(result);

        informationsPersonnelles.setNationalite("TOTO");
        result = InformationsPersonnellesUtile.isEuropeenOuSuisse(informationsPersonnelles);
        assertFalse(result);
    }

    /**
     * Test of isNotFrancaisOuEuropeenOuSuisse method, of class
     * InformationsPersonnellesUtile.
     */
    @Test
    void testIsNotFrancaisOuEuropeenOuSuisse() {
        System.out.println("isNotFrancaisOuEuropeenOuSuisse");
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setNationalite("AUTRE");
        boolean expResult = true;

        boolean result = InformationsPersonnellesUtile.isNotFrancaisOuEuropeenOuSuisse(informationsPersonnelles);
        assertEquals(expResult, result);

        informationsPersonnelles.setNationalite("Française");
        result = InformationsPersonnellesUtile.isNotFrancaisOuEuropeenOuSuisse(informationsPersonnelles);
        assertFalse(result);
    }

    /**
     * Test of hasTitreSejourEnFranceValide method, of class
     * InformationsPersonnellesUtile.
     */
    @Test
    void testHasTitreSejourEnFranceValide() {
        System.out.println("hasTitreSejourEnFranceValide");
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();

        boolean expResult = false;
        boolean result = InformationsPersonnellesUtile.hasTitreSejourEnFranceValide(informationsPersonnelles);
        assertEquals(expResult, result);

        informationsPersonnelles.setTitreSejourEnFranceValide(true);
        result = InformationsPersonnellesUtile.hasTitreSejourEnFranceValide(informationsPersonnelles);
        assertTrue(result);
    }

    /**
     * Test of isBeneficiaireACRE method, of class
     * InformationsPersonnellesUtile.
     */
    @Test
    void testIsBeneficiaireACRE() {
        System.out.println("isBeneficiaireACRE");
        Demandeur demandeur = new Demandeur();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        demandeur.setInformationsPersonnelles(informationsPersonnelles);

        boolean expResult = false;
        boolean result = InformationsPersonnellesUtile.isBeneficiaireACRE(demandeur);
        assertEquals(expResult, result);

        informationsPersonnelles.setBeneficiaireACRE(true);
        informationsPersonnelles.setMicroEntreprise(new MicroEntreprise());
        informationsPersonnelles.setMicroEntrepreneur(true);
        result = InformationsPersonnellesUtile.isBeneficiaireACRE(demandeur);
        assertTrue(result);
    }

    /**
     * Test of getNombreMoisDepuisCreationEntreprise method, of class
     * InformationsPersonnellesUtile.
     */
    @Test
    void testGetNombreMoisDepuisCreationEntreprise() {
        System.out.println("getNombreMoisDepuisCreationEntreprise");
        LocalDate dateDebutSimulation = LocalDate.now();
        dateDebutSimulation = dateDebutSimulation.minusDays(dateDebutSimulation.getDayOfMonth() - 1);

        Demandeur demandeur = new Demandeur();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        MicroEntreprise me = new MicroEntreprise();
        me.setDateRepriseCreationEntreprise(dateDebutSimulation.minusMonths(6));
        informationsPersonnelles.setMicroEntreprise(me);
        informationsPersonnelles.setMicroEntrepreneur(true);
        demandeur.setInformationsPersonnelles(informationsPersonnelles);

        int expResult = 5;
        int result = InformationsPersonnellesUtile.getNombreMoisDepuisCreationEntreprise(demandeur, dateDebutSimulation);
        assertEquals(expResult, result);

        informationsPersonnelles.setMicroEntrepreneur(false);
        result = InformationsPersonnellesUtile.getNombreMoisDepuisCreationEntreprise(demandeur, dateDebutSimulation);
        assertEquals(0, result);
    }

    /**
     * Test of hasCodePostal method, of class InformationsPersonnellesUtile.
     */
    @Test
    void testHasCodePostal() {
        System.out.println("hasCodePostal");
        Demandeur demandeur = new Demandeur();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        demandeur.setInformationsPersonnelles(informationsPersonnelles);

        boolean expResult = false;
        boolean result = InformationsPersonnellesUtile.hasCodePostal(demandeur);
        assertEquals(expResult, result);

        Logement l = new Logement();
        Coordonnees c = new Coordonnees();
        c.setCodePostal("33000");
        l.setCoordonnees(c);
        informationsPersonnelles.setLogement(l);

        result = InformationsPersonnellesUtile.hasCodePostal(demandeur);
        assertTrue(result);

    }

    /**
     * Test of hasMicroEntreprise method, of class
     * InformationsPersonnellesUtile.
     */
    @Test
    void testHasMicroEntreprise() {
        System.out.println("hasMicroEntreprise");
        Demandeur demandeur = new Demandeur();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setMicroEntrepreneur(true);
        informationsPersonnelles.setMicroEntreprise(new MicroEntreprise());
        demandeur.setInformationsPersonnelles(informationsPersonnelles);

        boolean expResult = true;
        boolean result = InformationsPersonnellesUtile.hasMicroEntreprise(demandeur);
        assertEquals(expResult, result);

        informationsPersonnelles.setMicroEntreprise(null);
        result = InformationsPersonnellesUtile.hasMicroEntreprise(demandeur);
        assertFalse(result);

    }

    /**
     * Test of getRevenusActuelsSur1MoisMicroEntreprise method, of class
     * InformationsPersonnellesUtile.
     */
    @Test
    void testGetRevenusActuelsSur1MoisMicroEntreprise() {
        System.out.println("getRevenusActuelsSur1MoisMicroEntreprise");
        Demandeur demandeur = new Demandeur();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setMicroEntrepreneur(true);
        MicroEntreprise m = new MicroEntreprise();
        m.setChiffreAffairesN(12000F);

        informationsPersonnelles.setMicroEntreprise(m);
        demandeur.setInformationsPersonnelles(informationsPersonnelles);

        float expResult = 1000F;
        float result = InformationsPersonnellesUtile.getRevenusActuelsSur1MoisMicroEntreprise(demandeur);
        assertEquals(expResult, result, 0);
    }

    /**
     * Test of isSalarie method, of class InformationsPersonnellesUtile.
     */
    @Test
    void testIsSalarie() {
        System.out.println("isSalarie");
        Demandeur demandeur = new Demandeur();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        demandeur.setInformationsPersonnelles(informationsPersonnelles);

        boolean expResult = false;
        boolean result = InformationsPersonnellesUtile.isSalarie(demandeur);

        assertEquals(expResult, result);

        informationsPersonnelles.setSalarie(true);
        expResult = true;
        result = InformationsPersonnellesUtile.isSalarie(demandeur);
        assertEquals(expResult, result);
    }

    /**
     * Test of hasCumulAncienEtNouveauSalaire method, of class
     * InformationsPersonnellesUtile.
     */
    @Test
    void testHasCumulAncienEtNouveauSalaire() {
        System.out.println("hasCumulAncienEtNouveauSalaire");
        Demandeur demandeur = new Demandeur();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setSalarie(true);
        informationsPersonnelles.setCumulAncienEtNouveauSalaire(true);
        demandeur.setInformationsPersonnelles(informationsPersonnelles);
        boolean expResult = true;
        boolean result = InformationsPersonnellesUtile.hasCumulAncienEtNouveauSalaire(demandeur);
        assertEquals(expResult, result);
    }
}