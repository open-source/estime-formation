package fr.pe.pfin.service.da184.application.ui.rest;

import fr.pe.pfin.service.da184.application.services.DonneesBRSAService;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = AdminController.class)
@ActiveProfiles("test")
class AdminControllerTest {

    @MockBean
    DonneesBRSAService donneesBRSAService;
    MockMvc mockMvc;

    @Autowired
    public AdminControllerTest(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Test
    @DisplayName("doit vérifier l'appel a la ressource d'admin du brsaData pour un csv")
    void doitVerifierLAppelCorrectALaRessourcebrsaData() throws Exception {
        when(donneesBRSAService.obtenirDonneesBRSA(any(), any())).thenReturn(new ByteArrayInputStream("test".getBytes(StandardCharsets.UTF_8)));
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get("/admin/brsa-datas")
                        .param("debut", LocalDate.now().minusDays(1).toString())
                        .param("fin", LocalDate.now().plusDays(1).toString())
                )
                .andExpect(status().isOk()).andReturn();

        verify(donneesBRSAService).obtenirDonneesBRSA(any(), any());
        assertThat(mvcResult.getResponse().getContentType()).isEqualTo("text/csv");
        assertThat(mvcResult.getResponse().getContentAsString()).isNotNull().isEqualTo("test");
    }

    @Test
    @DisplayName("doit retourner une erreur lors de l'appel à la ressource avec une date de debut après la date du jour")
    void doitRetournerUneErreurPourDateDebutApresDateDuJour() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/admin/brsa-datas")
                        .param("debut", LocalDate.now().plusDays(1).toString())
                        .param("fin", LocalDate.now().plusDays(2).toString())
                )
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(BadRequestException.class))
                .andExpect(result -> assertThat(result.getResolvedException().getMessage()).isEqualTo(AdminController.DATE_DEBUT_APRES_DATE_FIN));
    }

    @Test
    @DisplayName("doit retourner une erreur lors de l'appel à la ressource avec une date de fin avant la date de debut")
    void doitRetournerUneErreurPourDateFinAvantDateDebut() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/admin/brsa-datas")
                        .param("debut", LocalDate.now().toString())
                        .param("fin", LocalDate.now().minusDays(1).toString())
                )
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(BadRequestException.class))
                .andExpect(result -> assertThat(result.getResolvedException().getMessage()).isEqualTo(AdminController.DATE_FIN_AVANT_DATE_DEBUT));
    }

    @Test
    @DisplayName("doit retourner une erreur lors de l'appel à la ressource avec une periode de plus de 2mois")
    void doitRetournerUneErreurPourPeriodePlus2Mois() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/admin/brsa-datas")
                        .param("debut", LocalDate.now().minusMonths(3).toString())
                        .param("fin", LocalDate.now().toString())
                )
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertThat(result.getResolvedException()).isInstanceOf(BadRequestException.class))
                .andExpect(result -> assertThat(result.getResolvedException().getMessage()).isEqualTo(AdminController.PAS_PLUS_DE_2_MOIS_ENTRE_2_DATES));
    }
}
