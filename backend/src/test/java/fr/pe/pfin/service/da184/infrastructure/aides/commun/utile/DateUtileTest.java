package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.InformationsPersonnelles;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class DateUtileTest {

    @Test
    @DisplayName("doit retourner l'écart de mois entre 2 dates")
    void doitRetournerLecartMoisEntre2Dates() {
        assertThat(DateUtile.getNbrMoisEntreDeuxLocalDates(LocalDate.now(), LocalDate.now().minusDays(1))).isEqualTo(0);
    }
}
