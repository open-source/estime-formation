package fr.pe.pfin.service.da184.infrastructure.database.redis;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.InformationsPersonnelles;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.RessourcesFinancieresAvantSimulation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import fr.pe.pfin.service.da184.infrastructure.database.redis.models.BRSAData;
import fr.pe.pfin.service.da184.infrastructure.database.redis.repositories.BRSADataRepository;
import fr.pe.pfin.service.da184.infrastructure.database.redis.utile.RedisUtile;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class RedisUtileTest {
    @MockBean
    private BRSADataRepository brsaDataRepository;
    @Autowired
    RedisUtile instance;

    @Test
    @DisplayName("doit pas retourner d'objet BRSAData suite à la récupération dans le repository de tout")
    void saveBRSAAndRedis_ok() {
        BRSAData resultat = new BRSAData("id-redis", "id-redis","013", true, LocalDateTime.now(), 1, 2, new ArrayList<String>(), "65");
        resultat.setId("id-redis");
        when(brsaDataRepository.save(any())).thenReturn(resultat);
        String idDemandeur = "";
        Demandeur demandeur1 = new Demandeur();
        demandeur1.setRessourcesFinancieresAvantSimulation(new RessourcesFinancieresAvantSimulation());
        demandeur1.setInformationsPersonnelles(new InformationsPersonnelles());
        demandeur1.setIdDemandeurEmploi(idDemandeur);
        String idRedis = instance.saveBRSAAndRedis(demandeur1, null);
        assertThat(idRedis).isNotNull();
        assertThat(idRedis).isEqualTo("id-redis");
    }

    @Test
    @DisplayName("ne doit pas retourner d'objet BRSAData suite à la récupération dans le repository de tout")
    void saveBRSAAndRedis_sonde() {
        String idDemandeur = "SONDE000";
        Demandeur demandeur1 = new Demandeur();
        demandeur1.setIdDemandeurEmploi(idDemandeur);
        String idRedis = instance.saveBRSAAndRedis(demandeur1, new Simulation());
        assertThat(idRedis).isNotNull();
        assertThat(idRedis).isEqualTo("SONDE000");
    }
}
