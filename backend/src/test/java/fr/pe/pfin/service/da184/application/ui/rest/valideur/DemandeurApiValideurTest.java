package fr.pe.pfin.service.da184.application.ui.rest.valideur;

import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.BadRequestMessages;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class DemandeurApiValideurTest {

    @Test
    void controlerDonneeesEntreeDemandeurAbsentTest() {
        assertThatThrownBy(() -> DemandeurApiValideur.validerDonneesEntreeServiceSimulerMesAides(null))
                .isInstanceOf(HttpServerErrorException.class)
                .hasMessage(HttpStatus.BAD_REQUEST.value() + " " + BadRequestMessages.DEMANDEUR_OBLIGATOIRE.getMessage());
    }

}
