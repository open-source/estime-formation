package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.NationaliteEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypePopulationEnum;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurAAHTest extends Utile {

    @Test
    void doitAjouterUneAllocationAdulteHandicapeDansOpenFiscaIndividu() throws ParseException {
        OpenFiscaIndividu openFiscaIndividu = new OpenFiscaIndividu();
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.AAH, false, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesCAF(createAidesCAF());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationAAH(200f);
        OpenFiscaMappeurAAH.addAAHOpenFiscaIndividu(openFiscaIndividu, demandeur, LocalDate.now());
        assertThat(openFiscaIndividu.getAllocationAdulteHandicape()).hasSize(10);
    }

}
