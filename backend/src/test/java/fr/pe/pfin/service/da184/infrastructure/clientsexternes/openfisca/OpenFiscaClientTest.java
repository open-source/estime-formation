package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca;

import fr.pe.pfin.service.da184.application.configuration.ClientExternePoleEmploiProperties;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("Etant donnée un client OpenFisca")
class OpenFiscaClientTest {
    @Mock
    RestTemplate restTemplate;
    @Mock
    ClientExternePoleEmploiProperties clientExternePoleEmploiProperties;
    CircuitBreakerRegistry circuitBreakerRegistry = CircuitBreakerRegistry.ofDefaults();
    RetryRegistry retryRegistry = RetryRegistry.ofDefaults();
    OpenFiscaClient openFiscaClient;

    Demandeur demandeur;
    OpenFiscaRoot openFiscaRoot;

    @BeforeEach
    void init() {
        this.openFiscaClient = new OpenFiscaClient(restTemplate, circuitBreakerRegistry, retryRegistry, clientExternePoleEmploiProperties);
        demandeur = new Demandeur();
        OpenFiscaPeriodes openFiscaPeriodes = new OpenFiscaPeriodes();
        openFiscaPeriodes.put("01/2023", "2000-01-01");
        OpenFiscaIndividu openFiscaIndividu = new OpenFiscaIndividu();
        openFiscaIndividu.setDateNaissance(openFiscaPeriodes);
        openFiscaRoot = new OpenFiscaRoot();
        openFiscaRoot.setIndividus(Map.of("individu", openFiscaIndividu));
        when(clientExternePoleEmploiProperties.openFisca()).thenReturn("http://localhost/calculate");
    }

    @Nested
    @DisplayName("Quand appel la ressource calculate sur la méthode callApiCalculate")
    class Calculate {


        @SuppressWarnings("java:S5778")
        @Test
        @DisplayName("Et que le circuit est ouvert Alors le service retourne une CallNotPermittedException")
        void doitRetournerUnCallNotPermittedExceptionPourUnCircuitOuvert() {
            reset(clientExternePoleEmploiProperties);
            CircuitBreaker circuitBreaker = circuitBreakerRegistry.circuitBreaker(OpenFiscaClient.R4J_OPENFISCA_CALCULATE);
            circuitBreaker.transitionToForcedOpenState();
            assertThatThrownBy(() -> openFiscaClient.callApiCalculate(demandeur, LocalDate.now()))
                    .isInstanceOf(CallNotPermittedException.class);
            circuitBreaker.transitionToClosedState();
        }

        @SuppressWarnings("java:S5778")
        @Test
        @DisplayName("Et que le service ne répond pas Alors on a bien un retry d'appel")
        void doitRealiserUnRetryApresUneErreurServeur() {
            Retry retry = retryRegistry.retry(OpenFiscaClient.R4J_OPENFISCA_CALCULATE);
            long nbRetryAvant = retry.getMetrics().getNumberOfFailedCallsWithRetryAttempt();
            doThrow(HttpServerErrorException.class).when(restTemplate).postForObject(anyString(), any(), eq(OpenFiscaRoot.class));
            assertThatThrownBy(() -> openFiscaClient.callApiCalculate(demandeur, LocalDate.now()))
                    .isInstanceOf(HttpServerErrorException.class);
            long nbRetryRealise = retry.getMetrics().getNumberOfFailedCallsWithRetryAttempt();
            // 1 retry realisé == 3 appels de la ressource REST
            assertThat(nbRetryRealise).isGreaterThan(nbRetryAvant).isEqualTo(1L);
            verify(restTemplate, times(3)).postForObject(anyString(), any(), eq(OpenFiscaRoot.class));
            reset(restTemplate);
        }
    }

}
