package fr.pe.pfin.service.da184.infrastructure.aides.commun.utile;

import fr.pe.pfin.service.da184.application.ui.rest.ressource.InformationsPersonnelles;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class AgeUtileTest {

    @Test
    @DisplayName("doit retourner l'age de la personne")
    void doitRetournerLAgeDeLaPersonne() {
        Personne personne = new Personne();
        personne.setInformationsPersonnelles(new InformationsPersonnelles());
        personne.getInformationsPersonnelles().setDateNaissance(LocalDate.now().minusYears(25));
        assertThat(AgeUtile.calculerAge(personne)).isEqualTo(25);
    }


    @Test
    @DisplayName("doit retourner le nombre de mois entre l'age de la personne et le mois simulé")
    void doitRetournerLeNombreDeMoisEntreLAgeDelaPersonneEtLeMoisSimule() {
        Personne personne = new Personne();
        personne.setInformationsPersonnelles(new InformationsPersonnelles());
        personne.getInformationsPersonnelles().setDateNaissance(LocalDate.now().minusYears(25));
        assertThat(AgeUtile.calculerAgeMoisMoisSimule(personne, 1)).isEqualTo(301);
    }


}
