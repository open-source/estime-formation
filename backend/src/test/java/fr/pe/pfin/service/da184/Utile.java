package fr.pe.pfin.service.da184;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.*;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile.enleverMoisALocalDate;
import static fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile.getDateJour;
import static org.mockito.Mockito.mockStatic;

public class Utile {

    private static final int NOMBRE_MOIS_SALAIRES_AVANT_SIMULATION = 14;

    private MockedStatic<DateUtile> mockedDateUtile;

    @BeforeEach
    void init() {
        LocalDate dateDuJourMock = LocalDate.of(2023, 5, 1);
        mockedDateUtile = mockStatic(DateUtile.class, Mockito.CALLS_REAL_METHODS);
        mockedDateUtile.when(DateUtile::getDateJour).thenReturn(dateDuJourMock);
    }

    @AfterEach
    void unload() {
        mockedDateUtile.close();
    }

    protected Demandeur createDemandeurRSA(boolean isEnCouple, List<Personne> personnesACharge) {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.RSA, isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        return demandeur;
    }

    protected Demandeur createDemandeurASS(boolean isEnCouple, List<Personne> personnesACharge) {
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ASS, isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = enleverMoisALocalDate(DateUtile.getDate("2023-05-01"), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        return demandeur;
    }

    protected Demandeur createBaseDemandeur(TypePopulationEnum population, boolean isEnCouple, List<Personne> personnesACharge) {
        Demandeur demandeur = new Demandeur();
        demandeur.setIdDemandeurEmploi("idPoleEmploi");

        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setNom("DUPONT");
        informationsPersonnelles.setPrenom("DANIEL");
        Logement logement = new Logement();
        informationsPersonnelles.setLogement(logement);
        demandeur.setInformationsPersonnelles(informationsPersonnelles);

        demandeur.setBeneficiaireAides(creerBeneficiaireAides(population));

        SituationFamiliale situationFamiliale = new SituationFamiliale();
        intiSituationFamiliale(isEnCouple, personnesACharge, situationFamiliale, false);
        demandeur.setSituationFamiliale(situationFamiliale);

        FutureFormation futureFormation = new FutureFormation();
        Salaire salaire = new Salaire();
        futureFormation.setSalaire(salaire);
        demandeur.setFutureFormation(futureFormation);

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = new RessourcesFinancieresAvantSimulation();
        initRessourcesFinancieres(ressourcesFinancieres, population);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        return demandeur;
    }

    protected Personne createPersonne(LocalDate dateNaissance) {
        Personne personne1 = new Personne();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setDateNaissance(dateNaissance);
        personne1.setInformationsPersonnelles(informationsPersonnelles);
        return personne1;
    }

    protected Logement initLogement(boolean isLogementConventionne, String codeInsee) {
        Logement logement = new Logement();
        logement.setStatutOccupationLogement(StatutOccupationLogementEnum.LOCATAIRE_NON_MEUBLE.getLibelle());
        logement.setMontantLoyer(500f);
        logement.setCoordonnees(creerCoordonnees(codeInsee));
        logement.setConventionne(isLogementConventionne);
        return logement;
    }

    protected Coordonnees creerCoordonnees(String codeInsee) {
        Coordonnees coordonnees = new Coordonnees();
        coordonnees.setCodePostal("44200");
        coordonnees.setCodeInsee(codeInsee);
        return coordonnees;
    }

    private void intiSituationFamiliale(boolean isEnCouple, List<Personne> personnesACharge, SituationFamiliale situationFamiliale, Boolean isEnceinte) {
        situationFamiliale.setIsEnCouple(isEnCouple);
        if (isEnCouple) {
            Personne conjoint = new Personne();
            situationFamiliale.setConjoint(conjoint);
        }
        if (personnesACharge.size() > 0) {
            situationFamiliale.setPersonnesACharge(personnesACharge);
        }
        if (isEnceinte) {
            situationFamiliale.setIsEnceinte(isEnceinte);
        }
    }

    protected Personne createEnfant(int age) {
        Personne personne = new Personne();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setDateNaissance(DateUtile.getDateNaissanceFromAge(getDateJour(), age));
        personne.setInformationsPersonnelles(informationsPersonnelles);
        return personne;
    }

    private void initRessourcesFinancieres(RessourcesFinancieresAvantSimulation ressourcesFinancieres,
                                           TypePopulationEnum population) {
        switch (population) {
            case AAH, RSA:
                ressourcesFinancieres.setAidesCAF(creerAideCAF(population.getLibelle()));
                break;
            case ARE, ASS:
                ressourcesFinancieres.setAidesPoleEmploi(creerAidePoleEmploi(population.getLibelle()));
                break;
            case AAH_ASS:
                ressourcesFinancieres
                        .setAidesPoleEmploi(creerAidePoleEmploi(AideEnum.ALLOCATION_SOLIDARITE_SPECIFIQUE.getCode()));
                ressourcesFinancieres.setAidesCAF(creerAideCAF(AideEnum.ALLOCATION_ADULTES_HANDICAPES.getCode()));
                break;
            case AAH_ARE:
                ressourcesFinancieres.setAidesPoleEmploi(creerAidePoleEmploi(AideEnum.AIDE_RETOUR_EMPLOI.getCode()));
                ressourcesFinancieres.setAidesCAF(creerAideCAF(AideEnum.ALLOCATION_ADULTES_HANDICAPES.getCode()));
                break;
            default:
                break;
        }
        AllocationsLogement aidesLogement = creerAidesLogementVide();
        AidesFamiliales aidesFamiliales = new AidesFamiliales();
        AidesCAF aidesCAF = new AidesCAF();
        aidesCAF.setAidesLogement(aidesLogement);
        aidesCAF.setAidesFamiliales(aidesFamiliales);
        ressourcesFinancieres.setAidesCAF(aidesCAF);
    }

    protected AidesCPAM createPensionInvaliditeConjoint() {
        AidesCPAM aidesCPAMConjoint = new AidesCPAM();
        aidesCPAMConjoint.setPensionInvalidite(200f);
        return aidesCPAMConjoint;
    }

    protected FutureFormation initFormation(float dureeHebdo, float dureeEnMois, int remunerationNet, int distanceActiviteDomicile, int nombreTrajet) {
        FutureFormation futureFormation = new FutureFormation();
        futureFormation.setDureeFormationHebdomadaire(dureeHebdo);
        futureFormation.setDureeFormationEnMois(dureeEnMois);
        futureFormation.setSalaire(creerSalaireNet(remunerationNet));
        futureFormation.setDistanceActiviteDomicile(distanceActiviteDomicile);
        futureFormation.setNombreTrajetsDomicileTravail(nombreTrajet);

        return futureFormation;
    }

    protected Salaire[] ajouterSalaire(Salaire[] salaires, Salaire salaire, int numeroMois) {
        salaires[numeroMois] = salaire;
        return salaires;
    }

    protected Salaire[] creerSalaires(float montantNet, float montantBrut, int nombreMois) {
        Salaire[] salaires = new Salaire[NOMBRE_MOIS_SALAIRES_AVANT_SIMULATION];
        for (int index = 0; index < NOMBRE_MOIS_SALAIRES_AVANT_SIMULATION; index++) {
            salaires[index] = new Salaire();
            if (index < nombreMois) {
                salaires[index].setMontantMensuelBrut(montantBrut);
                salaires[index].setMontantMensuelNet(montantNet);
            }
        }
        return salaires;
    }

    protected Salaire creerSalaire(float montantNet, float montantBrut) {
        Salaire salaire = new Salaire();
        salaire.setMontantMensuelNet(montantNet);
        salaire.setMontantMensuelBrut(montantBrut);
        return salaire;
    }

    protected Salaire creerSalaireNet(float montantNet) {
        Salaire salaire = new Salaire();
        salaire.setMontantMensuelNet(montantNet);
        return salaire;
    }

    protected Salaire createSalaireConjoint(int montantNet, int montantBrut) {
        Salaire salaireConjoint = new Salaire();
        salaireConjoint.setMontantMensuelNet(montantNet);
        salaireConjoint.setMontantMensuelBrut(montantBrut);
        return salaireConjoint;
    }

    protected AidesCAF createAidesCAF() {
        AidesCAF aidesCAF = new AidesCAF();
        AllocationsLogement aidesLogement = creerAidesLogementVide();
        AidesFamiliales aidesFamiliales = createAidesFamiliales();
        aidesCAF.setAidesFamiliales(aidesFamiliales);
        aidesCAF.setAidesLogement(aidesLogement);
        return aidesCAF;
    }

    private AidesPoleEmploi creerAidePoleEmploi(String population) {
        AidesPoleEmploi aidesPoleEmploi = new AidesPoleEmploi();
        if (AideEnum.ALLOCATION_SOLIDARITE_SPECIFIQUE.getCode().equals(population)) {
            AllocationASS allocationASS = new AllocationASS();
            allocationASS.setAllocationJournaliereNet(0.0f);
            aidesPoleEmploi.setAllocationASS(allocationASS);
        }
        if (AideEnum.AIDE_RETOUR_EMPLOI.getCode().equals(population)) {
            AllocationARE allocationARE = new AllocationARE();
            allocationARE.setAllocationJournaliereBrute(0.0f);
            aidesPoleEmploi.setAllocationARE(allocationARE);
        }
        if (AideEnum.AIDE_RETOUR_EMPLOI.getCode().equals(population)) {
            AllocationARE allocationARE = new AllocationARE();
            allocationARE.setAllocationJournaliereBrute(0.0f);
            aidesPoleEmploi.setAllocationARE(allocationARE);
        }
        return aidesPoleEmploi;
    }

    protected AidesCAF creerAideCAF(String population) {
        AidesCAF aidesCAF = new AidesCAF();
        if (AideEnum.ALLOCATION_ADULTES_HANDICAPES.getCode().equals(population)) {
            aidesCAF.setAllocationAAH(0f);
        }
        if (AideEnum.RSA.getCode().equals(population)) {
            aidesCAF.setAllocationRSA(0f);
            aidesCAF.setProchaineDeclarationTrimestrielle(0);
        }
        return aidesCAF;
    }

    protected AidesFamiliales createAidesFamiliales() {
        AidesFamiliales aidesFamiliales = new AidesFamiliales();
        aidesFamiliales.setAllocationsFamiliales(0);
        aidesFamiliales.setComplementFamilial(0);
        aidesFamiliales.setAllocationSoutienFamilial(0);
        aidesFamiliales.setPrestationAccueilJeuneEnfant(0);
        return aidesFamiliales;
    }

    private AllocationsLogement creerAidesLogementVide() {
        return new AllocationsLogement();
    }

    protected AllocationsLogement creerAllocationLogement(float montant) {
        AllocationsLogement aidesLogement = creerAidesLogementVide();
        aidesLogement.setMoisNMoins1(montant);
        return aidesLogement;
    }

    private BeneficiaireAides creerBeneficiaireAides(TypePopulationEnum population) {
        switch (population) {
            case AAH:
                return creerBeneficiaireAides(true, false, false, false);
            case ARE:
                return creerBeneficiaireAides(false, true, false, false);
            case ASS:
                return creerBeneficiaireAides(false, false, true, false);
            case RSA:
                return creerBeneficiaireAides(false, false, false, true);
            case AAH_ASS:
                return creerBeneficiaireAides(true, false, true, false);
            case AAH_ARE:
                return creerBeneficiaireAides(true, true, false, false);
            default:
                return creerBeneficiaireAides(false, false, false, false);
        }
    }

    private BeneficiaireAides creerBeneficiaireAides(boolean beneficiaireAAH, boolean beneficiaireARE,
                                                     boolean beneficiaireASS, boolean beneficiaireRSA) {
        BeneficiaireAides beneficiaireAides = new BeneficiaireAides();
        beneficiaireAides.setBeneficiaireAAH(beneficiaireAAH);
        beneficiaireAides.setBeneficiaireASS(beneficiaireASS);
        beneficiaireAides.setBeneficiaireARE(beneficiaireARE);
        beneficiaireAides.setBeneficiaireRSA(beneficiaireRSA);
        return beneficiaireAides;
    }

    protected MoisTravailleAvantSimulation[] createMoisTravaillesAvantSimulation(Salaire[] salaires) {
        MoisTravailleAvantSimulation[] moisTravaillesAvantSimulation = new MoisTravailleAvantSimulation[NOMBRE_MOIS_SALAIRES_AVANT_SIMULATION];
        int index = 0;
        for (MoisTravailleAvantSimulation moisTravailleAvantSimulation : moisTravaillesAvantSimulation) {
            moisTravailleAvantSimulation = new MoisTravailleAvantSimulation();
            Salaire salaire = new Salaire();
            boolean isSansSalaire = true;
            LocalDate date = DateUtile.convertDateToLocalDate(new Date());

            if (index < salaires.length && salaires[index] != null) {
                salaire.setMontantMensuelBrut(salaires[index].getMontantMensuelBrut());
                salaire.setMontantMensuelNet(salaires[index].getMontantMensuelNet());
                if (salaires[index].getMontantMensuelNet() > 0) {
                    isSansSalaire = false;
                }
                date = enleverMoisALocalDate(date, index);
            }
            moisTravailleAvantSimulation.setSalaire(salaire);
            moisTravailleAvantSimulation.setSansSalaire(isSansSalaire);
            moisTravailleAvantSimulation.setDate(date);
            moisTravaillesAvantSimulation[index] = moisTravailleAvantSimulation;
            index++;
        }
        return moisTravaillesAvantSimulation;
    }

    protected PeriodeTravailleeAvantSimulation creerPeriodeTravailleeAvantSimulation(float montantNet, float montantBrut,
                                                                                     int nombreMois) {

        PeriodeTravailleeAvantSimulation periodeTravailleeAvantSimulation = new PeriodeTravailleeAvantSimulation();
        Salaire[] salaires = creerSalaires(montantNet, montantBrut, nombreMois);
        periodeTravailleeAvantSimulation.setMois(createMoisTravaillesAvantSimulation(salaires));
        return periodeTravailleeAvantSimulation;
    }

    protected String getStringFromJsonFile(String nameFile) throws URISyntaxException, IOException {

        URL resource = getClass().getClassLoader().getResource(nameFile);
        File file = new File(resource.toURI());

        return new ObjectMapper().readTree(file).toString();
    }

    protected Demandeur creerDemandeurAL(int prochaineDeclarationTrimestrielle) throws ParseException {
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        personnesACharge.add(createEnfant(10));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44109"));

        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 12, 20));

        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);
        demandeur.getRessourcesFinancieresAvantSimulation().setNombreMoisTravaillesDerniersMois(0);

        AidesFamiliales aidesFamiliales = new AidesFamiliales();
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAidesFamiliales(aidesFamiliales);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setProchaineDeclarationTrimestrielle(prochaineDeclarationTrimestrielle);

        AllocationsLogement aidesLogement = creerAllocationLogement(300f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAidesLogement(aidesLogement);

        return demandeur;
    }

    protected Demandeur createDemandeurEmploiMicroEntrepreneurAvecACRE(
            int nombreMoisDepuisReprisCreationEntreprise) throws ParseException {
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(9));
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ASS, isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);
        demandeur.getFutureFormation().setDistanceActiviteDomicile(80f);
        demandeur.getFutureFormation().setNombreTrajetsDomicileTravail(12);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 0, 0));
        LocalDate dateDerniereOuvertureDroitASS = enleverMoisALocalDate(DateUtile.getDate("2023-05-01"), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationSoutienFamilial(117f);

        demandeur.getInformationsPersonnelles().setBeneficiaireACRE(true);
        demandeur.getInformationsPersonnelles().setMicroEntrepreneur(true);
        demandeur.getInformationsPersonnelles().setMicroEntreprise(new MicroEntreprise());
        demandeur.getInformationsPersonnelles().getMicroEntreprise()
                .setTypeBenefices(TypesBeneficesMicroEntrepriseEnum.BIC.getCode());
        demandeur.getInformationsPersonnelles().getMicroEntreprise().setDateRepriseCreationEntreprise(
                enleverMoisALocalDate(getDateJour(), nombreMoisDepuisReprisCreationEntreprise));

        return demandeur;
    }

    protected Demandeur createDemandeurEmploiMicroEntrepreneurNonACRE(
            int nombreMoisDepuisReprisCreationEntreprise) throws ParseException {
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(9));
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.ASS, isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);
        demandeur.getFutureFormation().setDistanceActiviteDomicile(80f);
        demandeur.getFutureFormation().setNombreTrajetsDomicileTravail(12);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 0, 0));
        LocalDate dateDerniereOuvertureDroitASS = enleverMoisALocalDate(DateUtile.getDate("2023-05-01"), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationSoutienFamilial(117f);

        demandeur.getInformationsPersonnelles().setBeneficiaireACRE(false);
        demandeur.getInformationsPersonnelles().setMicroEntrepreneur(true);
        demandeur.getInformationsPersonnelles().setMicroEntreprise(new MicroEntreprise());
        demandeur.getInformationsPersonnelles().getMicroEntreprise()
                .setTypeBenefices(TypesBeneficesMicroEntrepriseEnum.BIC.getCode());
        demandeur.getInformationsPersonnelles().getMicroEntreprise().setDateRepriseCreationEntreprise(
                enleverMoisALocalDate(getDateJour(), nombreMoisDepuisReprisCreationEntreprise));

        return demandeur;
    }

    protected Demandeur creerDemandeurALTests(int prochaineDeclarationTrimestrielle) {
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple,
                personnesACharge);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelBrut(1200);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelNet(1000);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(300));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(prochaineDeclarationTrimestrielle);
        return demandeur;
    }

}
