package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.AllocationsLogement;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaFamille;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurAidesLogementTest extends Utile {
    private final LocalDate dateDebutSimulation = LocalDate.of(2023, 1, 1);

    @Test
    void doitRetournerUnMontantAideAuLogementEgalAZero() {
        assertThat(OpenFiscaMappeurAidesLogement.getMontantAideLogement(new OpenFiscaRoot(), LocalDate.now(), 0)).isEqualTo(0f);
    }

    @Test
    void doitRetournerUnEmptyStringPourUneAideLogement() {
        assertThat(OpenFiscaMappeurAidesLogement.getTypeAideLogement(new OpenFiscaRoot(), LocalDate.now(), 0)).isEmpty();
    }

    @Test
    void doitRetournerALPourUneAideLogement() {
        OpenFiscaRoot openFiscaRoot = new OpenFiscaRoot();
        openFiscaRoot.setIndividus(Map.of());
        OpenFiscaFamille openFiscaFamille = new OpenFiscaFamille();
        Demandeur demandeur = creerDemandeurALTests(0);
        AllocationsLogement allocationsLogement = demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesLogement();
        OpenFiscaPeriodes periodesAL = OpenFiscaMappeurPeriode.creerPeriodesAllocationsLogement(allocationsLogement, dateDebutSimulation, 0);
        openFiscaFamille.setAideLogement(periodesAL);
        openFiscaRoot.setFamilles(Map.of(ParametresOpenFisca.FAMILLE1,openFiscaFamille));
        openFiscaRoot.setMenages(Map.of());
        assertThat(OpenFiscaMappeurAidesLogement.getTypeAideLogement(openFiscaRoot, dateDebutSimulation, -1)).isEqualTo("AL");
    }

}
