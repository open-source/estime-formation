package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypePopulationEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


class OpenFiscaMappeurConjointTest extends Utile {

    private final LocalDate dateDebutSimulation = LocalDate.of(2023, 1, 1);


    @Test
    void mapDemandeurAvecConjointAAHToOpenFiscaPayloadTest() throws Exception {
        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/conjoint/demandeur-avec-conjoint-aah.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(true, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        Personne conjoint = demandeur.getSituationFamiliale().getConjoint();
        BeneficiaireAides beneficiaireAides = new BeneficiaireAides();
        beneficiaireAides.setBeneficiaireAAH(true);
        conjoint.setBeneficiaireAides(beneficiaireAides);
        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        AidesCAF aidesCAFConjoint = creerAideCAF(TypePopulationEnum.AAH.toString());
        aidesCAFConjoint.setAllocationAAH(900f);
        ressourcesFinancieresConjoint.setAidesCAF(aidesCAFConjoint);
        conjoint.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurAvecConjointASSToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/conjoint/demandeur-avec-conjoint-ass.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(true, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        Personne conjoint = demandeur.getSituationFamiliale().getConjoint();
        BeneficiaireAides beneficiaireAides = new BeneficiaireAides();
        beneficiaireAides.setBeneficiaireASS(true);
        conjoint.setBeneficiaireAides(beneficiaireAides);
        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        AidesPoleEmploi aidesPoleEmploi = new AidesPoleEmploi();
        AllocationASS allocationASS = new AllocationASS();
        allocationASS.setAllocationMensuelleNet(900f);
        aidesPoleEmploi.setAllocationASS(allocationASS);
        ressourcesFinancieresConjoint.setAidesPoleEmploi(aidesPoleEmploi);
        conjoint.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurAvecConjointAREToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/conjoint/demandeur-avec-conjoint-are.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(true, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        Personne conjoint = demandeur.getSituationFamiliale().getConjoint();
        BeneficiaireAides beneficiaireAides = new BeneficiaireAides();
        beneficiaireAides.setBeneficiaireARE(true);
        conjoint.setBeneficiaireAides(beneficiaireAides);
        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        AidesPoleEmploi aidesPoleEmploi = new AidesPoleEmploi();
        AllocationARE allocationARE = new AllocationARE();
        allocationARE.setAllocationMensuelleNet(900f);
        aidesPoleEmploi.setAllocationARE(allocationARE);
        ressourcesFinancieresConjoint.setAidesPoleEmploi(aidesPoleEmploi);
        conjoint.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurAvecConjointPensionInvaliditeToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/conjoint/demandeur-avec-conjoint-pension-invalidite.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(true, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        Personne conjoint = demandeur.getSituationFamiliale().getConjoint();
        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        AidesCPAM aidesCPAM = new AidesCPAM();
        aidesCPAM.setPensionInvalidite(200f);
        ressourcesFinancieresConjoint.setAidesCPAM(aidesCPAM);
        conjoint.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurAvecConjointSalaireToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/conjoint/demandeur-avec-conjoint-salaire.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(true, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        Personne conjoint = demandeur.getSituationFamiliale().getConjoint();
        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        Salaire salaireConjoint = new Salaire();
        salaireConjoint.setMontantMensuelNet(1200);
        salaireConjoint.setMontantMensuelBrut(1544);
        ressourcesFinancieresConjoint.setSalaire(salaireConjoint);
        ressourcesFinancieresConjoint.setHasTravailleAuCoursDerniersMois(true);
        ressourcesFinancieresConjoint
                .setPeriodeTravailleeAvantSimulation(creerPeriodeTravailleeAvantSimulation(1200, 1544, 13));
        conjoint.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurAvecConjointRevenusLocatifsToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/conjoint/demandeur-avec-conjoint-revenus-locatifs.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(true, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        Personne conjoint = demandeur.getSituationFamiliale().getConjoint();

        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        ressourcesFinancieresConjoint.setRevenusImmobilier3DerniersMois(3000f);
        conjoint.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurAvecConjointPensionRetraiteToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/conjoint/demandeur-avec-conjoint-pension-retraite.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(true, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        Personne conjoint = demandeur.getSituationFamiliale().getConjoint();

        RessourcesFinancieresAvantSimulation ressourcesFinancieresConjoint = new RessourcesFinancieresAvantSimulation();
        ressourcesFinancieresConjoint.setPensionRetraite(1000f);
        conjoint.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresConjoint);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }
}
