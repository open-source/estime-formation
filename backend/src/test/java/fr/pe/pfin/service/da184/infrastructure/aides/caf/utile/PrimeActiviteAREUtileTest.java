package fr.pe.pfin.service.da184.infrastructure.aides.caf.utile;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PrimeActiviteAREUtileTest {

    @Test
    @DisplayName("doit retourner vrai pour un numéro de mois à 4 sur Prime d'activité à calculer")
    void doitRetournerVraiPourNumeroMois4UnePrimeActiviteACalculer() {
        assertThat(PrimeActiviteAREUtile.isPrimeActiviteACalculer(4)).isTrue();
    }

    @Test
    @DisplayName("doit retourner faux pour un numéro de mois différent de 4 sur Prime d'activité à calculer")
    void doitRetournerFauxPourNumeroMoisDifferent4UnePrimeActiviteACalculer() {
        assertThat(PrimeActiviteAREUtile.isPrimeActiviteACalculer(3)).isFalse();
    }


    @Test
    @DisplayName("doit retourner vrai pour un numéro de mois à 5 sur Prime d'activité à verser")
    void doitRetournerVraiPourNumeroMois5UnePrimeActiviteAVerser() {
        assertThat(PrimeActiviteAREUtile.isPrimeActiviteAVerser(5)).isTrue();
    }

    @Test
    @DisplayName("doit retourner faux pour un numéro de mois différent de 5 sur Prime d'activité à verser")
    void doitRetournerFauxPourNumeroMoisDifferent5UnePrimeActiviteAVerser() {
        assertThat(PrimeActiviteAREUtile.isPrimeActiviteAVerser(4)).isFalse();
    }


}
