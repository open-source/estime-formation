package fr.pe.pfin.service.da184.testsintegration.simulation.openfisca.montants;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.OpenFiscaRetourSimulationHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class MontantsAideMobiliteTest extends Utile {
    private final LocalDate dateDebutSimulation = DateUtile.getDateJour();
    private final OpenFiscaClient openFiscaClient;

    @Autowired
    public MontantsAideMobiliteTest(OpenFiscaClient openFiscaClient) {
        this.openFiscaClient = openFiscaClient;
    }

    @Test
    void calculerMontantCelibataire1EnfantMoins10_60km_5jours() throws Exception {

        // Si DE France Métropolitaine, RSA, célibataire, 65km trajet travail-domicile 5 jours
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 60, 10));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideMobilite(openFiscaRoot, dateDebutSimulation);

        // Alors le demandeur est éligible à l'Aide Mobilité
        assertThat(openFiscaRetourSimulation.getMontantAideMobilite()).isEqualTo(BigDecimal.valueOf(0.23f * 60.0f * 10.0f).setScale(0, RoundingMode.HALF_UP).floatValue());
    }

    @Test
    void calculerMontantCelibataire1EnfantMoins10_62km_1jour() throws Exception {

        // Si DE France Métropolitaine, RSA, célibataire, 62km trajet travail-domicile 1 jour
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 62, 2));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideMobilite(openFiscaRoot, dateDebutSimulation);

        // Alors le demandeur est éligible à l'Aide Mobilité
        assertThat(openFiscaRetourSimulation.getMontantAideMobilite()).isEqualTo(BigDecimal.valueOf(0.23f * 62.0f * 2.0f).setScale(0, RoundingMode.HALF_UP).floatValue());
    }

    @Test
    void calculerMontantCelibataire1EnfantMoins10_80km_2jours() throws Exception {

        // Si DE France Métropolitaine, RSA, célibataire, 80km trajet travail-domicile 2 jours
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 4));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideMobilite(openFiscaRoot, dateDebutSimulation);

        // Alors le demandeur est éligible à l'Aide Mobilité
        assertThat(openFiscaRetourSimulation.getMontantAideMobilite()).isEqualTo(BigDecimal.valueOf(0.23f * 80.0f * 4.0f).setScale(0, RoundingMode.HALF_UP).floatValue());
    }

    @Test
    void calculerMontantCelibataire1EnfantMoins10_100km_3jours() throws Exception {

        // Si DE France Métropolitaine, RSA, célibataire, 100km trajet travail-domicile 3 jours
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 100, 6));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideMobilite(openFiscaRoot, dateDebutSimulation);

        // Alors le demandeur est éligible à l'Aide Mobilité
        assertThat(openFiscaRetourSimulation.getMontantAideMobilite()).isEqualTo(BigDecimal.valueOf(0.23f * 100.0f * 6.0f).setScale(0, RoundingMode.HALF_UP).floatValue());
    }

    @Test
    void calculerMontantCelibataire1EnfantMoins10_60km_4jours() throws Exception {

        // Si DE France Métropolitaine, RSA, célibataire, 60km trajet travail-domicile 4 jours
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 60, 8));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideMobilite(openFiscaRoot, dateDebutSimulation);

        // Alors le demandeur est éligible à l'Aide Mobilité
        assertThat(openFiscaRetourSimulation.getMontantAideMobilite()).isEqualTo(BigDecimal.valueOf(0.23f * 60.0f * 8.0f).setScale(0, RoundingMode.HALF_UP).floatValue());
    }

    @Test
    void calculerMontantCelibataire1EnfantMoins10_2265km_5jours() throws Exception {

        // Si DE France Métropolitaine, RSA, célibataire, 2265km trajet travail-domicile 5 jours
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 2265, 10));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideMobilite(openFiscaRoot, dateDebutSimulation);

        // Alors le demandeur est éligible à l'Aide Mobilité
        assertThat(openFiscaRetourSimulation.getMontantAideMobilite()).isEqualTo(5200.0f);
    }

    @Test
    void calculerEligibleMontantCelibataire1EnfantMoins10_DOM_30km_5jours() throws Exception {


        // Si DE DOM, RSA, célibataire, 30km trajet travail-domicile 5 jours
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "97611"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("97600");
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 30, 10));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideMobilite(openFiscaRoot, dateDebutSimulation);

        // Alors le demandeur est éligible à l'Aide Mobilité
        assertThat(openFiscaRetourSimulation.getMontantAideMobilite()).isEqualTo(BigDecimal.valueOf(0.23f * 30.0f * 10.0f).setScale(0, RoundingMode.HALF_UP).floatValue());
    }
}
