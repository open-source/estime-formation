package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurMicroEntrepriseTest {

    private final LocalDate dateDeSimulation = LocalDate.now();

    @Test
    void doitRetournerUnMontantMontantBeneficesMicroEntrepriseAZero() {
        assertThat(OpenFiscaMappeurMicroEntreprise.getMontantBeneficesMicroEntreprise(new OpenFiscaRoot(), LocalDate.now())).isEqualTo(0f);
    }

    @Test
    void doitRetournerLeMontantBeneficesMicroEntreprise() {
        OpenFiscaRoot openFiscaRoot = new OpenFiscaRoot();
        OpenFiscaIndividu openFiscaIndividu = new OpenFiscaIndividu();
        openFiscaIndividu.setBeneficesMicroEntreprise(new OpenFiscaPeriodes());
        openFiscaIndividu.getBeneficesMicroEntreprise().put(OpenFiscaMappeurPeriode.getPeriodeFormatee(dateDeSimulation), Double.valueOf(2200));
        openFiscaRoot.setIndividus(Map.of(ParametresOpenFisca.DEMANDEUR, openFiscaIndividu));
        assertThat(OpenFiscaMappeurMicroEntreprise.getMontantBeneficesMicroEntreprise(openFiscaRoot, dateDeSimulation)).isEqualTo(2200f);
    }

}
