package fr.pe.pfin.service.da184.testsintegration.simulation.openfisca.montants.primeactivite;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.OpenFiscaRetourSimulationHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class MontantPrimeActiviteCelibataireTest extends Utile {

    private static final int NUMERA_MOIS_SIMULE_PPA = 6;
    private final LocalDate dateDebutSimulation = DateUtile.getDateJour();
    private final OpenFiscaClient openFiscaClient;

    @Autowired
    public MontantPrimeActiviteCelibataireTest(OpenFiscaClient openFiscaClient) {
        this.openFiscaClient = openFiscaClient;
    }

    @Test
    void calculerPrimeActiviteCelibataireMoins26Test() {

        // Si DE France Métropolitaine, célibataire, 0 enfant,
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1998, 7, 5));
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelNet(552);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(333f);
    }

    @Test
    void calculerPrimeActiviteCelibatairePlus26Test() {

        // Si DE France Métropolitaine, célibataire, 0 enfant,
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(270f);
    }

    @Test
    void calculerPrimeActiviteCelibataire1EnfantASFTest() {

        // Si DE France Métropolitaine, célibataire, 1 enfant à charge de 6ans, 
        // asf 110€,
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationSoutienFamilial(110f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(480f);
    }

    @Test
    void calculerPrimeActiviteCelibataire1EnfantASFAPLTest() {

        // Si DE France Métropolitaine, célibataire, 1 enfant à charge de 6ans,
        // asf 110€, apl 300€,
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationSoutienFamilial(110f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(300f));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(480f);
    }

    @Test
    void calculerPrimeActiviteCelibataire2EnfantsTest() {

        // Si DE France Métropolitaine, célibataire, 2 enfants à charge de 6ans et 8ans,
        // asf 233€, af 133€,
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationsFamiliales(133f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationSoutienFamilial(233f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(480f);
    }

    @Test
    void calculerPrimeActiviteCelibataire2EnfantsAPLTest() {

        // Si DE France Métropolitaine, célibataire, 2 enfants à charge de 6ans et 8ans,
        // asf 233€, af 133€, apl 380€,
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationsFamiliales(133f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationSoutienFamilial(233f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(480f);
    }

    @Test
    void calculerPrimeActiviteCelibataire3EnfantsTest() {

        // Si DE France Métropolitaine, célibataire, 3 enfants à charge de 4ans, 6ans et 8ans, 
        // asf 350€, af 303€, cf 259€
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationsFamiliales(133f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationSoutienFamilial(233f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setComplementFamilial(259f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isEqualTo(480f);
    }

    @Test
    void calculerPrimeActiviteCelibataire3EnfantsAPLTest() {

        // Si DE France Métropolitaine, célibataire, 3 enfants à charge de 4ans, 6ans et 8ans, 
        // asf 350€, af 303€, cf 259€, apl 479€,
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationsFamiliales(303f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationSoutienFamilial(350f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setComplementFamilial(259f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(479f));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isBetween(300f, 350f);
    }

    @Test
    void calculerPrimeActiviteCelibataire4EnfantsTest() {

        // Si DE France Métropolitaine, célibataire, 4 enfants à charge de 4ans, 6ans, 8ans et 12 ans, 
        // asf 466€, af = 472€, cf 259€
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        personnesACharge.add(createEnfant(12));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationsFamiliales(472f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationSoutienFamilial(466f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setComplementFamilial(259f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isBetween(267f, 367f);
    }

    @Test
    void calculerPrimeActiviteCelibataire4EnfantsAPLTest() {

        // Si DE France Métropolitaine, célibataire, 4 enfants à charge de 4ans, 6ans, 8ans et 12 ans,
        // asf 466€, af 472€, cf 259€, apl 520€,
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(4));
        personnesACharge.add(createEnfant(6));
        personnesACharge.add(createEnfant(8));
        personnesACharge.add(createEnfant(12));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationsFamiliales(472f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setAllocationSoutienFamilial(466f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().getAidesFamiliales()
                .setComplementFamilial(259f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setAidesLogement(creerAllocationLogement(520f));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerPrimeActivite(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_PPA);

        assertThat(openFiscaRetourSimulation.getMontantPrimeActivite()).isBetween(268f, 350f);
    }
}
