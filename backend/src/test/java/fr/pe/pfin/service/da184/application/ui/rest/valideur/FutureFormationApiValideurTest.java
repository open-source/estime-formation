package fr.pe.pfin.service.da184.application.ui.rest.valideur;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.BadRequestMessages;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.FutureFormation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class FutureFormationApiValideurTest extends Utile {

    @Test
    void controlerDonneeesEntreeFutureFormationAbsenteTest() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(false, personnesACharge);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));
        demandeur.setFutureFormation(null);
        FutureFormation futureFormation = demandeur.getFutureFormation();

        assertThatThrownBy(() -> FutureFormationApiValideur.controlerDonnees(futureFormation))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(String.format(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "futureFormation"));
    }

    @Test
    void controlerDonneeesEntreeFutureFormationSalaireNet0Test() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(false, personnesACharge);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));

        demandeur.setFutureFormation(initFormation(35f, 6f, 0, 35, 10));
        FutureFormation futureFormation = demandeur.getFutureFormation();

        assertThatThrownBy(() -> FutureFormationApiValideur.controlerDonnees(futureFormation))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.SALAIRE_MENSUEL_NET_ZERO.getMessage());
    }
}
