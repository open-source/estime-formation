package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.periodes;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.OpenFiscaMappeurPeriode;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurPeriodesAideLogementTest extends Utile {
    private final LocalDate dateDebutSimulation = LocalDate.of(2023, 1, 1);

    private static Stream<Arguments> listeFichierEtNombreDeTrimestre() {
        return Stream.of(
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesALTests/periode-al-prochaine-declaration-tri-mois-0.json", 0),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesALTests/periode-al-prochaine-declaration-tri-mois-1.json", 1),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesALTests/periode-al-prochaine-declaration-tri-mois-2.json", 2),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesALTests/periode-al-prochaine-declaration-tri-mois-3.json", 3)
        );
    }

    @ParameterizedTest
    @MethodSource("listeFichierEtNombreDeTrimestre")
    void creerPeriodesAL(String emplacementFichier, int nbTrimestre) throws Exception {
        String openFiscaPayloadExpected = getStringFromJsonFile(emplacementFichier);
        Demandeur demandeur = creerDemandeurALTests(nbTrimestre);
        OpenFiscaPeriodes periodesAL = OpenFiscaMappeurPeriode.creerPeriodesAideLogement(demandeur, dateDebutSimulation);
        assertThat(periodesAL.toString()).hasToString(openFiscaPayloadExpected);
    }
}
