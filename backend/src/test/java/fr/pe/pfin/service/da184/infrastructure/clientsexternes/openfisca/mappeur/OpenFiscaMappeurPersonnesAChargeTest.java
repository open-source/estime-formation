package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurPersonnesAChargeTest extends Utile {
    private final LocalDate dateDebutSimulation = LocalDate.of(2023, 1, 1);

    @Test
    void mapDemandeurPersonneAChargeAAHToOpenFiscaPayloadTest() throws URISyntaxException, IOException, ParseException {

        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/personnesACharge/demandeur-avec-enfant-aah.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Personne personneACharge = new Personne();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setDateNaissance(LocalDate.of(2013, 5, 22));
        personneACharge.setInformationsPersonnelles(informationsPersonnelles);
        BeneficiaireAides beneficiaireAides = new BeneficiaireAides();
        beneficiaireAides.setBeneficiaireAAH(true);
        personneACharge.setBeneficiaireAides(beneficiaireAides);
        RessourcesFinancieresAvantSimulation ressourcesFinancieresPersonneACharge = new RessourcesFinancieresAvantSimulation();
        AidesCAF aidesCAFPersonneACharge = new AidesCAF();
        aidesCAFPersonneACharge.setAllocationAAH(900f);
        ressourcesFinancieresPersonneACharge.setAidesCAF(aidesCAFPersonneACharge);
        personneACharge.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresPersonneACharge);
        personnesACharge.add(personneACharge);
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurPersonneAChargeAREToOpenFiscaPayloadTest() throws URISyntaxException, IOException, ParseException {

        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/personnesACharge/demandeur-avec-enfant-are.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Personne personneACharge = new Personne();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setDateNaissance(LocalDate.of(2013, 5, 22));
        personneACharge.setInformationsPersonnelles(informationsPersonnelles);
        BeneficiaireAides beneficiaireAides = new BeneficiaireAides();
        beneficiaireAides.setBeneficiaireARE(true);
        personneACharge.setBeneficiaireAides(beneficiaireAides);
        RessourcesFinancieresAvantSimulation ressourcesFinancieresPersonneACharge = new RessourcesFinancieresAvantSimulation();
        AidesPoleEmploi aidesPoleEmploiPersonneACharge = new AidesPoleEmploi();
        AllocationARE allocationARE = new AllocationARE();
        allocationARE.setAllocationMensuelleNet(900f);
        aidesPoleEmploiPersonneACharge.setAllocationARE(allocationARE);
        ressourcesFinancieresPersonneACharge.setAidesPoleEmploi(aidesPoleEmploiPersonneACharge);
        personneACharge.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresPersonneACharge);
        personnesACharge.add(personneACharge);
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurPersonneAChargeASSToOpenFiscaPayloadTest() throws URISyntaxException, IOException, ParseException {

        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/personnesACharge/demandeur-avec-enfant-ass.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Personne personneACharge = new Personne();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setDateNaissance(LocalDate.of(2013, 5, 22));
        personneACharge.setInformationsPersonnelles(informationsPersonnelles);
        BeneficiaireAides beneficiaireAides = new BeneficiaireAides();
        beneficiaireAides.setBeneficiaireASS(true);
        personneACharge.setBeneficiaireAides(beneficiaireAides);
        RessourcesFinancieresAvantSimulation ressourcesFinancieresPersonneACharge = new RessourcesFinancieresAvantSimulation();
        AidesPoleEmploi aidesPoleEmploiPersonneACharge = new AidesPoleEmploi();
        AllocationASS allocationASS = new AllocationASS();
        allocationASS.setAllocationMensuelleNet(900f);
        aidesPoleEmploiPersonneACharge.setAllocationASS(allocationASS);
        ressourcesFinancieresPersonneACharge.setAidesPoleEmploi(aidesPoleEmploiPersonneACharge);
        personneACharge.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresPersonneACharge);
        personnesACharge.add(personneACharge);
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurPersonneAChargePensionInvaliditeToOpenFiscaPayloadTest() throws URISyntaxException, IOException, ParseException {
        // Si DE France Métropolitaine, en couple, conjoint 200 pension invalidite,
        // 1 enfant de 1an
        // futur contrat CDI avec salaire net 800€/mois
        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/personnesACharge/demandeur-avec-enfant-pension-invalidite.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Personne personneACharge = new Personne();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setDateNaissance(LocalDate.of(2013, 5, 22));
        personneACharge.setInformationsPersonnelles(informationsPersonnelles);
        RessourcesFinancieresAvantSimulation ressourcesFinancieresPersonneACharge = new RessourcesFinancieresAvantSimulation();
        AidesCPAM aidesCPAM = new AidesCPAM();
        aidesCPAM.setPensionInvalidite(200f);
        ressourcesFinancieresPersonneACharge.setAidesCPAM(aidesCPAM);
        personneACharge.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresPersonneACharge);
        personnesACharge.add(personneACharge);
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurPersonneAChargeSalaireToOpenFiscaPayloadTest() throws URISyntaxException, IOException, ParseException {

        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/personnesACharge/demandeur-avec-enfant-salaire.json");

        List<Personne> personnesACharge = new ArrayList<>();
        Personne personneACharge = new Personne();
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setDateNaissance(LocalDate.of(2013, 5, 22));
        personneACharge.setInformationsPersonnelles(informationsPersonnelles);
        BeneficiaireAides beneficiaireAides = new BeneficiaireAides();
        personneACharge.setBeneficiaireAides(beneficiaireAides);
        RessourcesFinancieresAvantSimulation ressourcesFinancieresPersonneACharge = new RessourcesFinancieresAvantSimulation();
        Salaire salairepersonneACharge = new Salaire();
        salairepersonneACharge.setMontantMensuelNet(900);
        salairepersonneACharge.setMontantMensuelBrut(1165);
        ressourcesFinancieresPersonneACharge.setSalaire(salairepersonneACharge);
        ressourcesFinancieresPersonneACharge.setHasTravailleAuCoursDerniersMois(true);
        ressourcesFinancieresPersonneACharge
                .setPeriodeTravailleeAvantSimulation(creerPeriodeTravailleeAvantSimulation(900, 1165, 13));
        personneACharge.setRessourcesFinancieresAvantSimulation(ressourcesFinancieresPersonneACharge);
        personnesACharge.add(personneACharge);
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);
        LocalDate dateDerniereOuvertureDroitASS = DateUtile.enleverMoisALocalDate(LocalDate.of(2023, 1, 1), 6);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setDateDerniereOuvertureDroit(dateDerniereOuvertureDroitASS);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void mapDemandeurPersonnesAChargeToOpenFiscaPayloadTest() throws Exception {

        String openFiscaPayloadExpected = getStringFromJsonFile(
                "clientsexternes.openfisca.mappeur/OpenFiscaMappeurIndividuTests/personnesACharge/demandeur-avec-enfants-et-af.json");

        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createPersonne(LocalDate.of(2013, 5, 22)));
        personnesACharge.add(createPersonne(LocalDate.of(2016, 7, 5)));
        personnesACharge.add(createPersonne(LocalDate.of(2014, 7, 5)));
        personnesACharge.add(createPersonne(LocalDate.of(2012, 7, 5)));
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 80, 12));

        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS()
                .setAllocationJournaliereNet(16.89f);

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = new RessourcesFinancieresAvantSimulation();
        AidesCAF aidesCAF = createAidesCAF();
        AidesFamiliales aidesFamiliales = new AidesFamiliales();
        aidesFamiliales.setAllocationsFamiliales(899);
        aidesFamiliales.setAllocationSoutienFamilial(799);
        aidesFamiliales.setComplementFamilial(699);
        aidesFamiliales.setPensionsAlimentairesFoyer(150);
        aidesCAF.setAidesFamiliales(aidesFamiliales);
        ressourcesFinancieres.setAidesCAF(aidesCAF);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        OpenFiscaRoot openFiscaPayload = OpenFiscaMappeur.mapDemandeurToOpenFiscaPayload(demandeur,
                dateDebutSimulation);

        assertThat(openFiscaPayload.toString()).hasToString(openFiscaPayloadExpected);
    }

}
