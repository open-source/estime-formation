package fr.pe.pfin.service.da184.application.ui.rest.valideur;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.BadRequestMessages;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.InformationsPersonnelles;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class InformationsPersonnellesApiValideurTest extends Utile {

    @Test
    void controlerDonneeesEntreeInformationsPersonnellesAbsenteTest() {
        assertThatThrownBy(() -> InformationsPersonnellesApiValideur.controlerDonnees(null))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "informationsPersonnelles");
    }

    @Test
    void controlerDonneeesEntreeInformationsPersonnellesDateNaissanceAbsenteTest() {
        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setLogement(initLogement(false, "44109"));
        informationsPersonnelles.setDateNaissance(null);

        assertThatThrownBy(() -> InformationsPersonnellesApiValideur.controlerDonnees(informationsPersonnelles))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "dateNaissance de informationsPersonnelles");
    }

    @Test
    void controlerDonneeesEntreeInformationsPersonnellesLogementAbsentTest() {

        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setLogement(null);

        assertThatThrownBy(() -> InformationsPersonnellesApiValideur.controlerDonnees(informationsPersonnelles))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "logement de informationsPersonnelles");
    }

    @Test
    void controlerDonneeesEntreeInformationsPersonnellesCoordonneeAbsenteTest() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(false, personnesACharge);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(1);
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);

        InformationsPersonnelles informationsPersonnelles = demandeur.getInformationsPersonnelles();
        informationsPersonnelles.getLogement().setCoordonnees(null);


        assertThatThrownBy(() -> InformationsPersonnellesApiValideur.controlerDonnees(informationsPersonnelles))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "coordonnees de logement");
    }

    @Test
    void controlerDonneeesEntreeInformationsPersonnellesCodePosteAbsentTest() {

        InformationsPersonnelles informationsPersonnelles = new InformationsPersonnelles();
        informationsPersonnelles.setLogement(initLogement(false, "44109"));
        informationsPersonnelles.getLogement().getCoordonnees().setCodePostal("");

        assertThatThrownBy(() -> InformationsPersonnellesApiValideur.controlerDonnees(informationsPersonnelles))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "codePostal de coordonnees");
    }

    @Test
    void controlerDonneeesEntreeInformationsPersonnellesNationaliteAbsenteTest() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(false, personnesACharge);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));
        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(1);
        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(false);

        InformationsPersonnelles informationsPersonnelles = demandeur.getInformationsPersonnelles();
        informationsPersonnelles.setNationalite(null);

        assertThatThrownBy(() -> InformationsPersonnellesApiValideur.controlerDonnees(informationsPersonnelles))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(),
                        "nationalite de informationsPersonnelles");
    }
}
