package fr.pe.pfin.service.da184.testsintegration.simulation.openfisca.eligibilite;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.OpenFiscaRetourSimulationHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class EligibiliteAideLogementTest extends Utile {
    private static final int NUMERA_MOIS_SIMULE_AL = 2;
    private static final LocalDate dateDebutSimulation = DateUtile.getDateJour();
    private final OpenFiscaClient openFiscaClient;

    @Autowired
    public EligibiliteAideLogementTest(OpenFiscaClient openFiscaClient) {
        this.openFiscaClient = openFiscaClient;
    }

    @SuppressWarnings("java:S5976")
    @Test
    @DisplayName("doit vérifier l'éligibilité à l'AL d'un demandeur célibataire ayant un loyer inférieur à 308€")
    void calculerEligibleAideLogementCelibataireTest() {

        // Si demandeur France Métropolitaine, célibataire, locataire
        // logement non conventionné
        // loyer inférieur à 308€
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "75056"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("75001");
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(300f);

        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 12, 20));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideLogement(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_AL);

        // Alors le demandeur est éligible à l'AL
        assertEquals(AideEnum.AIDES_LOGEMENT.getCode(), openFiscaRetourSimulation.getTypeAideLogement(), "Type d'aide doit être AL");
    }

    @Test
    @DisplayName("doit vérifier l'éligibilité à l'AL d'un demandeur en couple ayant un loyer inférieur à 372€")
    void calculerEligibleAideLogementCoupleTest() {

        // Si demandeur France Métropolitaine, couple, locataire
        // logement non conventionné
        // loyer inférieur à 372€
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "75056"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("75001");
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(350f);

        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 12, 20));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideLogement(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_AL);

        // Alors le demandeur est éligible à l'AL
        assertEquals(AideEnum.AIDES_LOGEMENT.getCode(), openFiscaRetourSimulation.getTypeAideLogement(), "Type d'aide doit être AL");
    }

    @SuppressWarnings("java:S5976")
    @Test
    @DisplayName("doit vérifier l'éligibilité à l'AL d'un demandeur en couple avec un enfant ayant un loyer inférieur à 420€")
    void calculerEligibleAideLogementCoupleAvecEnfantConventionneTest() {

        // Si demandeur France Métropolitaine, couple, locataire, 1 enfant
        // logement conventionné
        // loyer inférieur à 420€
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(12));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(true, "75056"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("75001");
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(400f);

        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 12, 20));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideLogement(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_AL);

        // Alors le demandeur est éligible à l'APL
        assertEquals(AideEnum.AIDES_LOGEMENT.getCode(), openFiscaRetourSimulation.getTypeAideLogement(), "Type d'aide doit être AL");
    }

    @Test
    @DisplayName("doit vérifier la non éligibilité à l'AL d'un demandeur célibataire ayant un loyer supérieur à 308€")
    void calculerNonEligibleAideLogementCelibataireTest() {

        // Si demandeur France Métropolitaine, célibataire, locataire
        // logement non conventionné
        // loyer supérieur à 308€
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "75056"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("75001");
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(1400f);

        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 12, 20));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideLogement(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_AL);

        // Alors le demandeur n'est pas éligible à l'AL
        assertEquals(0, openFiscaRetourSimulation.getMontantAideLogement(), "Aide au logement doit être égale à zéro");
    }

    @Test
    @DisplayName("doit vérifier la non éligibilité à l'AL d'un demandeur célibataire ayant un loyer supérieur à 308€ et un salaire supérieur à 1043€/mois")
    void calculerNonEligibleAideLogementCelibataireAvecRessourceTest() {

        // Si demandeur France Métropolitaine, célibataire, locataire
        // logement non conventionné
        // loyer supérieur à 308€
        // Salaire supérieur à 1043€/mois sur les 14 derniers mois
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "75056"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("75001");
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(1400f);

        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 12, 20));

        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(true);
        demandeur.getRessourcesFinancieresAvantSimulation().setSalaire(creerSalaire(1500, 1923));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideLogement(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_AL);

        // Alors le demandeur n'est pas éligible à l'AL
        assertEquals(0, openFiscaRetourSimulation.getMontantAideLogement(), "Aide au logement doit être égale à zéro");

    }

    @Test
    @DisplayName("doit vérifier l'éligibilité à l'AL d'un demandeur ayant un loyer inférieur à 420€ célibataire ayant un enfant")
    void calculerEligibleAideLogementCelibataireAvecEnfantTest() {

        // Si demandeur France Métropolitaine, célibataire, locataire, 1 enfant
        // logement non conventionné
        // loyer inférieur à 420€
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(12));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "75056"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("75001");
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(400f);

        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 12, 20));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideLogement(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_AL);

        // Alors le demandeur est éligible à l'AL
        assertEquals(AideEnum.AIDES_LOGEMENT.getCode(), openFiscaRetourSimulation.getTypeAideLogement(), "Type d'aide doit être AL");
    }

    @Test
    @DisplayName("doit vérifier l'éligibilité à l'AL d'un demandeur ayant un loyer inférieur à 420€ en couple ayant un enfant")
    void calculerEligibleAideLogementCoupleAvecEnfantTest() {

        // Si demandeur France Métropolitaine, couple, locataire, 1 enfant
        // logement non conventionné
        // loyer inférieur à 420€
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(15));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "75056"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("75001");
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(400f);

        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 12, 20));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideLogement(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_AL);

        // Alors le demandeur est éligible à l'AL
        assertEquals(AideEnum.AIDES_LOGEMENT.getCode(), openFiscaRetourSimulation.getTypeAideLogement(), "Type d'aide doit être AL");
    }

    @Test
    @DisplayName("doit vérifier la non éligibilité à l'AL d'un demandeur ayant un loyer supérieur à 420€ célibataire ayant un enfant")
    void calculerNonEligibleAideLogementCelibataireAvecEnfantTest() {

        // Si demandeur France Métropolitaine, célibataire, locataire, 1 enfant
        // logement non conventionné
        // loyer supérieur à 420€
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(15));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "75056"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("75001");
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(1800f);

        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 12, 20));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideLogement(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_AL);

        // Alors le demandeur n'est pas éligible à l'AL
        assertEquals(0, openFiscaRetourSimulation.getMontantAideLogement(), "Aide au logement doit être égale à zéro");
    }

    @Test
    @DisplayName("doit vérifier la non éligibilité à l'AL d'un demandeur ayant un loyer supérieur à 420€ célibataire ayant un enfant et salaire supérieur à 1600€/mois")
    void calculerNonEligibleAideLogementCelibataireAvecEnfantEtRessourceTest() {

        // Si demandeur France Métropolitaine, célibataire, locataire, 1 enfant
        // logement non conventionné
        // loyer supérieur à 420€
        // Salaire supérieur à 1600€/mois sur les 14 derniers mois
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(15));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "75056"));
        demandeur.getInformationsPersonnelles().getLogement().getCoordonnees().setCodePostal("75001");
        demandeur.getInformationsPersonnelles().getLogement().setMontantLoyer(1800f);

        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 12, 20));

        demandeur.getRessourcesFinancieresAvantSimulation().setHasTravailleAuCoursDerniersMois(true);
        demandeur.getRessourcesFinancieresAvantSimulation().setSalaire(creerSalaire(1700, 2179));

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAideLogement(openFiscaRoot,
                dateDebutSimulation, NUMERA_MOIS_SIMULE_AL);

        // Alors le demandeur n'est pas éligible à l'AL
        assertEquals(0, openFiscaRetourSimulation.getMontantAideLogement(), "Aide au logement doit être égale à zéro");
    }
}
