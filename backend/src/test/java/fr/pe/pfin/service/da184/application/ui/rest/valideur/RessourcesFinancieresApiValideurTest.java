package fr.pe.pfin.service.da184.application.ui.rest.valideur;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.BadRequestException;
import fr.pe.pfin.service.da184.application.ui.rest.exceptions.messages.BadRequestMessages;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.NationaliteEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypePopulationEnum;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class RessourcesFinancieresApiValideurTest extends Utile {

    @Test
    void controlerDonneeesEntreeRessourcesFinancieresDemandeurASSTest() {

        assertThatThrownBy(() -> RessourcesFinancieresApiValideur.controlerDemandeurAllocationsPoleEmploiASS(null))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "ressourcesFinancieres dans Demandeur");
    }

    @Test
    void controlerDonneeesEntreeRessourcesFinancieresDemandeurASSAidesPEAbsenteTest() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(1);

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = new RessourcesFinancieresAvantSimulation();
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        assertThatThrownBy(() -> RessourcesFinancieresApiValideur.controlerDemandeurAllocationsPoleEmploiASS(ressourcesFinancieres))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "aidesPoleEmploi dans RessourcesFinancieres de Demandeur");
    }

    @Test
    void controlerDonneeesEntreeRessourcesFinancieresDemandeurASSAJAbsenteTest() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(1);

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = new RessourcesFinancieresAvantSimulation();
        AidesPoleEmploi aidesPoleEmploi = new AidesPoleEmploi();
        AllocationASS allocationASS = new AllocationASS();
        allocationASS.setAllocationJournaliereNet(0f);
        aidesPoleEmploi.setAllocationASS(allocationASS);
        ressourcesFinancieres.setAidesPoleEmploi(aidesPoleEmploi);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        assertThatThrownBy(() -> RessourcesFinancieresApiValideur.controlerDemandeurAllocationsPoleEmploiASS(ressourcesFinancieres))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.MONTANT_INCORRECT_INFERIEUR_EGAL_ZERO.getMessage(), "allocationJournaliereNetASS");
    }

    @Test
    void controlerDonneeesEntreeRessourcesFinancieresDemandeurASSHasTravailleAuCoursDerniersMoisAbsenteTest() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurASS(false, personnesACharge);
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(1);

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = new RessourcesFinancieresAvantSimulation();
        AidesPoleEmploi aidesPoleEmploi = new AidesPoleEmploi();
        AllocationASS allocationASS = new AllocationASS();
        allocationASS.setAllocationJournaliereNet(16.89f);
        aidesPoleEmploi.setAllocationASS(allocationASS);
        ressourcesFinancieres.setAidesPoleEmploi(aidesPoleEmploi);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        assertThatThrownBy(() -> RessourcesFinancieresApiValideur.controlerDemandeurAllocationsPoleEmploiASS(ressourcesFinancieres))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "hasTravailleAuCoursDerniersMois dans RessourcesFinancieres");
    }

    @Test
    void controlerDonneeesEntreeRessourcesFinancieresDemandeurAAHTest() {

        assertThatThrownBy(() -> RessourcesFinancieresApiValideur.controlerDemandeurAllocationsCafAAH(null))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "ressourcesFinancieres dans Demandeur");
    }

    @Test
    void controlerDonneeesEntreeRessourcesFinancieresDemandeurAAHAllocationsCAFAbsenteTest() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.AAH, false, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(1);

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = new RessourcesFinancieresAvantSimulation();
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        assertThatThrownBy(() -> RessourcesFinancieresApiValideur.controlerDemandeurAllocationsCafAAH(ressourcesFinancieres))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "allocationsCAF dans RessourcesFinancieres de Demandeur");
    }

    @Test
    void controlerDonneeesEntreeRessourcesFinancieresDemandeurAAHAllocationAbsenteTest() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.AAH, false, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(1);

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = new RessourcesFinancieresAvantSimulation();
        AidesCAF aidesCAF = new AidesCAF();
        aidesCAF.setAllocationAAH(0f);
        ressourcesFinancieres.setAidesCAF(aidesCAF);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        assertThatThrownBy(() -> RessourcesFinancieresApiValideur.controlerDemandeurAllocationsCafAAH(ressourcesFinancieres))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.MONTANT_INCORRECT_INFERIEUR_EGAL_ZERO.getMessage(), "allocationMensuelleNetAAH");
    }

    @Test
    void controlerDonneeesEntreeRessourcesFinancieresDemandeurAAHPeriodeTravailleeAvantSimulationAbsenteTest() {

        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.AAH, false, personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.setFutureFormation(initFormation(35f, 6f, 757, 25, 10));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(1);

        RessourcesFinancieresAvantSimulation ressourcesFinancieres = new RessourcesFinancieresAvantSimulation();
        AidesCAF aidesCAF = new AidesCAF();
        aidesCAF.setAllocationAAH(900f);
        ressourcesFinancieres.setAidesCAF(aidesCAF);
        ressourcesFinancieres.setHasTravailleAuCoursDerniersMois(true);
        ressourcesFinancieres.setNombreMoisTravaillesDerniersMois(null);
        demandeur.setRessourcesFinancieresAvantSimulation(ressourcesFinancieres);

        assertThatThrownBy(() -> RessourcesFinancieresApiValideur.controlerDemandeurAllocationsCafAAH(ressourcesFinancieres))
                .isInstanceOf(BadRequestException.class)
                .hasMessage(BadRequestMessages.CHAMP_OBLIGATOIRE.getMessage(), "periodeTravailleeAvantSimulation dans RessourcesFinancieres de Demandeur");
    }
}
