package fr.pe.pfin.service.da184.infrastructure;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaIndividu;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import fr.pe.pfin.service.da184.infrastructure.database.redis.repositories.BRSADataRepository;
import fr.pe.pfin.service.da184.infrastructure.database.redis.utile.RedisUtile;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SimulateurAidesAdapteurTest extends Utile {
    final OpenFiscaRoot openFiscaRoot = new OpenFiscaRoot();
    @Mock
    OpenFiscaClient openFiscaClient;
    @InjectMocks
    SimulateurAidesAdapteur simulateurAidesAdapteur;
    Demandeur demandeur;
    @Mock
    private BRSADataRepository brsaDataRepository;
    @Mock
    private RedisUtile redisUtile;

    public SimulateurAidesAdapteurTest() {
        demandeur = createDemandeurRSA(false, List.of(createPersonne(LocalDate.of(1998, 1, 1))));
        demandeur.getInformationsPersonnelles().setMicroEntrepreneur(true);
        demandeur.getInformationsPersonnelles().setMicroEntreprise(new MicroEntreprise());
        demandeur.getInformationsPersonnelles().getMicroEntreprise().setDateRepriseCreationEntreprise(LocalDate.now().minusYears(1));
        demandeur.getInformationsPersonnelles().getMicroEntreprise().setChiffreAffairesN(27000f);
        demandeur.getInformationsPersonnelles().getMicroEntreprise().setChiffreAffairesNMoins1(15000f);
        demandeur.getInformationsPersonnelles().getMicroEntreprise().setChiffreAffairesNMoins2(15000f);
        demandeur.getInformationsPersonnelles().setHasRevenusImmobilier(true);
        demandeur.getFutureFormation().setDureeFormationEnMois(6);
        demandeur.getBeneficiaireAides().setBeneficiaireASS(true);
        demandeur.getRessourcesFinancieresAvantSimulation().setRevenusImmobilier3DerniersMois(1200f);
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesCPAM(new AidesCPAM());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCPAM().setPensionInvalidite(300f);
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesPoleEmploi(new AidesPoleEmploi());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().setAllocationASS(new AllocationASS());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(12f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setDateDerniereOuvertureDroit(LocalDate.now().minusMonths(6));
        OpenFiscaPeriodes openFiscaPeriodes = new OpenFiscaPeriodes();
        openFiscaPeriodes.put("01/2023", "2000-01-01");
        OpenFiscaIndividu openFiscaIndividu = new OpenFiscaIndividu();
        openFiscaIndividu.setDateNaissance(openFiscaPeriodes);
        openFiscaRoot.setIndividus(Map.of("individu", openFiscaIndividu));
        openFiscaRoot.setFamilles(Map.of());
        openFiscaRoot.setMenages(Map.of());
    }

    @Test
    @DisplayName("doit retourner un objet simulation pour un objet demandeur entrant")
    void doitRetournerUnObjetSimulationPourUnDemandeur() {
        List<String> allocations = new ArrayList<>();
        allocations.add("RSA");
        allocations.add("ASS");
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().setAllocationASS(new AllocationASS());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(0f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().setAllocationARE(new AllocationARE());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationARE().setAllocationJournaliereBrute(0f);
        when(openFiscaClient.callApiCalculate(eq(demandeur), any())).thenReturn(openFiscaRoot);
        Simulation simuResultat = simulateurAidesAdapteur.simuler(demandeur);
        assertThat(simuResultat).isNotNull();
    }

}
