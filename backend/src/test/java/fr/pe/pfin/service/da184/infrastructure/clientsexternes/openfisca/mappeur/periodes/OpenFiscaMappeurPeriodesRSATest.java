package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.periodes;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.OpenFiscaMappeurPeriode;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.OpenFiscaMappeurRSA;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurPeriodesRSATest extends Utile {

    private final LocalDate dateDebutSimulation = LocalDate.of(2023, 1, 1);

    private static Stream<Arguments> listeFichierEtNombreDeTrimestre() {
        return Stream.of(
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesRSATests/periode-rsa-prochaine-declaration-tri-mois-0.json", 0),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesRSATests/periode-rsa-prochaine-declaration-tri-mois-1.json", 1),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesRSATests/periode-rsa-prochaine-declaration-tri-mois-2.json", 2),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesRSATests/periode-rsa-prochaine-declaration-tri-mois-3.json", 3)
        );
    }

    @ParameterizedTest
    @MethodSource("listeFichierEtNombreDeTrimestre")
    void creerPeriodeRSA(String emplacementFichier, int nbTrimestre) throws Exception {
        String openFiscaPayloadExpected = getStringFromJsonFile(emplacementFichier);
        Demandeur demandeur = creerDemandeurRSATests(nbTrimestre);
        OpenFiscaPeriodes periodeAideeRSA = OpenFiscaMappeurPeriode.creerPeriodesOpenFiscaRSA(demandeur, dateDebutSimulation);
        assertThat(periodeAideeRSA.toString()).hasToString(openFiscaPayloadExpected);
    }

    @Test
    void doitRetournerZeroPourUnMontantRSAAvecDesInformationsVide() {
        assertThat(OpenFiscaMappeurRSA.getMontantRSA(new OpenFiscaRoot(), LocalDate.now(), 0)).isEqualTo(0f);
    }

    /***************** METHODES UTILES POUR TESTS ********/

    private Demandeur creerDemandeurRSATests(int prochaineDeclarationTrimestrielle) throws ParseException {
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelBrut(1200);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelNet(1000);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF()
                .setProchaineDeclarationTrimestrielle(prochaineDeclarationTrimestrielle);
        return demandeur;
    }

}
