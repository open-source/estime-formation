package fr.pe.pfin.service.da184.infrastructure.aides.poleemploi;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.*;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.OrganismeEnum;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class SimulateurAidesPoleEmploiTest extends Utile {

    @Test
    @DisplayName("doit ajouter une aide de type ASS")
    void doitAjouterUneAideDeTypeASS() {
        Demandeur demandeur = createDemandeurRSA(false, Collections.emptyList());
        demandeur.getRessourcesFinancieresAvantSimulation().setAidesPoleEmploi(new AidesPoleEmploi());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().setAllocationASS(new AllocationASS());
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setAllocationJournaliereNet(1200f);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesPoleEmploi().getAllocationASS().setDateDerniereOuvertureDroit(LocalDate.now().minusMonths(1));
        demandeur.setBeneficiaireAides(new BeneficiaireAides());
        demandeur.getBeneficiaireAides().setBeneficiaireASS(true);
        Map<String, Aide> aideRetour = new HashMap<>();
        Aide aideResultat = new Aide();
        aideResultat.setMontant(36000f);
        aideResultat.setReportee(false);
        aideResultat.setOrganisme(OrganismeEnum.FT.getNomCourt());

        SimulateurAidesPoleEmploi.simuler(aideRetour, 1, demandeur, LocalDate.now());

        assertThat(aideRetour).hasSize(1).containsKey(AideEnum.ALLOCATION_SOLIDARITE_SPECIFIQUE.getCode()).containsValue(aideResultat);
    }

}
