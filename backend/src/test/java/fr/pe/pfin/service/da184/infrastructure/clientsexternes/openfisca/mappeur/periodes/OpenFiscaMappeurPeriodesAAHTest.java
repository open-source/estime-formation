package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.periodes;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.PeriodeTravailleeAvantSimulation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.NationaliteEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.TypePopulationEnum;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur.OpenFiscaMappeurPeriode;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaPeriodes;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurPeriodesAAHTest extends Utile {
    private final LocalDate dateDebutSimulation = LocalDate.of(2023, 1, 1);

    private static Stream<Arguments> listeFichierEtNombreDeMois() {
        return Stream.of(
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesAAHTests/periode-aah-non-travaille-6-derniers-mois.json", 0),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesAAHTests/periode-aah-1-mois-travaille-6-derniers-mois.json", 1),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesAAHTests/periode-aah-2-mois-travaille-6-derniers-mois.json", 2),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesAAHTests/periode-aah-3-mois-travaille-6-derniers-mois.json", 3),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesAAHTests/periode-aah-4-mois-travaille-6-derniers-mois.json", 4),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesAAHTests/periode-aah-5-mois-travaille-6-derniers-mois.json", 5),
                Arguments.of("clientsexternes.openfisca.mappeur/OpenFiscaMappeurPeriodesAAHTests/periode-aah-6-mois-travaille-6-derniers-mois.json", 6)
        );
    }

    /********************
     * TESTS SUR creerPeriodesAidee
     ************************************************/

    @ParameterizedTest
    @MethodSource("listeFichierEtNombreDeMois")
    void creerPeriodeAideeAAHTest(String emplacementFichier, int nbMois) throws Exception {
        String openFiscaPayloadExpected = getStringFromJsonFile(emplacementFichier);
        Demandeur demandeur = creerDemandeurAAHTests(nbMois);
        OpenFiscaPeriodes periodeAideeAAH = OpenFiscaMappeurPeriode.creerPeriodesOpenFiscaAAH(demandeur, dateDebutSimulation);
        assertThat(periodeAideeAAH.toString()).hasToString(openFiscaPayloadExpected);
    }

    /***************** METHODES UTILES POUR TESTS ********/

    private Demandeur creerDemandeurAAHTests(int nombreMoisTravaillesAvantSimulation)
            throws ParseException {
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createBaseDemandeur(TypePopulationEnum.AAH, isEnCouple,
                personnesACharge);
        demandeur.getInformationsPersonnelles().setDateNaissance(LocalDate.of(1986, 7, 5));
        demandeur.getInformationsPersonnelles().setNationalite(NationaliteEnum.FRANCAISE.getValeur());
        demandeur.getFutureFormation().getSalaire().setMontantMensuelBrut(1200);
        demandeur.getFutureFormation().getSalaire().setMontantMensuelNet(1000);
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationAAH(900f);
        if (nombreMoisTravaillesAvantSimulation > 0) {
            PeriodeTravailleeAvantSimulation periodeTravailleeAvantSimulation = creerPeriodeTravailleeAvantSimulation(850, 1101, nombreMoisTravaillesAvantSimulation);
            demandeur.getRessourcesFinancieresAvantSimulation()
                    .setPeriodeTravailleeAvantSimulation(periodeTravailleeAvantSimulation);
        }
        return demandeur;
    }

}
