package fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.mappeur;

import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class OpenFiscaMappeurPrimeActiviteTest {
    @Test
    void doitRetournerUnMontantPrimeActiviteAZeroPourOpenFiscaRootVide() {
        assertThat(OpenFiscaMappeurPrimeActivite.getMontantPrimeActivite(new OpenFiscaRoot(), LocalDate.now(), 0)).isEqualTo(0f);
    }
}
