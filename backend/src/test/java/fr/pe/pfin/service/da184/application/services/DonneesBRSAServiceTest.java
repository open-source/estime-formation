package fr.pe.pfin.service.da184.application.services;

import fr.pe.pfin.service.da184.domaine.use_cases.DonneesBRSAPort;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DonneesBRSAServiceTest {
    @Mock
    DonneesBRSAPort donneesBRSAPort;
    @InjectMocks
    DonneesBRSAService donneesBRSAService;

    @Test
    @DisplayName("doit retourner un inputstream csv contenant les BRSADatas")
    void doitRetournerUnInputStream() {
        when(donneesBRSAPort.extraireDonneesBRSA(any(), any())).thenReturn(new ByteArrayInputStream("TEST".getBytes(StandardCharsets.UTF_8)));
        InputStream input = donneesBRSAService.obtenirDonneesBRSA(LocalDate.now().minusDays(1), LocalDate.now().plusDays(1));
        assertThat(input).isNotNull();
    }

    @Test
    @DisplayName("doit retourner une 200 lors de l'ajout d'une notation")
    void doitRetournerOKSiAjoutNotation() {
        when(donneesBRSAPort.addNotation(any(), any())).thenReturn(HttpStatus.OK);
        HttpStatus status = donneesBRSAService.addNotation("123456789", 6);
        assertThat(status).isEqualTo(HttpStatus.OK);
    }
}
