package fr.pe.pfin.service.da184.testsintegration.simulation.openfisca.eligibilite;

import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Personne;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaClient;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.OpenFiscaRetourSimulation;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.helper.OpenFiscaRetourSimulationHelper;
import fr.pe.pfin.service.da184.infrastructure.clientsexternes.openfisca.ressources.OpenFiscaRoot;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class EligibiliteAgepiTest extends Utile {

    private final OpenFiscaClient openFiscaClient;

    @Autowired
    public EligibiliteAgepiTest(OpenFiscaClient openFiscaClient) {
        this.openFiscaClient = openFiscaClient;
    }

    @Test
    @DisplayName("doit vérifier l'éligibilité à l'AGEPI d'un demandeur célibataire ayant un enfant de moins de 10 ans")
    void calculerEligibleCelibataire1EnfantMoins10Test() {

        // Si demandeur France Métropolitaine, célibataire, 1 enfant à charge moins de 10 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateJour();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot,
                dateDebutSimulation);
        // Alors le demandeur est éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isNotZero();
    }

    @Test
    @DisplayName("doit vérifier l'éligibilité à l'AGEPI d'un demandeur célibataire ayant 2 enfant de moins de 10 ans")
    void calculerEligibleCelibataire2EnfantsMoins10() {


        // Si demandeur France Métropolitaine, célibataire, 2 enfants à charge moins de 10 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        personnesACharge.add(createEnfant(5));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot,
                dateDebutSimulation);
        // Alors le demandeur est éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isNotZero();
    }

    @Test
    @DisplayName("doit vérifier l'éligibilité à l'AGEPI d'un demandeur célibataire ayant 2 enfant un de moins de 10 ans et un de plus de 10 ans")
    void calculerEligibleCelibataireEnfantMoins10EnfantPlus10() {
        // Si demandeur France Métropolitaine, célibataire, 1 enfant à charge moins de 10 ans,
        // 1 enfant à charge plus de 10 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(12));
        personnesACharge.add(createEnfant(5));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot,
                dateDebutSimulation);
        // Alors le demandeur est éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isNotZero();
    }

    @Test
    @DisplayName("doit vérifier la non éligibilité à l'AGEPI d'un demandeur célibataire n'ayant pas d'enfant")
    void calculerNonEligibleCelibataire0Enfant() {


        // Si demandeur France Métropolitaine, célibataire, 0 enfant à charge moins de 10 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot,
                dateDebutSimulation);
        // Alors le demandeur n'est pas éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isZero();
    }

    @Test
    @DisplayName("doit vérifier la non éligibilité à l'AGEPI d'un demandeur en couple n'ayant pas d'enfant")
    void calculerNonEligibleCouple() {


        // Si demandeur France Métropolitaine, en couple, 0 enfant à charge moins de 10 ans
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot,
                dateDebutSimulation);
        // Alors le demandeur n'est pas éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isZero();
    }

    @Test
    @DisplayName("doit vérifier la non éligibilité à l'AGEPI d'un demandeur en couple ayant 1 enfant de moins de 10ans")
    void calculerNonEligibleCouple1EnfantMoins10() {


        // Si demandeur France Métropolitaine, en couple, 1 enfant à charge moins de 10 ans
        boolean isEnCouple = true;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(8));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot,
                dateDebutSimulation);
        // Alors le demandeur n'est pas éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isZero();
    }

    @Test
    @DisplayName("doit vérifier la non éligibilité à l'AGEPI d'un demandeur en couple ayant un enfant de plus de 10 ans")
    void calculerNonEligibleCouple1EnfantPlus10() {


        // Si demandeur France Métropolitaine, en couple, 1 enfant à charge plus de 10 ans
        boolean isEnCouple = false;
        List<Personne> personnesACharge = new ArrayList<>();
        personnesACharge.add(createEnfant(12));
        Demandeur demandeur = createDemandeurRSA(isEnCouple, personnesACharge);
        // Lorsque je calcule le montant de l'AGEPI
        LocalDate dateDebutSimulation = DateUtile.getDateDebutMois();

        demandeur.getInformationsPersonnelles().setLogement(initLogement(false, "44109"));
        demandeur.setFutureFormation(initFormation(35f, 3f, 757, 0, 0));
        demandeur.getRessourcesFinancieresAvantSimulation().getAidesCAF().setAllocationRSA(500f);

        OpenFiscaRoot openFiscaRoot = openFiscaClient.callApiCalculate(demandeur, dateDebutSimulation);
        OpenFiscaRetourSimulation openFiscaRetourSimulation = OpenFiscaRetourSimulationHelper.calculerAgepi(openFiscaRoot,
                dateDebutSimulation);
        // Alors le demandeur n'est pas éligible à l'AGEPI
        assertThat(openFiscaRetourSimulation.getMontantAgepi()).isZero();
    }

}
