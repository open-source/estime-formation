package fr.pe.pfin.service.da184.testsintegration.simulation.temporalite.aideLogement;

import fr.pe.pfin.service.da184.RedisTestConfiguration;
import fr.pe.pfin.service.da184.Utile;
import fr.pe.pfin.service.da184.application.ui.rest.DemandeurController;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Demandeur;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.Simulation;
import fr.pe.pfin.service.da184.application.ui.rest.ressource.SimulationMensuelle;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.enumerations.AideEnum;
import fr.pe.pfin.service.da184.infrastructure.aides.commun.utile.DateUtile;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = RedisTestConfiguration.class)
@ActiveProfiles("test")
class DemandeurAideLogementProchaineDeclarationMois3Test extends Utile {

    private static final int PROCHAINE_DECLARATION_TRIMESTRIELLE = 3;

    private final DemandeurController demandeurController;

    @Autowired
    public DemandeurAideLogementProchaineDeclarationMois3Test(DemandeurController demandeurController) {
        this.demandeurController = demandeurController;
    }

    @SuppressWarnings("java:S5961")
    @Test
    void simulerALProchaineDeclarationMois3Couple2EnfantsConventionne() throws Exception {
        // Si DE Français, date naissance 05/07/1986, code postal 44200,
        // en couple, 2 enfants 9 et 7 ans,
        // locataire d'un logement non meublé, loyer 500€, charges 50€, codeInsee 44109
        // AL 300€, RSA 500€, prochaine déclaration trimestrielle mois M+3
        // Salaire CDI 35h/semaine 757€ net, kilométrage domicile -> taf = 12kms + 20 trajets
        // Pas travaillé au cours des 3 derniers mois

        Demandeur demandeur = creerDemandeurAL(PROCHAINE_DECLARATION_TRIMESTRIELLE);

        // Lorsque je simule mes prestations le 01/05/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);

            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isBetween(386f, 390f);
            });
        });
        // Alors les prestations du second mois 07/2023
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);

            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isBetween(386f, 390f);
            });
        });
        // Alors les prestations du troisième mois 08/2023
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);

            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isBetween(386f, 390f);
            });
        });
        // Alors les prestations du quatrième mois 09/2023
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(843f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(160f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isBetween(386f, 390f);
            });
        });
        // Alors les prestations du cinquième mois 10/2023
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(843f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(160f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isBetween(386f, 390f);
            });
        });
        // Alors les prestations du sixième mois 11/2023
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(843f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(160f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isBetween(386f, 390f);
            });
        });
    }

    @SuppressWarnings("java:S5961")
    @Test
    void simulerALProchaineDeclarationMois3Couple2Enfants() throws Exception {
        // Si DE Français, date naissance 05/07/1986, code postal 44200,
        // en couple, 2 enfants 9 et 7 ans,
        // locataire d'un logement non meublé, loyer 500€, charges 50€, codeInsee 44109
        // AL 300€, RSA 500€, prochaine déclaration trimestrielle mois M+3
        // Salaire CDI 35h/semaine 757€ net, kilométrage domicile -> taf = 12kms + 20 trajets
        // Pas travaillé au cours des 3 derniers mois

        Demandeur demandeur = creerDemandeurAL(PROCHAINE_DECLARATION_TRIMESTRIELLE);
        demandeur.getInformationsPersonnelles().getLogement().setConventionne(false);

        // Lorsque je simule mes prestations le 01/05/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);

            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(456f);
            });
        });
        // Alors les prestations du second mois 07/2023
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);

            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(456f);
            });
        });
        // Alors les prestations du troisième mois 08/2023
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);

            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(456f);
            });
        });
        // Alors les prestations du quatrième mois 09/2023
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(843f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(160f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(456f);
            });
        });
        // Alors les prestations du cinquième mois 10/2023
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(843f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(160f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(456f);
            });
        });
        // Alors les prestations du sixième mois 11/2023
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(843f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(160f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(456f);
            });
        });
    }

    @SuppressWarnings("java:S5961")
    @Test
    void simulerALProchaineDeclarationMois3() throws Exception {
        // Si DE Français, date naissance 05/07/1986, code postal 44200,
        // célibataire, pas d'enfant,
        // locataire d'un logement non meublé, loyer 500€, charges 50€, codeInsee 44109
        // AL 300€, RSA 500€, prochaine déclaration trimestrielle mois M+3
        // Salaire CDI 35h/semaine 757€ net, kilométrage domicile -> taf = 12kms + 20 trajets
        // Pas travaillé au cours des 3 derniers mois

        Demandeur demandeur = creerDemandeurAL(PROCHAINE_DECLARATION_TRIMESTRIELLE);
        demandeur.getInformationsPersonnelles().getLogement().setConventionne(false);

        // Lorsque je simule mes prestations le 01/05/2023
        Simulation simulation = demandeurController.simulerMesAides(demandeur);

        // Alors les prestations du premier mois 06/2023
        SimulationMensuelle simulationMois1 = simulation.getSimulationsMensuelles().get(0);
        assertThat(simulationMois1).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("06");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);

            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(456f);
            });
        });
        // Alors les prestations du second mois 07/2023
        SimulationMensuelle simulationMois2 = simulation.getSimulationsMensuelles().get(1);
        assertThat(simulationMois2).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("07");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);

            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(456f);
            });
        });
        // Alors les prestations du troisième mois 08/2023
        SimulationMensuelle simulationMois3 = simulation.getSimulationsMensuelles().get(2);
        assertThat(simulationMois3).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("08");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(2);

            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(500f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(456f);
            });
        });
        // Alors les prestations du quatrième mois 09/2023
        SimulationMensuelle simulationMois4 = simulation.getSimulationsMensuelles().get(3);
        assertThat(simulationMois4).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("09");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(160f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(456f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(843f);
            });
        });
        // Alors les prestations du cinquième mois 10/2023
        SimulationMensuelle simulationMois5 = simulation.getSimulationsMensuelles().get(4);
        assertThat(simulationMois5).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("10");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(160f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(456f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(843f);
            });
        });
        // Alors les prestations du sixième mois 11/2023
        SimulationMensuelle simulationMois6 = simulation.getSimulationsMensuelles().get(5);
        assertThat(simulationMois6).satisfies(simulationMensuelle -> {
            assertThat(simulationMensuelle.getDatePremierJourMoisSimule()).satisfies(dateMoisSimule -> {
                assertThat(DateUtile.getMonthFromLocalDate(dateMoisSimule)).isEqualTo("11");
                assertThat(dateMoisSimule.getYear()).isEqualTo(2023);
            });
            assertThat(simulationMensuelle.getAides()).hasSize(3);
            assertThat(simulationMensuelle.getAides().get(AideEnum.PRIME_ACTIVITE.getCode())).satisfies(ppa -> {
                assertThat(ppa.getMontant()).isEqualTo(160f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.AIDES_LOGEMENT.getCode())).satisfies(al -> {
                assertThat(al.getMontant()).isEqualTo(456f);
            });
            assertThat(simulationMensuelle.getAides().get(AideEnum.RSA.getCode())).satisfies(rsa -> {
                assertThat(rsa.getMontant()).isEqualTo(843f);
            });
        });
    }
}
