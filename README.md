# Estime Formation API

Code source de l'API d'Estime Formation.

Cet API est le moteur du simulateur d['Estime Formation](https://candidat.pole-emploi.fr/simulation-ressources-formation) hébergé sur le site candidat de Pôle Emploi. 

Estime formation est un simulateur permettant aux accompagnants d’estimer l’impact d’une rémunération de formation sur l’équilibre financier des bénéficiaires. Lors d’un entretien de suivi, l’accompagnant remplit un formulaire et reçoit instantanément une projection détaillée des ressources du bénéficiaires s’il rentre en formation. Cette estimation comprendra l’impact sur le RSA, l’AAH, l’ASS, les aides au logement, allocations familiales, la prime d’activité et encore d’autres dispositifs.

Ce simulateur est dépandant en partie d'[OpenFisca](https://openfisca.org/fr/) pour le calcul de certaines aides et accessible via son interface API en auto-hébergé ou via le front end du site [Estime Formation](https://candidat.pole-emploi.fr/simulation-ressources-formation)


## Description technique

### Backend 

ce projet est un projet `SpringBoot 3` s'exécutant à l'aide de `Java 17`

Le Backend contient la partie back du projet à savoir : l'exposition des API et l'appel vers [openfisca](https://openfisca.org/fr/).

## Pré-requis

- Java 17 et +
- Maven version 3.6.3 et +

## Configuration

### Backend

Le projet est un projet SpringBoot 3 : il se configure par l'intermédiaire de
- Variables d'environnement
- Propriétés système au démarrage de l'application
- Fichier yaml ou properties

Plus d'informations [ici](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#features.external-config)
## Déploiement

### Redis

Redis est nécessaire pour le fonctionnement du backend pour l'enregistrement des statistiques

```bash
docker run  -p 6379:6379 --name base-redis -d redis
```

### Backend

Pour déployer ce projet, après exécution d'une base redis, 

```bash
cd backend && mvn spring-boot:run
```

Par défaut, le projet démarre sur le port 8080

[Un OpenAPI](https://www.openapis.org/) est disponible sur le projet via l'url http://localhost:8080/swagger-ui.html
